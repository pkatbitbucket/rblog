define(['knockout', 'models', 'viewmodels', 'utils', 'jquery', 'crypt', 'components'], function(ko, models, viewmodels, utils, $, crypt) {
    var quote = viewmodels.quote;
    var showResults = ko.observable(false);
    var plans = ko.observableArray([]);
    var addonsVerbose = viewmodels.addonsVerbose;
    var showCarDetailsPopup = ko.observable(false);
    var selectedPlan = ko.observable(null);
    var overlayMode = ko.observable(null);
    var selectedAddons = ko.observableArray([]);
    var activityID = ko.observable();
    var transactionID = ko.observable();
    var insurers = {
        2: 'bajaj-allianz',
        3: 'bharti-axa',
        4: 'iffco-tokio',
        5: 'hdfc-ergo'
    };
    var NCBOptions = [{
        key: '0%',
        value: 0
    }, {
        key: '20%',
        value: 20
    }, {
        key: '25%',
        value: 25
    }, {
        key: '35%',
        value: 35
    }, {
        key: '45%',
        value: 45
    }, {
        key: '50%',
        value: 50
    }];
    var isClaimedOptions = [{
        key: 'No',
        value: 0
    }, {
        key: 'Yes',
        value: 1
    }];
    var voluntaryDeductibleOptions = [{
        key: '0',
        value: 0
    }, {
        key: 'Rs. 2500',
        value: 2500
    }, {
        key: 'Rs. 5000',
        value: 5000
    }, {
        key: 'Rs. 7500',
        value: 7500
    }, {
        key: 'Rs. 15000',
        value: 15000
    }];
    var toggleCarDetailsPopup = function() {
        if (showCarDetailsPopup() == false) {
            showCarDetailsPopup(true);
        } else {
            showCarDetailsPopup(false);
        }
        console.log(showCarDetailsPopup());
    };
    var selectAddon = function(addon) {
        if (selectedAddons().indexOf(addon) == -1) {
            selectedAddons.push(addon);
        } else {
            selectedAddons.remove(addon);
        }
        for (var i = 0; i < plans().length; i++) {
            var plan = plans()[i];
            var planAddons = plan.addonCovers();
            for (var j = 0; j < planAddons.length; j++) {
                if (planAddons[j].id == addon.id) {
                    if (planAddons[j].is_selected() == '0') {
                        planAddons[j].is_selected('1');
                    } else {
                        planAddons[j].is_selected('0');
                    }
                    plan.addonCovers.valueHasMutated();
                }
            }
        }
    };
    var isAddonSelected = function(addon) {
        return selectedAddons().indexOf(addon) > -1;
    };
    var maxIDV = ko.computed(function() {
        if (plans().length) {
            return Math.min.apply(Math, $.map(plans(), function(item) {
                return parseInt(item.maxIDV);
            }));
        } else {
            return 0;
        }
    });
    var minIDV = ko.computed(function() {
        if (plans().length) {
            return Math.max.apply(Math, $.map(plans(), function(item) {
                return parseInt(item.minIDV);
            }));
        } else {
            return 0;
        }
    });
    var meanIDV = ko.computed(function() {
        return (maxIDV() + minIDV()) / 2;
    });
    quote.idv.subscribe(function() {
        quote.minIdv(minIDV());
        quote.maxIdv(maxIDV());
    });
    var verboseAmount = function(amount) {
        return (amount / 100000).toFixed(2) + ' Lacs';
    };
    var getAddonVerbose = function(addon) {
        return addonsVerbose.filter(function(obj) {
            return obj.id == addon;
        })[0];
    };
    var cngFittedStatus = ko.observable('No').extend({
        store: {
            key: 'cngFittedStatus'
        }
    });
    cngFittedStatus.subscribe(function(newValue) {
        if (newValue != "No") {
            quote.isCNGFitted(1);
        } else {
            quote.isCNGFitted(0);
        }
    });
    var refreshPlan = function(plan) {
        policyRefresh('start');
        $.post('/motor/fourwheeler/api/quotes/' + quote.quoteId + '/refresh/' + insurers[plan.insurerId] + '/', JSON.parse(ko.toJSON(quote)), function(response) {
            response = crypt.parseResponse(response);
            policyRefresh('done');
            activityID(response.data.activityId);
            return new models.Plan(response.data.quote.quoteResponse);
        });
    };
    var showPremiumBreakup = function(plan) {
        console.log("PLAN >>>", plan);
        selectedPlan(plan);
        overlayMode('premium-breakup');
    };
    var pastpolicyExpiryDateConfirmed = ko.observable(false);
    var policyRefresh = ko.observable();
    var prepareBuy = function(plan) {
        selectedPlan(plan);
        overlayMode('buy');
        pastpolicyExpiryDateConfirmed(false);
        if (quote.isNewVehicle() == true) {
            confirmExpiryDate();
        }
    };
    var confirmExpiryDate = function() {
        var quoteId = quote.quoteId;
        policyRefresh('start');
        $.post('/motor/fourwheeler/api/quotes/' + quoteId + '/refresh/' + insurers[selectedPlan().insurerId] + '/', JSON.parse(ko.toJSON(quote)), function(response) {
            response = crypt.parseResponse(response);
            if (quote.isNewVehicle() == false) {
                pastpolicyExpiryDateConfirmed(true);
            }
            policyRefresh('done');
            console.log("MODIFIED RESPONSE >>>>", response);
            console.log(response.data);
            activityID(response.data.activityId);
            console.log("ACTIVITY ID >>>>>>>>>>>>>>>>>>>>>", activityID());
            var refreshedPlan = new models.Plan(response.data.quote.quoteResponse);
            selectedPlan(refreshedPlan);
        });
    };
    var confirmBuy = function() {
        console.log("confirmBuy");
        $.post('/motor/fourwheeler/api/confirm/' + quote.quoteId + '/' + selectedPlan().insurerSlug + '/', JSON.parse(ko.toJSON(quote)), function(response) {
            response = crypt.parseResponse(response);
            transactionID(response.data);
            window.location.replace('/motor/fourwheeler/' + insurers[selectedPlan().insurerId] + '/desktopForm/' + transactionID() + '/');
            console.log(transactionID());
        });
    };
    return {
        initialize: function() {
            plans([]);
            setTimeout(function() {
                showResults(true);
            }, 300);
            console.log("REQUEST >>", JSON.parse(ko.toJSON(quote)));
            $.post('/motor/fourwheeler/api/quotes/', JSON.parse(ko.toJSON(quote)), function(response) {
                response = crypt.parseResponse(response);
                console.log("Response >>>", response);
                var allPremiums = response["premiums"];
                quote.quoteId = response["quoteId"];
                plans($.map(allPremiums, function(item) {
                    if (item.exShowroomPrice) {
                        quote.exShowroomPrice(item.exShowroomPrice);
                    }
                    return new models.Plan(item);
                }));
            });
            return {
                showResults: showResults,
                quote: quote,
                plans: plans,
                maxIDV: maxIDV,
                minIDV: minIDV,
                meanIDV: meanIDV,
                verboseAmount: verboseAmount,
                allAddons: viewmodels.addonsVerbose,
                cngFittedStatus: cngFittedStatus,
                getAddonVerbose: getAddonVerbose,
                showPremiumBreakup: showPremiumBreakup,
                prepareBuy: prepareBuy,
                selectedPlan: selectedPlan,
                overlayMode: overlayMode,
                months: viewmodels.months,
                years: viewmodels.years,
                days: viewmodels.days,
                selectAddon: selectAddon,
                selectedAddons: selectedAddons,
                isAddonSelected: isAddonSelected,
                confirmExpiryDate: confirmExpiryDate,
                pastpolicyExpiryDateConfirmed: pastpolicyExpiryDateConfirmed,
                policyRefresh: policyRefresh,
                confirmBuy: confirmBuy,
                NCBOptions: NCBOptions,
                isClaimedOptions: isClaimedOptions,
                voluntaryDeductibleOptions: voluntaryDeductibleOptions,
                showCarDetailsPopup: showCarDetailsPopup,
                toggleCarDetailsPopup: toggleCarDetailsPopup
            }
        }
    };
});