'use strict';
require.config({
    baseUrl : '/static/motor_product/desktopMVPBuy/app/',
    paths: {
        'async': '/static/motor_product/global/lib/requirejs-plugins/async',
        'text': '/static/motor_product/global/lib/require/text',
        'knockout': '/static/motor_product/global/lib/knockout/knockout-3.2.0',
        'knockout_validation': '/static/motor_product/global/lib/knockout/knockout.validation',
        'knockout-amd-helpers': '/static/motor_product/global/lib/knockout/knockout-amd-helpers',
        'knockout-history': '/static/motor_product/global/lib/knockout/knockout-history',
        'knockout-router': '/static/motor_product/global/lib/knockout/knockout-router',
        'crypt': '/static/motor_product/global/lib/encryption/response',
        'models': '/static/motor_product/global/models',
        'carphotos': '/static/motor_product/global/carphotos',
        'rtocodes': '/static/motor_product/global/rtocodes',
        'viewmodels': '/static/motor_product/global/viewmodels',
        'utils': '/static/motor_product/global/utils',
        'cookie' : '/static/motor_product/global/lib/cookies/cookies',
        'bstore' : '/static/motor_product/global/bstore',
        'jquery' : '/static/motor_product/global/lib/jquery/jquery-1.11.0',
        'components' : '/static/motor_product/global/components',
        'rangeslider' : '/static/motor_product/global/lib/rangeslider/rangeslider',
        'pikaday' : '/static/motor_product/global/lib/pikaday/pikaday'

    },
    shim: {
        'knockout_validation' : {
            deps : ['knockout']
        },
        'knockout-amd-helpers': {
            deps: ['knockout']
        },
        'knockout-history': {
            deps: ['knockout']
        },
        'knockout-router': {
            deps: ['knockout', 'knockout-history', 'knockout-amd-helpers']
        }
    },
    urlArgs: 'v=1.0.0.0'
});

define(['assets/js/main', 'bstore'], function(main) {
    main.run();
});


