
define(['knockout', 'models', 'crypt', 'viewmodels','jquery','pikaday','components'], function(ko, models, crypt, viewmodels, $,Pikaday) {



    var userDetails = viewmodels.userDetails;




    userDetails.cust_dob.subscribe(function(newValue){
        console.log(">>>>>>>>>>>>>>>>>>",newValue);
//        console.log(userDetails.cust_marital_status());
    });

    //console.log(userDetails.cust_marital_status());








    var currentStage = ko.observable(1);



    var selectedOccupation = ko.observable(),
        selectedPastInsurer = ko.observable(),
        selectedCoverType = ko.observable();

    var parsedSelectedOccupation = ko.computed(function(){
        if(selectedOccupation()){
           return selectedOccupation().value;
        }
        else{
           return null;
        }

    });

    var parsedPastInsurer = ko.computed(function(){
        if(selectedPastInsurer()){
           return selectedPastInsurer().value;
        }
        else{
           return null;
        }

    });

    var parsedCoverType = ko.computed(function(){
        if(selectedCoverType()){
           return selectedCoverType().value;
        }
        else{
           return null;
        }

    });

    var setStage = function(stageNum){
        currentStage(stageNum);
        console.log(currentStage());
    };


    function stringToDate(_date,_format,_delimiter)
    {
        var formatLowerCase=_format.toLowerCase();
        var formatItems=formatLowerCase.split(_delimiter);
        var dateItems=_date.split(_delimiter);
        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        return new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    }


    console.log(data);

    var quote = data.quote_parameters,
        quote_response = data.quote_response;

    var fetch_cities = data.form_details.is_cities_fetch;

    // prefill values
    userDetails.vehicle_reg_no(quote['registrationNumber[]'].join('-'));


    var plan = new models.Plan(quote_response),
        form_details = data.form_details;

    var insurer = {
      id: insurer_id,
      title: insurer_title,
      slug: insurer_slug
    };

    var occupation_list = ko.observableArray([]),
        insurer_list = ko.observableArray([]),
        cover_type_list = ko.observableArray([]);

    for(var k in form_details.occupation_list){
        occupation_list.push({'key':form_details.occupation_list[k], 'value':k})
    }



    for(var m in form_details.insurers_list){
        insurer_list.push({'key':form_details.insurers_list[m], 'value':m})
    }

    for(var l in form_details.cover_type_list){
        cover_type_list.push({'value':l, 'key':form_details.cover_type_list[l]})
    }

    console.log("OLIST", cover_type_list());


    var today = new Date(),
        expiryDate = stringToDate(quote.pastPolicyExpiryDate, "dd-mm-yyyy","-");

    today.setHours(0,0,0,0);
    var hasExpired = today > expiryDate;


    var inspectionDates = [];

    for(var i=0;i<7;i++){
        var myDate = new Date();
        myDate.setDate(today.getDate() + i);
        var d = (myDate+'').split(' ');
        inspectionDates.push(d[2]+' '+d[1]+' '+d[3]);
    }






    var add_pincode = ko.observable();
    var reg_add_pincode = ko.observable();
    var add_state = ko.observable();
    var add_district = ko.observable();
    var add_city = ko.observable();
    var add_city_list = ko.observableArray([]);
    var selectedAddCity = ko.observable();
    var add_post_office = ko.observable();
    var selectedAddPostOffice = ko.observable();
    var add_post_office_list = ko.observableArray([]);

    var reg_add_state = ko.observable();
    var reg_add_district = ko.observable();
    var reg_add_city = ko.observable();
    var reg_add_city_list = ko.observableArray([]);
    var selectedRegCity = ko.observable();
    var reg_add_post_office = ko.observable();
    var selectedRegPostOffice = ko.observable();
    var reg_add_post_office_list = ko.observableArray([]);


    add_pincode.subscribe(function(newValue){


        if(newValue.length > 5){
            console.log(newValue);

            $.post('/motor/fourwheeler/api/pincode-details/' + insurer_slug + '/', {'pincode': newValue, 'csrfmiddlewaretoken': CSRF_TOKEN}, function (response) {
            response = crypt.parseResponse(response);

            add_state(response.province_name);
            add_district(response.district_name);
            add_post_office_list(response.post_office_details);

            if(fetch_cities){
                $.post('/motor/fourwheeler/api/cities/' + insurer_slug + '/', {'state': add_state, 'csrfmiddlewaretoken': CSRF_TOKEN}, function (response) {
                    response = crypt.parseResponse(response);
                    add_city_list(response.cities);
                });
            }
        });



        }
    });


    reg_add_pincode.subscribe(function(newValue){


        if(newValue.length > 5){
            console.log(newValue);

            $.post('/motor/fourwheeler/api/pincode-details/' + insurer_slug + '/', {'pincode': newValue, 'csrfmiddlewaretoken': CSRF_TOKEN}, function (response) {
            response = crypt.parseResponse(response);

            reg_add_state(response.province_name);
            reg_add_district(response.district_name);
            reg_add_post_office_list(response.post_office_details);

            if(fetch_cities){
                $.post('/motor/fourwheeler/api/cities/' + insurer_slug + '/', {'state': reg_add_state, 'csrfmiddlewaretoken': CSRF_TOKEN}, function (response) {
                    response = crypt.parseResponse(response);
                    reg_add_city_list(response.cities);
                });
            }
        });



        }
    });






    return {
        initialize : function(stage_type){




            return {

                userDetails:userDetails,
                currentStage:currentStage,
                setStage:setStage,
                quote:quote,
                plan:plan,
                form_details:form_details,
                insurer:insurer,
                CSRF_TOKEN:CSRF_TOKEN,
                hasExpired:hasExpired,
                occupation_list:occupation_list,
                insurer_list:insurer_list,
                cover_type_list:cover_type_list,
                inspectionDates:inspectionDates,
                selectedOccupation:selectedOccupation,
                parsedSelectedOccupation:parsedSelectedOccupation,
                selectedPastInsurer:selectedPastInsurer,
                parsedPastInsurer:parsedPastInsurer,
                selectedCoverType:selectedCoverType,
                parsedCoverType:parsedCoverType,
                add_pincode:add_pincode,
                reg_add_pincode:reg_add_pincode,
                add_state:add_state,
                add_district:add_district,
                add_city:add_city,
                add_city_list:add_city_list,
                selectedAddCity:selectedAddCity,
                add_post_office:add_post_office,
                add_post_office_list:add_post_office_list,
                selectedAddPostOffice:selectedAddPostOffice,
                reg_add_state:reg_add_state,
                reg_add_district:reg_add_district,
                reg_add_city:reg_add_city,
                reg_add_city_list:reg_add_city_list,
                selectedRegCity:selectedRegCity,
                reg_add_post_office:reg_add_post_office,
                reg_add_post_office_list:reg_add_post_office_list,
                selectedRegPostOffice:selectedRegPostOffice


            }
        }
    };
});
