define(['knockout', 'jquery'], function(ko, $) {

    function CheckboxWidgetViewModel(params) {
        var self = this;
        self.checkBoxTarget = params.checkBoxTarget;
        self.checkboxText = params.checkboxText;
        self.customClass = params.customClass || null;


        self.setCheckbox = function(){
            if(self.checkBoxTarget){
                self.checkBoxTarget(false);
            }
            else{
                self.checkBoxTarget(true);
            }
        };
    }


    return CheckboxWidgetViewModel;

});