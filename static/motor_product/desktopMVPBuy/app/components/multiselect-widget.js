define(['knockout', 'jquery'], function(ko, $) {


    function fireEvent(name, target) {
        var evt = document.createEvent("Events");
        evt.initEvent(name, true, true);
        target.dispatchEvent(evt);
    }



    var singleSelect = function(item){
        var self = this;
        self.defaultText = item.defaultText || 'Choose a value';
        self.selectedValue = item.selectedValue;
        self.keyword = ko.observable(null);
        self.items = item.data;
        self.optionText = item.optionText || null;
        self.onSelect = item.onSelect;
        self.selectedValueVerbose = ko.computed(function(){
            if(self.selectedValue()){
                if(self.optionText !== null){
                     return self.selectedValue()[self.optionText];
                }
                else{
                    return self.selectedValue();
                }
            }
        });

        self.selectValue = function(data){
          self.selectedValue(data);
            if(self.onSelect){
                self.onSelect();
            }
          fireEvent("valueSelected", document);
        };






        self.filteredItems = ko.computed(function(){

            var items = self.items;


            if (items.constructor.toString().indexOf('Array') == -1) {
                items = items();
            }

            if(self.keyword()){
                return ko.utils.arrayFilter(items, function(item) {
                    if(self.optionText !== null){
                        return item[self.optionText].toLowerCase().indexOf(self.keyword().toLowerCase()) > -1;
                    }
                    else{
                        return item.toLowerCase().indexOf(self.keyword().toLowerCase()) > -1;
                    }
                });
            }
            else{
                return items;
            }
        });
    };


    function MultiSelectWidgetViewModel(params) {
        var self = this;
        self.activeItem = ko.observable(null);
        self.items = $.map(params.$raw.items(), function(item){
           return new singleSelect(item);
        });



        self.showDropdown = function(data){
            self.activeItem(data);
        };

        self.hideDropdown = function(){
            self.activeItem(null);
        };

        window.addEventListener("valueSelected", function(){

            var activeItemChanged = false;

            for(var i=self.items.indexOf(self.activeItem()); i< self.items.length && i>-1; i++){
                   if (self.items[i].selectedValue() == undefined){
                       self.activeItem(self.items[i]);
                       activeItemChanged = true;
                       break;
                   }
            }

            if(!activeItemChanged){
                self.hideDropdown();
            }


        }, false);


    }


    return MultiSelectWidgetViewModel;

});