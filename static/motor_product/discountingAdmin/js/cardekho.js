var validation = {
    isEmailAddress: function(str) {
        var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return pattern.test(str);
    },
    isNotEmpty: function(str) {
        var pattern = /\S+/;
        return pattern.test(str);
    },
    isNumber: function(str) {
        var pattern = /^\d{10}$/;
        return pattern.test(str);
    },
    isName: function(str) {
        var pattern = /^[a-zA-Z ]{2,30}$/;
        return pattern.test(str);
    },
    isValidData: function(vehicleDataObj) {
        var error_status = true;
        $('#stepthree .error').addClass('hide');
        var length = vehicleDataObj.length;
        console.log(length);
        for (eid = 0; eid < length; eid++){
            var item = vehicleDataObj[eid];
            if (!validation.isNotEmpty(item.vehicleMake)) {
                $('#vehicleMake'+item.vehicleId+'_error').removeClass('hide'); error_status = false;
            }
            if (!validation.isNotEmpty(item.vehicleModel)) {
                $('#vehicleModel'+item.vehicleId+'_error').removeClass('hide'); error_status = false;
            }
            if (!validation.isNotEmpty(item.vehicleVariant)) {
                $('#vehicleVariant'+item.vehicleId+'_error').removeClass('hide'); error_status = false;
            }
            if (!validation.isNotEmpty(item.vehicleExpiry)) {
                $('#vehicleExpiry'+item.vehicleId+'_error').removeClass('hide'); error_status = false;
            }
        }
        return error_status;
    }
};
var addRow = function (id) {
    var vehicleRow = "";
    vehicleRow += "<div class='row vehicle-row' id='vehiclerow_"+id+"'><h4>"
    vehicleRow += " <img class='display_icon' src='/static/global/img/car-icon.png'><img class='display_icon' src='/static/global/img/bike-icon.png'> "
    vehicleRow += "    <button class='btn blue small pull-right removeVehicle'>Remove</button></h4>"
    vehicleRow += "    <div class='col-sm-8'>"
    vehicleRow += "        <div class='col-sm-4 plnone'>"
    vehicleRow += "            <label for=''>Make</label>"
    vehicleRow += "            <input id='vehicleMake"+id+"' type='text' class='form-input' placeholder='Maruti'/>"
    vehicleRow += "            <div class='error al-left' id='vehicleMake"+id+"_error'>This field is required</div>"
    vehicleRow += "        </div>"
    vehicleRow += "        <div class='col-sm-4 plnone'>"
    vehicleRow += "            <label for=''>Model</label>"
    vehicleRow += "            <input id='vehicleModel"+id+"' type='text' class='form-input' placeholder='Alto'/>"
    vehicleRow += "            <div class='error al-left' id='vehicleModel"+id+"_error'>This field is required</div>"
    vehicleRow += "        </div>"
    vehicleRow += "        <div class='col-sm-4 plnone'>"
    vehicleRow += "            <label for=''>Variant</label>"
    vehicleRow += "            <input id='vehicleVariant"+id+"' type='text' class='form-input' placeholder='LXi'/>"
    vehicleRow += "            <div class='error al-left' id='vehicleVariant"+id+"_error'>This field is required</div>"
    vehicleRow += "        </div>"
    vehicleRow += "    </div>"
    vehicleRow += "    <div class='col-sm-4 bdr-div' id='policy_document"+id+"'>"
    vehicleRow += "        <form action='/cardekho/uploadpolicy/' method='post' enctype='multipart/form-data' target='postframe'>"
    vehicleRow += "       <small>Upload policy document<br/>(Only <strong>PDF</strong> documents are acceptable)</small>"
    vehicleRow += "            <input class='form-input' name='policy_file' type='file' accept='.pdf'/>"
    vehicleRow += "            <input class='hide' name='elementId' type='text' value='"+id+"'/>"
    vehicleRow += "            <input class='btn blue small pull-right' type='submit' value='Upload'/>"
    vehicleRow += "            <iframe name='postframe' src='#' style='width:0; height:0px; border:0;'></iframe>"
    vehicleRow += "        </form>"
    vehicleRow += "        <div class='error' id='policy_document"+id+"_error'>Please Try again!</div>"
    vehicleRow += "    </div>"
    vehicleRow += "    <div class='col-sm-12'>"
    vehicleRow += "         <label>Last Policy Expiry Date</label>"
    vehicleRow += "         <input id='exp-date-"+id+"' class='form-input-date' type='text' placeholder='Enter date'/>"
    vehicleRow += "         <div class='error al-left' id='vehicleExpiry"+id+"_error'>This field is required</div>"
    vehicleRow += "    </div>"
    vehicleRow += "    <div class='clearfix'></div>"
    vehicleRow += "</div>"
    return vehicleRow;
}

$(document).ready(function () {
    window.datepick_cfg = {
        'dateFormat': "dd-mm-yyyy"
    };
    $('#exp-date-1').datepick(window.datepick_cfg);
    window.vehicle_type = "fourwheeler";
    if ($('#filedoc')[0]) {
        $(document).on('change', '#filedoc', function() {console.log("this might work");});
    }
    if ($('#emp-login')[0]) {
        $('#emp-login .error').addClass('hide');
        $(document).on('click','#login-user',function(){
            var empIdVal = $('#emp-id').val();
            var empPassVal = $('#pass').val();
            var $this = $(this);
            if(empIdVal && empPassVal) {
                $("#emp-login .loginerror").addClass('hide');
                $('#emp-login .error').addClass('hide');
                $(this).text('Just a moment..');
                loginuser(empIdVal, empPassVal, $this);
            }
            else {
                if(empIdVal =='') {
                    $('#iderror').removeClass('hide');
                }
                if(empPassVal =='') {
                    $('#passerror').removeClass('hide');
                }
            }
        });
    }
    if($('#stepone')[0]) {
        var firstattempt = true;
        $('#stepone .error').addClass('hide');
        $(document).on('click','#stepone_btn',function(){
            var email = $('#emp-email').val();
            var mobile = $('#emp-mob').val();
            if (!firstattempt) { stepone(email, mobile); }
            else {
                var pass1 = $('#pass-1').val();
                var pass2 = $('#pass-2').val();
                if (!validation.isNotEmpty(email) || !validation.isEmailAddress(email)){
                        $('#emailerror').removeClass('hide'); return;
                }
                if (!validation.isNotEmpty(mobile) || !validation.isNumber(mobile)){
                        $('#mobileerror').removeClass('hide').text('Enter a 10 digit mobile number'); return;
                }
                if (pass1 != pass2) {
                    if (!pass1 || !pass2) {
                        $("#stepone .alert-danger").removeClass("hide").text('Password is Required'); return;
                    }
                    else {
                        $("#stepone .alert-danger").removeClass("hide").text('Password do not match'); return;
                    }
                }
                else {
                    if ((pass1.length < 5) && (pass1.length != 0)) {
                        $("#stepone .alert-danger").removeClass("hide").text('Minimmum six characters are required'); return;
                    }
                    if (pass1.length != 0) {
                        $(this).text('Just a moment...');
                        $('#stepone .error').addClass('hide');
                        changepass(pass1, function (response) {
                            response = JSON.parse(response);
                            if(response.success){$(".changePass").text("Password changed"); $("#stepone_btn").text("Proceed..."); firstattempt=false;}
                            else{$("#stepone .alert-danger").removeClass("hide").text(response.error); $(this).text('Submit');}
                        });
                    }
                    else {
                        stepone(email, mobile);
                    }
                }
            }
        });
    }
    if($('#steptwo')[0]) {
        var vehicle_type = "fourwheeler";
        $('#steptwo .error').addClass('hide');
        $(document).on('click', '#vehicle_type li', function () {
            if (!($(this).hasClass('active'))) {
                $('#exp-date-1').val('');
            }
            vehicle_type = $(this).attr("data-type");
            $('#vehicle_type li').removeClass('active');
            $(this).addClass('active');
            console.log(vehicle_type);
        });
        $(document).on('click', '#steptwo_btn', function () {
            var expiry_date = $('#exp-date-1').val();
            if (!validation.isNotEmpty(expiry_date)) {
                $('#steptwo .error').removeClass('hide');
            }
            else {
                steptwo(vehicle_type, expiry_date, function (resp) {
                    resp = JSON.parse(resp);
                    //Send data to LMS
                    window.lmsdata = {
                        "mobile" : $('#emp-mob').val(),
                        "campaign" : 'motor-cardekhoEmp',
                        "past_policy_expire_date" : $('#exp-date-1').val()
                    };
                    window.csrf_token = resp.csrf_token;
                    sendData(window.csrf_token, window.lmsdata);
                    //End
                    if (resp.nextStep == "redirect") {
                        Cookies.set('discountCode', resp.cookie_value);
                        window.open(resp.url, '_target');
                    }
                    else {
                        $('#steptwo').addClass('hide');
                        $('#stepthree').removeClass('hide');
                        if (vehicle_type == "fourwheeler") {
                            $('#display_icon').attr('src','/static/global/img/car-icon.png');
                        }
                        else if (vehicle_type == "twowheeler") {
                            $('#display_icon').attr('src', '/static/global/img/bike-icon.png');
                        }
                        $('#last_expiry_date').text(expiry_date);
                    }
                });
            }
        });
    }
    if($('#stepthree')[0]) {
        $('#stepthree .error').addClass('hide');
        var vehicleData = [];
        var id = 1;
        var createVehicleObj = function(vehicleId) {
            var onevehicle = {
                "vehicleId": vehicleId,
                "vehicleMake": $('#vehicleMake'+vehicleId).val(),
                "vehicleModel": $('#vehicleModel'+vehicleId).val(),
                "vehicleVariant": $('#vehicleVariant'+vehicleId).val(),
                "vehicleExpiry" : $('#exp-date-'+vehicleId).val(),
                "vehiclaFile": $('#policy_document_url'+vehicleId).val(),
            }
            return onevehicle;
        }
        $(document).on('click', '#addVehicle', function () {
            id++;
            $('.otherVehicles').append(addRow(id));
            $('#stepthree .error').addClass('hide');
            $('#exp-date-'+id).datepick(window.datepick_cfg);
        });
        $(document).on('click', '.removeVehicle', function () {
            id--;
            var thisParentId = $(this).parents('.vehicle-row').attr('id');
            $('#'+thisParentId).remove();
        });
        $(document).on('click', '#stepthree_btn', function (){
            vehicleData = [];
            for (each_id =1; each_id <= id; each_id++) {
                vehicleData.push(new createVehicleObj(each_id));
            }
            if (validation.isValidData(vehicleData)) {
                window.lmsdata.vehicles = vehicleData;
                sendData(window.csrf_token, window.lmsdata);
                console.log('============================================');
                console.log(lmsdata);
                console.log('============================================');
                $('#stepfour').removeClass('hide');
                $('#stepthree').addClass('hide');
            }
            else {return;}
        });
    }
});
function loginuser(empId,empPass,btnEle) {
    $.post('/cardekho/login/', {'employee_id' : empId ,'password' : empPass }, function (response) {
        response = JSON.parse(response);
        if(response.success){
            window.location.href = "/cardekho/user/";
        }
        else{
            $("#emp-login .loginerror").removeClass('hide').text(response.error);
            btnEle.text('Submit');
        }
    });
}

function changepass(pass, callback) {$.post('/cardekho/changepassword/',{'change_password' : pass}, function (response) { callback(response);});}

function stepone(email, mobile) {
    $.post('/cardekho/user/', {'stepone' : JSON.stringify({'email' :email,'mobile': mobile,})}, function (response) {
        response = JSON.parse(response);
        if (response.success) {$('#stepone').addClass('hide');$('#steptwo').removeClass('hide');}
        else{$('#steponeerror').innerText = response.error;}
    });}

function steptwo(vehicle_type, expiry_date, callback) {
    $.post('/cardekho/user/',
            {'steptwo' : JSON.stringify({'vehicle_type':vehicle_type,'past_policy_expiry_date':expiry_date})},
            function(response){callback(response);});
}

function stepthree() {
    $.post('/cardekho/user/', {'stepthree' : "NoneNeed"}, function (response) {
        response = JSON.parse(response);
        csrf_token = response.csrf_token
        var data = {
            mobile: $('#emp-mob').val(),
            campaign: 'motor-cardekhoEmp',
            past_policy_expire_date: $('#exp-date-1').val(),
            vehicle_make: $("#vehicleMake").val(),
            vehicle_model: $("#vehicleModel").val(),
            vehicle_variant: $("#vehicleVariant").val()
        };
        sendData(csrf_token, data);
    });
}

function sendData(csrf_token, data) {
    $.post('/leads/save-call-time/', {data: JSON.stringify(data), csrfmiddlewaretoken: csrf_token}, function (response) {
            response = JSON.parse(response);
            if (response.success) console.log('LMS data send');
            else console.log('LMS Data failed');
    });
}

function uploadstatus(success, elementId, path) {
    if (success) {
        $('#policy_document'+elementId+'_error').addClass('hide');
        $('#policy_document'+elementId).html("<p>Uploaded Successfuly</p><input type='text' id='policy_document_url"+elementId+"' class='hide' value='"+path+"'>");
    }
    else {
        $('#policy_document'+elementId+'_error').removeClass('hide');
    }
}
