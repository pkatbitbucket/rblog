'use strict';
require.config({
    baseUrl : '/static/motor_product/discountingAdmin/',
    paths: {
        'knockout': '/static/motor_product/global/lib/knockout/knockout-3.1.0',
        'cookie' : '/static/motor_product/global/lib/cookies/cookies',
        'crypt': '/static/motor_product/global/lib/encryption/response',
        'bstore':'/static/motor_product/global/bstore',
        'jquery' : '/static/motor_product/global/lib/jquery/jquery-1.11.0',
        'models' : '/static/motor_product/discountingAdmin/js/models',

    },
    shim: {

    },
    urlArgs: 'v=1.0.0.0'
});

define(['js/main', 'bstore'], function(main) {
    main.run();
});


