define(['knockout', 'jquery', 'models'], function(ko, $, models) {

    var rtoSelector = new models.rtoSelector();
    var filterDl = new models.editableDl();
    var editableDl = new models.editableDl(rtoSelector);
    var dlList = new models.dlList(filterDl, editableDl);

    function run() {
        ko.applyBindings(this);
    }

    return {
        run:run,
        dlList:dlList,
        filterDl:filterDl,
        editableDl:editableDl,
        rtoSelector:rtoSelector,
    };

});
