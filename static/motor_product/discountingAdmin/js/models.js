define(['knockout', 'jquery'], function(ko, $) {

    var rtoSelector = function () {
        var self = this;
        self.id = ko.observable(null);
        self.initialized = ko.observable(false);
        self.cansave = ko.observable(false);
        self.rtos = ko.observableArray([]);

        self.filteredRtos = ko.observableArray([]);
        self.selectedRtos = ko.observableArray([]);

        self.shortlistedRtos = ko.observableArray([]);
        self.selectedShortlistedRtos = ko.observableArray([]);

        self.initialize = function () {
            $.post('/motor/admin/fourwheeler/' + INSURER + '/rtos/', {}, function(response) {
                response = JSON.parse(response);
                if (response.success) {
                    self.rtos($.map(response.data, function (item) {
                        return item;
                    }));
                    self.initialized(true);
                }
            });
        };

        self.id.subscribe(function() {
            if((self.id() != null) && (self.initialized())) {
                self.getRtosForDiscounting(self.id());
            }
        });

        self.initialized.subscribe(function() {
            if((self.id() != null) && (self.initialized())){
                self.getRtosForDiscounting(self.id());
            }
        });

        self.getRtosForDiscounting = function (id) {
            $.post('/motor/admin/fourwheeler/' + INSURER + '/discountings/rtos/', {'id' : id}, function(response) {
                response = JSON.parse(response);
                if (response.success) {
                    self.filter(response.data);
                }
            });
        }

        self.modifyRtosForDiscounting = function (id, rtos, is_delete, callback) {
            if (!self.cansave()) return;
            var is_delete_flag = 0;
            if (is_delete) is_delete_flag = 1;
            $.post('/motor/admin/fourwheeler/' + INSURER + '/discountings/rtos/modify', {'id' : id, 'rtos': rtos, 'delete': is_delete_flag}, function(response) {
                response = JSON.parse(response);
                if (response.success) {
                    callback();
                }
            });
        }

        self.filter = function (fetchedSelectedRtos) {
            var tempRtos = self.rtos().slice(0);;
            var tempSelectedRtos = [];

            var selectedRtosIndices = [];

            for (var rto in fetchedSelectedRtos) {
                var match = ko.utils.arrayFirst(tempRtos, function(item) {
                    return fetchedSelectedRtos[rto] == item.id;
                });
                tempSelectedRtos.push(match);
                var index = tempRtos.indexOf(match);
                selectedRtosIndices.push(index - selectedRtosIndices.length);
            }

            for (var index in selectedRtosIndices) {
                tempRtos.splice(selectedRtosIndices[index],1);
            }

            self.filteredRtos(tempRtos);
            self.filteredRtos.sort(self.sortOrder);

            self.shortlistedRtos(tempSelectedRtos);
            self.shortlistedRtos.sort(self.sortOrder);
            self.cansave(true);
        };

        self.shortlistRtos = function () {
            var selectedRtoIds = [];
            ko.utils.arrayForEach(self.selectedRtos(), function (rto) {
                selectedRtoIds.push(rto.id);
            })

            var callback = function() {
                self.shortlistedRtos(self.shortlistedRtos().concat(self.selectedRtos()));
                self.shortlistedRtos.sort(self.sortOrder);
                self.filteredRtos.removeAll(self.selectedRtos());
            }

            self.modifyRtosForDiscounting(self.id, selectedRtoIds, false, callback);
        };

        self.unshortlistRtos = function () {
            var selectedRtoIds = [];
            ko.utils.arrayForEach(self.selectedShortlistedRtos(), function (rto) {
                selectedRtoIds.push(rto.id);
            })

            var callback = function () {
                self.filteredRtos(self.filteredRtos().concat(self.selectedShortlistedRtos()));
                self.filteredRtos.sort(self.sortOrder);
                self.shortlistedRtos.removeAll(self.selectedShortlistedRtos());
            }

            self.modifyRtosForDiscounting(self.id, selectedRtoIds, true, callback);
        };

        self.sortOrder = function(left, right) {
            if (left.state == right.state) {
                return left.name == right.name ? 0 : (left.name < right.name ? -1 : 1);
            } else {
                return left.state < right.state ? -1 : 1;
            }
        };

        self.initialize();
    }

    var vehicleSelector = function () {
        var self = this;
        self.actualMake = ko.observable(null);
        self.actualModel = ko.observable(null);
        self.actualVariant = ko.observable(null);
        self.selectedMake = ko.observable(null).extend({ notify: 'always' });
        self.selectedModel = ko.observable(null).extend({ notify: 'always' });
        self.selectedVariant = ko.observable(null).extend({ notify: 'always' });
        self.makes = ko.observableArray([]);
        self.models = ko.observableArray([]);
        self.variants = ko.observableArray([]);

        self.initializer = undefined;

        self.initialize = function(make, model, variant) {
            self.actualMake(make)
            self.actualModel(model)
            self.actualVariant(variant)
            self.initializer = {
                'selectedMake': make,
                'selectedModel': model,
                'selectedVariant': variant,
            };
            self.getData({}, self.makes, 'selectedMake');
            self.models([]);
            self.variants([]);
        };

        self.selectedMake.subscribe(function() {
            self.actualMake(self.selectedMake());
            self.models([]);
            self.variants([]);
            if (self.selectedMake()) {
                self.getData({make: self.selectedMake()}, self.models, 'selectedModel');
            }
        });

        self.selectedModel.subscribe(function() {
            self.actualMake(self.selectedMake());
            self.actualModel(self.selectedModel());
            self.variants([]);
            if ((self.selectedModel()) && (self.selectedMake())) {
                self.getData({make: self.selectedMake(), model: self.selectedModel()}, self.variants, 'selectedVariant');
            }
        });

        self.selectedVariant.subscribe(function() {
            self.actualMake(self.selectedMake());
            self.actualModel(self.selectedModel());
            self.actualVariant(self.selectedVariant());
        });

        self.getData = function(parameters, populateParam, initialKey) {
            $.post('/motor/admin/fourwheeler/' + INSURER + '/vehicles/', parameters, function(response) {
                response = JSON.parse(response);
                if (response.success) {
                    populateParam($.map(response.data, function (item) {
                        return item;
                    }));
                    if(self.initializer) self[initialKey](self.initializer[initialKey]);
                }
            });
        }
    }

    var discountLoading = function() {
        var self = this;
        self.id = null;
        self.lower_age_limit = null;
        self.upper_age_limit = null;
        self.lower_cc_limit = null;
        self.upper_cc_limit = null;
        self.state = null;
        self.ncb = null;
        self.make = null;
        self.model = null;
        self.variant = null;
        self.rto_count = 0;
        self.discount = 0;
    }

    var editableDl = function(rtoSelector) {
        var self = this;
        self.id = undefined;
        self.rtoSelector = rtoSelector;
        self.vehicle = new vehicleSelector();
        self.state = ko.observable(null);
        self.ncb = ko.observable(null);
        self.rto_count = ko.observable(0);
        self.discount = ko.observable(0);
        self.vehicle.initialize();

        self.lower_age_limit = ko.observable(null).extend({ notify: 'always' });
        self.upper_age_limit = ko.observable(null);

        self.lower_age_ranges = [
            {'name': '0', 'value': null},
            {'name': '1', 'value': 1},
            {'name': '2', 'value': 2},
            {'name': '3', 'value': 3},
            {'name': '4', 'value': 4},
            {'name': '5', 'value': 5},
            {'name': '6', 'value': 6},
            {'name': '7', 'value': 7},
            {'name': '8', 'value': 8},
            {'name': '9', 'value': 9},
            {'name': '10', 'value': 10},
            {'name': '10+', 'value': 11},
        ]

        self.upper_age_ranges = [
            {'name': '0', 'value': 0},
            {'name': '1', 'value': 1},
            {'name': '2', 'value': 2},
            {'name': '3', 'value': 3},
            {'name': '4', 'value': 4},
            {'name': '5', 'value': 5},
            {'name': '6', 'value': 6},
            {'name': '7', 'value': 7},
            {'name': '8', 'value': 8},
            {'name': '9', 'value': 9},
            {'name': '10', 'value': 10},
        ]

        self.upper_limit_age_range = ko.observableArray(self.upper_age_ranges).extend({ notify: 'always' });

        self.initial_upper_age_limit = null;

        self.lower_age_limit.subscribe(function () {
            var lvalue = self.lower_age_limit();
            self.upper_limit_age_range([]);
            
            var range_list = self.upper_age_ranges;
            if (lvalue) {
                range_list = self.upper_age_ranges.slice(lvalue);
            }
            self.upper_limit_age_range(range_list);
        });

        self.upper_limit_age_range.subscribe(function(){
            if(self.upper_limit_age_range()) self.upper_age_limit(self.initial_upper_age_limit);
        });

        self.lower_cc_limit = ko.observable(null).extend({ notify: 'always' });
        self.upper_cc_limit = ko.observable(null);
        
        self.lower_cc_ranges = [
            {'name': '1', 'value': null},
            {'name': '501', 'value': 501},
            {'name': '1001', 'value': 1001},
            {'name': '1501', 'value': 1501},
            {'name': '2000+', 'value': 2001},
        ]

        self.upper_cc_ranges = [
            {'name': '500', 'value': 500},
            {'name': '1000', 'value': 1000},
            {'name': '1500', 'value': 1500},
            {'name': '2000', 'value': 2000},
        ]

        self.upper_limit_cc_range = ko.observableArray(self.upper_cc_ranges);

        self.initial_upper_cc_limit = null;

        self.lower_cc_limit.subscribe(function () {
            var lvalue = self.lower_cc_limit();
            var temp = self.upper_cc_limit();
            self.upper_limit_cc_range([]);
            
            var range_list = self.upper_cc_ranges;
            if (lvalue) {
                range_list = self.upper_cc_ranges.slice(parseInt(lvalue / 500));
            }
            self.upper_limit_cc_range(range_list);
            self.upper_cc_limit(temp);
        });

        self.upper_limit_cc_range.subscribe(function(){
            if(self.upper_limit_cc_range()) self.upper_cc_limit(self.initial_upper_cc_limit);
        });

        self.stateList = ['KL', 'GA', 'UA', 'JK', 'LD', 'NL', 'SK', 'CG', 'UK', 'GJ', 'WB', 'PY', 'AP', 'MN', 'AR', 'JH', 'BR', 'KA', 'OR', 'HR', 'RJ', 'MZ', 'HP', 'TN', 'UP', 'AN', 'MP', 'DN', 'DD', 'DL', 'CH', 'ML', 'PB', 'OD', 'TR', 'AS', 'MH'];
        self.ncbList = [
            {'name': 'No', 'value': null},
            {'name': 'Yes', 'value': 65},
        ]
    }

    editableDl.prototype.toJSON = function() {
        var copy = {};
        copy.id = this.id;
        copy.make = this.vehicle.actualMake();
        copy.model = this.vehicle.actualModel();
        copy.variant = this.vehicle.actualVariant();
        copy.state = this.state();
        if(this.lower_age_limit()) copy.lower_age_limit = this.lower_age_limit();
        if(this.upper_age_limit()) copy.upper_age_limit = this.upper_age_limit();
        if(this.lower_cc_limit()) copy.lower_cc_limit = this.lower_cc_limit();
        if(this.upper_cc_limit()) copy.upper_cc_limit = this.upper_cc_limit();
        if(this.ncb()) copy.ncb = this.ncb();
        copy.discount = this.discount;
        return copy;
    };

    editableDl.prototype.loadJSON = function (data) {
        this.id = data.id;
        this.rtoSelector.id(this.id);
        this.vehicle.initialize(data.make, data.model, data.variant);
        this.state(data.state);
        this.ncb(data.ncb);
        this.initial_upper_age_limit = data.upper_age_limit;
        this.lower_age_limit(data.lower_age_limit);
        this.initial_upper_cc_limit = data.upper_cc_limit;
        this.lower_cc_limit(data.lower_cc_limit);
        this.discount(data.discount);
    };

    var dlList = function(filterDl, editableDl, rtoSelector) {
        var self = this;
        self.dls = ko.observableArray([]);
        self.fdl = filterDl;
        self.edl = editableDl;
        self.selected = ko.observable();

        self.refresh = function() {
            $.post('/motor/admin/fourwheeler/' + INSURER + '/discountings/', {}, function(response) {
                response = JSON.parse(response);
                if (response.success) {
                    self.dls($.map(response.dls, function (item) {
                        return item;
                    }));
                }
            });
        }

        self.add = function() {
            $.post('/motor/admin/fourwheeler/' + INSURER + '/add', self.fdl.toJSON(), function(response) {
                response = JSON.parse(response);
                if (response.success) {
                    self.dls.push(response.item);
                }
            });
        }

        self.duplicate = function() {
            var _this = this;
            $.post('/motor/admin/fourwheeler/' + INSURER + '/add', {'id' : _this.id}, function(response) {
                response = JSON.parse(response);
                if (response.success) {
                    self.dls.push(response.item);
                }
            });
        }

        self.remove = function() {
            var _this = this;
            $.post('/motor/admin/fourwheeler/' + INSURER + '/delete', {'id' : _this.id}, function(response) {
                self.dls.remove(_this);
            });
        }

        self.edit = function() {
            var _this = this;
            if(self.selected() == _this) {
                $.post('/motor/admin/fourwheeler/' + INSURER + '/modify', self.edl.toJSON(), function(response) {
                    response = JSON.parse(response);
                    if (response.success) {
                        self.dls.replace(_this, response.item);
                        if(self.selected == _this) self.selected(null);
                    }
                });
            } else {
                self.selected(this);
                self.edl.loadJSON(this);
            }
        }
    }

    return {
        dlList:dlList,
        editableDl:editableDl,
        rtoSelector:rtoSelector,
    };
});
