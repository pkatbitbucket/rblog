define([], function() {


    return {
        ViewQuotes:'ViewQuotes',
        OptimizeQuotes:'OptimizeQuotes',
        CancelOptimizeQuotes:'CancelOptimizeQuotes',

        OpenIDVPopup:'OpenIDVPopup',
        CloseIDVPopup:'CloseIDVPopup',
        UpdateIDV:'UpdateIDV',

        OpenEditPolicy:'OpenEditPolicy',
        CloseEditPolicy:'CloseEditPolicy',
        EditQuotes:'EditQuotes',

        OpenDiscountsPopup:'OpenDiscountsPopup',
        CloseDiscountsPopup:'CloseDiscountsPopup',
        ApplyDiscounts:'ApplyDiscounts',
        ChangeDiscountsPopup:'ChangeDiscountsPopup',

        AddZeroDep:'AddZeroDep',
        RemoveZeroDep:'RemoveZeroDep',
        UpdatePassengerCover:'UpdatePassengerCover',

        OpenAccessoriesPopup:'OpenAccessoriesPopup',

        AddDriverCover:'AddDriverCover',
        RemoveDriverCover:'RemoveDriverCover',

        AddPassengerCover:'AddPassengerCover',
        RemovePassengerCover:'RemovePassengerCover',

        SelectAddon:'SelectAddon',
        DeselectAddon:'DeselectAddon',

        SortPlans:'SortPlans',

        OpenPremiumBreakupPopup:'OpenPremiumBreakupPopup',
        ClosePremiumBreakupPopup:'ClosePremiumBreakupPopup',
        BuyFromPremiumBreakup:'BuyFromPremiumBreakup',

        BuyPlan:'BuyPlan',
        BuyPlanUnoptimised:'BuyPlanUnoptimised',
        BuyOptimiseFirst:'BuyOptimiseFirst',

        ShareQuotes:'ShareQuotes',
        CallMe:'CallMe',

        MakePayment: 'MakePayment',

    };



});