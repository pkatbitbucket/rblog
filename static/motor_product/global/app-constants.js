define(['configuration',], function(settings) {

    var papAmounts = (VEHICLE_TYPE == 'fourwheeler'? [0, 10000, 50000, 100000, 200000]:[0, 10000, 50000, 100000])

    return {
        age_wise_previous_ncb: {
            0: 0,
            1: 0,
            2: 20,
            3: 25,
            4: 35,
            5: 45,
            6: 50
        },

        paPassengerAmounts: papAmounts,

        ncb_options: [{
                key: '0%',
                value: 0
            }, {
                key: '20%',
                value: 20
            }, {
                key: '25%',
                value: 25
            }, {
                key: '35%',
                value: 35
            }, {
                key: '45%',
                value: 45
            }, {
                key: '50%',
                value: 50
            }

        ],

        new_ncb_options: [{
                key: '20%',
                value: 20
            }, {
                key: '25%',
                value: 25
            }, {
                key: '35%',
                value: 35
            }, {
                key: '45%',
                value: 45
            }, {
                key: '50%',
                value: 50
            }

        ],

        boolean_options: [{
            key: 'No',
            value: 0
        }, {
            key: 'Yes',
            value: 1
        }],

        occupation_options: [{
            key: 'Doctors registered with Government',
            value: 5
        }, {
            key: 'Central / State Government Employees',
            value: 4
        }, {
            key: 'Teacher in Govt. recognized Institutions',
            value: 3
        }, {
            key: 'Defense and Para Military Service',
            value: 2
        }, {
            key: 'Practicing Chartered Accountant',
            value: 1
        }, {
            key: 'Others',
            value: 0
        }, ],

        addons: settings.getConfiguration().addons,
        cng_fitted_options: settings.getConfiguration().cng_fitted_options,
        vd_options: settings.getConfiguration().vd_options,
        is_cng_kit_valid: settings.getConfiguration().is_cng_kit_valid,
        vehicle_type: settings.getConfiguration().vehicle_type
    };
});