define(['knockout'], function(ko) {
    ko.components.register('multiselect-widget', {
        viewModel: require('./components/multiselect-widget'),
        template: require('raw!./components/multiselect-widget.html'),
    });

    ko.components.register('plancard', {
        viewModel: require('./components/plan-card'),
        template: require('raw!./components/plan-card.html'),
    });

    ko.components.register("number-inputbox",{
        viewModel: require('./components/number-inputbox'),
        template: require('raw!./components/number-inputbox.html'),
    });

    ko.components.register("error-label",{
        viewModel:function(params){
            var self = this;
            self.value = params.value;
        },
        template:'<label class="error" data-bind="text:$data.value"></label>'
    });

    ko.components.register("my-select",{
        viewModel: require('./components/month-selectbox'),
        template: require('raw!./components/month-selectbox.html')
    });

    ko.components.register("date-picker",{
        viewModel: require('./components/date-picker-widget'),
        template: require('raw!./components/date-picker-widget.html')
    });

    ko.components.register("radiobox-widget",{
        viewModel: require('./components/radioBox-widget'),
        template: require('raw!./components/radioBox-widget.html')
    });

    ko.components.register("registration-number-widget",{
        viewModel: require('./components/registration-number-widget'),
        template: require('raw!./components/registration-number-widget.html') 
    })

    return {};
});