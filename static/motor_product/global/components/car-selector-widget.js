define(['knockout', 'jquery'], function(ko, $) {

	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	var miniQuote = function(){
		var self = this;
		self.make = ko.observable().extend({
            store: {
                key : 'vehicleMake'
            }
        });

        self.model = ko.observable().extend({
            store: {
                key : 'vehicleModel'
            }
        });

        self.variant = ko.observable().extend({
            store: {
                key : 'vehicleVariant'
            }
        });

        self.vehicleId = ko.observable(null).extend({
            store: {
                key : 'vehicleId'
            }
        });

        self.isNewVehicle = ko.observable(null).extend({
            store: {
                key : 'isNewVehicle'
            }
        });

        self.policyExpired = ko.observable(false).extend({
            store: {
                key : 'policyExpired'
            }
        });

        self.man_year = ko.observable().extend({
            store: {
                key: 'man_year'
            }
        });

        self.man_month = ko.observable().extend({
            store: {
                key: 'man_month'
            }
        });

        self.manufacturingDate = ko.computed(function(){
            var man_month = ('0'+ (months.indexOf(self.man_month())+1)).slice(-2),
                man_year = self.man_year();

            return '01-'+man_month+'-'+man_year;
        });


        self.rto_info = ko.observable().extend({
            store:{
                key : 'rto_info'
            }
        });

        self.reg_no = ko.observable().extend({
            store:{
                key : 'reg_no'
            }
        });

        self.registrationNumber = ko.computed(function(){

            if(self.isNewVehicle()){

                if(self.rto_info() && self.rto_info() != "undefined"){
                    var regNoArray  = ['0', '0', '0', '0'];

                    regNoArray[0] = self.rto_info().id.split('-')[0];
                    regNoArray[1] = self.rto_info().id.split('-')[1];

                    return regNoArray;
                }
                else{
                    return null;
                }
            }

            else{
                if(self.reg_no()){

                    return self.reg_no().split('-');
                }
                else{
                    return null;
                }
            }


        });

        self.reg_month = ko.observable().extend({
            store:{
                key : 'reg_month'
            }
        });

        self.reg_year = ko.observable().extend({

            store:{
                key : 'reg_year'
            }
        });

        self.registrationDate = ko.computed(function(){

            // get the index of the entered month in the months array and append a 0 if index < 10
            var reg_month = ('0'+ (months.indexOf(self.reg_month())+1)).slice(-2),
                reg_year = self.reg_year();

            return '01-'+reg_month+'-'+reg_year;


        });
	}

	var miniQuoteInstance = new miniQuote();


    function CarSelectorWidgetViewModel(params) {
        var self = this;
        self.setupJSON = params.setupJSON;
        self.miniQuote = miniQuoteInstance;
        self.miniQuote.make(self.setupJSON.make.value);
        self.miniQuote.model(self.setupJSON.model.value);
        self.miniQuote.variant(self.setupJSON.variant.value);
        self.miniQuote.vehicleId(self.setupJSON.vehicleId.value);
        self.miniQuote.isNewVehicle(self.setupJSON.isNewVehicle.value);
        self.miniQuote.policyExpired(self.setupJSON.policyExpired.value);
        self.miniQuote.man_year(self.setupJSON.man_year.value);
        self.miniQuote.man_month(self.setupJSON.man_month.value);
        self.miniQuote.reg_year(self.setupJSON.reg_year.value);
        self.miniQuote.reg_month(self.setupJSON.reg_month.value);
        self.miniQuote.reg_no(self.setupJSON.reg_no.value);
        self.miniQuote.rto_info(self.setupJSON.rto_info.value);
        self.redirect_url(self.setupJSON.redirect_url);
    }


    return CarSelectorWidgetViewModel;

});