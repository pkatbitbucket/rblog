define(['knockout', 'pikaday'], function(ko, Pikaday) {



    function formatDate(date){
        var dd = ('0'+date.getDate()).slice(-2),
            mm = ('0'+(date.getMonth()+1)).slice(-2),
            yyyy = date.getFullYear();

        return dd + '-' + mm + '-' + yyyy;
    }



    function stringToDate(_date,_format,_delimiter)
    {
        var formatLowerCase=_format.toLowerCase();
        var formatItems=formatLowerCase.split(_delimiter);
        console.log("DATE HERE>>",_date);
        var dateItems=_date.split(_delimiter);


        var monthIndex=formatItems.indexOf("mm");
        var dayIndex=formatItems.indexOf("dd");
        var yearIndex=formatItems.indexOf("yyyy");
        var month=parseInt(dateItems[monthIndex]);
        month-=1;
        return new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    }



    ko.bindingHandlers.calendarInput = {

        update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
            var options = valueAccessor();




            var picker = new Pikaday(
                {
                    field: element,
                    firstDay: 1,
//                    minDate: new Date('2000-01-01'),
//                    maxDate: new Date('2020-12-31'),
                    yearRange: [options.minYear,options.maxYear],
                    defaultDate:options.defaultDate,
                    setDefaultDate:options.setDefaultDate,
                    onSelect: function(date){
                        options.selectedValue(formatDate(date));
                    }

                });
        }
    };


    function DateInputWidgetViewModel(params) {
        var self = this;
        self.selectedValue = params.selectedValue;
        self.minYear = params.minYear;
        self.maxYear = params.maxYear;
        self.defaultDate = params.defaultDate? stringToDate(params.defaultDate,'dd-mm-yyyy', '-') : new Date();
        self.setDefaultDate = params.setDefaultDate || false;




    }


    return DateInputWidgetViewModel;

});