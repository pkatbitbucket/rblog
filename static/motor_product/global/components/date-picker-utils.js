define('',function _utils(){
    var monthList = [
        {index:"",  shortName:"", label:"Month"} ,
        {index:"01",  shortName:"Jan", label:"January"} ,
        {index:"02",  shortName:"Feb", label:"February"} ,
        {index:"03",  shortName:"Mar", label:"March"} ,
        {index:"04",  shortName:"Apr", label:"April"} ,
        {index:"05",  shortName:"May", label:"May"} ,
        {index:"06",  shortName:"Jun", label:"June"},
        {index:"07",  shortName:"Jul", label:"July"} ,
        {index:"08",  shortName:"Aug", label:"August"} ,
        {index:"09",  shortName:"Sep", label:"September"} ,
        {index:"10", shortName:"Oct", label:"October"} ,
        {index:"11", shortName:"Nov", label:"November"} ,
        {index:"12", shortName:"Dec", label:"December"}
    ];
    var selectionProp = "index";

    function isNumberKey(e){
        var keyCode = e.keyCode ? e.keyCode : e.which;
        return  ((keyCode<=57 && keyCode != 32) || // Num keys above Qwerty Key and Special keys like backspace and tab
                (keyCode>=96 && keyCode<=105) ||   // Num pad keys
                (keyCode >=112 && keyCode <= 145)) // 
    }

    function isNumberValue(val){
        if(val)
            return !isNaN(val);
        else return false;
    }

    function isDate(d,m,y, format){
        var ret = {
            success: true,
            data:[],
            errors:[]
        }
        var valid_d = true;
        var valid_m = isNumberValue(m)
        var valid_y = isNumberValue(y)

        if(format.indexOf("d")>=0)
            valid_d = isNumberValue(d);

        if(y.length == 2){
            y = parseInt(y)+2000>new Date().getFullYear()? parseInt(y)+ 1900:parseInt(y) + 2000;
        }
        else if(y.length == 3){
            y = parseInt(y)+1900;
        }

        if(parseInt(d) > 31){
            ret.errors.push("Date cannot be more than 31");
        }
        else if(d>daysInMonth(m,y)){
            var _monthName = monthList.filter(function(month){return (month.index == m);})
            ret.errors.push("Invalid date ("+ d +") forgiven month ("+_monthName[0].label +")");
        }
        if(valid_y && valid_m && valid_d){
            ret.success = true;
            
            ret.data = createDate(d,m,y, format);
        }
        else {
            if(!valid_y && !valid_m && !valid_d){}
            else{
                ret.success = false;
                ret.errors.push("Invalid Input")
            }
        }
        return ret;
    }

    function createDate(d,m,y, format){
        var formatter  = "";
        if(format.indexOf("-")>=0)
            formatter = "-";
        else if(format.indexOf("/")>=0)
            formatter = "/";
        else if(format.indexOf(" "))
            formatter = " ";

        var _dateArr = format.split(formatter);
        var _formattedDate = "";
        _dateArr.forEach(function(row,index){
            if(row.indexOf("d")>=0){
                _formattedDate += d.trim();
            }
            else if(row.toLowerCase().indexOf("m")>=0){
                var temp = monthList.filter(function(month){ return (month.index == m);});
                _formattedDate += temp[0][selectionProp]
            }
            else if(row.indexOf("y")>=0){
                if(row.indexOf("yyyy")>=0)
                    _formattedDate += y;
                else _formattedDate += y.substring(y.length-2);
            }
            _formattedDate += formatter;    
        });

        if(format.indexOf("d")<0)
            d="01";
        if(m === undefined)
            m="01";
        if(y === undefined)
            y = "1900";

        var dateObj= {
            _date:  new Date(y, m,d),
            _formattedDate:_formattedDate.substring(0, _formattedDate.length-1)
        }
        return dateObj
    }

    function daysInMonth(month,year) {
        return new Date(year, month, 0).getDate();
    }

    function trigger(element){
        console.log(element)
        var event;
        event = document.createEvent('MouseEvents');
        event.initMouseEvent('mousedown', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
        element.dispatchEvent(event);
    }

    return {
        monthList: monthList,
        isDate: isDate,
        isNumberValue: isNumberValue,
        trigger: trigger
    }
});