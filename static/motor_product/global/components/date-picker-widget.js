define(['knockout','./date-picker-utils'], function(ko, _utils){

    function datePickerViewModel(params){
        var self = this;
        self.date=ko.observable(params.date || '');
        self.month=ko.observable(params.month || '');
        self.year=ko.observable(params.year || '');

        self.showDate = ko.observable(false);
        self.showMonth = ko.observable(false);
        self.showYear = ko.observable(false);

        self.dateFormat = params.dateFormat || "dd-mm-yyyy";

        if(self.dateFormat.indexOf("d")>=0){
            self.showDate(true);
        }
        if(self.dateFormat.toLowerCase().indexOf("m")>=0){
            self.showMonth(true);

            if(self.dateFormat.indexOf("MMM")>=0)
                selectionProp = "shortName";
            else if(self.dateFormat.indexOf("M")>=0)
                selectionProp = "label";
            else selectionProp = "index";
        }
        if(self.dateFormat.indexOf("y")>=0){
            self.showYear(true);
        }

        self.validateDate = function(){
            if( self.date() && self.month() && self.year() && 
                self.date().length>0 && self.month().length>=0 && self.year().length>0){
                var _d = _utils.isDate(self.date(), self.month(), self.year(), self.dateFormat);
                if(_d){
                    params.returnValue(_d);
                }
            }
        }

        self.dateClass = params.dateClass || '';
        self.monthClass = params.monthClass || '';
        self.yearClass = params.yearClass || '';

        self.date.subscribe(function(data){
            self.validateDate();
        })
        self.month.subscribe(function(data){
            self.validateDate();
        })
        self.year.subscribe(function(data){
            self.validateDate();
        });

        if(typeof params.initialValue == 'function'){
            self.initialValue = params.initialValue()
        }
        else{
            self.initialValue = params.initialValue
        }

        if(!self.initialValue){
            self.initialValue = ""
        }

        if(self.initialValue.indexOf('/')>=0){
            self.initialValue = self.initialValue.replace(/\//g,'-');
        }

        //self.initialValue = params.initialValue || "";
        if(self.initialValue.length>0 || typeof(self.initialValue) === "object"){
            try{
                if(typeof(self.initialValue) == "string" && self.initialValue.indexOf("-")>0){
                    var _dateArray = self.initialValue.split("-");
                    self.date(_dateArray[0]);
                    self.month(_dateArray[1]);
                    self.year(_dateArray[2]);
                }
                else{
                    self.date(self.initialValue.getDate());
                    self.month(self.initialValue.getMonth()+1);
                    self.year(self.initialValue.getFullYear());
                }
            }catch(ex){
                self.date("");
                self.month("");
                self.year("");
            }
        }
    }

    return datePickerViewModel;
});