define(['knockout', 'jquery'], function(ko, $) {

    function createClass(name, rules) {
        var style = document.createElement('style');
        style.type = 'text/css';
        document.getElementsByTagName('head')[0].appendChild(style);
        if (!(style.sheet || {}).insertRule)
            (style.styleSheet || style.sheet).addRule(name, rules);
        else
            style.sheet.insertRule(name + "{" + rules + "}", 0);
    }

    function get_element_stats(element) {
        var height = element.offsetHeight;
        var width = element.offsetWidth;
        var rect = element.getBoundingClientRect();
        return {
            h: height,
            w: width,
            x: rect.left,
            y: rect.top
        };
    }

    function PopupViewModel(params) {
        var self = this;
        self.customClass = params.customClass;
        self.parent_element = params.parent_element;
        self.active = params.active;

        self.initial_style = ko.computed(function() {
            if (self.parent_element()) {
                var stats = get_element_stats(self.parent_element());
                return "width: " + stats.w + 'px;' + "height: " + stats.h + 'px;' + "left:" + stats.x + 'px;' + "top:" + stats.y + 'px;';
            } else {
                return "";
            }
        });

        self.style = ko.observable();

        self.parent_element.subscribe(function() {

            if (self.parent_element()) {
                self.style(self.initial_style());
                setTimeout(function() {
                    self.style("left: 100px; width: 100px; height: 200px; top:100px;");
                }, 10);
            }

            // setTimeout(function() {
            //     self.style("left: 100px");
            // }, 200);
        });
    }

    return PopupViewModel;

});