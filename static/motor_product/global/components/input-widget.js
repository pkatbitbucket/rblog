define(['knockout', 'jquery'], function(ko, $) {

    function InputWidgetViewModel(params) {
        var self = this;
        self.customClass = params.customClass;
        self.fieldValue = params.fieldValue;
        self.valueUpdate = params.valueUpdate || 'keyup';
        self.label = params.label || '';
        self.placeholder = params.placeholder || params.label;

    }


    return InputWidgetViewModel;

});