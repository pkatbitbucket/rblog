define(['knockout','./date-picker-utils'], function(ko, _utils){
	function monthSelectBoxViewModel(params){
        var self = this;
        self.option = _utils.monthList
        self.selectedValue = params.value || '';
        self.dateFormat = params.dateFormat || "dd-mm-yyyy";

        self.onchange = function(data, event){
            params.value = self.selectedValue;
            var ctrl = event.currentTarget.parentElement;
            var next = ctrl.nextElementSibling.children[0];
            next.focus()
        }
    }

    return monthSelectBoxViewModel;
});