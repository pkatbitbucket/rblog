define(['knockout'], function(ko) {

    var singleSelect = function(item) {
        var self = this;
        self.defaultText = item.defaultText || 'Choose a value';
        self.exampleText = item.exampleText || '';
        self.selectedValue = item.selectedValue || ko.observable(null);
        self.selectedValueParsed = item.selectedValueParsed || ko.observable(null);
        self.customClass = item.customClass || null;
        self.keyword = ko.observable(null);
        self.items = item.data;
        self.isddVisible = ko.observable(false);
        self.optionText = item.optionText || null;
        self.optionValue = item.optionValue || null;
        self.onSelect = item.onSelect;
        self.inlineLabel = item.inlineLabel;
        self.selectedValueVerbose = ko.computed(function() {
            if (self.selectedValue()) {
                if (self.optionText !== null) {
                    return self.selectedValue()[self.optionText];
                } else {
                    return self.selectedValue();
                }
            }
        });

        if (self.selectedValueParsed() != null && self.selectedValue() == null && self.optionValue) {

            self.selectedValue(self.items.filter(function(item) {
                return item[self.optionValue] == self.selectedValueParsed();
            })[0]);
        }


        self.handlemousedown = function(e){
             var source = e.target;
                var found = false;
                while(source.parentNode){
                    found = (source.classList.contains('active') && source.classList.contains('multiselect-item'));
                    if(found) return;
                    source = source.parentNode;
                }
            self.hideDropdown();
        }

        self.selectedValueUpdate = function() {
            if (self.selectedValue()) {
                if (self.optionValue !== null) {
                    self.selectedValueParsed(self.selectedValue()[self.optionValue]);
                } else {
                    self.selectedValueParsed(self.selectedValue());
                }
            }
        };

        function keyPressEvent(event){
            var pos = undefined;
            var keyCode = event.keyCode ? event.keyCode : event.which;
            var parentContainer = $(".multiselect-item.active").children(".multiselect-dd").children()[1];
            if(parentContainer){
                $(parentContainer).scrollTop(0);
                var current = $(parentContainer).find(".dd-element.selected").first();
                if(current == undefined || current.length ==0){
                    current = $(parentContainer).find(".dd-element").first();
                    $(current).addClass("selected");
                    return;
                }
                switch(keyCode){
                    case 13:
                        $(current).trigger("click");
                        break;
                    case 38:
                        if(!$(current).is(":first-child")){
                            var prev = $(current).prev();
                            $(current).removeClass("selected");
                            $(prev).addClass("selected");
                            pos = $(prev).position();   
                        }
                        else{
                            pos = $(current).position();
                        }
                        break;
                    case 40:
                        if(!$(current).is(":last-child")){
                            var next = $(current).next();
                            $(current).removeClass("selected");
                            $(next).addClass("selected");
                            pos = $(next).position();                   
                        }
                        else{
                            pos = $(current).position();
                        }
                        break;
                }
                if(pos){
                    $(parentContainer).scrollTop(pos.top - $(parentContainer).height()/2);
                }                   
            } 
        }

        self.showDropdown = function(data, event) {
            self.isddVisible(true);
            // register keyup event to handle navigation using keys
            $(".multiselect-item.active").children(".multiselect-dd").off("keydown");
            $(".multiselect-item.active").children(".multiselect-dd").bind({
                "keydown": function(e){
                    keyPressEvent(e);      
                }
            });

            $(document.body).on('mousedown', function(e) {
                var isKeywordClick = $(e.target).parents('.keyword');
                var $parentClass = $(e.target).parents('.multiselect-item.active');

                if (!$parentClass.length) {
                    // Remove key events to prevent multiple bindings.
                    $(".multiselect-item.active").children(".multiselect-dd").off("keydown");
                    self.hideDropdown();
                }

                if (!isKeywordClick) {
                    $('html').off('mousedown');
                }
            });

            var parentContainer = $(".multiselect-item.active").children(".multiselect-dd").children()[1];
            var current = $(parentContainer).find(".dd-element.selected").first();
            $(parentContainer).scrollTop(0);
            console.log(current);
            if(current.length>0){
                $(parentContainer).scrollTop($(current).position().top - $(parentContainer).height()/2);
            }
        };
        
        self.hideDropdown = function() {
            self.isddVisible(false);
            document.removeEventListener("mousedown", self.handlemousedown);
        };

        self.toggleDropdown = function(data, event) {
            if (self.isddVisible() == true) {
                self.hideDropdown();
            } else {
                self.showDropdown(data, event);
            }
        };

        self.selectValue = function(data) {
            self.selectedValue(data);
            if (self.onSelect) {
                self.onSelect();
            }

            self.selectedValueUpdate();
            self.hideDropdown();
        };

        self.parsedItems = ko.computed(function() {
            var items = self.items;
            if (items.constructor.toString().indexOf('Array') == -1) {
                items = items();
            }
            return items;
        });

        self.filteredItems = ko.computed(function() {
            var items = self.items;
            if (items.constructor.toString().indexOf('Array') == -1) {
                items = items();
            }
            if (self.keyword()) {
                return ko.utils.arrayFilter(items, function(item) {
                    if (self.optionText !== null) {
                        return item[self.optionText].toLowerCase().replace(/[^a-zA-Z0-9]/g, "").indexOf(self.keyword().toLowerCase().replace(/[^a-zA-Z0-9]/g, "")) > -1;
                    } else {
                        return item.toLowerCase().replace(/[^a-zA-Z0-9]/g, "").indexOf(self.keyword().toLowerCase().replace(/[^a-zA-Z0-9]/g, "")) > -1;
                    }
                });
            } else return items;
        });
    };

    function MultiSelectWidgetViewModel(params) {
        var self = this;
        self.items = params.$raw.items().map(function(item) {
            return new singleSelect(item);
        });
        self.hideSearch = params.hideSearch || false;
        self.customClass = params.customClass || null;
    }

    return MultiSelectWidgetViewModel;
});
