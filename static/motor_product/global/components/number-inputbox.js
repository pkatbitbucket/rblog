define(['knockout','./date-picker-utils'], function(ko, _utils){
    function numberInputBoxViewModel(params){
        var self = this;
        self.value = params.value || 'none';
        self.placeholder = params.placeholder || '';
        self.className = params.className;
        self.maxlength = params.maxlength;
        self.placeholder = params.placeholder;
        var returnData = {
            success: true,
            data: [],
            errors: []
        }

        self.validate_number = function(data, event){
            if(typeof(data.value) == "function")
                returnData.data = data.value();
            else
                returnData.data  = data.value;
            if(!_utils.isNumberValue(data.value())){
                returnData.success = false;
                returnData.errors.push("Entered value is not a number");
            }
            else{
                returnData.success = true;
                returnData.errors = [];
            }
            params.value = returnData;
        }
        self.keyDown = function(data,event){
            var value = event.target.value;
            var keyCode = event.keyCode ? event.keyCode : event.which;

         if(!((keyCode<=57 && keyCode != 32) || // Num keys above Qwerty Key and Special keys like backspace and tab
            (keyCode>=96 && keyCode<=105) ||   // Num pad keys
            (keyCode >=112 && keyCode <= 145))){
            event.preventDefault();
            return false;
         }

            return true;
        }

        self.keyUp = function(data, event){
            var value = event.target.value;
            var keyCode = event.keyCode ? event.keyCode : event.which;
            if(value && self.maxlength >0 && value.length >= self.maxlength && keyCode>46){
                var ctrl = event.currentTarget.parentElement;
                if(ctrl && ctrl.nextElementSibling){
                    _utils.trigger(ctrl.nextElementSibling.children[0]);
                }
            }
        }
    }

    return numberInputBoxViewModel;
});