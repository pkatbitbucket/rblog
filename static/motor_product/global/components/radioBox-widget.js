define(['knockout'], function(ko){
	function radioBoxViewModel(params){
        var self = this;
        self.optionList = params.options() || [];
        self.selectedValue = ko.observable(params.initialValue() || self.optionList[0].value);
        self.className = params.className || '';
        self.updateValue = function(data, event){
            self.selectedValue(data.value);
        }
        self.selectedValue.subscribe(function(data){
            params.value(data);
        })
    }

    return radioBoxViewModel;
});