define(['knockout', 'jquery', 'rangeslider'], function(ko, $) {



    ko.bindingHandlers.rangeSliderBinding = {

        update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {

            var _value = valueAccessor();
            $(element).rangeslider({

                polyfill: false,
                rangeClass: 'rangeslider',
                fillClass: 'rangeslider__fill',
                handleClass: 'rangeslider__handle',
                onInit: function() {

                },
                onSlide: function(position, value) {
                    _value.value(value);
                },

                onSlideEnd: function(position, value) {

                }
            }).val(_value.value()).change();

        }
    };


    function RangeSlideWidgetViewModel(params) {
        var self = this;
        self.min = params.min;
        self.max = params.max;
        self.initial = params.initial;
        self.step = params.step;
        self.value = params.value;
    }


    return RangeSlideWidgetViewModel;

});
