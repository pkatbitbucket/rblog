define(['knockout'], function(ko){
	function registrationNumberViewModel(params){
		var self = this;
		self.initialValue = params.initialValue || [];
		self.state = ko.observable(params.state || '');
		self.district = ko.observable(params.district || '');
		self.series = ko.observable(params.series || '');
		self.number = ko.observable(params.number || '');
		self.className = params.className;
		self.value = ko.observable();
		self.enableSeries = params.enableSeries || false;
		if(self.initialValue && self.initialValue.length>0){
			var formatter = '';
			if(self.initialValue.indexOf('-')>0){
				formatter = '-'
			}
			else formatter = ' ';

			var _regNo = self.initialValue.toUpperCase().split(formatter);
			self.state(_regNo[0] || '');
			self.district(_regNo[1] || '');
			self.series(_regNo[2] || '');
			self.number(_regNo[3] || '');
		}
		else
			updateValue(params, self);

		self.state.subscribe(function(data, element){updateValue(params,  self)});
		self.district.subscribe(function(data, element){updateValue(params,  self)});
		self.series.subscribe(function(data, element){updateValue(params,  self)});
		self.number.subscribe(function(data, element){updateValue(params,  self)});
		
		self.keyDown = function(data,event){
			var ele = event.target;
			var keyCode = event.keyCode ? event.keyCode : event.which;
			if(ele.maxLength == ele.value.length && keyCode>46){
				event.preventDefault();
			}
			else{
				return true;
			}
		}
		self.autoTab = function(data,event){
			var ele = event.target;
			var keyCode = event.keyCode ? event.keyCode : event.which;
			if(ele.maxLength == ele.value.length && keyCode>46){
				if(event.target.nextElementSibling)
				event.target.nextElementSibling.focus();
			}
			else{
				return true;
			}
		}
	}

	function updateValue(params, self){
		var h = '-';

		// Remove in between spaces
		// Example "MH-02-CB-4  4"
		self.series(self.series().replace(/ /g,''));
		self.number(self.number().replace(/ /g,''));

		if(self.state() && self.district() && self.series() && self.number() && 
			self.state().trim().length>0 && self.district().trim().length>0 && self.series().trim().length>0 && self.number().trim().length>0){
			self.value( self.state() + h + 
						self.district() + h + 
						self.series() + h + 
						self.number());
			params.returnValue(self.value());
		}
	}

	return registrationNumberViewModel;
})