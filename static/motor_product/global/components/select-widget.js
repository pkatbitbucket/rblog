define(['knockout', 'jquery'], function(ko, $) {

    function SelectWidgetViewModel(params) {
        var self = this;
        self.selectedValue = params.selectedValue;
        self.items = params.data;
        self.defaultText = params.defaultText || 'Choose a value';
        self.optionText = params.optionText || null;
        self.keyword = ko.observable();
        self.customClass = params.customClass || null;

        self.dropdownVisible = ko.observable(false);

        self.selectValue = function(data){
          self.selectedValue(data);
        };

        self.toggleDropdown = function(){
          /*$('.select-widget-dropdown').removeClass('reveal');*/
          if(self.dropdownVisible() == true){
             self.dropdownVisible(false);
          }
          else{
             self.dropdownVisible(true);
          }
        };

        self.filteredItems = ko.computed(function(){
            if(self.keyword()){
                return ko.utils.arrayFilter(self.items, function(item) {
                    return item.indexOf(self.keyword()) > -1;
                });
            }
            else{
                return self.items;
            }
        });


    }


    return SelectWidgetViewModel;

});