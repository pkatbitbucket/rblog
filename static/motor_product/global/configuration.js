define([], function() {

    var vehicle_type = VEHICLE_TYPE;

    var Configuration = {};

    Configuration.fourwheeler = {
        fuel_type_list: [
            {
                'id': 'PETROL',
                'name': 'Petrol',
                'code': 'petrol',
            },
            {
                'id': 'DIESEL',
                'name': 'Diesel',
                'code': 'diesel',
            },
            {
                'id': 'INTERNAL_LPG_CNG',
                'name': 'CNG/LPG company fitted',
                'code': 'cng_lpg_internal',
            },
            {
                'id': 'PETROL',
                'name': 'CNG/LPG externally fitted',
                'code': 'cng_lpg_external',
            }
        ],
        cng_fitted_options: [{
                key: 'No',
                value: 0
            }, {
                key: 'Company Fitted',
                value: 1
            }, {
                key: 'Externally Fitted',
                value: 2
            },

        ],
        vd_options: [{
            key: 'No Deductible',
            value: 0,
            saveupto: 0,
        }, {
            key: 'Rs. 2,500',
            value: 2500,
            saveupto: 750,
        }, {
            key: 'Rs. 5,000',
            value: 5000,
            saveupto: 1500,
        }, {
            key: 'Rs. 7,500',
            value: 7500,
            saveupto: 2000,
        }, {
            key: 'Rs. 15,000',
            value: 15000,
            saveupto: 2500,
        }],
        addons: [{
            name: 'Zero Depreciation',
            id: 'addon_isDepreciationWaiver',
            desc: 'In case of every claim settlement, an amount is supposed to be paid from your pocket depending on the standard depreciation logic applied on the parts involved in the repair. If you do not want any amount to be paid from your end in case of simple repairs then go for Zero Depreciation.'
        }, {
            name: 'Invoice Cover',
            id: 'addon_isInvoiceCover',
            desc: 'In case of theft or total damage to your car, you are only eligible for reimbursement up to the Insured declared value of your car, which will be very less than the Invoice value. In case of such an event, selecting Invoice cover makes you eligible for full Invoice amount reimbursement.'
        }, {
            name: '24x7 Roadside Assistance',
            id: 'addon_is247RoadsideAssistance',
            desc: 'Road side assistance provides support for basic on-road breakdown situations like tyre change, battery jump start, emergency fuel, medical assistance etc which are not covered under Insurance. As the price is very nominal, it is a good to have add-on.'
        }, {
            name: 'Engine Protector',
            id: 'addon_isEngineProtector',
            desc: 'When the Engine of the car is submerged in a water logged area, using or cranking the engine can result in engine ceasing. This is not covered under regular Insurance. Engine protector covers such non-accidental exclusions related to your engine. It is a must buy for luxury cars where engine is very costly & is placed at low ground clearance.'
        }, {
            name: 'NCB Protection',
            id: 'addon_isNcbProtection',
            desc: 'No Claim Bonus or NCB is the discount you get on your premium for every consecutive claim free year. This can go up to 50% (or even higher) for 5 or more claim free years. This can also get down to zero with even a single claim, so if you have a good NCB then opt for NCB protection to preserve your NCB even after you make a claim.'
        }, ],
        is_cng_kit_valid: true,
        vehicle_type: 'fourwheeler',
        lms_default_campaign: 'MOTOR',
        ga_category: 'MOTOR'
    };

    Configuration.twowheeler = {
        fuel_type_list: [
            {
                'id': 'PETROL',
                'name': 'Petrol',
                'code': 'petrol',
            },
            {
                'id': 'DIESEL',
                'name': 'Diesel',
                'code': 'diesel',
            }
        ],
        cng_fitted_options: [{
            key: 'No',
            value: 0
        }],
        vd_options: [{
            key: 'No Deductible',
            value: 0,
            saveupto: 0,
        }, {
            key: 'Rs. 500',
            value: 500,
            saveupto: 50,
        }, {
            key: 'Rs. 750',
            value: 750,
            saveupto: 75,
        }, {
            key: 'Rs. 1,000',
            value: 1000,
            saveupto: 125,
        }, {
            key: 'Rs. 1,500',
            value: 1500,
            saveupto: 200,
        }, {
            key: 'Rs. 3,000',
            value: 3000,
            saveupto: 250,
        }],
        addons: [{
            name: 'Zero Depreciation',
            id: 'addon_isDepreciationWaiver',
            desc: 'In case of every claim settlement, an amount is supposed to be paid from your pocket depending on the standard depreciation logic applied on the parts involved in the repair. If you do not want any amount to be paid from your end in case of simple repairs then go for Zero Depreciation.'
        }, ],
        is_cng_kit_valid: false,
        vehicle_type: 'twowheeler',
        lms_default_campaign: 'MOTOR-BIKE',
        ga_category: 'BIKE'
    };

    var getConfiguration = function() {
        return Configuration[vehicle_type];
    };

    return {
        getConfiguration: getConfiguration
    }
});