// Polyfill to set customEvent. This is not supported by IE.
// Ref: https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent#Polyfill
(function () {
  if ( typeof window.CustomEvent === "function" ) return false;

  function CustomEvent ( event, params ) {
    params = params || { bubbles: false, cancelable: false, detail: undefined };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
   }

  CustomEvent.prototype = window.Event.prototype;

  window.CustomEvent = CustomEvent;
})();

define(['actions','cookie'], function(actions, Cookies) {

    //  './mixpanel-actions', = mixpanelActions

    // Custom Event Creator

    var cust_ev = function(ev, data) {
        var evt = new CustomEvent(ev, {
            detail: data
        });
        window.dispatchEvent(evt);
    };

    var add_listener = function(evname, func) {
        window.addEventListener(evname, function(e) {
            if (func) func(e);
        });
    };


    var mix_panel_track = function(event,param){
        var param = (param)? param : ''
        mixpanel.track(event, param)
    };

    var gtm_push = function(gtm_event, gtm_category, gtm_action, gtm_label, gtm_extra_params){
        var data = {
            "event":gtm_event,
            "category":gtm_category,
            "action":gtm_action,
            "label":gtm_label,
        }
        for(param in gtm_extra_params){
            data[param] = gtm_extra_params[param]
        }

        dataLayer.push(data);
    };

    var vehicle_type_verbose, vtv;

    vehicle_type_verbose = vtv =  (VEHICLE_TYPE == 'fourwheeler' ? 'Car':'Bike');

    var new_verbose = function(value){
        if(value.is_expired)
            return 'Expired';
        else
            return (value.is_new? 'New': 'Renew');
    }

    var optimize_events_parameters = function(events_params){
        switch(events_params.detail.defaults.optimize_quotes_on)
        {
            case "on-load":
                return ['EventOptimizeQuote',' - Optimize Quote','','Optimize Quote'];

            case "on-edit":
                return ['EventEditPolicyDetails',' - Edit Policy Details','','Optimize Quotes'];

            case "on-premium-buy":
                return ['EventOptimizeQuotePremium',' - Optimize Quote Popup Premium','','Optimize Quote'];

            case "on-buy":
                return ['EventOptimizeQuoteFinal',' - Optimize Quote Popup Final','','Optimize Quote'];
        }

    }

    var buy_events_parameters = function(events_params){
        switch(events_params.detail.defaults.buy_plan_on)
        {
            case "on-no-quotes-premium-buy":
                return ['EventOptimizeQuotePremium',' - Optimize Quote Popup Premium','','Clicked'];

            case "on-quotes-premium-buy":
                return ['EventBuyPremium',' - Buy Plan On Premium Popup','',events_params.detail.defaults.insurer];

            case "on-no-quotes-buy":
                return ['EventOptimizeQuoteFinal',' - Optimize Quote Popup Final','','Clicked'];

            case "on-quotes-buy":
                return ['EventBuyPlan',' - Buy Plan','',events_params.detail.defaults.insurer];

            default:
                return ['EventBuyPlan',' - Buy Plan', '', events_params.detail.defaults.insurer];
        }

    }


    // Successful submission of Get Instant quotes
    add_listener(actions.ViewQuotes, function(e) {
        var quote = e.detail;
        var _date = quote.pastPolicyExpiryDate().split('-');
        var expiryDate = new Date(_date[2], _date[1]-1, _date[0]);

        var data = {
            param1: quote.vehicle().name,
            param2: quote.fuel_type().name,
            param3: quote.variant().name,
            param4: quote.rto_info().name,
        };

        if(!quote.isNewVehicle()){
            data['param5'] = quote.reg_year();
            data['param6'] = quote.expirySlot();
            data['param7'] = quote.email() && quote.email().length>0?"Email ID Given":"Email ID Not Given";
        }
        data['param7'] = quote.email() && quote.email().length>0?"Email ID Given":"Email ID Not Given";

        var policyExpired = false;
        if(expiryDate<new Date()){
            policyExpired = true;
        }

        var new_verbose_params = { is_new: quote.isNewVehicle() , is_expired : policyExpired };
        gtm_push(vtv+'EventGetQuote', vtv+' - Get Quote', new_verbose(new_verbose_params)+' '+vtv,'', data);

    });


    // Click optimize quote on the popup on the results page

    add_listener(actions.OptimizeQuotes, function(e) {

        var even_params = optimize_events_parameters(e);
        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+even_params[0], vtv+even_params[1], new_verbose(new_verbose_params)+' '+vtv,even_params[3], {});

        //  gtm_push(vtv+'EventOptimizeQuote', vtv+' - Optimize Quote', new_verbose(e.detail.defaults.is_new)+' '+vtv,'Optimize Quote', {});

    });


    // Click "I will fill this later" on the popup on the results page

    add_listener(actions.CancelOptimizeQuotes, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventFillLater', vtv+' - Optimize Quote', new_verbose(new_verbose_params)+' '+vtv,'Fill Later', {});

    });

    // Click on Edit IDV on Results Page

    add_listener(actions.OpenIDVPopup, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventEditIDV', vtv+' - Edit IDV', new_verbose(new_verbose_params)+' '+vtv,'Edit Clicked', {});

    });


    // Click on Close IDV on the IDV Popup Results Page

    add_listener(actions.CloseIDVPopup, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventEditIDV', vtv+' - Edit IDV', new_verbose(new_verbose_params)+' '+vtv,'Close IDV', {});

    });


    // Update IDV to Best Deal on the popup that appears after clicking on Edit IDV
    // Update IDV to a Customized Amount on the popup that appears after clicking on Edit IDV

    add_listener(actions.UpdateIDV, function(e) {

        var label = (e.detail.idvValue == 0 ? 'For The Best Deal': e.detail.idvValue );
        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventEditIDV', vtv+' - Edit IDV', new_verbose(new_verbose_params)+' '+vtv, label, {});

    });

    // Click on Edit Policy Details on Results Page

    add_listener(actions.OpenEditPolicy, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventEditPolicyDetails', vtv+' - Edit Policy Details', new_verbose(new_verbose_params)+' '+vtv,'Edit Clicked', {});

    });

    // Click on Close Edit Policy Details Popup on Results Page

    add_listener(actions.CloseEditPolicy, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventEditPolicyDetails', vtv+' - Edit Policy Details', new_verbose(new_verbose_params)+' '+vtv,'Close Policy Details', {});

    });

    // Submit Optimize Quotes on Edit Policy Details Popup on Results Page
        // Goto Optimise Quotes


    // click on Apply under Discounts section

    add_listener(actions.OpenDiscountsPopup, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventApplyDiscount', vtv+' - Apply Discount', new_verbose(new_verbose_params)+' '+vtv,'Apply Clicked', {});

    });

    // click on Cancel under Discounts section

    add_listener(actions.CloseDiscountsPopup, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventApplyDiscount', vtv+' - Apply Discount', new_verbose(new_verbose_params)+' '+vtv,'Cancel Discount', {});

    });

    // Click on Apply Discount on the pop up for Discounts

    add_listener(actions.ApplyDiscounts, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventApplyDiscount', vtv+' - Apply Discount', new_verbose(new_verbose_params)+' '+vtv,'Discount Applied', {
            param1: e.detail.vdValue
        });

    });

    // Click on Change Discount on the pop up for Discounts

    add_listener(actions.ChangeDiscountsPopup, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventApplyDiscount', vtv+' - Apply Discount', new_verbose(new_verbose_params)+' '+vtv,'Change Clicked', {} );

    });


    // Click on Add Zero Depreciation

    add_listener(actions.AddZeroDep, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventZeroDepreciation', vtv+' - Add Zero Depreciation', new_verbose(new_verbose_params)+' '+vtv,'', {});

    });

    // Click on Remove Zero Depreciation

    add_listener(actions.RemoveZeroDep, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventZeroDepreciation', vtv+' - Remove Zero Depreciation', new_verbose(new_verbose_params)+' '+vtv,'', {});

    });

    // Check The Additional Covers Check Boxes on the results page for Invoice Cover
    // Check The Additional Covers Check Boxes on the results page for 24X7 Roadside Assistance
    // Check The Additional Covers Check Boxes on the results page for Engine Protector
    // Check The Additional Covers Check Boxes on the results page for NCB Protection

    add_listener(actions.SelectAddon, function(e) {
        var label = e.detail.addonName + ' Checked';
        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventAdditionalCovers', vtv+' - Additional Covers', new_verbose(new_verbose_params)+' '+vtv,label, {});

    });

    // Check The Additional Covers Check Boxes on the results page for Driver Cover

    add_listener(actions.AddDriverCover, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventAdditionalCovers', vtv+' - Additional Covers', new_verbose(new_verbose_params)+' '+vtv,'Driver Cover Checked', {});

    });

    // Check The Additional Covers Check Boxes on the results page for Passenger Cover

    add_listener(actions.AddPassengerCover, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventAdditionalCovers', vtv+' - Additional Covers', new_verbose(new_verbose_params)+' '+vtv,'Passenger Cover Checked', {});
    });


    // Select The amount after Checking  The Additional Covers Check Boxes on the results page for Passenger Cover

    add_listener(actions.UpdatePassengerCover, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventPassengerCover', vtv+' - Passenger Covers', new_verbose(new_verbose_params)+' '+vtv,e.detail.defaults.PassengerCover, {});
    });

    // Click The Additional Covers Check Boxes on the results page for Accessories

    add_listener(actions.OpenAccessoriesPopup, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventAdditionalCovers', vtv+' - Additional Covers', new_verbose(new_verbose_params)+' '+vtv,'Accessories Checked', {});
    });

    // Click on Update Accessories when  the Cover Your Car Accessories Pop Up Appears

    add_listener(actions.UpdateAccessories, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventUpdateAccessories', vtv+' - Update Accessories Covers', new_verbose(new_verbose_params)+' '+vtv,e.detail.defaults.idvElectrical+'|'+e.detail.defaults.idvNonElectrical, {});

    });

    // Uncheck The Additional Covers Check Boxes on the results page for Invoice Cover
    // Uncheck The Additional Covers Check Boxes on the results page for 24X7 Roadside Assistance
    // Uncheck The Additional Covers Check Boxes on the results page for Engine Protector
    // Uncheck The Additional Covers Check Boxes on the results page for NCB Protection

    add_listener(actions.DeselectAddon, function(e) {

        var label = e.detail.addonName + ' Unchecked';
        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventAdditionalCovers', vtv+' - Additional Covers', new_verbose(new_verbose_params)+' '+vtv,label, {});


    });

    // Uncheck The Additional Covers Check Boxes on the results page for Driver Cover

    add_listener(actions.RemoveDriverCover, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventAdditionalCovers', vtv+' - Additional Covers', new_verbose(new_verbose_params)+' '+vtv,'Driver Cover Unchecked', {});

    });

    // Uncheck The Additional Covers Check Boxes on the results page for Passenger Cover

    add_listener(actions.RemovePassengerCover, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventAdditionalCovers', vtv+' - Additional Covers', new_verbose(new_verbose_params)+' '+vtv,'Passenger Cover Unchecked', {});
    });


    // Sort the Plans on the Results page by Best match
    // Sort the Plans on the Results page by Premium
    // Sort the Plans on the Results page by IDV

    add_listener(actions.SortPlans, function(e) {

        var label = e.detail.sortCriteria;
        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventSortPolicy', vtv+' - Sort Policy', new_verbose(new_verbose_params)+' '+vtv,label, {});


    });

    // Click on See Premium Breakup on the results page

    add_listener(actions.OpenPremiumBreakupPopup, function(e) {
        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventSeePremium', vtv+' - See Premium Breakup', new_verbose(new_verbose_params)+' '+vtv, e.detail.defaults.insurer, {
            param1: e.detail.defaults.sortCriteria,
            param2: e.detail.defaults.ZeroDepStatus,
            param3: e.detail.defaults.DiscountStatus,
            param4: e.detail.defaults.additionalCovers,
            param5: e.detail.defaults.plan_position,
        });

    });

    // Click on the Buy Premium on the popup of premium details

    add_listener(actions.BuyFromPremiumBreakup, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventBuyPremium', vtv+' - Buy Plan On Premium Popup', new_verbose(new_verbose_params)+' '+vtv, e.detail.defaults.insurer, {
            param1: e.detail.sortCriteria,
            param2: e.detail.ZeroDepStatus,
            param3: e.detail.DiscountStatus,
            param4: e.detail.additionalCovers,
            param5: e.detail.plan_position,
        });

    });

    // Click on the Back To Results on the popup of premium details

    add_listener(actions.ClosePremiumBreakupPopup, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventBackResults', vtv+' - Back To Results Premium Popup', new_verbose(new_verbose_params)+' '+vtv,'', {});

    });

    // Click on the Buy Premium on the popup of premium details to generate Optimize results Pop Up
    // Click on Optmize Quotes on Premium Break Up Pop Up


    // Click on Buy Plan to Go to the Proposer Form

    add_listener(actions.BuyPlan, function(e) {

        var even_params = buy_events_parameters(e);
        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+even_params[0], vtv+even_params[1], new_verbose(new_verbose_params)+' '+vtv,even_params[3], {
            param1: e.detail.defaults.sortCriteria,
            param2: e.detail.defaults.ZeroDepStatus,
            param3: e.detail.defaults.DiscountStatus,
            param4: e.detail.defaults.additionalCovers,
            param5: e.detail.defaults.plan_position,
        });
    });

    // Click on Buy Plan to generate Optimize results Pop Up

    add_listener(actions.BuyPlanUnoptimised, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventOptimizeQuoteFinal', vtv+' - Optimize Quote Popup Final', new_verbose(new_verbose_params)+' '+vtv,'Clicked', {});

    });

    // Click on Optmize Quotes on Buy Plan Pop Up

    add_listener(actions.BuyOptimiseFirst, function(e) {

    });

    // Click Call Me after entering phone number

    add_listener(actions.ShareQuotes, function(e) {

        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventShareQuotes', vtv+' - Share These Quotes', new_verbose(new_verbose_params)+' '+vtv,'Shared', {});

    });

    // Click on Share these quotes

    add_listener(actions.CallMe, function(e) {
        var new_verbose_params = { is_new: e.detail.defaults.is_new, is_expired : e.detail.defaults.is_expired() };

        gtm_push(vtv+'EventCallMe', vtv+' - Call Me Result Page', new_verbose(new_verbose_params)+' '+vtv,'CallScheduled', {});

    });


    // Click on Make Payment
    add_listener(actions.MakePayment, function(e) {
        gtm_push( vtv+'EventMakePayment', vtv+' - Make Payment', e.detail.defaults.insuranceType + " "+vtv, e.detail.defaults.event_label , {});

    });

    return {
        cust_ev: cust_ev,
        gtm_push:gtm_push
    };
});