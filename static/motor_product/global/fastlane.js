define(['form-models', 'crypt'], function(formmodels, crypt){
    function getFastLaneData(newValue, data){
        // Create registration number with all formats.
        regNo = normalizeRegNumber(newValue);

        // strip '-' from registration number
        regNo = regNo.replace(/-/g, '');

        $.ajax({
            type: 'POST',
            url: '/motor/fourwheeler/api/vehicle-info/',
            data: {'name':regNo, 'csrfmiddlewaretoken': CSRF_TOKEN},
            success: function(response){                
                // New structure
                if( response &&
                    response.data && 
                    response.data.fastlane &&
                    response.data.fastlane.response && 
                    response.data.fastlane.response.result &&
                    response.data.fastlane.response.result.vehicle){

                        var _result = response.data.fastlane.response.result.vehicle
                        
                        _result.vehicle_master_id = response.data.vehicle_master_id;
                        _result.vehicle_model_id = response.data.vehicle_model_id;

                        data(_result);
                }
                else{
                    data({});
                }
            },
            error:function(error){
                data({});
            },
            async:true
        });
    }

    // Accept registration number and return formatted number
    // Example: mh-02-az-1 -> MH-02-AZ-0001
    function normalizeRegNumber(data){
        var regNo ="";
        if(data.indexOf('-')>0){
            var arr = data.split('-');
            var temp= "";
            
            if(arr[3].trim().length <4){
                for (var i=0; i < (4-arr[3].trim().length); i++){
                    temp += "0";
                }
            }
            regNo = arr[0].trim() + "-" + arr[1].trim() + "-" + arr[2].trim() + "-" + temp + arr[3].trim();
            return regNo.toUpperCase();
        }
        else{
            return data;
        }
    }

    return{
        getFastLaneData:getFastLaneData,
        normalizeRegNumber:normalizeRegNumber
    }
});