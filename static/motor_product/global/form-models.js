define(['knockout', 'utils', 'rtoStateMapping'], function(ko, utils, rtoStateMapping) {

    var form_details = data.form_details;

    var occupation_list = [];

    if (form_details.occupation_list) {
        for (var k in form_details.occupation_list) {
            occupation_list.push({
                'key': form_details.occupation_list[k],
                'value': k
            });
        }
    }

    var financier_list = [];

    if (form_details.financier_list) {
        for (var l in form_details.financier_list) {
            financier_list.push({
                'value': l,
                'key': form_details.financier_list[l]
            });
        }
    }

    var pyp_insurer_list = [];

    if (form_details.insurers_list) {
        for (var m in form_details.insurers_list) {
            pyp_insurer_list.push({
                'key': form_details.insurers_list[m],
                'value': m
            });
        }
    }

    var state_list = [];

    if (form_details.state_list) {
        for (var m in form_details.state_list) {
            state_list.push({
                'key': form_details.state_list[m],
                'value': m
            });
        }
    }

    var relationship_list = [];
    if(form_details.relationship_list) relationship_list = form_details.relationship_list;

    var TypeValidators = {
        NonEmpty: function(data, error) {
            if (data === "" || data === undefined || data === null) {
                error('This field cannot be empty.');
                return false;
            }
            return true;
        },
        NonEmptySelect:function(data, error){
            if (data === "" || data === undefined || data === null) {
                error('Please select a value.');
                return false;
            }
            return true;   
        },
        NumberValidation: function(data, error) {
            var matched = data.match('[0-9]+');
            if (matched) return true;
                error('Entry must be a number.');
            return false;
        },
        YearValidation: function(data, error) {
            var matched = data.length == 4;
            if (matched) return true;
                error('Year must be 4 digits');
            return false;
        },
        EmailValidation: function(data, error) {
            //var reg = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9]{2}(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

            // Vinay's regex
            var reg = /^([A-Z0-9]+([+._-]?[A-Z0-9]+)*)@((?:[A-Z0-9-]+\.)*\w[A-Z0-9-]{0,66})\.([A-Z]{2,6}(?:\.[A-Z]{2})?)$/i

            if (!reg.test(data)){ 
                error('Please enter a valid Email Address.');
                return false;   
            }
            error("");
            return true;
        },
        PhoneValidation: function(data, error) {
            if(data.length != 10){
                error('Mobile Number cannot be of less than 10 digits.');
                return false;
            }
            var matched = data.match('[7-9][0-9]{9}');
            if (!matched) {
                error('Please enter a valid Phone Number.');
                return false;
            }

            return true;
        },
        PincodeValidation: function(data, error) {
            var matched = data.match('^((1[1-9])|([2-9][0-9]))[0-9]{4}');
            if (matched) return true;
                error('Please enter a valid Area Pincode.');
            return false;
        }
    }

    var CleanMethods = {
        BooleanToOneZero: function(field, value) {
            if (value) {
                return 1
            } else {
                return 0
            };
        },
        BinarySelectField: function(field, value) {
            if (field.OptionsList()[0] == value) {
                return 1;
            } else {
                return 0;
            }
        },
        UpperCase: function(field, value) {
            return value.trim().toUpperCase();
        },
        LowerCase: function(field, value) {
            return value.trim().toLowerCase();
        },
        NoSpace: function(field, value) {
            return value.replace(/ /g, "");
        },
        PhoneNumberClean: function(field, value) {
            if (value.length == 11 && value[0] == 0) {
                field.Value(value.substring(1));
                return value.substring(1);
            }
            return value;
        }
    }

    var Field = function(params) {
        var self = this;
        this.parent = params.parent;
        this.Id = params.id;    
        this.Name = params.name;
        this.Tag = params.tag;
        this.Type = params.type;
        this.Desc = params.desc;
        this.Template = params.template;
        this.Value = ko.observable(params.value).extend({
            notify: 'always'
        });

        visibleDefault = true;
        if (typeof(params.visible) == 'boolean') {
            visibleDefault = params.visible;
        }

        this.cssclass = ko.observable(params.cssClass);

        this.visible = ko.observable(visibleDefault);
        this.error = ko.observable(false);
        this.errorMessage = ko.observable('Error in field.');
        
        this.cleanedValue = ko.observable('');

        // this.cleanedValueVerbose = ko.computed(function(){
        //     var value = self.cleanedValue();
        //     if(value && value.split('-').length == 3){
        //         return utils.stringToDate(value);
        //     }
        //     return self.cleanedValue();
        // });

        this.title = ko.observable(params.title);

        this.clean = params.clean;
        this.validators = params.validators;
        this.dependents = params.dependents;
        this.otherFieldChanged = params.otherFieldChanged;

        this.extra = params;

        var self = this;

        this.isAvailable = function() {
            return this.visible();
        }

        this.Value.subscribe(function() {
            for (var index in self.validators) {
                var validate = self.validators[index];
                if ((typeof(validate) == 'function') && (!validate(self.getValue(), self.errorMessage))) {
                    self.error(true);
                    return;
                } else if ((typeof(validate) == 'string') && self.parent.extraMethods.hasOwnProperty(validate)) {
                    if (!self.parent.extraMethods[validate](self.getValue(), self.errorMessage)) {
                        self.error(true);
                        return;
                    }
                } else if ((typeof(validate) == 'string') && TypeValidators.hasOwnProperty(validate)) {
                    if (!TypeValidators[validate](self.getValue(), self.errorMessage)) {
                        self.error(true);
                        return;
                    }
                }
            }
            for (var index in self.dependents) {
                var dependent = self.dependents[index];
                self.parent.getFieldById(dependent).notify(self.Id, self.getValue());
            }
            self.error(false);
        });

        this.notify = function(id, value) {
            if (typeof(self.otherFieldChanged) == 'function') {
                self.otherFieldChanged(self, id, value);
            } else if ((typeof(self.otherFieldChanged) == 'string') && self.parent.extraMethods.hasOwnProperty(self.otherFieldChanged)) {
                self.parent.extraMethods[self.otherFieldChanged](self, id, value);
            }
        }

        this.hasValidData = function(isSilent) {
            for (var index in self.validators) {
                var validate = self.validators[index];
                if ((typeof(validate) == 'function') && (!validate(self.getValue(), self.errorMessage))) {
                    if(!isSilent){
                        self.error(true);
                    }
                    return false;
                } else if ((typeof(validate) == 'string') && self.parent.extraMethods.hasOwnProperty(validate)) {
                    if (!self.parent.extraMethods[validate](self.getValue(), self.errorMessage)) {
                        if(!isSilent){
                            self.error(true);
                        }
                        return false;
                    }
                } else if ((typeof(validate) == 'string') && TypeValidators.hasOwnProperty(validate)) {
                    if (!TypeValidators[validate](self.getValue(), self.errorMessage)) {
                        if(!isSilent){
                            self.error(true);
                        }
                        return false;
                    }
                }
            }
            self.error(false);
            return true;
        }

        this.getValue = function() {
            var fieldValue = self.Value();
            try {
                if (typeof(this.clean) == 'function') {
                    fieldValue = self.clean(self, fieldValue);
                } else if ((typeof(self.clean) == 'string') && self.parent.extraMethods.hasOwnProperty(self.clean)) {
                    fieldValue = self.parent.extraMethods[self.clean](self, fieldValue);
                } else if ((typeof(self.clean) == 'string') && CleanMethods.hasOwnProperty(self.clean)) {
                    fieldValue = CleanMethods[self.clean](self, fieldValue);
                }
            } catch (e) {
                fieldValue = undefined;
            }
            return fieldValue;
        }
    }

    var TextField = function(params) {
        params.type = "text";
        params.tag = "input";
        params.template = 'nonCheckElement';
        this.hideContainer = ko.observable(params.hideContainer);
        var self = this;
        var maxlength = (params.maxlength)? params.maxlength: 100;
        self.maxlength = ko.observable(maxlength);

        self.trim = function(field, value) {
            return value.trim();
        }

        self.isProcessing = ko.observable(false);

        if (!params.clean) {
            params.clean = self.trim;
        }
        Field.call(self, params);
    }

    var NumberField = function(params) {
        params.type = "number";
        params.tag = "input";
        params.template = 'nonCheckElement';
        this.hideContainer = ko.observable(params.hideContainer);
        var maxlength = (params.maxlength)? params.maxlength: 0;
        this.maxlength = ko.observable(maxlength);

        this.trim = function(field, value) {
            return value.trim();
        }
        this.isProcessing = ko.observable(false);
        this.keyDown = function(field,event){
            var value = event.target.value;
            var keyCode = event.keyCode ? event.keyCode : event.which;
            if(value && maxlength >0 && value.length >= maxlength && keyCode>46){
                event.preventDefault();
                return false;
            }
            else return true;
        }

        if (!params.clean) {
            params.clean = this.trim;
        }

        Field.call(this, params);
    }

    var TelField = function(params) {
        params.type = "tel";
        params.tag = "input";
        params.template = 'nonCheckElement';

        this.hideContainer = ko.observable(params.hideContainer || "");
        var maxlength = (params.maxlength)? params.maxlength: 0;
        this.maxlength = ko.observable(maxlength);

        this.trim = function(field, value) {
            return value.trim();
        }
        this.isProcessing = ko.observable(false);
        this.keyDown = function(field,event){
            var value = event.target.value;
            var keyCode = event.keyCode ? event.keyCode : event.which;
            if(value && maxlength >0 && value.length >= maxlength && keyCode>46){
                event.preventDefault();
                return false;
            }
            else return true;
        }

        if (!params.clean) {
            params.clean = this.trim;
        }

        Field.call(this, params);
    }

    var HiddenField = function(params) {
        params.type = "hidden";
        params.tag = "input";
        params.template = 'nonCheckElement';
        this.hideContainer = ko.observable(params.hideContainer);
        this.isProcessing = ko.observable(false);

        this.maxlength = ko.observable(null);

        Field.call(this, params);
    }

    var CheckField = function(params) {
        params.type = "checkbox";
        params.tag = "input";
        params.template = 'checkElement';
        params.validators = [];

        params.value = (params.checked === true)
        Field.call(this, params);
    }

    var Label = function(params) {
        params.tag = "label";
        params.template = 'labelElement';
        this.title = params.title
        Field.call(this, params);
    }

    var SelectField = function(params) {
        params.type = "select";
        params.tag = "select";
        params.template = 'selectElement';
        // params.validators = [];
        var self = this;
        var _optionList = [];

        Field.call(self, params);

        self.placeholder = params.placeholder || '';

        self.OptionsList = ko.observableArray();
        if(params.isObservable){
            _optionList = params.options();
            params.options.subscribe(function(new_value){
                params.validators = [];
                self.OptionsList(new_value);
            });
        }
        else{
            _optionList = params.options
        }  

        self.OptionsList(_optionList);
    }

    var datePickerField = function(params){
        params.type = "datePicker-widget";
        params.tag = "datePicker-widget";
        params.template = "datePicker";

        var self = this;
        self.error=ko.observable();
        self.errorMessage = ko.observable();

        self.returnValue = ko.observable(null).extend({
            notify: 'always'
        });

        self.returnValue.subscribe(function(value){
            if(value.success)
                self.Value(value.data._formattedDate);
            else
                self.Value("");
        });

        Field.call(this, params);
    }

    var SearchSelectField = function(params) {
        params.type = "searchselect";
        params.tag = "searchselect";
        params.template = 'searchSelectElement';

        this.placeholder = params.placeholder || '';
        this.OptionsList = ko.observableArray(params.options);

        this.selectedOption = ko.observable(null).extend({
            notify: 'always'
        });

        Field.call(this, params);

        var self = this;
        this.selectedOption.subscribe(function (value) {
            if (!value) return;
            if (self.Value() != value[params.optionsValue]) {
                self.Value(value[params.optionsValue]);
            }
        });

        this.setSelectedToValue = function(value) {
            for (var index in self.OptionsList()) {
                var element = self.OptionsList()[index];
                if (value == element[self.extra.optionsValue]) {
                    self.selectedOption(element);
                    return;
                }
            }
            if (self.OptionsList().length > 0) {
                self.selectedOption(self.OptionsList()[0]);
            }
        };

        this.Value.subscribe(function (value){
            self.setSelectedToValue(value);
        });
    }

    var RadioBoxField = function(params) {
        params.type = "radioBox-widget";
        params.tag = "radioBox-widget";
        params.template = 'radioBoxElement';

        Field.call(this, params);

        var self = this;
        self.OptionsList = ko.observableArray(params.options);
        self.selectedOption = ko.observable(null).extend({
            notify: 'always'
        });

        self.onchange = params.onchange;

        if(!self.Value() || (self.Value() &&self.Value().length == 0)){
            self.Value(params.options[0].value);
        }


        self.selectedOption.subscribe(function (value) {
            if(value)
                self.Value(value);
            if(self.onchange)
                self.onchange(self.Value());
        });
    }

    var RegistrationNumberField = function(params) {
        var _rtoName = data.quote_parameters["registrationNumber[]"][0];
        var _rtoNum = data.quote_parameters["registrationNumber[]"][1];
        var self = this;

        params.type = "registration-number-widget";
        params.tag = "registration-number-widget";
        params.template = 'registrationNumberElement';

        Field.call(this, params);

        // Auto populate Registration Number based on RTO selected
        if( !this.Value() || 
            (this.Value() && this.Value().length == 0)){
            self.initialValue = _rtoName + '-' + _rtoNum;
        }
        else{ self.initialValue = null; }
        
        self.enableSeries = _rtoName == "DL"?true:false;

        self.selectedValue = ko.observable(null).extend({
            notify: 'always'
        });

        self.selectedValue.subscribe(function (value) {
            if(value)
                self.Value(value);
        });

        self.Value.subscribe(function(newValue){
            // To be removed Oct. 2015.
            // Used to handle UI migration issues.
            var fieldValue = self.Value();
            try {
                if (typeof(this.clean) == 'function') {
                    fieldValue = self.clean(self, fieldValue);
                } else if ((typeof(self.clean) == 'string') && self.parent.extraMethods.hasOwnProperty(self.clean)) {
                    fieldValue = self.parent.extraMethods[self.clean](self, fieldValue);
                } else if ((typeof(self.clean) == 'string') && CleanMethods.hasOwnProperty(self.clean)) {
                    fieldValue = CleanMethods[self.clean](self, fieldValue);
                }
            } catch (e) {
                fieldValue = undefined;
            }

            self.initialValue = fieldValue
        })
    }

    var ButtonField = function(params) {
        this.parent = params.parent;
        this.Id = params.id;
        this.Name = params.name;
        this.Type = "button";
        this.Template = "buttonElement";
        this.title = ko.observable(params.title);
        this.cssclass = params.cssclass;
        this.clickHandler = params.clickHandler;
        this.extra = params.extra;
        this.isProcessing = ko.observable(false);
        this.disabled = ko.observable(false);

        var self = this;

        visibleDefault = true;
        if (typeof(params.visible) == 'boolean') {
            visibleDefault = params.visible;
        }
        this.visible = ko.observable(visibleDefault);

        this.isAvailable = function() {
            return false;
        }

        this.onClick = function() {
            if (typeof(self.clickHandler) == 'function') {
                self.clickHandler(self);
            } else if ((typeof(self.clickHandler) == 'string') && self.parent.extraMethods.hasOwnProperty(self.clickHandler)) {
                self.parent.extraMethods[self.clickHandler](self);
            }
        }
    }

    var Container = function(params) {
        this.parent = params.parent;
        this.Id = params.id;
        this.Desc = params.desc;
        this.Name = ko.observable(params.name);
        this.SummaryName = params.summaryName;
        this.State = ko.observable('collapsed');
        this.Type = "container";
        this.Template = "containerElement";
        this.children = [];
        this.otherFieldChanged = params.otherFieldChanged;
        this.clean = params.clean;
        this.validators = params.validators;
        this.cssclass = ko.observable(params.cssclass);
        this.isDone = ko.observable(false);
        this.showEditButton = ko.observable(false);
        this.summaryObject = ko.observable();


        visibleDefault = true;
        if (typeof(params.visible) == 'boolean') {
            visibleDefault = params.visible;
        }
        this.visible = ko.observable(visibleDefault);

        this.error = ko.observable(false);
        this.errorMessage = ko.observable('Error in block.');

        var self = this;

        this.isAvailable = function() {
            return this.visible();
        }

        this.notify = function(id, value) {
            if (typeof(self.otherFieldChanged) == 'function') {
                self.otherFieldChanged(self, id, value);
            } else if ((typeof(self.otherFieldChanged) == 'string') && self.parent.extraMethods.hasOwnProperty(self.otherFieldChanged)) {
                self.parent.extraMethods[self.otherFieldChanged](self, id, value);
            }
        }

        this.blockValid = function() {
            var value = self.getValue();
            for (var index in self.validators) {
                var validate = self.validators[index];
                if ((typeof(validate) == 'function') && (!validate(value, self.errorMessage))) {
                    self.error(true);
                    return false;
                } else if ((typeof(validate) == 'string') && self.parent.extraMethods.hasOwnProperty(validate)) {
                    if (!self.parent.extraMethods[validate](value, self.errorMessage)) {
                        self.error(true);
                        return;
                    }
                } else if ((typeof(validate) == 'string') && TypeValidators.hasOwnProperty(validate)) {
                    if (!TypeValidators[validate](value, self.errorMessage)) {
                        self.error(true);
                        return false;
                    }
                }
            }
            self.error(false);
            return true;
        }

        this.hasValidData = function(isSilent) {
            var isvalid = true;
            for (var index in self.children) {
                var child = self.children[index];
                if (!child.isAvailable()) continue;
                if (!child.hasValidData(isSilent)) isvalid = false;
            }
            if (isvalid) {
                return this.blockValid();
            }

            // else {
            //     self.error(true);
            //     return false;
            // }
        }

        this.getValue = function() {
            var retval = {};
            var summaryObject = {};
            for (var index in self.children) {
                var child = self.children[index];
                if (!child.isAvailable()) continue;
                var childvalue = child.getValue();
                if (child.Type == "container") {
                    var childvaluestring = "";
                    for (var entry in childvalue) {
                        retval[entry] = childvalue[entry];
                        childvaluestring += " | "+childvalue[entry];
                    }
                    if(child.SummaryName){
                        var summaryValue = childvaluestring
                        summaryObject[""+child.SummaryName] = summaryValue;    
                    }
                     
                } else {
                    retval[child.Name] = childvalue;
                    //summaryObject[child.title()] = childvalue;
                }
            }
            var cleanedretval = retval;
            self.summaryObject(summaryObject);


            if (typeof(self.clean) == 'function') {
                cleanedretval = self.clean(self, retval);
            } else if ((typeof(self.clean) == 'string') && self.parent.extraMethods.hasOwnProperty(self.clean)) {
                cleanedretval = self.parent.extraMethods[self.clean](self, retval);
            }

        
            return cleanedretval;
        }

        this.getSummary = ko.computed(function(){  

            var summary = '<ul class="summary-items">';
            //var selectOptions = self.OptionsList;   

            for(var key in self.summaryObject()){
                var value = self.summaryObject()[key];
                var showkey = true;

                if(key == 'Married'){
                    if(value)
                        value = value.indexOf("Un")>=0?'No':'Yes';
                }
                else if(key == 'Name'){
                    value = value.split(' | ').join(' ')
                }
                else if(key == 'Occupation'){
                    value = occupation_list.filter(function(item){
                        return item.value == value.trim();
                    })[0];
                    value = value.key;
                }
                else if(key == 'Financier'){
                    if(parseInt(value.replace(' | ','')) == 0){
                        showkey = false;
                    }
                    else{
                         value = value.split(' | ').join('').substr(1);
                    }
                }
                else if(key == 'Previous policy'){
                    var pypcode = value.split(' | ')[1];
                    var insurer = pyp_insurer_list.filter(function(item){
                        return item.value == pypcode;
                    })[0];
                    if(insurer){
                        insurer = insurer.key;
                        value = value.replace(pypcode, insurer);
                    }
                }
                else if(key == "Engine and Chassis numbers"){
                    showkey = !window.isFastLaneData;
                }
                else if(key == 'Correspondence Address'){
                    value = value.split(" ").join("<br/>");
                }

                if(showkey){
                    summary += "<li><span class='key'>"+key+":</span> "+value.replace(' | ','').trim()+"</li>"    
                }
                
            }
            return summary+'</ul>';
        })
    }

    var FormClass = function(extraMethods) {
        var self = this;
        self.extraMethods = {};
        self.fields = {};
        self.tree = new Container({
            'id': 'main-form',
            'name': 'main-form'
        });

        if (extraMethods) {
            self.extraMethods = extraMethods;
        }

        this.getFieldById = function(id) {
            return self.fields[id];
        }

        this.putFields = function(fields, parent) {
            for (var index in fields) {
                var field = fields[index];
                var actualfield = self.putField(field);
                if (field.type == "container") {
                    self.putFields(field.children, actualfield);
                }
                parent.children.push(actualfield);
            }
        }

        this.putField = function(params) {
            params.parent = this;
            var field = undefined;
            switch (params.type) {
                case "radiobox":
                    field = new RadioBoxField(params);
                    break;
                case "text":
                    field = new TextField(params);
                    break;
                case "number":
                    field = new NumberField(params);
                    break;
                case "tel":
                    field = new TelField(params);
                    break;
                case "hidden":
                    field = new HiddenField(params);
                    break;
                case "checkbox":
                    field = new CheckField(params);
                    break;
                case "select":
                    field = new SelectField(params);
                    break;
                case "searchselect":
                    field = new SearchSelectField(params);
                    break;
                case "datePicker":
                    field = new datePickerField(params);
                    break;
                case "container":
                    field = new Container(params);
                    break;
                case "button":
                    field = new ButtonField(params);
                    break;
                case "registrationNumber":
                    field = new RegistrationNumberField(params);
                    break;
                case "label":
                    field = new Label(params);
                    break;
            }   

            self.fields[field.Id] = field;
            return field
        }

        this.hasValidData = function() {
            return self.tree.hasValidData();
        }

        this.getValue = function() {
            return self.tree.getValue();
        }
    }

    return {
        TypeValidators: TypeValidators,
        CleanMethods: CleanMethods,
        FormClass: FormClass,
    }
});