define(['knockout','utils','configuration'],function(ko,utils, settings){
    var liveOfflineRTOs = [
        'MH-01',
        'MH-02',
        'MH-03',
        'MH-04',
        'MH-05',
        'MH-06',
        'MH-43',
        'MH-46',
        'MH-47',
        'MH-48',
        'MH-12',
        'MH-14',
        'KA-01',
        'KA-02',
        'KA-03',
        'KA-04',
        'KA-05',
        'KA-10',
        'KA-41',
        'KA-43',
        'KA-50',
        'KA-51',
        'KA-52',
        'KA-53',
        'KA-57',
        'AP-09',
        'AP-10',
        'AP-11',
        'AP-12',
        'AP-13',
        'DL-1',
        'DL-2',
        'DL-3',
        'DL-4',
        'DL-5',
        'DL-6',
        'DL-7',
        'DL-8',
        'DL-9',
        'DL-10',
        'DL-11',
        'DL-12',
        'DL-13',
        'DL-16',
        'DL-17',
        'DL-18',
        'HR-38',
        'HR-51',
        'UP-14',
        'UP-16',
        'UP-18',
    ];

    var liveOfflineInsurers = [
        'l-t',
        'bharti-axa'
    ];

    var validateExpiredFlow = function(vehicleType, insurer, rto, mobileNo, policyDate){
        if(policyDate){
            var today = new Date();
            today.setHours(0,0,0,0);
            var _diff = utils.getDaysBetweenDates(utils.stringToDate(policyDate, 'dd-mm-yyyy', '-'), today);
            
            return (vehicleType == 'fourwheeler' && 
                liveOfflineInsurers.indexOf(insurer)>=0 &&
                liveOfflineRTOs.indexOf(rto)>=0 &&
                mobileNo && mobileNo.toString().length>0 &&
                _diff >0);
        }
        else{
            return false;
        }
    }

    var sendInspectionLeadtoLMS= function (plan, quote, url){
        if(quote.pastPolicyExpiryDate()){
            if (validateExpiredFlow(
                settings.getConfiguration().vehicle_type, 
                plan.insurerSlug, 
                quote.rto_info().id, 
                quote.mobile(), 
                quote.pastPolicyExpiryDate())
                ) {

                var extra = {
                    event:"inspection_created_frontend",
                    campaign: 'motor-offlinecases',
                    lms_campaign: 'motor-offlinecases',
                    quote_url: url
                }

                var data = {
                    vehicle: quote.vehicle().name,
                    variant: quote.variant().name,
                    quote: JSON.parse(ko.toJSON(quote))
                };
                var today = new Date();
                today.setDate(today.getDate() + 7);
                document.cookie = "lms_offline=true; expires=" + today + ";path=/";

                utils.lmsUtils.sendGeneric(data, 'call-form', quote.mobile(), extra, function(){});
            }    
        }
    };

    return {
        sendInspectionLeadtoLMS:sendInspectionLeadtoLMS,
        validateExpiredFlow:validateExpiredFlow,
        liveOfflineList: {
            insurers: liveOfflineInsurers,
            rtos: liveOfflineRTOs
        }
    }
}); 