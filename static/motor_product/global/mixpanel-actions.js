define([], function() {


    return {
        CarProductPageSubmittedNewPolicy:'CarProductPageSubmittedNewPolicy',
        CarProductPageSubmittedRenewPolicy:'CarProductPageSubmittedRenewPolicy',

        ExpiredCarLPViewed:'ExpiredCarLPViewed',
        ExpiredCarSubmitted:'ExpiredCarSubmitted',

        CarResultsPageBuy:'CarResultsPageBuy',

        CarPaymentGatewayNewCar:'CarPaymentGatewayNewCar',
        CarPaymentGatewayRenewCar:'CarPaymentGatewayRenewCar',

		BikeResultsReached:'BikeResultsReached',
		BikeResultsBuy:'BikeResultsBuy',
		BikePaymentGateway:'BikePaymentGateway',

        BikeProductPageSubmittedNewPolicy:'CarProductPageSubmittedNewPolicy',
        BikeProductPageSubmittedRenewPolicy:'CarProductPageSubmittedRenewPolicy',

        ExpiredBikeLPViewed:'ExpiredCarLPViewed',
        ExpiredBikeSubmitted:'ExpiredCarSubmitted',

        BikeResultsPageBuy:'BikeResultsPageBuy',

        BikePaymentGatewayNewCar:'BikePaymentGatewayNewCar',
        BikePaymentGatewayRenewCar:'BikePaymentGatewayRenewCar',

    };



});
