define(['knockout'], function(ko) {

    var SERVICE_TAX_RATE = 0.14;

    var Vehicle = function(data) {
        var self = this;
        self.id = ko.observable(data.id);
        self.make = ko.observable(data.make);
        self.model = ko.observable(data.model);
        self.variant = ko.observable(data.variant);
        self.type = ko.observable(data.type);
    };

    var Plan = function(data) {
        var self = this;
        self.calculatedAtIDV = data.calculatedAtIDV;
        self.serviceTaxRate = data.serviceTaxRate;
        if (!self.serviceTaxRate) {
            self.serviceTaxRate = SERVICE_TAX_RATE;
        }
        self.insurerId = data.insurerId;
        self.insurerSlug = data.insurerSlug;
        self.insurerName = data.insurerName;
        self.maxIDV = data.maxIDV;
        self.minIDV = data.minIDV;
        self.bypassInspection = data.bypassInspection;
        self.serviceTax = data.premiumBreakup.serviceTax;
        self.addonsApplicable = data.addonsApplicable;
        self.voluntaryDeductible = data.premiumBreakup.voluntaryDeductible;
        self.dependentCovers = data.premiumBreakup.dependentCovers;
        self.selectedAddonsAvailable = 0;
        self.paPassenger = ko.observable(0);
        self.paPassengerAmounts = data.premiumBreakup.paPassengerAmounts;
        self.rank = ko.observable(0);

        self.addonCovers = ko.observableArray(data.premiumBreakup.addonCovers.map(function(item) {
            return new Cover(item, self.serviceTaxRate);
        }));

        self.selectedAddonCovers = ko.computed(function() {
            return ko.utils.arrayFilter(self.addonCovers(), function(item) {
                return item.is_selected() == 1;
            });
        });


        self.selectedAddonCoversIds = ko.computed(function() {
            return self.selectedAddonCovers().map(function(item) {
                return item.id;
            });
        })

        // Remove any duplicate add-ons in basic covers
        // odExtra was coming twice for Bajaj.
        var _basicAddOn = {};
        data.premiumBreakup.basicCovers.forEach(function(item) {
            _basicAddOn[item.id] = new Cover(item, self.serviceTaxRate);
        });

        self.basicCovers = ko.observableArray(Object.keys(_basicAddOn).map(function(k){return _basicAddOn[k]}));

        self.discounts = ko.observableArray(data.premiumBreakup.discounts.map(function(item) {
            return new Cover(item, self.serviceTaxRate);
        }));

        self.special_discounts = ko.observableArray(data.premiumBreakup.specialDiscounts.filter(function(item){
            return item.id !== "corporateEmployeeDiscount"
        }).map(function(item) {
            return new Cover(item, self.serviceTaxRate);
        }));

        self.employeeDiscount = ko.computed(function(){
            var emp_dis = data.premiumBreakup.specialDiscounts.filter(function(item){
                return item.id === "corporateEmployeeDiscount"
            }).map(function(item) {
                return new Cover(item, self.serviceTaxRate);
            })[0];
            return emp_dis || {};
        });

         self.get_addon_from_id = function(id) {

            var covers = self.addonCovers().concat(self.basicCovers());

            var matched_addons = covers.filter(function(item) {
                return item.id == id;
            });

            if(matched_addons.length){

                var matched_addon = matched_addons[0];
                var sum = parseInt(matched_addons[0].premium + matched_addons[0].service_tax);


                // if(self.dependentCovers && self.dependentCovers[matched_addon.id] && self.dependentCovers[matched_addon.id].length > 0){
                //     var dependentCovers = self.dependentCovers[matched_addon.id];
                //     for (var i = 0; i < dependentCovers.length; i++) {
                //         var dependent = covers.filter(function(item){
                //             return item.id == dependentCovers[i];
                //         });

                //         if(dependent.length){
                //             sum += parseInt(dependent[0].premium + dependent[0].service_tax)
                //         }
                //     };

                // }

                return "+ " + sum;

            }else {
                return "NA";
            }
        };

        self.get_addon_from_id_raw = function(id) {

            var covers = self.addonCovers().concat(self.basicCovers());

            var matched_addons = covers.filter(function(item) {
                return item.id == id;
            });

            if (matched_addons.length) {
                return parseInt(matched_addons[0].premium + matched_addons[0].service_tax);
            } else {
                return 0;
            }
        };

        self.get_cover_from_id = function(id) {

            var matches = self.basicCovers().filter(function(item) {
                return item.id == id;
            });

            if (matches.length > 0) {
                return matches[0];
            } else {
                return "None";
            }


        };






        self.getAddonPremium = function(addon) {

            var planAddon = ko.utils.arrayFilter(self.addonCovers(), function(item) {
                return item.id == addon.id;
            })[0];

            if (planAddon) {
                return planAddon.premium;
            } else {
                return "Not Offered"
            }
        };

        self.totalServiceTax = ko.computed(function() {
            var st = 0;

            for (var i = 0; i < self.addonCovers().length; i++) {

                if (self.addonCovers()[i].is_selected() == "1") {
                    st = st + parseFloat(self.addonCovers()[i].service_tax);
                }
            }

            for (var i = 0; i < self.basicCovers().length; i++) {

                if (self.basicCovers()[i].is_selected() == "1") {
                    st = st + parseFloat(self.basicCovers()[i].service_tax);
                }
            }

            for (var i = 0; i < self.discounts().length; i++) {
                if (self.discounts()[i].is_selected() == "1") {
                    st = st + parseFloat(self.discounts()[i].service_tax);
                }
            }

            for (var i = 0; i < self.special_discounts().length; i++) {
                if (parseInt(self.special_discounts()[i].is_selected()) == 1) {
                    st = st + parseFloat(self.special_discounts()[i].service_tax);
                }
            }

            return Math.round(st);
        });

        self.calculatedPremium = ko.computed(function() {

            var cp = 0;

            for (var i = 0; i < self.addonCovers().length; i++) {

                if (self.addonCovers()[i].is_selected() == "1") {
                    cp = cp + parseFloat(self.addonCovers()[i].premium);
                }
            }

            for (var i = 0; i < self.basicCovers().length; i++) {

                if (self.basicCovers()[i].is_selected() == "1") {
                    cp = cp + parseFloat(self.basicCovers()[i].premium);
                }
            }

            for (var i = 0; i < self.discounts().length; i++) {

                if (self.discounts()[i].is_selected() == "1") {
                    cp = cp + parseFloat(self.discounts()[i].premium);
                }
            }

            for (var i = 0; i < self.special_discounts().length; i++) {
                if (parseInt(self.special_discounts()[i].is_selected()) == 1) {
                    cp = cp + parseFloat(self.special_discounts()[i].premium);
                }
            }

            cp += parseFloat(self.totalServiceTax());

            return Math.round(cp);

        });

        self.calculatedBasicCover = ko.computed(function() {
            return get_aggregate_premium_from_covers(self.basicCovers());
        });

        self.calculatedEmployeeDiscount = ko.computed(function(){
            return get_aggregate_premium_from_covers(self.employeeDiscount(), true);
        })


        self.calculatedDiscounts = ko.computed(function() {
            return get_aggregate_premium_from_covers(self.discounts());
        });

        self.calculatedSpecialDiscounts = ko.computed(function() {
            return get_aggregate_premium_from_covers(self.special_discounts());
        });

        self.vd_discount = ko.computed(function(){
            var vd_discount = self.special_discounts().filter(function(item){
                return item.id == 'voluntaryDeductible'
            });
            var c =0, st=0;
            if(vd_discount.length){
                c = c + parseFloat(vd_discount[0].premium);
                st = st + parseFloat(vd_discount[0].service_tax);

                return Math.round(c + st);
            }
            else{
                return null;
            }
        });

        self.special_discount = ko.computed(function(){
            if(self.vd_discount() && self.calculatedSpecialDiscounts() !== 0){
                return self.calculatedSpecialDiscounts() - self.vd_discount
            }
            else{
                if(self.calculatedSpecialDiscounts() !== 0){
                    return self.calculatedSpecialDiscounts()
                }
                else{
                    return null
                }
            }

        });

        self.calculatedAddonCovers = ko.computed(function() {
            return get_aggregate_premium_from_covers(self.addonCovers());
        });

        self.calculatedPAPassengerPremium = ko.computed(function() {
            var pap = 0,
                st = 0;

            if (self.paPassengerAmounts) {
                pap = self.paPassengerAmounts[self.paPassenger()] || 0;
            }
            st = pap * self.serviceTaxRate;
            return Math.round(pap + st);
        });


        self.calculatedTotalPremium = ko.computed(function() {
            return  self.calculatedAddonCovers() +
                    self.calculatedSpecialDiscounts() +
                    self.calculatedDiscounts() +
                    self.calculatedBasicCover() +
                    self.calculatedPAPassengerPremium() +
                    (self.calculatedEmployeeDiscount() || 0);
        });


    };

    var get_aggregate_premium_from_covers = function(covers, isEmployeeDiscount) {
        var c = 0,
            st = 0;

        if(Array.isArray(covers)){
            for (var i = 0; i < covers.length; i++) {
                if  (covers[i].is_selected() == "1") {
                    c = c + parseFloat(covers[i].premium);
                    st = st + parseFloat(covers[i].service_tax);
                }
            }
        }
        else if(typeof(covers) === "object"){
            c = covers.premium;
            st = covers.service_tax;
        }

        return Math.round(c + st);
    };

    var Cover = function(data, serviceTaxRate) {
        var self = this;
        self.id = data.id;
        self.premium = data.premium;
        self.default = data.default;
        self.is_selected = ko.observable(data.default);
        self.name = data.name;
        self.add_service_tax = true;
        if (data.add_service_tax != undefined) {
            self.add_service_tax = data.add_service_tax;
        }
        self.service_tax = 0;
        if (self.add_service_tax) {
            self.service_tax = self.premium * serviceTaxRate;
        }
    };


    return {
        Vehicle: Vehicle,
        Plan: Plan,
        Cover: Cover,
    };



});
