define('', function(){
	var rtoStateMapping = {
		'AN':'Andaman & Nicobar Islands',
		'AP':'Andhra Pradesh',
		'AR':'Arunachal Pradesh',
		'AS':'Assam',
		'BR':'Bihar',
		'CG':'Chhattisgarh',
		'CH':'Chandigarh',
		'DD':'Daman and Diu',
		'DL':'Delhi',
		'DN':'Dadra & Nagar Haveli',
		'GA':'Goa',
		'GJ':'Gujarat',
		'HP':'Himachal Pradesh',
		'HR':'Haryana',
		'JH':'Jharkhand',
		'JK':'Jammu & Kashmir',
		'KA':'Karnataka',
		'KL':'Kerala',
		'LD':'Lakshadweep',
		'MH':'Maharashtra',
		'ML':'Meghalaya',
		'MN':'Manipur',
		'MP':'Madhya Pradesh',
		'MZ':'Mizoram',
		'NL':'Nagaland',
		'OD':'Odisha',
		'PB':'Punjab',
		'PY':'Pondicherry',
		'RJ':'Rajasthan',
		'RJ':'Rajasthan',
		'TN':'Tamil Nadu',
		'TS':'Telangana',
		'TR':'Tripura',
		'UK':'Uttarakhand',
		'UP':'Uttar Pradesh',
		'WB':'West Bengal',
	}

	var getStateName = function(rto){
		return rtoStateMapping[rto];
	}

	return {getStateName: getStateName};
})
