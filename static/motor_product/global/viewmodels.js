
define(['knockout', 'models', 'utils'], function(ko, models, utils) {

    var today = new Date();
    var tomm = new Date();
    tomm.setDate(tomm.getDate() + 1),
    tomm_dd = tomm.getDate(),
    tomm_mm = tomm.getMonth(),
    tomm_yyyy = tomm.getFullYear();

    // Reset Timestamp.
    today.setHours(0,0,0,0);
    tomm.setHours(0,0,0,0);

    var dd = today.getDate();
    var mm = today.getMonth();

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (tomm_dd < 10) {
        tomm_dd = '0' + tomm_dd;
    }

    var days = [];
    for (var i = 1; i < 32; i++) {
        if (i < 10) {
            i = '0' + i;
        } else {
            i += '';
        }
        days.push(i);
    }

    var dob_years = function(startYear) {
        var currentYear = new Date().getFullYear(), years = [];
        startYear = startYear || 1950;

        while ( startYear <= currentYear ) {
            years.push(startYear + '');
            startYear++
        }
        return years;
    }

    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var years = [];
    var man_years = ko.observableArray(["2015", "2014"]);
    var expiryYears = ko.observableArray([]);
    var expiryMonths = ko.observableArray(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
    var twExpiryYears = ["2015", "2014", "2013", "2012"];
    var newCarYears = ["2015", "2014"];
    var policy_date_list = [];

    function populateManYears(){
        var _minCapDate = new Date();
        var _maxCapDate = new Date();
        _minCapDate.setFullYear(_minCapDate.getFullYear() -2);
        _minCapDate.setMonth(_minCapDate.getMonth() + 1);
        man_years([]);
        for(var i=_maxCapDate.getFullYear(); i>=_minCapDate.getFullYear(); i--){
            man_years.push(i);
        }
    }

    function populatePolicyDates(){
        var _today = new Date();
        for (var i=1; i<=7; i++){
            _today.setDate(_today.getDate() + 1);
            policy_date_list.push((_today.getDate()<10?"0":"") + _today.getDate()+ " "+ utils.monthNumberToName(_today.getMonth()));
        }
    }

    function populateRegistrationYear(){
        var yearCap = 15;
        var year = today.getFullYear();
        // Populate past 15 years - Previous year
        for (var i= 1; i<yearCap; i++){
            years.push(year - i);
        }

        // If Today is greater than T-60, add current year
        var  _31Dec = new Date(today.getFullYear(), 11, 31);
        var _diff = utils.getDaysBetweenDates(_31Dec, today);
        if(Math.abs(_diff)<=60)
            years.unshift(today.getFullYear());
    }

    //var populateExpiredYears = ko.computed(function (isIndexPage){
    var populateExpiredYears = function (policyExpired,isIndexPage){
        var _minCapDate = new Date(), _maxCapDate = new Date();
        // var policyExpired = true;
        // if(self.pastPolicyExpiryDate){
        //     policyExpired = (+utils.stringToDate(quotex.pastPolicyExpiryDate, 'dd-mm-yyyy', '-') < +today);
        // }

        // Reset values before populating
        expiryYears([]);
        expiryMonths([]);

        // For Expired flow, populate from T-90 to T+60 only
        _minCapDate.setDate(_minCapDate.getDate() - 89);
        _maxCapDate.setDate(_maxCapDate.getDate() + 60);

        _minCapDate.setHours(0,0,0,0);
        _maxCapDate.setHours(0,0,0,0);

        if(isIndexPage){
            _maxCapDate = new Date();
        }

        // Populate Years
        if(_minCapDate.getFullYear() != _maxCapDate.getFullYear()){
            expiryYears.push(_maxCapDate.getFullYear());
            expiryYears.push(_minCapDate.getFullYear());
        }
        else{
            expiryYears.push(_maxCapDate.getFullYear());
        }

        // Populate Months
        if(_minCapDate.getMonth() > _maxCapDate.getMonth()){
            for(var i = _minCapDate.getMonth(); i<=11; i++){
                expiryMonths.push(months[i]);
            }
            for(var i = 0; i<=_maxCapDate.getMonth(); i++){
                expiryMonths.push(months[i]);
            }
        }
        else{
            for (var i = _minCapDate.getMonth(); i<=_maxCapDate.getMonth(); i++){
                expiryMonths.push(months[i]);
            }
        }
    };

    populateRegistrationYear();
    populateExpiredYears();
    populatePolicyDates();
    populateManYears();

    mm = months[mm];
    tomm_mm = months[tomm_mm];

    var monthToNumber = function(month) {
        var n = months.indexOf(month) + 1;
        if (n < 10) {
            n = '0' + n;
        } else {
            n += '';
        }
        return n;
    };


    var current = new Date(),
        current_year = current.getFullYear(),
        current_month = current.getMonth(),
        current_month_verbose = months[current_month];


    var man_date = new Date();
    man_date.setMonth(man_date.getMonth() - 2);

    var man_month = man_date.getMonth();
    var man_month_verbose = months[man_month];

    var quote = function() {
        var self = this;
        self.vehicle = ko.observable().extend({
            store: {
                key: 'vehicle'
            }
        });

        self.fuel_type = ko.observable().extend({
            store: {
                key: 'fuel_type'
            }
        });

        self.variant = ko.observable().extend({
            store: {
                key: 'vehicleVariant'
            }
        });

        // If the vehicle is new
        self.isNewVehicle = ko.observable(null).extend({
            store: {
                key: 'isNewVehicle'
            }
        });

        // RTO Info
        self.rto_info = ko.observable().extend({
            store: {
                key: 'rto_info'
            }
        });

        self.isUsedVehicle = ko.observable(null).extend({
            store: {
                key: 'isUsedVehicle'
            }
        });

        // Registration date of the vehicle: We take registration year and month from the user and convert it to registration date by considering the first day of the month entered.
        self.reg_date = ko.observable('01').extend({
            store: {
                key: 'reg_date'
            }
        });

        self.reg_month = ko.observable().extend({
            store: {
                key: 'reg_month'
            }
        });

        self.reg_year = ko.observable().extend({
            store: {
                key: 'reg_year'
            }
        });

        self.man_date = ko.observable('01').extend({
            store: {
                key: 'man_date'
            }
        });

        self.man_month = ko.observable(man_month_verbose).extend({
            store: {
                key: 'man_month'
            }
        });
        self.man_year = ko.observable(current_year).extend({
            store: {
                key: 'man_year'
            }
        });

        self.policy_start_date = ko.observable(dd).extend({
            store: {
                key: 'policy_start_date'
            }
        });

        self.policy_start_month = ko.observable(current_month_verbose).extend({
            store: {
                key: 'policy_start_month'
            }
        });

        self.policy_start_year = ko.observable(yyyy).extend({
            store: {
                key: 'policy_start_year'
            }
        });


        self.expiry_date = ko.observable().extend({
            store: {
                key: 'expiry_date'
            }
        });

        self.expiry_month = ko.observable().extend({
            store: {
                key: 'expiry_month'
            }
        });

        self.expiry_year = ko.observable().extend({
            store: {
                key: 'expiry_year'
            }
        });

        self.expiry_error = ko.observable(false);
        self.expiry_errorMessage = ko.observable();


        self.idv = ko.observable(0).extend({
            store: {
                key: 'idv'
            }
        });

        self.previousNCB = ko.observable(null).extend({
            store: {
                key: 'previousNCB'
            }
        });

        self.newNCB = ko.observable(null).extend({
            store: {
                key: 'newNCB'
            }
        });

         self.isNCBCertificate = ko.observable(0).extend({
            store: {
                key: 'isNCBCertificate'
            }
        });

        self.isClaimedLastYear = ko.observable(0).extend({
            store: {
                key: 'isClaimedLastYear'
            }
        });

        self.isCNGFitted = ko.observable(0).extend({
            store: {
                key: 'isCNGFitted'
            }
        });

        self.cngKitValue = ko.observable(0).extend({
            store: {
                key: 'cngKitValue'
            }
        });

        self.voluntaryDeductible = ko.observable(0).extend({
            store: {
                key: 'voluntaryDeductible'
            }
        });

        self.idvElectrical = ko.observable(0).extend({
            store: {
                key: 'idvElectrical'
            }
        });

        self.idvNonElectrical = ko.observable(0).extend({
            store: {
                key: 'idvNonElectrical'
            }
        });

        self.addon_isDepreciationWaiver = ko.observable(0).extend({
            store: {
                key: 'addon_isDepreciationWaiver'
            }
        });

        self.addon_isKeyReplacement = ko.observable(0).extend({
            store: {
                key: 'addon_isKeyReplacement'
            }
        });

        self.addon_isInvoiceCover = ko.observable(0).extend({
            store: {
                key: 'addon_isInvoiceCover'
            }
        });

        self.addon_isNcbProtection = ko.observable(0).extend({
            store: {
                key: 'addon_isNcbProtection'
            }
        });

        self.addon_is247RoadsideAssistance = ko.observable(0).extend({
            store: {
                key: 'addon_is247RoadsideAssistance'
            }
        });

        self.addon_isEngineProtector = ko.observable(0).extend({
            store: {
                key: 'addon_isEngineProtector'
            }
        });

        self.addon_isDriveThroughProtected = ko.observable(0).extend({
            store: {
                key: 'addon_isDriveThroughProtected'
            }
        });

        self.extra_paPassenger = ko.observable(0).extend({
            store: {
                key: 'extra_paPassenger'
            }
        });

        self.extra_isLegalLiability = ko.observable(0).extend({
            store: {
                key: 'extra_isLegalLiability'
            }
        });

        self.extra_user_occupation = ko.observable().extend({
            store: {
                key: 'user_occupation'
            }
        });

        self.extra_isAntiTheftFitted = ko.observable(0).extend({
            store: {
                key: 'extra_isAntiTheftFitted'
            }
        });

        self.extra_isMemberOfAutoAssociation = ko.observable(0).extend({
            store: {
                key: 'extra_isMemberOfAutoAssociation'
            }
        });

        self.extra_isTPPDDiscount = ko.observable(0).extend({
            store: {
                key: 'extra_isTPPDDiscount'
            }
        });

        self.extra_user_dob_date = ko.observable().extend({
            store: {
                key: 'user_dob_date'
            }
        });
        self.extra_user_dob_month = ko.observable().extend({
            store: {
                key: 'user_dob_month'
            }
        });
        self.extra_user_dob_year = ko.observable().extend({
            store: {
                key: 'user_dob_year'
            }
        });

        self.quoteId = ko.observable(null).extend({
            store: {
                key: 'quoteId'
            }
        });

        self.discountCode = ko.observable(null).extend({
            store: {
                key: 'discountCode',
                type: 'cookie',
                keytype: 'raw'
            }
        });

        self.expirySlot = ko.observable(7).extend({
            store: {
                key: 'expirySlot'
            }
        });

        self.hasAddedDiscounts = ko.observable(false).extend({
            store: {
                key: 'hasAddedDiscounts'
            }
        });

        self.hasOptimizedQuotes = ko.observable(false).extend({
            store: {
                key: 'hasOptimizedQuotes'
            }
        });

        self.hasSeenOptimizePopup = ko.observable(false).extend({
            store: {
                key: 'hasSeenOptimizePopup'
            }
        });

        self.mobile = ko.observable().extend({
            store: {
                key: 'mobileNo',
                type: 'cookie',
                keytype: 'raw'
            }
        });

        self.email = ko.observable().extend({
            store: {
                key: 'email',
                type: 'cookie',
                keytype: 'raw'
            }
        });

        self.previous_policy_insurer_name = ko.observable(false).extend({
            store: {
                key: 'previous_policy_insurer_name'
            }
        });

        // Computed function
        self.pastPolicyExpiryDate = ko.computed(function() {
            // get the index of the entered month in the months array and append a 0 if index < 10
            var expiry_month = ('0' + (months.indexOf(self.expiry_month()) + 1)).slice(-2),
                expiry_year = self.expiry_year(),
                expiry_date = self.expiry_date();

            if (expiry_month == '00') {
                expiry_month = '01';
            }

            if (!expiry_date) {
                expiry_date = '01';
            }

            return expiry_date + '-' + expiry_month + '-' + expiry_year;

        });

        self.isExpired = ko.computed(function(){
            var policyExpiryDate = utils.stringToDate(self.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-');
            var _today = new Date();

            _today.setHours(0,0,0,0);
            policyExpiryDate.setHours(0,0,0,0);

            return (+policyExpiryDate < +_today);
        });

        self.newPolicyStartDate = ko.computed(function() {
            var policy_start_date, policy_start_month, policy_start_year;

            // For new vehicle, set selected date
            if(self.isNewVehicle()){
                policy_start_date = self.policy_start_date();
                policy_start_month = utils.monthNametoNumber(self.policy_start_month()) + 1;
                policy_start_year = self.policy_start_year();
            }
            else{
                var _today = new Date();

                // Default values:
                // Expired Flow: T + 1
                // Renew Flow: PastPolicyExpiryDate + 1
                if(!self.isExpired()){
                    _today = utils.stringToDate(self.pastPolicyExpiryDate(), 'dd-mm-yyyy', '-');
                }

                _today.setDate(_today.getDate() + 1);

                policy_start_date = _today.getDate();
                policy_start_month = _today.getMonth() + 1;
                policy_start_year = _today.getFullYear();
            }

            return utils.twoDigitValue(policy_start_date) + '-' + utils.twoDigitValue(policy_start_month) + '-' + policy_start_year;
        });

        self.registrationDate = ko.computed(function() {


            if (self.isNewVehicle()) {
                return self.newPolicyStartDate();
            } else {
                // get the index of the entered month in the months array and append a 0 if index < 10
                var reg_month = ('0' + (months.indexOf(self.reg_month()) + 1)).slice(-2),
                    reg_year = self.reg_year(),
                    reg_date = self.reg_date();

                if (reg_month == '00') {
                    reg_month = '01';
                }
                if (!reg_date) {
                    reg_date = '01';
                }
                return reg_date + '-' + reg_month + '-' + reg_year;
            }
        });

        self.manufacturingDate = ko.computed(function() {
            if (self.isNewVehicle()) {
                var man_month = ('0' + (months.indexOf(self.man_month()) + 1)).slice(-2),
                    man_year = self.man_year(),
                    man_date = '01';
                if (man_month == '00') {
                    man_month = '01';
                }
                return man_date + '-' + man_month + '-' + man_year;
            } else {
                return self.registrationDate();
            }
        });

        self.extra_user_dob = ko.computed(function() {
            if(self.extra_user_dob_month() && self.extra_user_dob_year() && self.extra_user_dob_date()){

                var dob_month,
                    dob_year = self.extra_user_dob_year(),
                    dob_date = self.extra_user_dob_date();

                if(isNaN(self.extra_user_dob_month())){
                    dob_month = ('0' + (months.indexOf(self.extra_user_dob_month()) + 1)).slice(-2)
                }
                else{
                    dob_month = ('0' + self.extra_user_dob_month()).slice(-2)
                }

                var dob_year = self.extra_user_dob_year(),
                    dob_date = self.extra_user_dob_date();

                return dob_date + '-' + dob_month + '-' + dob_year;
            }
            else{
                return null;
            }
        });



        self.csrfmiddlewaretoken = CSRF_TOKEN;

        self.resetExpiryDate = function() {
            var today = new Date();
            today.setDate(today.getDate() + self.expirySlot());
            var expiry_datestr = utils.dateToString(today).split('-');
            self.expiry_date(expiry_datestr[0]);
            self.expiry_month(months[parseInt(expiry_datestr[1]) - 1]);
            self.expiry_year(expiry_datestr[2]);
        };

        self.fastlane_success = ko.observable(false).extend({
            store: {
                key: 'fastlane_success'
            }
        });

        self.rds_id = ko.observable(false).extend({
            store: {
                key: 'rds_id'
            }
        });

        self.ncb_unknown = ko.observable(false).extend({
            store: {
                key: 'ncb_unknown'
            }
        });

        self.fastlane_model_changed =  ko.observable(false).extend({
            store: {
                key: 'fastlane_model_changed'
            }
        });

        self.fastlane_variant_changed =  ko.observable(false).extend({
            store: {
                key: 'fastlane_variant_changed'
            }
        });

        self.fastlane_fueltype_changed =  ko.observable(false).extend({
            store: {
                key: 'fastlane_fueltype_changed'
            }
        });

        function cleanFastLane_success(){
            if (self.fastlane_success() !== undefined){
                if(!isNaN(self.fastlane_success())){
                    self.fastlane_success()?self.fastlane_success(true):self.fastlane_success(false);
                }
            }
        };

        function cleanIsNewVehicle(){
            if(self.isNewVehicle() !== undefined){
                if(isNaN(self.isNewVehicle())){
                    var newVal = self.isNewVehicle();

                    // To handle cases like "true" || "false"
                    if(typeof(self.isNewVehicle()) === "string"){
                        newVal = self.isNewVehicle() == "true"?1:0;
                    }
                    // To handle cases where value is true or false
                    else if(typeof(self.isNewVehicle()) === "boolean"){
                        newVal = self.isNewVehicle()?1:0;
                    }

                    self.isNewVehicle(newVal);
                }
            }
        }

        cleanIsNewVehicle();
        cleanFastLane_success();

        self.reg_number_bifurcated = ko.observable().extend({
            store: {
                key: 'reg_number'
            }
        });

        // Send to server as primary key.
        // Cannot have alphabets in RTO section
        self.reg_number = ko.computed(function(){
            if(self.reg_number_bifurcated() &&
                self.reg_number_bifurcated().length>0){

                var reg_array = self.reg_number_bifurcated().slice(0);
                var alphaRegex = /[a-z]/gi;
                var numRegex = /^\d$/;

                // Replace any character in RTO
                if(!numRegex.test(reg_array[1])){
                    reg_array[1] = reg_array[1].replace(alphaRegex, '');
                }

                if(Array.isArray(reg_array))
                    return reg_array.join('-');
                else
                    return "";
            }
        });

        self.formatted_reg_number = ko.observable().extend({
            store: {
                key: 'formatted_reg_number'
            }
        });

        function computeFormattedRegNumber(){
            if(self.reg_number_bifurcated()){
                self.formatted_reg_number(self.reg_number_bifurcated().join('-'));
            }

            // For cases where formatted Reg number is received as a plain string without hyphens
            if (self.formatted_reg_number() && 
                self.formatted_reg_number().length>0 &&
                self.formatted_reg_number().indexOf("-")<0){

                var reg_no = self.formatted_reg_number();
                var state = reg_no.substring(0,2);
                var numArr = reg_no.substring(2).split(/[a-z]/i).filter(function(a){if(a) return a });
                var rto = reg_no.substring(2).split(/\d/).filter(function(a){if(a) return a })[0] || "";

                var district = numArr[0] || 0;
                var series = numArr[1] || "";
                self.formatted_reg_number(state + "-"+ district + "-" + rto  + "-" + series);
            }
            if(self.formatted_reg_number())
                self.formatted_reg_number(self.formatted_reg_number().toUpperCase());
        }
        computeFormattedRegNumber();
    };

    quote.prototype.toJSON = function() {
        var copy = ko.toJS(this);
        var regNoArray = ['0', '0', '0', '0'];
        var now = new Date();

        copy.vehicleId = copy.variant.id;
        regNoArray[0] = copy.rto_info.id.split('-')[0];
        regNoArray[1] = copy.rto_info.id.split('-')[1];
        copy.registrationNumber = regNoArray;

        if (copy.isNewVehicle) {
            copy.isNewVehicle = 1;
        } else {
            copy.isNewVehicle = 0;
        }

        if (copy.isUsedVehicle){
            copy.isUsedVehicle = 1
            copy.isNewVehicle = 0
        }


        if (copy.isClaimedLastYear == 1) {
            copy.previousNCB = 0;
        }

        delete copy.fuel_type;
        delete copy.man_date;
        delete copy.man_month;
        delete copy.man_year;
        delete copy.expiry_date;
        delete copy.expiry_month;
        delete copy.expiry_year;
        delete copy.reg_date;
        delete copy.reg_month;
        delete copy.reg_year;
        delete copy.extra_user_dob_date;
        delete copy.extra_user_dob_month;
        delete copy.extra_user_dob_year;
        delete copy.policy_start_date;
        delete copy.policy_start_month;
        delete copy.policy_start_year;
        delete copy.rto_info;
        delete copy.vehicle;
        delete copy.variant;
        delete copy.hasSeenOptimizePopup;
        delete copy.hasOptimizedQuotes;
        delete copy.hasAddedDiscounts;

        // if(copy.isUsedVehicle == 1){
        //     var today = new Date()
        //     today.setDate(today.getDate() + 2);
        //     copy.newPolicyStartDate = utils.dateToString(today);
        // }

        return copy;
    };

    return {
        quote: new quote(),
        days: days,
        months: months,
        years: years,
        man_years: man_years,
        expiryYears: expiryYears,
        expiryMonths: expiryMonths,
        twExpiryYears:twExpiryYears,
        newCarYears: newCarYears,
        dob_years:dob_years,
        policy_date_list:policy_date_list,
        populateExpiredYears:populateExpiredYears
    };
});
