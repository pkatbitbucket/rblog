var gulp = require("gulp");
var gutil = require("gulp-util");
var webpack = require('webpack');
var gulpWebpack = require("gulp-webpack");
var webpackConfig = require("./webpack.config.js");
var WebpackDevServer = require("webpack-dev-server");
var stripDebug = require('gulp-strip-debug');
var named = require('vinyl-named');
var del = require('del');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');

// The development server (the recommended option for development)
gulp.task("default", ["webpack-dev-server"]);

// Build and watch cycle (another option for development)
// Advantage: No server required, can run app from filesystem
// Disadvantage: Requests are not blocked until bundle is available,
//               can serve an old app on refresh
gulp.task("build-dev", ["webpack:build-dev"], function() {
    gulp.watch(["app/**/*"], ["webpack:build-dev"]);
});

// Production build
gulp.task("build", ["webpack:build", "generate:apis"]);

gulp.task("webpack:build", function(callback) {
    // modify some webpack config options
    var myConfig = Object.create(webpackConfig);
    myConfig.plugins = myConfig.plugins.concat(
        new webpack.DefinePlugin({
            "process.env": {
                // This has effect on the react lib size
                "NODE_ENV": JSON.stringify("production")
            }
        }),
        new webpack.optimize.DedupePlugin()
        //new webpack.optimize.UglifyJsPlugin()
    );
    myConfig.progress = true;

    // run webpack
    return gulp.src('./entry/**')
        .pipe(named())
        .pipe(gulpWebpack(myConfig, webpack))
        .pipe(uglify())
        .pipe(stripDebug())
        .pipe(gulp.dest('build/'));

});

gulp.task("clean:motor", function() {
    var fs = require('fs');
    var stats = JSON.parse(fs.readFileSync('build/stats.json', 'utf8'));
    del([
        'build/*.js',
        '!build/*' + stats.hash + '*.js',
        '!build/stats.json',
        '!build/coverfox-apis.js',
    ]);
});

gulp.task('generate:apis', function() {
    var fs = require('fs');
    var app = JSON.parse(fs.readFileSync('../../base/apis/jsapis/config.json', 'utf8'));
    gulp.src(['../../base/apis/jsapis/coverfox-apis.js'])
    .pipe(replace(/<appId>/g, app.applicationId))
    .pipe(replace(/<appKey>/g, app.applicationKey))
    .pipe(replace(/<appBaseUrl>/g, app.applicationBaseUrl))
    .pipe(named())
    .pipe(uglify())
    .pipe(stripDebug())
    .pipe(gulp.dest('build/'));
});

// modify some webpack config options
var myDevConfig = Object.create(webpackConfig);
myDevConfig.devtool = "sourcemap";
myDevConfig.debug = true;

// create a single instance of the compiler to allow caching
var devCompiler = webpack(myDevConfig);

gulp.task("webpack:build-dev", function(callback) {
    // run webpack
    devCompiler.run(function(err, stats) {
        if(err) throw new gutil.PluginError("webpack:build-dev", err);
        gutil.log("[webpack:build-dev]", stats.toString({
            colors: true
        }));
        callback();
    });
});

gulp.task("webpack-dev-server", function(callback) {
    // modify some webpack config options
    var myConfig = Object.create(webpackConfig);
    myConfig.devtool = "eval";
    myConfig.debug = true;

    // Start a webpack-dev-server
    new WebpackDevServer(webpack(myConfig), {
        publicPath: "/" + myConfig.output.publicPath,
        stats: {
            colors: true
        }
    }).listen(8080, "localhost", function(err) {
        if(err) throw new gutil.PluginError("webpack-dev-server", err);
        gutil.log("[webpack-dev-server]", "http://localhost:8080/webpack-dev-server/index.html");
    });
});
