  $(function() {
      $( "#id_scheduled_on" ).datepicker({dateFormat: 'dd-mm-yy'});

      $('.show-report').on('click',function(e){
          e.preventDefault();
          report_url = $(this).attr('href');
          $.ajax({
              url:report_url,
              method:'get',
              success:function(data){
                  $('.lightbox-outer').show();
                  $('.report-box').html(data);

              }
          });
      });
      $('.ajax-form').on('click',function(e){
          e.preventDefault();
          container = $(this).parents('.card');
          report_url = $(this).attr('href');
          $.ajax({
              url:report_url,
              type:'get',
              success:function(data){
                  container.find('.ajax_form_container').show();
                  container.find('.ajax_form_container .lightbox-inner').html(data);
                $('.callback_time').datetimepicker();

              }
          });
      });
      $('.ajax_form_container').on('submit','form',function(e){
          if($(this).attr('action').indexOf('callback')!=-1){
              e.preventDefault();
              data = $(this).serialize();
              container = $(this).parents('.card');
              $(this).find('input[type="submit"]').attr('disabled','disabled');
              var report_url = $(this).attr('action');
              $.ajax({
                  url:report_url,
                  type:"post",
                  data:data,
                  success:function(data){
                      if($.parseJSON(data).success)
                      {
                        container.find('.ajax_form_container').hide();
                      }
                  }
              });
          }
      });

      $('.ajax_form_container .close').on('click', function(){
          container = $(this).parents('.card');
          container.find('.lightbox-inner').html('');
          container.find('.ajax_form_container').hide();
      });

  });
