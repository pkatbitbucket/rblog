    $(document).ready(function(){
        $.ajax({
            url:report_url,
            type:'get',
            success:function(data){
                $('.report').html(data);
            }
        });

        $('.submit-report').on('click',function(e){
            e.preventDefault();
            url = $(this).attr('href');
            data = $('.report form').serialize();
            $.ajax({
                url:report_url,
                type:'post',
                data:data,
                success:function(data){
                    window.location.href = url;
                }
            });
            return false;
        });

        $('.add-document').on('click',function(e){
            e.preventDefault();
            this_elem = $(this);
            if(this_elem.text()=='Cancel')
            {
                $('.new-document').hide();
                $(this).text('Add Document');
                return false;
            }
            url = $(this).attr('href');

            $.ajax({
                url:url,
                type:'get',
                success:function(data){
                    $('.new-document').html(data).show();
                    this_elem.text('Cancel');
                    $('.new-document form').ajaxForm(function(data){
                        if($.parseJSON(data)['success']){
                            location.reload();
                        }
                        else{
                            console.log(data.errors);
                        }
                    });
                }
            });
            return false;
        });
        total_docs= $('.doc_thumbnails .document_details').length;

        var changeImage = function(this_elem){
            var img_src = this_elem.find('img').attr('src');
            var img_tag = this_elem.find('.img-tag').text();
            var document_id = this_elem.find('.document_id').text();
            $('.lightbox').show();
            $('.lightbox-inner img').attr('src',img_src);
            $('.lightbox-inner .title').text(img_tag);
            $('.lightbox-inner .document_id').val(document_id);
            if((this_elem).is(':last-child')){
                $('.next').hide();
            }
            if((this_elem).is(':first-child')){
                $('.prev').hide();
            }

        }

        $('.document_details').on('click',function(){
            lb_img_index = $(this).index();
            changeImage($(this));
        });

        $('.next').on('click',function(){
            lb_img_index++;
            if(lb_img_index >= 0 && lb_img_index <= total_docs-1)
                changeImage($('.document_details:eq('+lb_img_index+')'));
            else
                lb_img_index--;
        });

        $('.prev').on('click',function(){
            lb_img_index--;
            if(lb_img_index >= 0 && lb_img_index <= total_docs-1)
                changeImage($('.document_details:eq('+lb_img_index+')'));
            else
                lb_img_index++;
        });

        $('.close').on('click',function(){
            $('.lightbox').hide();
        });

        $("body").keyup(function(e){
            if($('.lightbox').css('display')!='none'){
                if((e.which|| e.keyCode)==37)
                    {
                        lb_img_index--;
                        if(lb_img_index >= 0 && lb_img_index <= total_docs-1)
                            changeImage($('.document_details:eq('+lb_img_index+')'));
                        else
                            lb_img_index++;
                        return false;
                    }
                if((e.which|| e.keyCode)==39)
                    {
                        lb_img_index++;
                        if(lb_img_index >= 0 && lb_img_index <= total_docs-1)
                           changeImage($('.document_details:eq('+lb_img_index+')'));
                        else
                            lb_img_index--;
                        return false;
                    }
                if((e.which|| e.keyCode)==27)
                    {
                        $('.lightbox').hide();
                        return false;
                    }
            }
        });
    });

function confirmDeletePhoto(e){
    var r = confirm("Are you sure you want to delete this photo?");
    if (r == false) {
        e.preventDefault();
    }
}