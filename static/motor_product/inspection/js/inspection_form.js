var ViewModel = function() {
    var self = this;
    self.daily_slots = ko.observableArray();
    self.show_widget = ko.observable(false);
    self.show_form = ko.observable(false);
    self.message = ko.observable("");
    self.global_errors = ko.observableArray([]);
    self.ins_city = ko.observable();
    self.ins_area = ko.observable();
    self.fav_slot = ko.observable();
    self.submitted = ko.observable(false);
    self.form_data = ko.observable({
        "mobile": ko.observable(MOBILE),
        "area": ko.observable(),
        "city": ko.observable(),
        "scheduled_on": ko.observable(''),
        "schedule_slot": ko.observable(''),
        "transaction": TRANSACTION_ID,
        "address": ko.observable(ADDRESS),
    });
    self.create_slot = function(data, available) {
        return {
            'id': data.id,
            'start_time': data.start_time,
            'end_time': data.end_time,
            'available': ko.observable(available),
            'selected': ko.observable(false)
        };
    };

    self.create_day = function(day, slots) {
        return {
            'day': day,
            'slots': slots,
        };
    };
    self.refresh_slots = function(date) {
        date = typeof date !== 'undefined' ? date : new Date();
        area_id = self.form_data().area();
        var search_date = [date.getDate(), date.getMonth() + 1, date.getFullYear()].join("-");
        self.daily_slots.removeAll();
        $.get("/motor/api/get-available-slots/", {
            'date_from': search_date,
            'area': area_id,
            "transaction": TRANSACTION_ID,
        }, function(data) {
            if(!data.success) {
                if(data.errors.city) {
                    self.message(data.errors.city);
                    self.show_widget(false);
                    return;
                }
            }
            day_dict = {};
            data = data.message;
            for (var i in data.available_slots) {
                var obj = data.available_slots[i];
                if (!day_dict[obj.date]) {
                    day_dict[obj.date] = {
                        'slots': []
                    };
                }
                day_dict[obj.date].slots.push({
                    'slot': self.create_slot(obj.slot, obj.available)
                });
            }
            var slot_list = [];
            for (var day in day_dict) {
                self.daily_slots.push(self.create_day(day, day_dict[day].slots));
            }
            self.show_widget(true);
        }, 'json');
    };

    self.book_slot = function(data_instance, scheduled_on) {
        for (var day in self.daily_slots()) {
            for (var slot in self.daily_slots()[day].slots) {
                self.daily_slots()[day].slots[slot].slot.selected(false);
            }
        }
        data_instance.slot.selected(true);
        vm.form_data().scheduled_on(scheduled_on);
        vm.form_data().schedule_slot(data_instance.slot.id);
    };


    self.submit_form = function() {
        self.submitted = ko.observable(true);
        if(self.ins_area()=='other'){
            self.form_data().area(null);
            self.form_data().scheduled_on('');
            self.form_data().schedule_slot('');

        }
        else{
            self.form_data().area(self.ins_area);
        }

        $.ajax({
                type: "POST",
                url: "/motor/api/get-available-slots/",
                data: ko.toJS(vm.form_data()),
                success: function(rdata) {
                    vm.global_errors.removeAll();
                    if(rdata.success) {
                        self.show_widget(false);
                        self.message(rdata.message);
                        console.log("Success", rdata);
                    } else {
                        console.log("Failed", rdata);
                        self.submitted = ko.observable(false);
                        vm.global_errors.push(rdata.error);
                    }
                },
                dataType: 'json',
        });
        return false;
    };
    self.validate_phno = function(){
        return num_validate(vm.form_data().mobile(), true);
    };
    self.form_valid = function(){
        vm.global_errors.removeAll();
        if( !self.form_data().scheduled_on() && !self.form_data().schedule_slot() && self.show_widget()){
            vm.global_errors.push("Please choose a slot for inspection");
            return false;
        }
        if(!self.validate_phno()){
            return false;
        }
        if(!self.form_data().address()){
            vm.global_errors.push("Please enter address");
            return false;
        }
         return true;
    };
    self.changeCity = function(){
        self.show_form(true);
        self.ins_area($('#inspection_city').val());
        self.ins_city($('#inspection_city :selected').parent().attr('data-value'));
        if(self.ins_area()!="other"){
            vm.form_data().city(self.ins_city());
            vm.form_data().area(self.ins_area());
            vm.refresh_slots(new Date());
            return false;

        }
        else{
            self.show_widget(false);
        }
    }

    self.fav_slot.subscribe(function(checked){
        console.log(checked);
        if(checked){

        }
    });

    //self.refresh_slots();
};


var vm = new ViewModel();
$(document).ready(function() {
    ko.applyBindings(vm);
    $("#id_city_name").autocomplete({
        source: AUTOCOMPLETE_URL,
        select: function(event, ui) {
            console.log(">>>>", event, ui);
            $("#id_city").val(ui.item.id);
            $("#id_city_name").val(ui.item.city);
            $("#id_state_name").html(ui.item.state);
            vm.form_data().city(ui.item.id);
            //vm.refresh_slots(new Date());
            return false;
        },
        response: function(event, ui) {
            $("#id_city").val("");
            $("#id_state_name").html("");
        }
    });
});


function num_validate(cnum, req_error) {
    var res = null;
    if (cnum.trim().length === 0) {
        if (req_error) {
            vm.global_errors.push("Phone number is mandatory");
        }
        return false;
    } else {
        res = cnum.match(/[7-9]{1}\d{9}/);
        if ($.isEmptyObject(res)) {
            vm.global_errors.push('Please enter a valid 10 digit phone number');
            return false;
        } else {
            return true;
        }
    }
}
