'use strict';
require.config({
    baseUrl : '/static/motor_product/mobileMVP/app',
    paths: {
        'async': '/static/motor_product/mobileMVP/app/lib/requirejs-plugins/async',
        'text': '/static/motor_product/mobileMVP/app/lib/require/text',
        'knockout': '/static/motor_product/mobileMVP/app/lib/knockout/knockout-3.1.0',
        'knockout_validation': '/static/motor_product/mobileMVP/app/lib/knockout/knockout.validation',
        'knockout-amd-helpers': '/static/motor_product/mobileMVP/app/lib/knockout/knockout-amd-helpers',
        'knockout-history': '/static/motor_product/mobileMVP/app/lib/knockout/knockout-history',
        'knockout-router': '/static/motor_product/mobileMVP/app/lib/knockout/knockout-router',
        'cookie' : '/static/motor_product/mobileMVP/app/lib/cookies/cookies',
        'crypt': '/static/motor_product/global/lib/encryption/response',
        'models':'/static/motor_product/global/models',
        'rtocodes': '/static/motor_product/global/rtocodes',
        'bstore':'/static/motor_product/global/bstore',
        'viewmodels':'/static/motor_product/global/viewmodels',
        'utils':'/static/motor_product/global/utils',
        'jquery' : '/static/motor_product/mobileMVP/app/lib/jquery/cf-jquery'

    },
    shim: {
        'knockout_validation' : {
            deps : ['knockout']
        },
        'knockout-amd-helpers': {
            deps: ['knockout']
        },
        'knockout-history': {
            deps: ['knockout']
        },
        'knockout-router': {
            deps: ['knockout', 'knockout-history', 'knockout-amd-helpers']
        }
    },
    urlArgs: 'v=1.0.0.0'
});

define(['assets/js/main', 'bstore'], function(main) {
    main.run();
});


