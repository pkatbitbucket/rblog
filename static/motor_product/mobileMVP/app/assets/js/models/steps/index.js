
define(['knockout', 'models', 'viewmodels', 'utils','jquery','rtocodes','crypt'], function(ko, models, viewmodels, utils,$,rtocodes,crypt) {



    var quote = viewmodels.quote;
    var userVehicle = viewmodels.vehicle;
    var make_list = ko.observableArray([]);
    var model_list = ko.observableArray([]);
    var variant_list = ko.observableArray([]);
    var RTO_codes = rtocodes.RTO_CODE_ARRAY;
    var plans = ko.observableArray([]);
    var selectedPlan = ko.observable();

    function getMakes(){
        $.post('/motor/fourwheeler/api/makes/', {'csrfmiddlewaretoken': CSRF_TOKEN}, function(response){
            response = crypt.parseResponse(response);
            make_list(response.makes);
        });

    }



    function getModels(){
        $.post('/motor/fourwheeler/api/makes/'+userVehicle.make().id+'/models/', {'csrfmiddlewaretoken': CSRF_TOKEN}, function(response){
            response = crypt.parseResponse(response);
            model_list(response.models);
        });
        userVehicle.variant(null);
    }

    function getVariants(){
        $.post('/motor/fourwheeler/api/makes/'+userVehicle.make().id+'/models/'+userVehicle.model().id+'/variants/', {'csrfmiddlewaretoken': CSRF_TOKEN}, function(response){
            response = crypt.parseResponse(response);
            variant_list(response.variants);
        });
    }

    function setVehicleId(){
        quote.vehicleId(userVehicle.variant().id);

    }

    userVehicle.make.subscribe(function(){
       userVehicle.model(null);
       userVehicle.variant(null);
       getModels();
    });

    userVehicle.model.subscribe(function(newValue){
       if(newValue){
           userVehicle.variant(null);
           getVariants();
       }

    });

    userVehicle.variant.subscribe(function(newValue){
        if(newValue) {
            setVehicleId();
        }
    });


    quote.rto_info.subscribe(function(newValue){
        console.log(newValue);
    });


    var addonList =[
        {'name':'RPI','value':'100'},
        {'name':'Voluntary excess','value':'2000'},
        {'name':'Electrical acessories','value':'10000'},
        {'name':'No claim Bonus','value':'20%'},
        {'name':'AAI discount','value':'400'},
    ];


    var maxIDV = ko.computed(function(){
        if(plans().length){

            return Math.min.apply(Math, $.map(plans(), function(item){
                return parseInt(item.maxIDV);
            }));

        }
        else{
            return 0;
        }

    });

    var minIDV = ko.computed(function(){
        if(plans().length){

            return Math.max.apply(Math, $.map(plans(), function(item){
                return parseInt(item.minIDV);
            }));

        }
        else{
            return 0;
        }

    });


    quote.idv.subscribe(function(){
       quote.minIdv(minIDV());
       quote.maxIdv(maxIDV());
    });

    var meanIDV = ko.computed(function(){
        return (maxIDV() + minIDV()) / 2;
    });



    var verboseAmount = function(amount){
        return (amount/100000).toFixed(2) + ' Lacs';
    };




    var resultsViewModel = function() {
        var self = this;
        self.tipAlert = ko.observable(false);
        self.tipAlertClose = function(){
            self.tipAlert(false);
        };
        self.buyPlan = function(mode){
            self.overlayMode(mode);
        };
        self.loaderType = ko.observable('simple');
        self.loadingScreen= ko.observable(false);
        self.resultsPageOpen = ko.observable(false);
        self.infoOverlay = ko.observable(false);
        self.infoOverlayVal = ko.observable('');
        self.setInfoMode = function(value) {
            self.tipAlert(false);
            self.infoOverlay(true);
            self.infoOverlayVal(value);
            window.scrollTo(0,0);
        };
        self.closeInfoMode = function(){
            self.tipAlert(false);
            self.infoOverlayVal('');
            self.infoOverlay(false);
            window.scrollTo(0,0);
        };
        self.overlayMode = ko.observable(''); // for modal overlay mode
        self.setOverlayMode = function(mode){
            self.overlayMode(mode);
            self.tipAlert(false);
            window.scrollTo(0,0);
        };
        self.removeOverlayMode = function(){
          if(self.overlayMode() == 'idv_filter' || self.overlayMode() == 'addons') {
              self.loadingScreen(true);
              self.loaderType('simple');
              self.loadingScreen(false);
          }
          self.tipAlert(false);
          self.overlayMode('');
          window.scrollTo(0,0);
        };

        self.showPremiumBreakup = function(plan){
            selectedPlan(plan);
            self.setOverlayMode('premium_breakup');

        };

        self.cngpf = ko.observable(false).extend({
            store: {
                key : 'cngpf'
            }
        });
        self.cngmf = ko.observable(false).extend({
                store: {
                    key : 'cngmf'
                }
            });

        self.setCNGNotFitted = function(){
            self.cngpf(false);
            self.cngmf(false);
            quote.isCNGFitted(0);
            quote.cngKitValue(0);
        };

        self.setCNGPreFitted = function(){
            self.cngpf(true);
            self.cngmf(false);
            quote.isCNGFitted(1);
            quote.cngKitValue(0);
        };

        self.setCNGManuallyFitted = function(){
            self.cngmf(true);
            self.cngpf(false);
            quote.isCNGFitted(1);
        };
        self.showResults = function(logic,loading){


            

            self.resultsPageOpen(logic);
            self.loadingScreen(loading);
            self.loaderType('quote');



            $.post('/motor/fourwheeler/api/quotes/', JSON.parse(ko.toJSON(quote)), function(response){

                response = crypt.parseResponse(response);
                console.log("Response >>>", response);

                var allPremiums = response["premiums"];
                quote.quoteId = response["quoteId"];



                plans($.map(allPremiums, function(item){
                    if(item.exShowroomPrice){
                      quote.exShowroomPrice(item.exShowroomPrice);
                    }
                    return new models.Plan(item);
                }));

                self.loadingScreen(false);
                self.tipAlert(true);
                self.removeOverlayMode();
                window.scrollTo(0,0);
            });









        };

        self.updateQuote = function(){
            self.showResults(true,true);

        };

    };

    console.log(addonList);


    var selectedAddons = ko.observableArray([]);



    var selectAddon = function(addon){
        if(selectedAddons().indexOf(addon) == -1){
            selectedAddons.push(addon);
        }
        else{
            selectedAddons.remove(addon);
        }


        for(var i= 0; i<plans().length; i++){
            var plan = plans()[i];
            var planAddons = plan.addonCovers();

            for(var j= 0; j<planAddons.length;j++){

                if(planAddons[j].id == addon.id){

                    if(planAddons[j].is_selected() == '0'){
                        planAddons[j].is_selected('1');
                    }
                    else{
                        planAddons[j].is_selected('0');
                    }
                    plan.addonCovers.valueHasMutated();
                }
            }
        }


    };



    var isAddonSelected = function(addon){

        return selectedAddons().indexOf(addon) > -1;

    };


    return {
        initialize : function(stage_type) {
            getMakes();
            console.log(stage_type);
            return {
                quote:quote,
                userVehicle:userVehicle,
                months:viewmodels.months,
                years:viewmodels.years,
                addonList:addonList,
                make_list:make_list,
                model_list:model_list,
                variant_list:variant_list,
                RTO_codes:RTO_codes,
                plans:plans,
                maxIDV:maxIDV,
                minIDV:minIDV,
                meanIDV:meanIDV,
                verboseAmount:verboseAmount,
                selectedAddons:selectedAddons,
                selectAddon:selectAddon,
                isAddonSelected:isAddonSelected,
                selectedPlan:selectedPlan,
                allAddons:viewmodels.addonsVerbose,
                results: new resultsViewModel()
            }
        }
    };
});
