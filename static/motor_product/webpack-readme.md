## WEBPACK SETUP

### Install NodeJS

##### Ubuntu

	sudo apt-get install npm nodejs	
	
If you get "node not found" error when you enter the `node` command, symlink `nodejs` to `node`:
	
	sudo ln -s /usr/bin/nodejs /usr/bin/node
	
	
##### MacOS
	
	brew install nodejs

If you are on MacOS and you don't know what `brew` is, stop existing!
	
### Setup Webpack (Development)
	npm install -g webpack (sudo if ubuntu)
	cd static/motor_product
	npm install
	webpack 
	
Using `webpack` to generate bundles can be painful in development. To make this easier, one should use `--watch`. To install, do
	
	webpack --watch
	
Now bundle will be regenerated everytime there is a change in javascript. `--inline` option will include a runtime in the bundle to enable live-reload on the client.
		
### Setup Webpack (Production)
	npm install -g webpack
	cd static/motor_product
	npm install
	webpack -p
	
## Webpack for Knockout Brief

* Use commonjs require (`require("lib")`) wherever you can.
* Use amd require (`require(["lib1", "lib2"])`) when you would like to load libs asynchronously (ondemand).
* When using knockout, avoid `require: {}` form of template/model loading. It won't work with webpack. Instead use the commonjs form.
* requirejs loading is not compatible with webpack. Its a pain in the ass to get right. Avoid it.
* Most of the times, you can do without using bower/grunt/gulp.
