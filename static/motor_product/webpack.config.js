var path = require('path');
var ContextReplacementPlugin = require("webpack/lib/ContextReplacementPlugin");
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

module.exports = {
    context: __dirname,
    entry: {
        desktop: "./entry/desktop.js",
        buy: "./entry/buy.js",
        mobile: "./entry/mobile.js",
    },
    resolve: {
        modulesDirectories: [
            'node_modules',
        ],
        alias: {
                   'async': __dirname + '/global/lib/requirejs-plugins/async',
                   'text': __dirname + '/global/lib/require/text',
                   'requirejs$': __dirname + '/global/lib/require/require',
                   'knockout': __dirname + '/global/lib/knockout/knockout-3.2.0',
                   'knockout-amd-desktop': __dirname + '/amd-js/knockout-amd-desktop',
                   'knockout-amd-buy': __dirname + '/amd-js/knockout-amd-buy',
                   'knockout-amd-mobile': __dirname + '/amd-js/knockout-amd-mobile',
                   'knockout-history': __dirname + '/global/lib/knockout/knockout-history',
                   'knockout-router': __dirname + '/global/lib/knockout/knockout-router',
                   'models': __dirname + '/global/models',
                   'viewmodels': __dirname + '/global/viewmodels',
                   'utils': __dirname + '/global/utils',
                   'configuration': __dirname + '/global/configuration',
                   'form-models': __dirname + '/global/form-models',
                   'cookie' : __dirname + '/global/lib/cookies/cookies',
                   'bstore' : __dirname + '/global/bstore',
                   'main-desktop' : __dirname + '/desktopMVP/app/assets/js/main',
                   'main-buy' : __dirname + '/desktopMVPBuy/app/assets/js/main',
                   'main-mobile' : __dirname + '/mobile/assets/js/main',
                   'components' : __dirname + '/global/components.js',
                   'events' : __dirname + '/global/events.js',
                   'constants' : __dirname + '/global/app-constants.js',
                   'actions' : __dirname + '/global/app-actions.js',
                   'crypt': __dirname + '/global/lib/encryption/response.js',
                   'rtoStateMapping': __dirname + '/global/rto-state-mapping.js',
                   'fastlane': __dirname + '/global/fastlane.js',
                   'liveOffline': __dirname + '/global/liveOffline.js'
                   //'login': __dirname + '/global/login.js',

        }
    },
    resolveLoader: { root: __dirname + "node_modules" },

    module: {
        noParse: [
            '/knockout-3\.2\.0\.js/',
        ],
        preLoaders: [
            /*{
                test: /\.js$/,
                exclude: /global\/lib/,
                loader: "jshint-loader",
            }*/
        ],
        loaders: [
            { test: /knockout-3\.2\.0\.js/, loader: "imports?require=>__webpack_require__" },
            //{ test: /knockout-3\.2\.0\.js/, loader: "imports?require=>false" },
            //{ test: /knockout-amd-desktop\.js/, loader: "imports?config=>{templates_dir: '', models_dir: ''}" },
            //{ test: /knockout-amd-buy\.js/, loader: "imports?config=>{templates_dir: '', models_dir: ''}" },
            //{ test: /knockout-amd-webpack\.js/, loader: "imports?config=>{templates_dir: '', models_dir: ''}" },
            //{ test: /require\.js$/, loader: "exports?requirejs!script" },
	        {test: /pikaday.js/, loader: "imports?require=>false"},
        ]
    },

    plugins: [
        new CommonsChunkPlugin("common", "common.[hash].js"), //["desktop", "buy"], 2),
        function() {
            this.plugin("done", function(stats) {
                require("fs").writeFileSync(
                    path.join(__dirname, "./build", "stats.json"),
                    JSON.stringify(stats.toJson())
                );
                require("fs").utimesSync(
                    path.join(__dirname, "../../base/settings.py"),
                    new Date(),
                    new Date()
                );

            });
        },
    ],

    jshint: {
    },

    output: {
        path: __dirname + "/build/",
        publicPath : '/static/motor_product/build/',
        pathInfo : true,
        filename: "[name].[hash].bundle.js",
    },
};
