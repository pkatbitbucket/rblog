var resend_count = 0;

function close_popup(){
    $('#otp_popup').addClass('hide');
    $('#sent_otp').addClass('hide');
    $('#resend_otp').text('Resend OTP');
}

function send_otp() {
    $('.otp-error').hide();
    $.post("/otp/send/", {'csrfmiddlewaretoken' : CSRF_TOKEN, 'contact': contact}, function (data) {
        if (data.success) {
            $('#otp_popup').removeClass('hide');
            $('#mob-num').text($('#contact-me-num').val());
        } else {
            $('#otp_error_message').text('SMS delivery failed. Please try again');
            $('#otp_error_message').removeClass('hide');
        }
    });
}

function resend_otp () {
    $('.otp-error').hide();
    var otp_button = $('#otp_submit_btn');
    var resend_btn = $('#resend_otp');
    if(resend_count > 2){
        $('#resending_otp').addClass('hide');
        resend_btn.addClass('hide');
        $('#sent_otp').addClass('hide');
        return;
    }
    resend_count++;
    resend_btn.addClass('hide');
    $('#sent_otp').addClass('hide');
    $('#resending_otp').removeClass('hide');
    $('#wrong-number').addClass('hide');
    otp_button.addClass('disable');
    otp_button.disabled = false;
    $.post("/otp/send/", {'csrfmiddlewaretoken' : CSRF_TOKEN, 'contact': contact}, function (data) {
        if (data.success) {
            otp_button.removeClass('disable');
            otp_button.disabled = false;
        } else {
            $('#otp_error_message').text('OTP delivery failed. Please try again');
            $('#otp_error_message').show();
        }
        $('#resending_otp').addClass('hide');
        $('#sent_otp').removeClass('hide');
        $('#wrong-number').removeClass('hide');
        resend_btn.removeClass('hide');
        resend_btn.text('Send Again');
    });
}

function verify_otp (otp) {
    $('.otp-error').addClass('hide');
    var otp_button = $('#otp_submit_btn').first();
    otp_button.text('Verifying');
    otp_button.addClass('disable');
    otp_button.disabled = true;
    $.post("/otp/verify/",
        {'otp': otp, 'contact': contact, 'csrfmiddlewaretoken' : CSRF_TOKEN},
        function (result) {
            if (result.success) {
                window.location = result.redirect_to
            } else {
                $('.otp-error').text('OTP is invalid');
                $('.otp-error').removeClass('hide');
                otp_button.text('Verify');
                otp_button.removeClass('disable');
                otp_button.disabled = false;
            }
        },
        'json'
    );
}

$(document).on('click', '#otp_submit_btn', function() {
    var otp = $('#otp').val().trim();
    if (otp) {
        verify_otp(otp);
    } else {
        $('.otp-error').text('Please enter the OTP');
        $('.otp-error').removeClass('hide');
    }
});

$(document).on('click', '#resend_otp', function(e) {
    e.preventDefault();
    resend_otp();
});
