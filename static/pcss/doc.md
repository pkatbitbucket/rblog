# Variables


You can use variables inside values, selectors and at-rule’s parameters.

```css
$blue: #056ef0;
$column: 200px;

.menu {
    width: calc(4 * $column);
}
.menu_link {
    background: $blue;
    width: $column;
}
```

```css
.menu {
    width: calc(4 * 200px);
}
.menu_link {
    background: #056ef0;
    width: 200px;
}
```



### Interpolation

There is special syntax if you want to use variable inside CSS words:

```css
$prefix: my-company-widget

$prefix { }
$(prefix)_button { }
```


# Nesting and media queries

Example

```css
.phone {
    &_title {
        width: 500px;
        @media (max-width: 500px) {
            width: auto;
        }
        body.is_dark & {
            color: white;
        }
    }
    img {
        display: block;
    }
}
```

will be processed to:

```css
.phone_title {
    width: 500px;
}
@media (max-width: 500px) {
    .phone_title {
        width: auto;
    }
}
body.is_dark .phone_title {
    color: white;
}
.phone img {
    display: block;
}
```

# Imports


```css
/* can consume `node_modules`, `web_modules`, `bower_components` or local modules */
@import "cssrecipes-defaults"; /* == @import "./node_modules/cssrecipes-defaults/index.css"; */

@import "normalize.css/normalize"; /* == @import "./bower_components/normalize.css/normalize.css"; */

@import "css/foo.css"; /* relative to stylesheets/ according to `from` option above */

body {
  background: black;
}
```

will give you:

```css
/* ... content of ./node_modules/my-css-on-npm/index.css */

/* ... content of ./bower_components/my-css-on-bower/index.css */

/* ... content of foo.css */

@media (min-width: 25em) {
/* ... content of bar.css */
}

body {
  background: black;
}
```

## special imports for media queries

```css
@import "css/bar.css" (min-width: 25em);

```

will give you:

```css
@media (min-width: 25em) {
/* ... content of bar.css */
}

```

# Autoprefixing

Dont use any vendor prefixes. Autoprefixer will take care of it.

```css
.container{
	display:flex;
}

```

will give you:

```css
.container{
	display:-webkit-box;
	display:-webkit-flex;
	display:-ms-flexbox;
	display:flex;
}

```

# Extending styles

You can extend styles

```css
.box{
	box-shadow: 1px 2px 3px rgba(0,0,0,0.1);
	border-radius: 3px;
}

.card{
	@extend .box;
	background-color:red;
}

```
will give you

```css

.box, .card{
    box-shadow: 1px 2px 3px rgba(0,0,0,0.1);
    border-radius: 5px;
}

.card{
    background-color:red;
}

```

# Color functions

Darken colors

```css
.btn{
	background-color:red;
	&:hover{
		background-color: color(red, b(20%));
	}
}

```

Lighten colors

```css
.btn{
	background-color:red;
	&:hover{
		background-color: color(red, l(+20%));
	}
}

```





