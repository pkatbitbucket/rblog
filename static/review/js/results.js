$('.extra_form_success').hide();
$('.extra_header_success').hide();
$('#extra_data_init').click(function(){
    $('#extra_data_modal').modal('show')

});

$("[id^=id_bd]").each(function(){
    $(this).mask("99/99/9999",{placeholder:""});
    if (!$(this).val()) {
        $(this).val('dd/mm/yyyy');
    }
});
$("[id^=rem_id_bd]").each(function(){
    $(this).click(function(){
        var iid = $(this).attr('id').split('rem_id_')[1];
        $('#item_' + iid).remove();
    });
});
$('.soc_after').hide();
var socialing_in = function(){
    $('.soc_before').hide();
    $('.soc_after').show();
};
var socialing_out = function(){
    $('.soc_after').hide();
    $('.soc_before').show();
};
$('.share_options').hover(socialing_in, socialing_out);


var feature_template = $("#feature_template").html();
_.each(product_structure, function(product){
    $("#ur-risk-product").append(_.template(feature_template,{product : product }));
});


/* scroll to expert panel */

$('a#expert-sec').click(function(){
    $('html, body').animate({scrollTop: $('#ur-rev-panel').offset().top + $('#ur-rev-panel').height()}, 'slow');

});

/*  code for like bubble starts */

// bubble setting
var bubbleWidth = 0.1*($(document).width());
$('.bubble-container').css({
    'position': 'fixed', /* allows us to absolutely position the pseudo elements within the bubble */
    'top': '10%', /* to set starting position of bubble to come */
    'left': '-10%', /* to set starting position of bubble to come */
    'z-index': 10,
    'display': 'none',
    'width': bubbleWidth,
    'height': bubbleWidth
}); //set bubble size according to the window size

// bubble movement


function makeNewPosition(){
    // Get viewport dimensions (remove the dimension of the div)
    var h = $(window).height() - 50;
    var w = $(window).width() - 50;
    var nh = Math.floor(Math.random() * h);
    var nw = Math.floor(Math.random() * w);
    return [nh,nw];
}

function animateDiv(){
    var newq = makeNewPosition();
    var oldq = $('.bubble-container');
    var speed = calcSpeed([parseFloat(oldq.css('top')), parseFloat(oldq.css('left'))], newq);

    $('.bubble-container').css('display','block').animate({ top: newq[0], left: newq[1] }, speed, function(){
        animateDiv();
    });
}

function calcSpeed(prev, next) {
    var x = Math.abs(prev[1] - next[1]);
    var y = Math.abs(prev[0] - next[0]);
    var greatest = x > y ? x : y;
    var speedModifier = 0.06;
    var speed = Math.max(7000, Math.ceil(greatest/speedModifier));
    return speed;
}

var animation_started = false;

// to do when user touches the bubble

$('.bubble').hover(function(){
    var topPosition = $(window).height() - $('.bubble-container').height();
    //var marginCorrection = $('.bubble-container').width()/2;
    //var socLeft = ($('.bubble-container').width()-$('.soc-btn').width())/2;
    //$('.soc-btn').css({'left': socLeft}); // to center align the container and icon
    $(this).addClass('active');
    $('#social_modal').modal('show');
    $(this).parent('.bubble-container').stop(true).hide();

});

/* show hide button */
var nextMove = 'shown';
$('.classic-button').click(function(){
    var distance = $('.classic').width();
    var css = {};
    if (nextMove == 'shown') {
        css = {'left' :-distance};
        nextMove = 'hide';
        $(this).text('+');
    }
    else  {
        css = {'left':'0px'};
        nextMove = 'shown';
        $(this).text('x');
    }
    $('.classic-holder').animate(css,200);
});

/*  code for like bubble ends */



//initialization
$('#wrapper-box').css('visibility','visible');
$('[id^=bg]').css('opacity','0');
$('#bg1').css('opacity','1');
$('.heading-container').css({'opacity':'0', 'left':'2000px'}); /* set 2000px + or - to make headline folw from right or left */
// TimelineLite for title animation, then start up superscrollorama when complete
(new TimelineLite({onComplete:initScrollAnimations}))
    .from($('#welcome'), 0.7, {delay:1, css:{opacity:'0'}, ease:Expo.easeOut} )
    .from( $('#title-line2'), 0.8, {delay: 0.2,css:{opacity:'0'}, ease:Expo.easeOut})
    .from( $('#show-me'), 1, {delay: 0,css:{opacity:'0',top:'-80px'}, ease:Back.easeOut});


var controller = $.superscrollorama({triggerAtCenter: true});

function initScrollAnimations() {
    $("span.risk-box").parent().removeClass("active");

    addTweensToModel();
    //show enquiry question data on scroll
    controller.addTween(
        '#change1',
        (new TimelineLite())

            .append([
                TweenMax.to($('#bg2'), 0.4, {css:{opacity:1}, immediateRender:true}),
                TweenMax.from($('#sect2'), 1,{css:{opacity:0}, ease: Expo.easeOut}),
                TweenMax.from($('#wt-enq .query-showcase'), 1,{css:{opacity:0}, ease: Expo.easeOut})
            ]),
        200 // scroll duration of tween
    );

    controller.addTween(
        '#change2',
        (new TimelineLite())

            .append([
                TweenMax.to($('#bg3'), 0.4, {css:{opacity:1}, immediateRender:true}),
                TweenMax.to($('#scroll-icon'), 0.1,{css:{'display':'inline-block'}, immediateRender:true}),
                TweenMax.from($('#sect3'), 1,{css:{opacity:0}, ease: Expo.easeOut}),
                TweenMax.from($('#how-cal'), 1,{css:{opacity:0}, ease: Expo.easeOut})
            ]),
        200 // scroll duration of tween
    );


    controller.addTween(
        '#change3',
        (new TimelineLite())

            .append([
                TweenMax.to($('#bg4'), 0.4, {css:{opacity:1}, immediateRender:true}),
                //TweenMax.from($('#sect4'), 1,{css:{opacity:0}, ease: Expo.easeOut}),
                //TweenMax.from($('#lets-risk'), 0.1,{css:{opacity:0}}),
                //TweenMax.from($('#my-risks'), 1,{css:{opacity:0}, ease: Expo.easeOut})
            ]),
        200 // scroll duration of tween
        ,100 //offset for risk header
    );


    controller.addTween (
        '#get-risk-horizontal',

        (new TimelineLite())

            .append([
                TweenMax.to($('#list-table'), 1.5, {css:{'opacity':'1', 'top':'0px'}, ease:Expo.easeIn}),
                TweenMax.to($('#lets-handle'), 0.5, {css:{'opacity':'0'}}),
                //TweenMax.from($('#ur-risk-product'), 0.7, {css:{'opacity':'0'}}),
            ]),
        200 // scroll duration of tween
        ,200 // offset for risk header
    );

    controller.addTween(
        '#change4',
        (new TimelineLite())

            .append([
                TweenMax.to($('#bg5'), 0.4, {css:{opacity:1}}),
            ]),
        200 // scroll duration of tween
    );
    // bubble animation part start embedded here
    controller.addTween(
        '#change5',
        (new TimelineLite())

            .append([
                TweenMax.to($('#bg6'), 0.4, {css:{opacity:1}, immediateRender:true,
                    onStart : function(){
                        if(!animation_started){
                            animation_started = true;
                            animateDiv(); // for like button bubble animation to start
                        }
                    }
                }),
                TweenMax.to($('#risk-list'), 0.2, {css:{opacity:0}, immediateRender:true}),
            ]),
        200 // scroll duration of tween
    );

    //pinning action using superscrollorama
    var pinList = [
        ['#sect2','#wt-enq'],
        ['#sect3','#how-cal'],
        ['#sect4','#my-risks'],
        ['#sect6','#line']
    ];
    $.each(pinList, function(index, value){
        //set duration, in pixels scrolled, for pinned element
        var pinDur1 = $(value[1]).height();
        var getWidth1 = $(value[0]).width();
        var getHeight1 = $(value[0]).height();
        // animation timeline for pinned element
        var pinAnimation1 = new TimelineLite();
        pinAnimation1.append(TweenMax.from($(value[0]), 0.5, {css:{ 'width': $(value[0]).width() }, ease:Expo.easeOut}));

        controller.pin($(value[0]), pinDur1, {
            anim: pinAnimation1,
            onPin: function(){
                $(value[0]).css({'width':getWidth1, 'height': '100%'});
                $('#scroll-icon').css({'display':'inline-block'});
                if (value[0] == '#sect6'){
                    $('#scroll-icon').css({'display':'none'});
                }
            },
            onUnpin: function(){
                $(value[0]).css({'width':'100%','height': $(value[0]).height()});
                $('#scroll-icon').css({'display':'none'});
            }
        });

    });
    $.each(product_structure, function(prod){
        var product = product_structure[prod];
        //set duration, in pixels scrolled, for pinned element
        var pinWho =  $("#fproduct_" + product.meta.id + " .product-block");
        var pinhgt =  $("#fproduct_" + product.meta.id + " .top-adjust");
        var pinDur = $(pinhgt).height();
        var getWidth = $(pinWho).width();
        var getHeight = $(pinWho).height();
        var topAdjust = $("#list-table").height();
        $( "#fproduct_" + product.meta.id + " .distinct-box:eq(1)").css('margin-bottom','50%'); //gap after sections
        // animation timeline for pinned element
        var pinAnimation = new TimelineLite();
        pinAnimation.append([
            TweenMax.from($(pinWho), 0.5, {}),
        ]);

        controller.pin($(pinWho), pinDur, {
            anim: pinAnimation,
            onPin: function(){
                $(pinWho).css({'width': getWidth});
                $(pinWho).css({'height': $(pinWho).height()});
                $(pinWho).find('.list-of-risk li').each(function(){
                    $(this).animate(
                        {'left':'0px'}, 700, 'easeInExpo',
                        function(){
                            $(this).find('.risk-icon').animate({'right':'0px'}, 500, 'easeOutBounce');
                        }
                    );
                });
                // $(pinWho).addClass('active');
                //$(".pointing-block2").css('color','rgba(255,255,255,0.1)');
                //$(product.meta.risks).addClass('active');
                //$("#changer").addClass('active').text("Risks this product covers");
            },
            onUnpin: function(){
                $(pinWho).css({'width': "80%"});
                $(pinWho).css({'height':'auto'});
                //$(pinWho).removeClass('active');
                //$(product.meta.risks).removeClass('active');
                //$("#changer").removeClass('active').text("These are the risks we identified");
                //$(".pointing-block2").css('color', '#ffffff');
            },
            offset: -20 //-$("#list-table").height() -1, //pin time arrangement setting
        });
    });
};

var first_load = true;
var resizeTimeout;
$(window).resize(function(){
    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(function(){
        if(!first_load){
            controller.triggerCheckAnim();
        }
        first_load = false;
    }, 1000);
});


