
$(document).ready(function() {
    var feature_template = $("#feature_template").html();
    _.each(product_structure, function(product){
        $("#ur-risk-product").append(_.template(feature_template,{product : product }));
    });
    /* scroll to expert panel */

    $('a#expert-sec').click(function(){
        $('html, body').animate({scrollTop: $('#ur-rev-panel').offset().top + $('#ur-rev-panel').height}, 'slow');

    });
    $('#extra_data_init').click(function(){
        $('#extra_data_modal').modal('show')

    });
    $('#ie-share-btn').click(function(){
        $('#social_modal').modal('show');
    });

    $('.soc_after').hide();
    var socialing_in = function(){
        $('.soc_before').hide();
        $('.soc_after').show();
    }
    var socialing_out = function(){
        $('.soc_after').hide();
        $('.soc_before').show();
    }
    $('.share_options').hover(socialing_in, socialing_out);

});
