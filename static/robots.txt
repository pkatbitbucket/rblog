User-agent: *

Disallow: /*.php$
Disallow: /*.inc$
Disallow: */trackback/
Disallow: /trackback/
Disallow: /accounts/
Disallow: /health-plan/
Disallow: /motor/car-insurance/
Disallow: /leads/
Disallow: /lp/
Disallow: /cms/
Disallow: /static/uploads/policy-documents/
Disallow: /motor/twowheeler-insurance/
Disallow: /home-insurance/
Disallow: /user/profile/
Disallow: /static/uploads/policy-brochures/
Disallow: /jargon-busting/
Disallow: /get-started/
Disallow: /jagoinvestor/
Disallow: /two-wheeler-insurance/product
Disallow: /travel-insurance/form/
Disallow: /term-insurance/quotes/

Sitemap: https://www.coverfox.com/sitemap.xml
