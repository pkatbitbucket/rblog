var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};

var validate_ped = {
    validator: function(val, validate) {
        if (val == 'no')
            return true
    },
    message: 'You cannot buy online policy, please contact customer care'
};

var mobile_validate = {
    validator: function(val,validate){
        var re =/^[1-9]\d{9}$/
     var match = val.match(re);
     if(match) return true;

    },
    message :'Please enter a valid phone number.'
}

var pincode_validation = {
    validator: function(val,validate){
        var re =/^[1-9]\d{5}$/
        var match = val.match(re);
     if(match) return true;

    },
    message :'Please enter a valid pin code.'
    }



var RELATIONS_TEXT = ["Employee","Self","Spouse","Father","Mother","Son","Daughter","Uncle","Aunty","Niece","Nephew","Sister","Brother","Friend","Colleague"]

var NOMINEE_RELATIONS = ['Self', 'Spouse','Son','Daughter-In-Law','Daughter','Father','Mother','Brother(s) or Sister(s)','Father-in-Law','Mother-in-Law','Grand Father','Grand Mother', 'Grand Son','Grand Daughter','Spouse','Father', 'Mother', 'Uncle', 'Aunty', 'Niece', 'Nephew', 'Brother', 'Sister', 'Son', 'Daughter', 'Employee', 'Friend', 'Colleague', 'Others']


function get_age(dob) {
    dob = $.datepick.parseDate("dd-M-yyyy", dob);
    var today = new Date();
    var age_str, age_in_years, age_in_mths;
    var ydiff = today.getFullYear() - dob.getFullYear();
    var mdiff = today.getMonth() - dob.getMonth();
    var ddiff = today.getDate() - dob.getDate();

    age_in_years = ydiff;
    if(mdiff < 0 || (mdiff == 0 && ddiff < 0)) --age_in_years;
    if (age_in_years > 0) {
        age_str = age_in_years + (age_in_years == 1 ? ' year' : ' years');
    }
    else {
        if(ydiff == 1)  mdiff = 12 + mdiff;
        age_in_mths = mdiff - (ddiff >= 0 ? 0 : 1);
        age_str = age_in_mths + (age_in_mths == 1 ? ' mth' : ' mths');
    }
    return (age_in_years > 0) ? age_in_years : (age_in_mths / 12);
}

var member_model = function(type, DATA){
    var self =this;
    self.type = type;
    self.Relationship = ko.observable(DATA[type+'Relationship']).extend({required:true});
    self.fname = ko.observable(DATA[self.type+'FName']).extend({required: true});
    self.lname = ko.observable(DATA[self.type+'LName']).extend({required:true});
    self.dob = ko.observable(DATA[self.type+'DOB']).extend({required: true, validation: {
            validator: function(val) {
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var age = get_age(val);

                var valid_age = ( age <= 60);
                if(!valid_age) {
                    this.message = 'Sorry! Only customer less than 60 years of age can buy a policy.Please Contact Customer Care';
                }
                return valid_age;
            }
        }
});
    self.passport = ko.observable(DATA[self.type+'Passport']).extend({required:true});
    self.nominee_name = ko.observable(DATA[self.type+'NomineeName']).extend({required: true});
    self.physician_name = ko.observable(DATA[self.type+'PhysicianName']).extend({required: true});
    self.relationship = ko.observable(DATA[self.type+'Relationship']).extend({required: true});
    self.nominee_relationship = ko.observable(DATA[self.type+'Nominee_Relationship']).extend({required: true});
    self.phone = ko.observable(DATA[self.type+'phone']).extend({required: true});
    self.pre_exis_disease = ko.observable(DATA[self.type+'pedYesNoTravel']).extend({required: true, validation: validate_ped});

    self.relationship_text = RELATIONS_TEXT;
    self.nominee_relations = NOMINEE_RELATIONS;

    self.gender = ko.observable(DATA[self.type+'gender']).extend({required: true});
//    self.salute = ko.observable(DATA[self.type+'Salute']).extend({required: true});


    self.self_proposer_visible = ko.computed(function(){
        return (self.relationship() != 'Self')
    });

};

var address_model = function(type,legend, DATA){
    var self = this;
    self.type = type;
    self.legend = legend;
    self.visible = ko.observable(true);
    self.addr1 = ko.observable(DATA[self.type+'_ResAddr1']).extend({required: true});
    self.addr2 = ko.observable(DATA[self.type+'_ResAddr2']).extend({required: true});
    self.pincode = ko.observable(DATA[self.type+'_pincode']).extend({required: true,validation: pincode_validation});
    self.state = ko.observable(DATA[self.type+'_state']).extend({required: true});
    self.city = ko.observable(DATA[self.type+'_city']).extend({required: true});
    //self.state.subscribe(function(val){ console.log(val);});
    self.state_options = STATES;
    self.city_options = ko.computed(function(){
    return CITIES[self.state()]
    });

};

address_model.prototype.copy_from = function(address) {
    var self = this;
    self.addr1(address.addr1());
    self.addr2(address.addr2());
    self.state(address.state());
    self.city(address.city());
    self.pincode(address.pincode());
}

var buy_model = function(DATA) {
    var self = this;
    self.fname = ko.observable(DATA['FName']).extend({required: true});
    self.lname = ko.observable(DATA['LName']).extend({required:true});
    self.dob = ko.observable(DATA['DOB']).extend({required: true, validation: {
            validator: function(val) {
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var age = get_age(val);
                var valid_age = ( age >= 18);
                if(!valid_age) {
                    this.message = 'Sorry! Only customer more than 18 years of age can buy a policy.Please Contact Customer Care';
                }
                return valid_age;
            }
        }
});


    self.email = ko.observable(DATA['email']).extend({email: true});
    self.mobile = ko.observable(DATA['mobile']).extend({
        required: true,
        pattern: {
            message: 'Please enter a valid phone number.',
            params: /^[1-9]\d{9}$/
        }
    });
    self.mobile = ko.observable(DATA['mobile']).extend({required: true,validation: mobile_validate});
    self.gender = ko.observable(DATA['gender']).extend({required: true});

    self.trip_start = ko.observable(DATA.trip_start != '0');
    self.alert = ko.observable(DATA.alert != '0');
    self.tc_agree = ko.observable(DATA.tc_agree != '0');

    self.country = ko.observable(DATA.country).extend({
    required: true,
    });
    var present_address = new address_model('present','Communication Address',DATA);
    var permanent_address = new address_model('permanent','Home Address (For emergancy purposes)',DATA);
    self.addresses = ko.observableArray([present_address, permanent_address]);
    self.same_permanent_address = ko.observable(DATA.same_permanent_address != undefined ? DATA.same_permanent_address : true);
        permanent_address.visible(!self.same_permanent_address());
        self.same_permanent_address.subscribe(function(val) {
            permanent_address.visible(!val);
        });

    /*self.country_options = COUNTRIES;*/
    self.avail_country = COUNTRIES;

    self.chosen_countries = ko.observableArray();
    self.post_countries = ko.observable();
    self.chosen_countries.subscribe(function(val){
        var str="";
        for(var i in val){
            str+=val[i]+",";
        }
        console.log(val);
        console.log(str);
        self.post_countries(str);

    });

    self.departure_city =ko.observable(DATA['depature_city']).extend({required: true});
    self.destination_city =ko.observable(DATA['destination_city']).extend({required: true});

    self.members_list = ko.observableArray(
        ko.utils.arrayMap(MEMBERS, function(mem) {
            return new member_model(mem, DATA);
        })
    );

    self.relation_error_visible = ko.computed(function(){
        var count = 0;
        for (i = 0; i<self.members_list().length; ++i)
        {
            if (self.members_list()[i].relationship() == 'SELF- PRIMARY MEMBER')
            count++;
        }
        return count > 1;
    });

    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  ($('p.validationMessage').filter(':visible').length > 0);

        if(!errors) {
            if(self.same_permanent_address())
                permanent_address.copy_from(present_address);

            return true;
        }
        return false;
    };


};

ko.validation.configuration['insertMessages'] = false;
ko.validation.configuration['grouping']['deep'] = true;

var DATA = DATA || {};
var FORM = new buy_model(DATA);
ko.applyBindings(FORM);
ko.validation.group(FORM).showAllMessages(false);
