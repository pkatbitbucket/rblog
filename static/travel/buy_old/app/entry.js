var ko = require('knockout')

ko.components.register('buy-form', {
    viewModel: require('./index.js'),
    template: require('raw!./index.tmpl.html')
});
ko.applyBindings();

