define(['knockout', 'jquery', 'utils','./lib/cookies', 'form-models', './general-form', 
        './insurers/others-form',
        './insurers/reliance-form',
        './insurers/bharti-axa-form',
        './insurers/bajaj-form',
        './insurers/religare-form'],
        function(ko, $, utils, cookies, formmodels, generalform, othersform, relianceform, bhartiform, bajajform, religareform) {

    var disablePayment = ko.observable();
    var errorMessage = ko.observable("");
    var all_forms = {};

    setTimeout(function(){
        ko.computed(function(){
            all_forms_json = {}
            all_forms_json['members'] = [];
            for(var i in all_forms.member_forms){
                var formview = all_forms.member_forms[i];
                all_forms_json.members.push(formview.getValue());
            }
            var formview = all_forms.contact_form;
            all_forms_json.contact = formview.getValue();

            var formview = all_forms.proposer_form;
            if(formview) all_forms_json.proposer = formview.getValue();
            var big_json = extraMethods.cleanLmsData(all_forms_json);
            if(big_json['mobile']!=undefined && big_json['email']!=undefined){
                sendDataToLMS(big_json,"proposal-form");
            }
        });
    },1000);

    var sendDataToLMS = function(data,label){
        data['campaign'] = 'travel';
        data['label'] = label;
        data['insurer_slug'] = insurer_slug;
        data['product_type'] = 'travel'; //talk to LMS team about this shit.
        data['transaction_id'] = transaction_id;
        data['triggered_page'] = window.location.href;

        cookies.set('email',data['email']);
        cookies.set('mobileNo',data['mobile']);
        for(var k in quote_details){ data[k] = quote_details[k]; } //quote_details coming from back end.
        $.ajax({
            url: '/leads/save-call-time/',
            type: 'POST',
            data: {'data': JSON.stringify(data), 'csrfmiddlewaretoken': CSRF_TOKEN},
            success:function(resp){
            },
        });
    }

    var extraMethods = {
        DateValidation: function(data, error){
            var pat = "^[0-9]\+-[0-9]\+-[0-9]{4}$";
            if(data.search(pat) == 0) return true;
            error("date should be in DD-MM-YYYY format");
            return false;
        },
        NomineeAgeValidation: function(data, error) {
            if (data < 18) {
                error('Nominee must be more than 18 years of age.');
                return false;
            }
            return true;
        },
        PassportValidation: function(data, error) {
            var matched = data.match('^[A-Z][0-9]{7}$');
            if (matched) return true;
            error('Passport number is invalid.');
            return false;
        },
        TermsAndConditionsValidation: function(data, error) {
            if (data.accept_terms) return true;
            error('Please accept the above terms and conditions to proceed.');
            return false;
        },
        DOBClean: function(field, value) {
            var dateStr = value.trim().replace(/ /g, "").replace(/\//g, "-");
            var parts = dateStr.split('-');

            if (parts.length == 3) {
                while (parts[0].length < 2) parts[0] = '0' + parts[0];
                while (parts[1].length < 2) parts[1] = '0' + parts[1];
            }

            var dateStr = parts.join("");
            var day = dateStr.match("^([3][0-1]|[1-2][0-9]|[0]{0,1}[1-9])");
            dateStr = dateStr.substr(day.index + day[0].length);
            day = day[0];

            while (day.length < 2) day = '0' + day;

            var month = dateStr.match("^([1][0-2]|[0]{0,1}[1-9])");
            dateStr = dateStr.substr(month.index + month[0].length);
            month = month[0];

            while (month.length < 2) month = '0' + month;

            var year = dateStr.substr(0, 4);
            var thisYear = parseInt((new Date()).getFullYear());
            if (year.length == 3) {
                if (parseInt(year) > (thisYear % 1000)) {
                    year = '1' + year;
                } else {
                    year = '2' + year;
                }
            } else if (year.length == 2) {
                if (parseInt(year) > (thisYear % 100)) {
                    year = '19' + year;
                } else {
                    year = '20' + year;
                }
            } else if (year.length == 1) {
                if (parseInt(year) > (thisYear % 10000)) {
                    year = '190' + year;
                } else {
                    year = '200' + year;
                }
            }

            var newvalue = day + '-' + month + '-' + year;
            field.cleanedValue(newvalue);
            return newvalue;
        },
        DOBValidation: function(data, error) {
            var x = data.split('-');
            if ( (x[0]=='' || isNaN(x[0])) || (x[1]=='' || isNaN(x[1])) || (x[2]=='' || isNaN(x[2])) ) {
                error("Invalid date");
                return false;
            }

            var cust_dob = utils.stringToDate(data, 'dd-mm-yyyy', '-'),
                cust_age = utils.calculateAgeFromDate(cust_dob);
            if (cust_age < 18) {
                error("You must be atleast 18 years of age");
                return false;
            }
            return true;
        },
        toggleRegistrationAddress: function(field, id, value) {
            if (id == 'add-same') {
                field.visible(!(value === 1));
            }
        },
        fetchCitiesForState: function(field, id, value) {
            if ((id == 'loc_add_state') || (id == 'loc_reg_add_state')) {
                if (data.form_details.is_cities_fetch) {
                    $.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurer_slug + '/', {
                        'state': value,
                        'csrfmiddlewaretoken': CSRF_TOKEN
                    }, function(response) {
                        response = crypt.parseResponse(response);
                        if (response.data.cities) {
                            field.OptionsList(response.data.cities);
                            field.Value(data.user_form_details[field.Id]);
                            field.visible(true);
                        }
                    });
                }
            }
        },
        fetchLocationDetails: function(field, id, value) {
            return;
            if (id == 'add_pincode') {
                $.post('/motor/' + VEHICLE_TYPE + '/api/pincode-details/' + insurer_slug + '/', {
                    'pincode': value,
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    response = crypt.parseResponse(response);
                    field.parent.getFieldById('add_state').Value(response.data.province_name);
                    field.parent.getFieldById('add_district').Value(response.data.district_name);
                    if (data.form_details.is_cities_fetch) {
                        $.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurer_slug + '/', {
                            'state': response.data.province_name,
                            'csrfmiddlewaretoken': CSRF_TOKEN
                        }, function(response) {
                            response = crypt.parseResponse(response);
                            if (response.data.cities) {
                                field.parent.getFieldById('add_city').OptionsList(response.data.cities);
                                field.parent.getFieldById('add_city').Value(data.user_form_details['add_city']);
                                field.visible(true);
                            }
                        });
                    } else {
                        field.visible(true);
                    }
                });
            }

            if (id == 'reg_add_pincode') {
                $.post('/motor/' + VEHICLE_TYPE + '/api/pincode-details/' + insurer_slug + '/', {
                    'pincode': value,
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    response = crypt.parseResponse(response);
                    field.parent.getFieldById('reg_add_state').Value(response.data.province_name);
                    field.parent.getFieldById('reg_add_district').Value(response.data.district_name);
                    if (data.form_details.is_cities_fetch) {
                        $.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurer_slug + '/', {
                            'state': response.data.province_name,
                            'csrfmiddlewaretoken': CSRF_TOKEN
                        }, function(response) {
                            response = crypt.parseResponse(response);
                            if (response.data.cities) {
                                field.parent.getFieldById('reg_add_city').OptionsList(response.data.cities);
                                field.parent.getFieldById('reg_add_city').Value(data.user_form_details['reg_add_city']);
                                field.visible(true);
                            }
                        });
                    } else {
                        field.visible(true);
                    }
                });
            }
        },
        toggleOtherField: function(field,id,value){
            field.visible( (value=='yes') ? true:false);
        },
        disablePaymentMode: function(field,id,value){
            if(id=='bool_disease'){
                disablePayment( (value=='yes') ? true:false);
                errorMessage("Sorry! You cannot buy this policy because you answered 'yes' for disease question");
            }
        },
        NoSpaceWithUpperCase: function(field, value) {
            return value.replace(/ /g,"").toUpperCase();
        },
        cleanGeneralFormData: function(all_forms_json) {
            return all_forms_json;
        },
        cleanGeneralForm: function(all_forms){
            var count = 1;
            for(var i in all_forms.member_forms){
                var member = all_forms.member_forms[i];
                var td = member.getFieldById('traveller-details');
                td.Name("Traveller "+(count++)+" Information");
            }
            return all_forms;
        },
    }


    INSURER_FORMS = {
        'bharti-axa':bhartiform,
        'reliance': relianceform,
        'hdfc-ergo': 'hdfcergofrom',
        'bajaj': bajajform,
        'religare': religareform,
        'star-health': 'starform',
    }

    var insurerform = othersform;
    if (insurer_slug in INSURER_FORMS) {
        insurerform = INSURER_FORMS[insurer_slug];
    }

    for (var k in insurerform.extraMethods) {
        extraMethods[k] = insurerform.extraMethods[k];
    }


    if((['star-health','religare']).indexOf(insurer_slug)>=0){
        var formview = new formmodels.FormClass(extraMethods);
        formview.putFields(generalform.getProposerForm, formview.tree);
        all_forms.proposer_form = formview;
    }
    all_forms.member_forms = [];
    var travellerDetailsForm = generalform.getTravellerDetailsForm(insurerform);
    for(var i=0;i<form_details.num_insured;++i){
        var formview = new formmodels.FormClass(extraMethods);
        formview.putFields(travellerDetailsForm, formview.tree);
        all_forms.member_forms.push(formview);
    }

    var formview = new formmodels.FormClass(extraMethods);
    formview.putFields(generalform.getContactForm, formview.tree);
    all_forms.contact_form = formview;

    extraMethods.cleanGeneralForm(all_forms);
    extraMethods.cleanInsurerForm(all_forms);

    var autosaving = ko.observable(false);

    var form_wait = ko.observable(false);

    return function() {
        return {
            Forms: all_forms,
            form_details: form_details,
            quote_details: quote_details,
            plan_details: plan_details,
            CSRF_TOKEN: CSRF_TOKEN,
            form_wait: form_wait,
            disablePayment:disablePayment,
            errorMessage: errorMessage,
            insurer_slug:insurer_slug,
            initialize: function() {
            },

            submitForm: function() {
                //form_wait(true);
                var big_json_data = {};
                var all_forms_json = {}
                all_forms_json['members'] = [];
                for(var i in all_forms.member_forms){
                    var formview = all_forms.member_forms[i];
                    if (formview.hasValidData()) {
                        var formjson = formview.getValue();
                        all_forms_json.members.push(formjson);
                    }
                    else    return;
                }
                var formview = all_forms.contact_form;
                if (formview.hasValidData()) {
                    var formjson = formview.getValue();
                    all_forms_json.contact = formjson;
                }
                else    return;
                var formview = all_forms.medical_form;
                if(formview){
                    if (formview.hasValidData()) {
                        var formjson = formview.getValue();
                        all_forms_json.medical = formjson;
                    }
                    else    return;
                }
                var formview = all_forms.proposer_form;
                if(formview){
                    if (formview.hasValidData()) {
                        var formjson = formview.getValue();
                        all_forms_json.proposer = formjson;
                    }
                    else return;
                }
                all_forms_json = extraMethods.cleanGeneralFormData(all_forms_json);
                big_json_data  = extraMethods.cleanInsurerFormData(all_forms_json);

                validation = extraMethods.validation(all_forms_json);

                if(!validation.valid){
                    disablePayment(true);
                    errorMessage("Error: "+validation.error);
                    setTimeout(function(){ disablePayment(false); },3000);
                    return;
                }
                this.saveFormDetails();

                var submit_buy_url = '/travel/transaction/' + transaction_id + '/final/';
                var buy_url = '/travel/transaction/' + transaction_id + '/to-process/';
                $.post(submit_buy_url,{'csrfmiddlewaretoken':CSRF_TOKEN,'data': JSON.stringify(big_json_data)}, function(response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        if (self == top) {
                            window.location.href = buy_url;
                        } else {
                            top.location.href = SITE_URL + buy_url;
                        }
                    } else {
                        alert(response.errorMessage);
                    }
                });
            },
            saveFormDetails: function() {
                all_forms_json = {}
                all_forms_json['members'] = [];
                for(var i in all_forms.member_forms){
                    var formview = all_forms.member_forms[i];
                    all_forms_json.members.push(formview.getValue());
                }
                var formview = all_forms.contact_form;
                all_forms_json.contact = formview.getValue();

                var formview = all_forms.proposer_form;
                if(formview) all_forms_json.proposer = formview.getValue();

                var big_json = extraMethods.cleanLmsData(all_forms_json);
                if(big_json['mobile']!=undefined && big_json['email']!=undefined){
                    sendDataToLMS(big_json,"make-payment");
                }
                else{
                    console.log("LMS data push FAILED!!");
                }
            }
        };
    };
});
