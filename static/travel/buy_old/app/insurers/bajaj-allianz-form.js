define(['knockout'], function(ko) {

    var extraMethods = {
        cleanInsurerForm: function (form) {
        },
        cleanInsurerFormData: function(formdata) {
           return formdata;
        },
    }

    return {
        extraMethods: extraMethods,
    }
});
