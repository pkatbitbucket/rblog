define(['knockout', 'jquery', 'cookie'], function(ko, $, Cookies) {

    //var allAddons = settings.getConfiguration().addons;

    var DOBFormat = function(format){
        month_array = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        arr = format.split("-");
        if(!isNaN(arr[1])){
            return arr[0]+"-"+month_array[Number(arr[1])-1]+"-"+arr[2];
        }
        else{
            return format;
        }
    }
    setInterval(function() {
        if (window.olark) {
            var cd = new Date().toISOString().split(".")[0];
            var d = cd.split("T")[0].split('-');
            var t = cd.split("T")[1];
            window.olark('api.chat.updateVisitorStatus', {
                snippet: 'Last seen: ' + [d[2], d[1], d[0]].join('-') + " " + t
            });
        }
    }, 60000);

    ko.bindingHandlers.inr_currency = {
        update: function(element, valueAccessor, allBindingsAccessor) {
            var valueWasNegative = false;
            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value < 0) {
                value = value * -1;
                valueWasNegative = true;
            }
            var x = value.toString();
            var res = x;
            if (x.length > 3) {
                var afterPoint = '';
                if (x.indexOf('.') > 0)
                    afterPoint = x.substring(x.indexOf('.'), x.length);
                x = Math.floor(x);
                x = x.toString();
                var lastThree = x.substring(x.length - 3);
                var otherNumbers = x.substring(0, x.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            }
            //var prefix = allBindingsAccessor().currencyPrefix || "Rs.";
            var prefix = "";
            ko.bindingHandlers.text.update(element, function() {
                var sign = valueWasNegative ? '-' : '';
                return prefix + " " + sign + " " + res;
            });
        },
    };



    var hide_chat_box = function() {
        var otcounter = 0;
        var ot = setInterval(function() {
            console.log('TRYING CHAT BOX');
            if (window.olark) {
                console.log("CHAT BOX HIDE");
                window.olark('api.box.hide');
                clearInterval(ot);
            }
            otcounter++;
            if (otcounter > 50) {
                clearInterval(ot);
            }
        }, 3000);
    };
    var show_chat_box = function() {
        var otcounter = 0;
        var ot = setInterval(function() {
            console.log('TRYING CHAT BOX');
            if (window.olark) {
                console.log("CHAT BOX READY");
                window.olark('api.box.show');
                clearInterval(ot);
            }
            otcounter++;
            if (otcounter > 50) {
                clearInterval(ot);
            }
        }, 3000);
    };
    var notify = function(message_list) {
        console.log("NOTIFY >>", message_list);
        if (window.olark) {
            if (!(message_list instanceof Array)) {
                message_list = [message_list];
            }
            window.olark('api.chat.sendNotificationToOperator', {
                body: message_list.join('\n')
            });
        }
    };
    var event_log = function(action, label, value) {
        if (window.dataLayer) {
            window.dataLayer.push({
                'event': 'GAEvent',
                //'eventCategory': settings.getConfiguration().ga_category,
                'eventAction': action,
                'eventLabel': label,
                'eventValue': value
            });
        }
    };
    var gtm_event_log = function(event_name) {
        if (window.dataLayer) {
            window.dataLayer.push({
                'event': event_name
            });
        }
    };
    var gtm_data_log = function(data) {
        if (window.dataLayer) {
            window.dataLayer.push(data);
        }
    };
    var page_view = function(page, title) {
        if (window.dataLayer) {
            window.dataLayer.push({
                'event': 'virtual_page_view',
                'virtual_page_path': page,
                'virtual_page_title': title
            });
        }
    };
    var load_time = function(action, label, load_time, source) {
        if (window.dataLayer) {
            window.dataLayer.push({
                'event': 'ga_timer',
                'timingCategory': source,
                'timingVar': action,
                'timingLabel': label,
                'timingValue': load_time
            });
        }
    };

    var split = function(a, n) {
        var len = a.length,
            out = [],
            i = 0;
        while (i < len) {
            var size = Math.ceil((len - i) / n--);
            out.push(a.slice(i, i += size));
        }
        return out;
    };

    var insurers = {
        1: 'l-t',
        2: 'bajaj-allianz',
        3: 'bharti-axa',
        4: 'iffco-tokio',
        5: 'hdfc-ergo',
        7: 'tata-aig',
        8: 'liberty-videocon',
        9: 'universal-sompo',
        10: 'new-india',
        11: 'oriental',
    };
    var validPhone = function(data) {
        if (data.length == 10) { data = '0' + data;}
        return data.match('0[0-9]{10}');
    };
    var lmsUtils = {
        validPhone : function(data) {
            if (data == undefined || data == null) return false;
            if (data.length == 10) { data = '0' + data;}
            if (data.match('0[0-9]{10}')) return true;
        },
        sendData: function(formData, label) {
            try {
                if (typeof formData == 'string') {
                    jsonData = {};
                    $.each(formData, function() {
                        if (jsonData[this.name]) {
                            if (!jsonData[this.name].push) {
                                jsonData[this.name] = [jsonData[this.name]];
                            }
                            jsonData[this.name].push(this.value || '');
                        } else {
                            jsonData[this.name] = this.value || '';
                        }
                    });
                } else {
                    jsonData = formData;
                }

                if (!lmsUtils.validPhone(jsonData['cust_phone'])) {
                    return;
                }

                // LMS backend requires name, email and mobile as keys,
                // hence mapping from buy-form

                if (label == 'buy-form' || label == 'proposal-form') {
                    jsonData['transaction_url'] = window.location.href;
                    jsonData['mobile'] = jsonData['cust_phone'];
                    jsonData['name'] = jsonData['cust_first_name'] + ' ' + jsonData['cust_last_name'];
                    jsonData['email'] = jsonData['cust_email'];

                    delete jsonData['cust_phone'];
                    delete jsonData['cust_first_name'];
                    delete jsonData['cust_last_name'];
                    delete jsonData['cust_email'];
                }
                //////////////////////////////////////////////////////////////////
                //jsonData['campaign'] = settings.getConfiguration().lms_default_campaign;
                jsonData['label'] = label;
                jsonData['device'] = 'Desktop';
                jsonData['triggered_page'] = window.location.href;
                event_log('CallScheduled', label, 0);

                $.post('/leads/save-call-time/', {
                    'data': JSON.stringify(jsonData),
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    console.log("LMS data sent.");
                });
            } catch (e) {
                console.log("Error sending data to LMS.");
            }
        },
        sendGeneric: function(data, label, mobile, extras, callback) {
            try {
                if (!lmsUtils.validPhone(mobile)) {
                    return;
                }
                var jsonData = {};
                //jsonData['campaign'] = settings.getConfiguration().lms_default_campaign;
                jsonData['label'] = label;
                jsonData['mobile'] = mobile;
                jsonData['device'] = 'Desktop';
                jsonData['triggered_page'] = window.location.href;
                jsonData['data'] = data;

                for (var key in extras) {
                    jsonData[key] = extras[key];
                }


                console.log(jsonData);
                event_log('CallScheduled', label, 0);

                $.post('/leads/save-call-time/', {
                    'data': JSON.stringify(jsonData),
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    callback();
                    console.log("LMS data sent.");
                });
            } catch (e) {
                console.log("Error sendng data to LMS");
            }
        }
    };
    var motorOlark = {
        getVehicleAndRTO: function(quote) {
            var infoStrs = [];

            var newVehicleStr = 'Old';
            if (quote.isNewVehicle()) {
                newVehicleStr = 'New';
            }
            infoStrs.push('Vehicle Type: ' + newVehicleStr);

            var vehicleStr = '';
            if (quote.vehicle()) vehicleStr += quote.vehicle().name;
            if (quote.fuel_type()) vehicleStr += ' ' + quote.fuel_type().name;
            if (quote.variant()) vehicleStr += ' ' + quote.variant().name;
            if (vehicleStr != '') {
                infoStrs.push('Vehicle: ' + vehicleStr);
            }

            if (quote.rto_info()) {
                infoStrs.push('RTO Location: ' + quote.rto_info().name);
            }

            if (quote.isCNGFitted()) {
                infoStrs.push('CNG Kit Value: ' + quote.cngKitValue());
            }
            return infoStrs;
        },
        getIdvDetails: function(quote) {
            var infoStrs = [];

            var idvStr = ''
            if (quote.idv()) {
                idvStr = 'Idv: Lowest';
            } else {
                idvStr = 'Idv: ' + quote.idv();
            }
            infoStrs.push(idvStr);

            infoStrs.push('Registration Date: ' + quote.registrationDate());
            infoStrs.push('Manufacturing Date: ' + quote.manufacturingDate());
            infoStrs.push('Voluntary Deductible: ' + quote.voluntaryDeductible());
            infoStrs.push('Idv Electrical: ' + quote.idvElectrical() + ', Idv NonElectrical: ' + quote.idvNonElectrical());

            if (quote.isCNGFitted()) {
                infoStrs.push('CNG Kit Value: ' + quote.cngKitValue());
            }

            infoStrs.push('-------------------------------------------------------');
            infoStrs.push('Claimed Last Year: ' + quote.isClaimedLastYear());
            infoStrs.push('Previous NCB: ' + quote.previousNCB());
            infoStrs.push('-------------------------------------------------------');

            return infoStrs;
        },
        getAddonDetails: function(quote) {
            var infoStrs = [];
            for (var index in allAddons) {
                var addon = allAddons[index];
                infoStrs.push(addon.name + ' ' + quote[addon.id]());
            }
            return infoStrs;
        },
        sendStartPageNotification: function(quote) {
            try {
                var infoStrs = motorOlark.getVehicleAndRTO(quote);
                if (quote.reg_year()) {
                    infoStrs.push('Registration Year: ' + quote.reg_year());
                }
                notify(infoStrs);
            } catch (e) {
                console.log(e);
            }
        },
        sendQuoteNotification: function(quote, quoteUrl) {
            try {
                var infoStrs = motorOlark.getVehicleAndRTO(quote);
                infoStrs.push('-------------------------------------------------------');
                infoStrs = infoStrs.concat(motorOlark.getIdvDetails(quote));
                infoStrs.push('-------------------------------------------------------');
                inforStr = infoStrs.concat(quoteUrl);
                notify(infoStrs);
            } catch (e) {
                console.log(e);
            }
        },
        sendAddOnsNotification: function(quote) {
            try {
                var infoStrs = motorOlark.getAddonDetails(quote);
                notify(infoStrs);
            } catch (e) {
                console.log(e);
            }
        },
        sendPlansNotification: function(plans) {
            try {
                var planStrList = [];
                for (var index in plans) {
                    var p = plans[index];
                    var planStr = p.insurerSlug + ' | ' + p.selectedAddonsAvailable + ' | ' + p.calculatedAtIDV + ' | ' + p.calculatedPremium();
                    planStrList.push(planStr);
                }
                notify(planStrList);
            } catch (e) {
                console.log(e);
            }
        },
        sendSelectPlanNotification: function(plan) {
            try {
                var planStrList = ['Selected Plan', plan.insurerSlug + ' | ' + plan.calculatedAtIDV + ' | ' + plan.calculatedPremium()];
                notify(planStrList);
            } catch (e) {
                console.log(e);
            }
        },
        sendRefreshedPlanNotification: function(quote, plan) {
            try {
                var infoStrs = motorOlark.getVehicleAndRTO(quote);
                infoStrs.push('-------------------------------------------------------');
                infoStrs = infoStrs.concat(motorOlark.getIdvDetails(quote));
                infoStrs = infoStrs.concat(motorOlark.getAddonDetails(quote));
                infoStrs.push('-------------------------------------------------------');
                infoStrs.push(plan.insurerSlug + ' | ' + plan.calculatedAtIDV + ' | ' + plan.calculatedPremium());
                notify(infoStrs);
            } catch (e) {
                console.log(e);
            }
        },
        sendBuyFormNotification: function(formurl) {
            try {
                notify(['Buy form url: ', formurl]);
            } catch (e) {
                console.log(e);
            }
        },
    };

    var dateToString = function(date) {
        dd = date.getDate(),
        mm = date.getMonth() + 1,
        yyyy = date.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        return dd + '-' + mm + '-' + yyyy;
    };

    function stringToDate(_date, _format, _delimiter) {

        var formatLowerCase = _format.toLowerCase();
        var formatItems = formatLowerCase.split(_delimiter);
        var dateItems = _date.split(_delimiter);
        var monthIndex = formatItems.indexOf("mm");
        var dayIndex = formatItems.indexOf("dd");
        var yearIndex = formatItems.indexOf("yyyy");
        var month = parseInt(dateItems[monthIndex]);
        month -= 1;
        return new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    }

    var validation = {
        isEmailAddress: function(str) {
            var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            return pattern.test(str);
        },
        isNotEmpty: function(str) {
            if (str == undefined) {
                return false;
            }
            var pattern = /\S+/;
            return pattern.test(str);
        },
        isNumber: function(str) {
            var pattern = /^\d+$/;
            return pattern.test(str);
        },
    };

    var getDaysBetweenDates = function(date1, date2) {
        var timeDiff = date2.getTime() - date1.getTime(),
            diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    }

    function firstDayOfPreviousMonth(yourDate) {
        return new Date(yourDate.getFullYear(), yourDate.getMonth() - 1, 1);
    }

    function calculateAgeFromDate(date) {
        var todayDate = new Date(),
            todayYear = todayDate.getFullYear(),
            todayMonth = todayDate.getMonth(),
            todayDay = todayDate.getDate(),
            startMonth = date.getMonth(),
            startDay = date.getDate(),
            startYear = date.getFullYear(),
            age = todayYear - startYear;

        if (todayMonth < startMonth - 1) {
            age--;
        }

        if (startMonth - 1 == todayMonth && todayDay < startDay) {
            age--;
        }
        return age;
    }

    function arr_diff(a1, a2) {
        var a = [],
            diff = [];
        for (var i = 0; i < a1.length; i++)
            a[a1[i]] = true;
        for (var i = 0; i < a2.length; i++)
            if (a[a2[i]]) delete a[a2[i]];
            else a[a2[i]] = true;
        for (var k in a)
            diff.push(k);
        return diff;
    }

    return {
        split: split,
        notify: notify,
        event_log: event_log,
        gtm_event_log: gtm_event_log,
        gtm_data_log: gtm_data_log,
        page_view: page_view,
        load_time: load_time,
        show_chat_box: show_chat_box,
        hide_chat_box: hide_chat_box,
        insurers: insurers,
        motorOlark: motorOlark,
        lmsUtils: lmsUtils,
        validation: validation,
        dateToString: dateToString,
        stringToDate: stringToDate,
        getDaysBetweenDates: getDaysBetweenDates,
        firstDayOfPreviousMonth: firstDayOfPreviousMonth,
        calculateAgeFromDate: calculateAgeFromDate,
        arr_diff:arr_diff,
        DOBFormat: DOBFormat,
    }
});
