var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};

var Member_model = function(type, DATA) {
    var self = this;
    self.type = type;
    self.fname = ko.observable(DATA[self.type+'FName']).extend({required: true});
    self.lname = ko.observable(DATA[self.type+'LName']).extend({required:true});
    self.nname = ko.observable(DATA[self.type+'NName']).extend({required:true});
    self.aname = ko.observable(DATA[self.type+'AName']);
    self.nnamerelation = ko.observable(DATA[self.type+'NNameRelation']);
    self.anamerelation = ko.observable(DATA[self.type+'ANameRelation']);
    self.hpassport = ko.observable(DATA[self.type+'hpassport']).extend({required:true});
    self.passportno_visible = ko.computed(function(){
        return self.hpassport() == 'Y';
    });
    self.passportno = ko.observable(DATA[self.type+'passportno']);
    self.dob = ko.observable(DATA[self.type+'DOB']).extend({required: true, validation: date_validation});
    self.gender = ko.observable(DATA[self.type+'gender']).extend({required: true});
}
var buy_model = function(DATA) {
    var self = this;

    self.FName = ko.observable(DATA.FName).extend({
    required: true,
    });

    self.LName = ko.observable(DATA.LName).extend({
    required: true,
    });

    self.NName = ko.observable(DATA.NName).extend({
    required: true,
    });
    self.AName = ko.observable(DATA.AName).extend({
    required: true,
    });
    self.NNameRelation = ko.observable(DATA.NNameRelation).extend({
    required: true,
    });
    self.ANameRelation = ko.observable(DATA.ANameRelation).extend({
    required: true,
    });
    self.gender = ko.observable(DATA.gender).extend({
    required: true,
    });

    self.hpassport = ko.observable(DATA.hpassport).extend({
    required: true,
    });

    self.passportno_visible = ko.computed(function(){
        return self.hpassport() == 'Y';
    });
    self.passportno = ko.observable(DATA.passportno)
    self.DOB = ko.observable(DATA.DOB).extend({
    required: true,
    validation: date_validation
    });

    self.ResAddr1 = ko.observable(DATA.ResAddr1).extend({
    required: true,
    });
    self.ResAddr2 = ko.observable(DATA.ResAddr2).extend({
    required: true,
    });
    self.ResAddr3 = ko.observable(DATA.ResAddr3).extend({
    required: true,
    });

    self.state = ko.observable(DATA.state).extend({
    required: true,
    });

    self.city = ko.observable(DATA.city).extend({
    required: true,
    });

    self.pincode = ko.observable(DATA.pincode).extend({
    required: true,
    });

    self.members_list = ko.observableArray(
        ko.utils.arrayMap(MEMBERS, function(mem) {
            return new Member_model(mem, DATA);
        }));

    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  $('p.validationMessage').filter(':visible').length;
        if(errors > 0) return false;
        else {
            return true;
        }
    };
};
var DATA = DATA || {};

ko.validation.configuration['insertMessages'] = false;
ko.validation.configuration['grouping']['deep'] = true;
var FORM = new buy_model(DATA);
ko.applyBindings(FORM);
ko.validation.group(FORM).showAllMessages(false);
