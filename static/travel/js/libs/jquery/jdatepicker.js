ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var el = $(element);
        var config = {
            showAnim: '',
            todayClass: 'hide',
	    yearRange: "-100:+10",
        };
        function set_config(name, property, def) {
            var val = property || def;
            if(!val) {
                return;
            }
            config[name] = val;
            if(ko.isObservable(property)) {
                config[name] = property();
                property.subscribe(function(val) {
                    config[name] = val;
                    destroy();
                    apply_config();
                });
            }
        };
        function apply_config() {
            el.datepick(config);
        };
        function destroy() {
            el.datepick('destroy');
        };

        var acc = allBindingsAccessor();
        set_config('dateFormat', acc.dpFormat, "dd-M-yyyy")
        set_config('minDate', acc.dpStartDate);
        set_config('maxDate', acc.dpEndDate);
        set_config('showTrigger', acc.dpTrigger);
        set_config('onSelect', function() {
            valueAccessor()(el.val());
        });

        apply_config();

        ko.utils.domNodeDisposal.addDisposeCallback(element, destroy);
    },
};

