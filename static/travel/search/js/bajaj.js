var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};
function get_age(dob) {
    dob = $.datepick.parseDate("dd-M-yyyy", dob);
    var today = new Date();
    var age_str, age_in_years, age_in_mths;
    var ydiff = today.getFullYear() - dob.getFullYear();
    var mdiff = today.getMonth() - dob.getMonth();
    var ddiff = today.getDate() - dob.getDate();

    age_in_years = ydiff;
    if(mdiff < 0 || (mdiff == 0 && ddiff < 0)) --age_in_years;
    if (age_in_years > 0) {
        age_str = age_in_years + (age_in_years == 1 ? ' year' : ' years');
    }
    else {
        if(ydiff == 1)  mdiff = 12 + mdiff;
        age_in_mths = mdiff - (ddiff >= 0 ? 0 : 1);
        age_str = age_in_mths + (age_in_mths == 1 ? ' mth' : ' mths');
    }
    return (age_in_years > 0) ? age_in_years : (age_in_mths / 12);
}

var Member = function(DATA, member_type, counter) {
    var self = this;
    self.member = member_type + (counter+1);
    self.label = member_type[0].toUpperCase() + member_type.slice(1) + ' ' + (counter+1);
    self.name = ko.observable(DATA[self.member+'_name']);
    self.dob = ko.observable(DATA[self.member+'_dob']).extend({
        required: true,
        validation: date_validation
    });
    self.assignee_name = ko.observable(DATA[self.member+'_assignee_name']);
    self.relation = ko.observable(DATA[self.member+'_relation']);
    self.gender = ko.observable(DATA[self.member+'_gender']);
};

var search_model = function(DATA) {
    var self = this;
    self.activity_id = window.ACTIVITY_ID;
    self.form_visible = ko.observable(!self.activity_id);
    self.cancel = function() {
        self.form_visible(false);
        window.scrollTo(0,0);
    };
    self.modify = function() {
        self.form_visible(true);
        window.scrollTo(0,0);
    };

    self.traveller_type = ko.observable(DATA.traveller_type).extend({
        required: true
    });

    self.traveller_type_options = [
        'Individual',
        'Family',
        'Student',
        'Business Multi-Trip'
    ];

    self.relation_options = [
        'Self',
        'Spouse',
        'Child',
        'Other'
    ];

    self.no_of_child = ko.observable(DATA.no_of_child);
    self.no_of_child_visible = ko.computed(function() {
        return self.traveller_type() == 'Family';
    });
    self.children_list = ko.computed(function() {
        var temp_children_list = [];
        for(var i = 0; i < self.no_of_child(); i++) {
            var member_type = 'child';
            temp_children_list.push(new Member(DATA, member_type, i));
        }
        return temp_children_list;
    });


    self.no_of_adult = ko.observable(DATA.no_of_adult);
    self.no_of_adult_visible = ko.computed(function() {
        return self.traveller_type() == 'Family';
    });
    self.adult_list = ko.computed(function() {
        var temp_adult_list = [];
        for(var i = 0; i < self.no_of_adult(); i++) {
            var member_type = 'adult';
            temp_adult_list.push(new Member(DATA, member_type, i));
        }
        return temp_adult_list;
    });


    self.region = ko.observable(DATA.region).extend({
        required: true
    });

    self.region_options = ko.computed(function(){
        if(self.traveller_type() == 'Individual')
            return [
                'WORLDWIDE INCLUDING USA AND CANADA',
                'WORLDWIDE EXCLUDING USA AND CANADA',
                'ASIA ONLY EXCLUDING JAPAN'
            ];
        if(self.traveller_type() == 'Family')
            return [
                'SCHENGEN MEMBER STATES / WORLDWIDE EXCLUDING USA AND CANADA',
                'WORLDWIDE EXCLUDING USA AND CANADA',
                'SCHENGEN MEMBER STATE',
            ];
        if(self.traveller_type() == 'Business Multi-Trip')
            return [
                'SCHENGEN MEMBER STATES',
                'SCHENGEN MEMBER STATES / WORLDWIDE INCLUDING USA AND CANADA',
                'WORLDWIDE INCLUDING USA AND CANADA',
            ];
        if(self.traveller_type() == 'Student')
            return [
                'SCHENGEN MEMBER STATES',
                'SCHENGEN MEMBER STATES / WORLDWIDE EXCLUDING USA AND CANADA',
                'SCHENGEN MEMBER STATES / WORLDWIDE INCLUDING USA AND CANADA',
                'WORLDWIDE EXCLUDING USA AND CANADA',
                'WORLDWIDE INCLUDING USA AND CANADA'
            ];
    });

    self.from_date = ko.observable(DATA.from_date).extend({
        required: true,
        validation: {
            validator: function(val) {
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var d1 = new Date();
                var d2 = $.datepick.parseDate("dd-M-yyyy", val);
                if( d2 < d1) {
                    this.message = 'Travel date should be in future';
                    return false;
                }
                var days_diff = (d2 - d1) / 86400000;
                if(days_diff > 90) {
                    this.message = 'Departure date cannot be later than 90 days from today';
                    return false;
                }
                return true;
            }
        }
    });

    self.date_range_visible = ko.observable(true);
    self.to_date = ko.observable(DATA.to_date).extend({
        required: {onlyIf: self.date_range_visible},
        validation: {
            validator: function(val) {
                if(!self.date_range_visible()) return true;
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var d1 = self.from_date();
                var d2 = val;
                if(d1) {
                    d1 = $.datepick.parseDate("dd-M-yyyy", d1);
                    d2 = $.datepick.parseDate("dd-M-yyyy", d2);
                    this.message = 'To date should be greater than from date';
                    return d2 > d1;
                }
                return true;
            }
        }
    });

    self.dob = ko.observable(DATA.dob).extend({
        required: true,
        validation: {
            validator: function(val) {
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var age = get_age(val);
                if(age < 18) {
                    this.message = 'Proposer should be minimum 18 years old';
                    return false;
                }
                return true;
            }
        }
    });

    self.child_dob = ko.observable(DATA.child_dob).extend({
        required: true,
        validation: date_validation
    });


    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  $('p.validationMessage').filter(':visible').length;
        if(errors > 0) return false;
        else {
            if(!self.no_of_child_visible()) self.no_of_child('');
            if(!self.no_of_adult_visible()) self.no_of_adult('');
            return true;
        }
    };
};
var DATA = DATA || {};
var FORM = new search_model(DATA);
ko.validation.configuration['insertMessages'] = false;
$(function() {
    ko.applyBindings(FORM);
    ko.validation.group(FORM).showAllMessages(false);
    window.scrollTo(0,0);
});
