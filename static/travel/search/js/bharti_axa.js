var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};

var search_model = function(DATA) {
    var self = this;
    self.activity_id = window.ACTIVITY_ID;
    self.form_visible = ko.observable(!self.activity_id);
    self.cancel = function() {
        self.form_visible(false);
        window.scrollTo(0,0);
    };
    self.modify = function() {
        self.form_visible(true);
        window.scrollTo(0,0);
    };
    self.trip_type = ko.observable(DATA.trip_type || 'S');
    self.travel_type = ko.observable(DATA.travel_type || 'NON_SCH');
    self.policy_type = ko.observable(DATA.policy_type);
    self.policy_type_options = [
        {id: 'I', name: 'Individual'},
        {id: 'F', name: 'Family'},
        {id: 'S', name: 'Student'}
    ];
    self.max_trip_length = ko.observable(DATA.max_trip_length);
    self.max_trip_length_visible = ko.computed(function() {
        return self.trip_type() == 'A' && self.policy_type()!=='S';
    });

    self.trip_type_visible = ko.computed(function(){
        return self.policy_type() != 'S';
    })
    self.family_type_visible = ko.computed(function(){
        return self.policy_type() == 'F';
    });
    self.family_type = ko.observable(DATA.family_type);
    self.Travelling_To = ko.observable(DATA.Travelling_To);
    self.region_options = ko.computed(function() {
        if(self.policy_type() == 'S')
            return [
                {rid: '1', rname: 'world including usa and canada'},
                {rid: '6', rname: 'world excluding usa and canada'},
            ];
        else if  ((self.policy_type() =='I'|| self.policy_type() =='F') && self.trip_type()== 'S')
            return [
                {rid: 'SCH', rname: 'Schengen'},
                {rid: '1', rname: 'world including usa and canada'},
                {rid: '2', rname: 'world excluding usa and canada but including japan'},
                {rid: '3', rname: 'asia excluding japan'},
            ];
        else if  ((self.policy_type() =='I' || self.policy_type() == 'F') && self.trip_type()== 'A')
            return [
                {rid: '1', rname: 'world including usa and canada'},
                {rid: '2', rname: 'world excluding usa and canada but including japan'},
            ];
    });

    self.first_name = ko.observable(DATA.first_name).extend({
        required: true,
    });
    self.last_name = ko.observable(DATA.last_name).extend({
        required: true
    });
    self.age = ko.observable(DATA.age).extend({
        required: true
    });

    self.mobile = ko.observable(DATA.mobile).extend({
        required: true,
        pattern: {
            message: 'Please enter a valid phone number.',
            params: /^[1,9]\d{9}$/
        }
    });

    self.email_id = ko.observable(DATA.email_id).extend({
        required: true,
        email: true
    });

    self.sportsperson = ko.observable(DATA.sportsperson).extend({
        required: true
    });

    self.dangerous_activity = ko.observable(DATA.dangerous_activity).extend({
        required: true
    });

    self.pre_existing_illness = ko.observable(DATA.pre_existing_illness).extend({
        required: true
    });
    self.DOB = ko.observable(DATA.DOB).extend({
        required: true,
        validation: date_validation
    });
    self.traveller_type_visible = ko.computed(function() {
        return self.trip_type() != 'A';
    });
    self.traveller_type = ko.observable(DATA.traveller_type || 'Individual');

    self.cover_type_visible = ko.computed(function() {
        return self.traveller_type_visible() && (self.traveller_type() == 'Family');
    });
    self.cover_type = ko.observable(DATA.cover_type).extend({
        required: {onlyIf: self.cover_type_visible}
    });
    self.cover_type_options = [
        'Self + Spouse',
        'Self + Spouse + Child1',
        'Self + Spouse + Child1 + Child2',
        'Self + Child1',
        'Self + Child1 + Child2'
    ];


    self.from_date = ko.observable(DATA.from_date).extend({
        required: true,
        validation: {
            validator: function(val) {
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var d1 = new Date();
                var d2 = $.datepick.parseDate("dd-M-yyyy", val);
                return d2 > d1;
            },
            message :  'Travel date should be in future'
        }

    });

    self.to_date_visible = ko.computed(function() {
        return self.trip_type() != 'A' || self.policy_type()=='S' ;
    });
    self.to_date = ko.observable(DATA.to_date).extend({
        required: {onlyIf: self.to_date_visible},
        validation: {
            validator: function(val) {
                if(!self.to_date_visible()) return true;
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var d1 = self.from_date();
                var d2 = val;
                if(d1) {
                    d1 = $.datepick.parseDate("dd-M-yyyy", d1);
                    d2 = $.datepick.parseDate("dd-M-yyyy", d2);
                    this.message = 'To date should be greater than from date';
                    return d2 > d1;
                }
                return true;
            }
        }
    });

    self.trip_duration = ko.computed(function() {
        var d1 = self.from_date();
        var d2 = self.to_date();
        if(d1 && d2) {
            d1 = $.datepick.parseDate("dd-M-yyyy", d1);
            d2 = $.datepick.parseDate("dd-M-yyyy", d2);
            return (d2 - d1) / 86400000;
        }
        else
            return '';
    });



    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  $('p.validationMessage').filter(':visible').length;
        if(errors > 0) return false;
        else {
            if(self.policy_type()=='S') self.trip_type('S');
            //if(!self.traveller_type_visible()) self.traveller_type('');
            //if(!self.cover_type_visible()) self.cover_type('');
           // if(!self.region_visible()) self.region('');
           // re seeing some interesting investments via collaborations focusing on the back-end and logistics supply chain and that's going to be absolutely critical for delivering real food security for India. India has got this extraordinary agricultural wealth that could create huge jobs and better living standards for farmers and greater choice for consumers. It's a win-win.if(!self.to_date_visible()) self.to_date('');
            return true;

        }
    };
};
var DATA = DATA || {};
var FORM = new search_model(DATA);
ko.validation.configuration['insertMessages'] = false;
$(function() {
    ko.applyBindings(FORM);
    ko.validation.group(FORM).showAllMessages(false);
    window.scrollTo(0,0);
});
