var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};

var search_model = function(DATA) {
    var self = this;
    self.activity_id = window.ACTIVITY_ID;
    self.form_visible = ko.observable(!self.activity_id);
    self.cancel = function() {
        self.form_visible(false);
        window.scrollTo(0,0);
    };
    self.modify = function() {
        self.form_visible(true);
        window.scrollTo(0,0);
    };
    self.policy_type = ko.observable(DATA.policy_type).extend({
        required: true
    });
    self.geographical_coverage = ko.observable(DATA.geographical_coverage).extend({
        required: true
    });
    self.trip_type = ko.observable(DATA.trip_type).extend({
        required: true
    });
    self.trip_type_visible = ko.computed(function(){
     return self.policy_type() == 'I';
    });
    self.policy_type_options = [
        {id: 'I', name: 'Self'},
        {id: 'F', name: 'Family'},
        {id: 'S', name: 'Student'},
    ];

    self.student_traveldays = ko.observable(DATA.student_traveldays)
    self.present_loc = ko.observable(DATA.present_loc).extend({ required: true });
    self.nationality = ko.observable(DATA.nationality).extend({ required: true })
    self.student_trip_duration_visible = ko.computed(function(){
      return self.policy_type() == 'S';
    });

    self.present_loc_visible = ko.computed(function(){
      return self.policy_type() == 'S';
    });
    self.nationality_visible = ko.computed(function(){
      return self.policy_type() == 'S';
    });


    self.family_type_visible = ko.computed(function(){
        return self.policy_type() == 'F';
    });

    self.family_type = ko.observable(DATA.family_type).extend({ required:{onlyIf : self.family_type_visible} });
    self.family_type_options = [
        {id: 'self+spouse', name: 'self+spouse'},
        {id: 'self+spouse+child1', name: 'self+spouse+child1'},
        {id: 'self+spouse+child1+child2', name: 'self+spouse+child1+child2'},
    ];
    self.additional_children_visible = ko.computed(function(){
          return self.family_type() == 'self+spouse+child1+child2';
    });
    self.dependent_parent_visible = ko.computed(function(){
          return self.policy_type() == 'F';
    });
    self.dependent_parent = ko.observable(DATA.dependent_parent).extend({ required:{onlyIf : self.dependent_parent_visible} });
    self.additional_children_options = ko.computed(function() {
        if ((self.family_type() == 'self+spouse+child1+child2') && (self.dependent_parent() == 'Both'))
            return [
                {id: '0', name: '0'},
            ];
        else if ((self.family_type() == 'self+spouse+child1+child2') && (self.dependent_parent() == 'Father' || self.dependent_parent() == 'Mother'))
            return [
                {id: '0', name: '0'},
                {id: '1', name: '1'},
            ];
        else if (self.family_type() == 'self+spouse+child1+child2')
            return [
                {id: '0', name: '0'},
                {id: '1', name: '1'},
                {id: '2', name: '2'},
            ];
    });
    self.additional_children = ko.observable(DATA.additional_children).extend({ required:{onlyIf : self.additional_children_visible} });
    self.region_options = ko.computed(function() {
        if(self.trip_type() == 'A')
            return [
                {rid: 'Worldwide', rname: 'Worldwide'},
            ];
        else  if   (self.policy_type()=='S')
            return [
                {rid:'WorldWide-Excluding USA/Canada',rname:'WorldWide-Excluding USA/Canada'},
                {rid:'WorldWide-Including USA/Canada',rname:'WorldWide-Including USA/Canada'},
            ];
        else  if   (self.policy_type()=='F')
            return [
                {rid:'WorldWide-Excluding USA/Canada',rname:'WorldWide-Excluding USA/Canada'},
            ];
        else
            return [
                {rid:'WorldWide-Excluding USA/Canada',rname:'WorldWide-Excluding USA/Canada'},
                {rid:'Asia - Excluding Japan',rname:'Asia - Excluding Japan'},
                {rid:'WorldWide-Including USA/Canada',rname:'WorldWide-Including USA/Canada'},
            ];
    });
    self.max_trip_length = ko.observable(DATA.max_trip_length);
    self.max_trip_length_visible = ko.computed(function() {
        return self.trip_type() == 'A';
    });
    self.trip_duration_visible = ko.computed(function(){
        if  ((self.trip_type() == 'A') || (self.policy_type() == 'S'))
            return false;
        else
            return true;
    })

    self.traveller_type_visible = ko.computed(function() {
        return self.trip_type() != 'A';
    });
    self.dob = ko.observable(DATA.dob).extend({ required: true });

    self.from_date = ko.observable(DATA.from_date).extend({
        required: true,
     validation: {
           validator: function(val) {
               if(!date_validation.validator(val)) {
                  this.message = date_validation.message;
                    return false;
              }
               var d1 = new Date();
               var d2 = $.datepick.parseDate("dd-M-yyyy", val);
                return d2 > d1;
            },
            message:'Travel date should be in future'
        }
    });

    self.to_date_visible = ko.computed(function() {
        if  (self.trip_type() == 'A' || (self.policy_type()=='S'))
            return false;
        else
            return true;
    });
    self.to_date = ko.observable(DATA.to_date).extend({
        required: {onlyIf: self.to_date_visible},
        validation: {
            validator: function(val) {
                if(!self.to_date_visible()) return true;
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var d1 = self.from_date();
                var d2 = val;
                if(d1) {
                    d1 = $.datepick.parseDate("dd-M-yyyy", d1);
                    d2 = $.datepick.parseDate("dd-M-yyyy", d2);
                    this.message = 'To date should be greater than from date';
                    return d2 > d1;
                }
                return true;
            }
        }
    });

    self.trip_duration = ko.computed(function() {
        var d1 = self.from_date();
        var d2 = self.to_date();
        if(d1 && d2) {
            d1 = $.datepick.parseDate("dd-M-yyyy", d1);
            d2 = $.datepick.parseDate("dd-M-yyyy", d2);
            return ((d2 - d1) / 86400000)+1;
        }
        else
            return '';
    });



    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  $('p.validationMessage').filter(':visible').length;
        if(errors > 0) return false;
        else {
            if(self.policy_type()=='S') self.trip_type('S');
            //if(!self.traveller_type_visible()) self.traveller_type('');
            //if(!self.cover_type_visible()) self.cover_type('');
           // if(!self.region_visible()) self.region('');
           // re seeing some interesting investments via collaborations focusing on the back-end and logistics supply chain and that's going to be absolutely critical for delivering real food security for India. India has got this extraordinary agricultural wealth that could create huge jobs and better living standards for farmers and greater choice for consumers. It's a win-win.if(!self.to_date_visible()) self.to_date('');
            return true;

        }
    };
};
var DATA = DATA || {};
var FORM = new search_model(DATA);
ko.validation.configuration['insertMessages'] = false;
$(function() {
    ko.applyBindings(FORM);
    ko.validation.group(FORM).showAllMessages(false);
    window.scrollTo(0,0);
});

