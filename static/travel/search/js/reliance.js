var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};

var COUNTRIES = ['Afganistan', 'Albania', 'Algeria', 'Andorra', 'Angola', 'Argentina', 'Armenia', 'Aruba', 'Ascension', 'Australia', 'Austria', 'Azerbaijan Republic', 'Azores', 'Bahamas', 'Bermuda', 'Bahrain', 'Bangkok', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bhutan', 'Bolivia', 'Boputatswana', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'Brunei Darussalam', 'Bulgaria', 'Burkina Fasso', 'Burundi', 'Cayman Islands', 'Cambodia', 'Cameroon', 'Canada', 'Canary Island', 'Cape Verde', 'Central African Republic', 'Chad', 'Chile', 'China', 'Ciskei', 'Cocos Keeling Island', 'Colombia', 'Comoros', 'Congo', 'Cook Island', 'Costa Rica', 'Croatia', 'Cuba', 'Cyprus', 'Czech Republic', 'Denmark', 'Diego Garcia', 'Djibouti', 'Dominican Republic', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador Rep.', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Island', 'Fiji Republic', 'Finland', 'Fr.Guinea', 'Fr.Polynesia', 'France', 'France', 'Gabonese Republic', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guinea', 'Guinea Bissau', 'Guyana', 'Haiti', 'Honduras', 'Hongkong', 'Hungary', 'Ivory Coast', 'Iceland', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kirghistan', 'Kiribati', 'Korea (North)', 'Korea (South)', 'Kuwait', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechstein', 'Lithuania', 'Luxembourg', 'MONTENEGRO', 'Macao', 'Macedonia', 'Madagascar', 'Madeira Island', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Mariana Island', 'Marshal Island', 'Martinque', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia', 'Moldova', 'Monaco', 'Mongolia', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'NICARAGUA', 'NORFOLK ISLAND', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'Netherlands Anthilles', 'New Caledonia', 'New Zealand', 'Niger', 'Nigeria', 'Niue Island', 'Norway', 'Oman', 'PUERTO RICO', 'Pakistan', 'Palau', 'Palestine', 'Panama', 'Papau New Guinea', 'Paraguay', 'Peru', 'Phillipines', 'Poland', 'Portugal', 'Qatar', 'Reunion', 'Rodrigues Island', 'Romania', 'Russian Federation', 'Rwandese Republic', 'Serbia', 'SlovakiA', 'St Kitts & Nevis', 'St Lucia (WI)', 'St Vincent & Grenadines (WI)', 'Samoa American', 'Samoa Western', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovak Republic', 'Slovenia', 'Soloman Island', 'Somalia Democratic Republic', 'South Africa', 'Spain', 'Srilanka', 'St.Helena', 'St.Pierre and Miquelon', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Turks & Caicos Islands', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togolese Republic', 'Tonga', 'Transkei', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Tuvalu', 'Uganda', 'USA', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'Uruguay', 'Uzbekistan', 'Virgin Islands (British)', 'Virgin Islands (US)', 'Vanuatu', 'Vatican City State', 'Venda', 'Venezuela', 'Vietnam', 'West Indies', 'Wallis and Futina Island', 'Yemen (Saana)', 'Yogoslavia', 'Zaire', 'Zambia', 'Zimbabwe'];

var DISEASES = ['None', 'Cancer / Leukemia / Malignant Tumor', 'Cardiac ailments', 'COPD', 'HIV /AIDS', 'Insulin Dependent Diabetes', 'Kidney Ailment', 'Liver Disease', 'Neurological Disorder / Stroke / Paralysis', 'Thalasemia', 'Any Other'];

var search_model = function(DATA) {
    var self = this;
    self.activity_id = window.ACTIVITY_ID;
    self.form_visible = ko.observable(!self.activity_id);
    self.cancel = function() {
        self.form_visible(false);
        window.scrollTo(0,0);
    };
    self.modify = function() {
        self.form_visible(true);
        window.scrollTo(0,0);
    };
    self.COUNTRIES = COUNTRIES;
    self.trip_type = ko.observable(DATA.trip_type || 'single');

    self.max_trip_length = ko.observable(DATA.max_trip_length);
    self.max_trip_length_visible = ko.computed(function() {
        return self.trip_type() == 'multiple';
    });


    self.traveller_type_visible = ko.computed(function() {
        return self.trip_type() != 'multiple';
    });
    self.traveller_type = ko.observable(DATA.traveller_type || 'Individual');


    self.cover_type_visible = ko.computed(function() {
        return self.traveller_type_visible() && (self.traveller_type() == 'Family');
    });
    self.cover_type = ko.observable(DATA.cover_type).extend({
        required: {onlyIf: self.cover_type_visible}
    });
    self.cover_type_options = [
        'Applicant + Spouse',
        'Applicant + Spouse + Child1',
        'Applicant + Spouse + Child1 + Child2',
        'Applicant + Child1',
        'Applicant + Child1 + Child2'
    ];


    self.region_visible = ko.computed(function() {
        return self.traveller_type_visible() && (self.traveller_type() == 'Individual');
    });
    self.region = ko.observable(DATA.region).extend({
        required: {onlyIf: self.region_visible}
    });
    //self.region_options = [
    //    {id: 'Asia', name:'Asia (except Japan)'},
    //    {id: 'Schengen', name: 'Schengen'},
    //    {id: 'World except USA & Canada', name: 'World except USA & Canada'},
    //    {id: 'World including Japan, USA & Canada', name: 'World including USA & Canada'}
    //];
    self.region_options = [
        'Asia (except Japan)',
        'Schengen',
        'World except USA & Canada',
        'World including USA & Canada'
    ];


    self.visiting_usa_visible = ko.computed(function() {
        return (self.trip_type() == 'multiple') || (self.traveller_type() != 'Individual');
    });
    self.visiting_usa = ko.observable(DATA.visiting_usa).extend({
        required: {onlyIf: self.visiting_usa_visible}
    });


    self.from_date = ko.observable(DATA.from_date).extend({
        required: true,
        validation: date_validation
    });

    self.to_date_visible = ko.computed(function() {
        return self.trip_type() != 'multiple';
    });
    self.to_date = ko.observable(DATA.to_date).extend({
        required: {onlyIf: self.date_range_visible},
        validation: date_validation
    });


    self.dob = ko.observable(DATA.dob).extend({
        required: true,
        validation: date_validation
    });

    // Addon Benefits
    self.addon_benefits_visible = ko.computed(function() {
        return self.traveller_type() == 'Student';
    });
    self.addon_benefits = ko.observable(DATA.addon_benefits).extend({
        required: {onlyIf: self.addon_benefits_visible}
    });

    self.indian_citizen = ko.observable(DATA.indian_citizen).extend({required: true});

    // Is OCI / NONOCI?
    self.is_oci_or_nonoci_visible = ko.computed(function() {
        return self.indian_citizen() == 'no';
    });
    self.is_oci_or_nonoci = ko.observable(DATA.is_oci_or_nonoci).extend({
        required: {onlyIf: self.is_oci_or_nonoci_visible}
    });

    // Is OCI or NONOCI
    self.oci_or_nonoci_visible = ko.computed(function() {
        return self.is_oci_or_nonoci_visible() && (self.is_oci_or_nonoci() == 'yes');
    });
    self.oci_or_nonoci = ko.observable(DATA.oci_or_nonoci).extend({
        required: {onlyIf: self.oci_or_nonoci_visible}
    });

    // OCI Selected
    self.oci_selected = ko.computed(function() {
        return self.oci_or_nonoci_visible() && (self.oci_or_nonoci() == 'OCI');
    });
    self.country_of_stay = ko.observable(DATA.country_of_stay).extend({
        required: {onlyIf: self.oci_selected}
    });
    self.oci_number = ko.observable().extend({
        required: {onlyIf: self.oci_selected}
    });

    // Non OCI Selected
    self.non_oci_selected = ko.computed(function() {
        return self.oci_or_nonoci_visible() && (self.oci_or_nonoci() == 'NONOCI');
    });
    self.passport_issuer = ko.observable(DATA.passport_issuer).extend({
        required: {onlyIf: self.non_oci_selected}
    });

    // Emmigration Visa
    self.immigration_visa_visible = ko.computed(function() {
        return self.trip_type() != 'multiple';
    });
    self.immigration_visa = ko.observable(DATA.immigration_visa).extend({
        required: {onlyIf: self.immigration_visa_visible}
    });

    self.sports = ko.observable(DATA.sports || 'no').extend({required: true});

    self.ped = ko.observable(DATA.ped || 'None');


    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  $('p.validationMessage').filter(':visible').length;
        if(errors > 0) return false;
        else {
            if(!self.max_trip_length_visible()) self.max_trip_length('');
            if(!self.traveller_type_visible()) self.traveller_type('');
            if(!self.cover_type_visible()) self.cover_type('');
            if(!self.region_visible()) self.region('');
            if(!self.visiting_usa_visible()) self.visiting_usa('');
            if(!self.to_date_visible()) self.to_date('');
            if(!self.addon_benefits_visible()) self.addon_benefits('');
            if(!self.is_oci_or_nonoci_visible()) self.is_oci_or_nonoci('');
            if(!self.oci_or_nonoci_visible()) self.oci_or_nonoci('');
            if(!self.oci_selected()) self.country_of_stay('');
            if(!self.oci_selected()) self.oci_number('');
            if(!self.non_oci_selected()) self.passport_issuer('');
            if(!self.immigration_visa_visible()) self.immigration_visa('');
            return true;
        }
    };
};
var DATA = DATA || {};
var FORM = new search_model(DATA);
ko.validation.configuration['insertMessages'] = false;
$(function() {
    ko.applyBindings(FORM);
    ko.validation.group(FORM).showAllMessages(false);
    window.scrollTo(0,0);
});
