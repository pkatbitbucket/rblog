
var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};

var PLANS = {'Explore - Asia' : ['US $ 25,000', 'US $ 50,000', 'US $ 1,00,000'],
	     'Explore - Africa' : ['US $ 25,000', 'US $ 50,000', 'US $ 1,00,000'],
	     'Explore - Canada' : ['US $ 50,000', 'US $ 1,00,000'],
	     'Explore - Gold - Worldwide - Excluding US & Canada' : ['US $ 50,000', 'US $ 1,00,000', 'US $ 3,00,000', 'US $ 5,00,000'],
	     'Explore - Gold - Worldwide' : ['US $ 50,000', 'US $ 1,00,000', 'US $ 3,00,000', 'US $ 5,00,000'],
	     'Explore - Platinum - Worldwide - Excluding US & Canada' : ['US $ 50,000', 'US $ 1,00,000', 'US $ 3,00,000', 'US $ 5,00,000'],
	     'Explore - Platinum - Worldwide' : ['US $ 50,000', 'US $ 1,00,000', 'US $ 3,00,000', 'US $ 5,00,000'],
	     'Explore - Europe' : ['EUR 30000', 'EUR 1,00,000']};

NO_OF_FAMILY_MEMBERS = [1,2,3,4,5,6]

AGE_BANDS = ['up to 40 Years',
	     '41 Years - 60 Years',
	     '61  Years - 70 Years',
	     '> 70 Years']

var member_model = function(type, DATA){
    var self = this;
    self.type = type;
    self.age_band = ko.observable(DATA[self.type+'_age_band']).extend({required:true});
    self.travellers_age_bands_options = AGE_BANDS;
};

var search_model = function(DATA) {
    var self = this;
    self.activity_id = window.ACTIVITY_ID;
    self.form_visible = ko.observable(!self.activity_id);
    self.cancel = function() {
        self.form_visible(false);
        window.scrollTo(0,0);
    };
    self.modify = function() {
        self.form_visible(true);
        window.scrollTo(0,0);
    };
    self.trip_type = ko.observable(DATA.trip_type || 'single');
    
    self.max_trip_length = ko.observable(DATA.max_trip_length).extend({
	required : true
	});
    self.max_trip_length_visible = ko.computed(function() {
        return self.trip_type() == 'multiple';
    });
    
    //self.traveller_type = ko.observable(DATA.traveller_type || 'Individual');
    
    self.plan = ko.observable(DATA.plan).extend({required:true});
    
    self.no_of_travellers_options = NO_OF_FAMILY_MEMBERS;
    
    self.no_of_travellers = ko.observable(DATA.no_of_travellers || 1).extend({
	required : true
	});
    
    self.member_list = ko.computed(function(){
	var member_list = []
	for (i = 1; i <= self.no_of_travellers(); i++)
	{
	    member_list.push(new member_model('member'+i, DATA));
	}
	
	return member_list;
    });
    
    self.plan_options = ko.computed(function() {
	if (self.trip_type() == 'multiple')
	    {
		return ['Explore - Gold - Worldwide - Excluding US & Canada',
			  'Explore - Gold - Worldwide',
			  'Explore - Platinum - Worldwide - Excluding US & Canada',
			  'Explore - Platinum - Worldwide'];
	    }
	else
	    {
		return ['Explore - Asia',
			  'Explore - Africa',
			  'Explore - Canada',
			  'Explore - Gold - Worldwide - Excluding US & Canada',
			  'Explore - Gold - Worldwide',
			  'Explore - Platinum - Worldwide - Excluding US & Canada',
			  'Explore - Platinum - Worldwide',
			  'Explore - Europe'];
		}
    });

    self.sum_insured_options = ko.computed(function() {
	if(!self.plan())
	    return [];
	else
	{
	    return PLANS[self.plan()]
	}
    });
    
    self.from_date = ko.observable(DATA.from_date).extend({
        required: true,
        validation: date_validation
    });

    self.to_date = ko.observable(DATA.to_date).extend({
        required: true,
        validation: date_validation
    });
   
    /*self.dob = ko.observable(DATA.dob).extend({
        required: true,
        validation: date_validation
    });*/

    self.sum_insured = ko.observable(DATA.sum_insured).extend({required:true});
    
    self.to_date_visible = ko.computed(function() {
        return self.trip_type() != 'multiple';
    });

/*--  self.no_of_travellers_visible = ko.computed(function() { 
	return self.traveller_type() == 'FAMILYFLOATER';
	});*/
    
     self.validate_form = function() {
	 var validation_group = ko.validation.group(self);
         validation_group.showAllMessages();
         var errors =  $('p.validationMessage').filter(':visible').length;
         if(errors > 0) return false;
	 else
	     {
		 if(!self.max_trip_length_visible()) self.max_trip_length('');
		 //if(!self.no_of_travellers_visible()) self.no_of_travellers(1);
		 return true;
	     }
     };

}

var DATA = DATA || {};
var FORM = new search_model(DATA);
ko.validation.configuration['insertMessages'] = false;
$(function() {
    ko.applyBindings(FORM);
    ko.validation.group(FORM).showAllMessages(false);
    window.scrollTo(0,0);
});
