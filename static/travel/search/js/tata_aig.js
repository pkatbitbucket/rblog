var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};
function get_age(dob) {
    dob = $.datepick.parseDate("dd-M-yyyy", dob);
    var today = new Date();
    var age_str, age_in_years, age_in_mths;
    var ydiff = today.getFullYear() - dob.getFullYear();
    var mdiff = today.getMonth() - dob.getMonth();
    var ddiff = today.getDate() - dob.getDate();

    age_in_years = ydiff;
    if(mdiff < 0 || (mdiff == 0 && ddiff < 0)) --age_in_years;
    if (age_in_years > 0) {
        age_str = age_in_years + (age_in_years == 1 ? ' year' : ' years');
    }
    else {
        if(ydiff == 1)  mdiff = 12 + mdiff;
        age_in_mths = mdiff - (ddiff >= 0 ? 0 : 1);
        age_str = age_in_mths + (age_in_mths == 1 ? ' mth' : ' mths');
    }
    return (age_in_years > 0) ? age_in_years : (age_in_mths / 12);
}

var AGE_LIMITS = {
    'Annual Gold': {'min': 19, 'max': 71},
    'Annual Platinum': {'min': 19, 'max': 71},
    'Gold': {'min': 0.5, 'max': 71},
    'Plan A': {'min': 16, 'max': 36},
    'Plan B': {'min': 16, 'max': 36},
    'Platinum': {'min': 56, 'max': 71},
    'Senior': {'min': 71, 'max': 86},
    'Silver': {'min': 0.5, 'max': 71},
    'Silver Plus': {'min': 0.5, 'max': 71},
    'Ultimate': {'min': 16, 'max': 36},
};
var search_model = function(DATA) {
    var self = this;
    self.activity_id = window.ACTIVITY_ID;
    self.form_visible = ko.observable(!self.activity_id);
    self.cancel = function() {
        self.form_visible(false);
        window.scrollTo(0,0);
    };
    self.modify = function() {
        self.form_visible(true);
        window.scrollTo(0,0);
    };
    self.travel_type = ko.observable(DATA.travel_type || 'NON_SCH');
    self.sum_assured = ko.observable(DATA.sum_assured);
    self.schengen = ko.observable(DATA.schengen);
    self.policy_type = ko.observable(DATA.policy_type);
    self.policy_type_options = [
        {id: 'I', name: 'Individual'},
        {id: 'S', name: 'Student'}
    ];
    self.trip_type_visible = ko.computed(function(){
     return  self.policy_type()!='S';
    });
    self.trip_type = ko.observable(DATA.trip_type).extend({
        required: {onlyIf: self.trip_type_visible}
    });
    self.plan_type = ko.observable(DATA.plan_type).extend({required: true});
    self.plan_type_options =  ko.computed(function() {
       if (self.trip_type() == 'A')

          return [
        {id: 'Gold', name: 'Gold'},
        {id: 'Platinum', name: 'Platinum'},
          ];
          else  if (self.policy_type() == 'I')
       return  [
        {id: 'Gold', name: 'Gold'},
        {id: 'Silver', name: 'Silver'},
        {id: 'Silver Plus', name: 'Silver Plus'},
        {id: 'Platinum', name: 'Platinum'},
        {id: 'Senior', name: 'Senior'},
    ];
       else if (self.policy_type() == 'S')
          return [
        {id: 'Plan A', name: 'Plan A'},
        {id: 'Plan B', name: 'Plan B'},
        {id: 'Ultimate', name: 'Ultimate'},
          ];

    });
    self.sum_assured_options = ko.computed(function() {
        if (self.policy_type() == 'S')
            return [
                {id: '50,000 USD', name: '50,000 USD'},
                {id: '100,000 USD', name: '100,000 USD'},
                {id: '250,000 USD', name: '250,000 USD'},
            ];
            else if(self.trip_type() == 'S')
            return [
                {id: '50,000 USD', name: '50,000 USD'},
                {id: '100,000 USD', name: '100,000 USD'},
                {id: '250,000 USD', name: '250,000 USD'},
                {id: '500,000 USD', name: '500,000 USD'},
            ];
        else if (self.trip_type() == 'A')
            return [
                {id: '250,000 USD', name: '250,000 USD'},
                {id: '500,000 USD', name: '500,000 USD'},
            ];
    });
    self.max_trip_length = ko.observable(DATA.max_trip_length);
    self.max_trip_length_visible = ko.computed(function(){
        return self.trip_type() == 'A';
    });
    self.geographical_coverage = ko.observable(DATA.geographical_coverage).extend({required: true});
    self.geographical_coverage_options = ko.computed(function() {
        if(self.policy_type() == 'S')
            return [
                {id: 'World wide including Americas', name: 'Including Americas'},
                {id: 'World wide excluding Americas', name: 'Excluding Americas'},
                {id: 'Asia', name: 'Asia'},
            ];
        else
            return [
                {id: 'Worldwide including Americas', name: 'Including Americas'},
                {id: 'Worldwide excluding Americas', name: 'Excluding Americas'},
                {id: 'Asia', name: 'Asia'},
            ];
    });
    self.from_date = ko.observable(DATA.from_date).extend({
        required: true,
        validation: {
            validator: function(val) {
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var d1 = new Date();
                var d2 = $.datepick.parseDate("dd-M-yyyy", val);
                return d2 > d1;
            },
            message:'Travel date should be in future'
            }
    });

    self.to_date_visible = ko.computed(function() {
        return !(self.trip_type() == 'A' || self.policy_type()=='S');
    });

    self.to_date = ko.observable(DATA.to_date).extend({
        required: {onlyIf: self.to_date_visible},
        validation: {
            validator: function(val) {
                if(!self.to_date_visible()) return true;
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var d1 = self.from_date();
                var d2 = val;
                if(d1) {
                    d1 = $.datepick.parseDate("dd-M-yyyy", d1);
                    d2 = $.datepick.parseDate("dd-M-yyyy", d2);
                    this.message = 'To date should be greater than from date';
                    return d2 > d1;
                }
                return true;
            }
        }
    });

    self.trip_duration = ko.computed(function() {
        var d1 = self.from_date();
        var d2 = self.to_date();
        if(d1 && d2) {
            d1 = $.datepick.parseDate("dd-M-yyyy", d1);
            d2 = $.datepick.parseDate("dd-M-yyyy", d2);
            return (d2 - d1) / 86400000 + 1;
        }
        else
            return '';
    });
    self.DOB = ko.observable(DATA.DOB).extend({
        required: true,
        validation: {
            validator: function(val) {
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                if(!self.plan_type()) return true;
                var age = get_age(val);
                var age_limits = AGE_LIMITS[self.plan_type()];
                var valid_age = (age >= age_limits.min && age < age_limits.max);
                if(!valid_age) {
                    var min_age = age_limits.min < 1 ? Math.floor(age_limits.min * 12)+' months' : age_limits.min+' years';
                    var max_age = (age_limits.max - 1) + ' years';
                    this.message = 'Sorry! The age limit for this plan is from ' + min_age + ' to ' + max_age;
                }
                return valid_age;
            }
        }
    });

    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  $('p.validationMessage').filter(':visible').length;
        if(errors > 0) return false;
        else {
            if(self.policy_type()=='S') self.trip_type('S');
            //if(!self.traveller_type_visible()) self.traveller_type('');
            //if(!self.cover_type_visible()) self.cover_type('');
           // if(!self.region_visible()) self.region('');
           // if(!self.to_date_visible()) self.to_date('');
            return true;

        }
    };
};
var DATA = DATA || {};
var FORM = new search_model(DATA);
ko.validation.configuration['insertMessages'] = false;
$(function() {
    ko.applyBindings(FORM);
    ko.validation.group(FORM).showAllMessages(false);
    window.scrollTo(0,0);
});
