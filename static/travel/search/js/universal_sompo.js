
var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};

var PLANS = {'Travel-Worldwide' : ['Platinum','Gold', 'Silver'],
             'Travel-Worldwide Excluding US & Canada' : ['Platinum','Gold', 'Silver'],
             'Annual Multi-Trip' : ['Platinum','Gold'],
             'Student-Travel' : ['Primary','Buddy','Intellectual'],
             'Student-Travel Excluding Us & Canada' : ['Primary','Buddy','Intellectual'],
             'Travel-Asia' : ['Platinum','Gold'],
	         };

NO_OF_FAMILY_MEMBERS = [1,2,3,4,5,6]

AGE_BANDS = ['up to 40 Years',
	     '41 Years - 60 Years']

AGE_BAND_STUDENT =['16 Years-35 Years']

var member_model = function(type, DATA, plan){
    var self = this;
    self.type = type;
    self.age_band = ko.observable(DATA[self.type+'_age_band']).extend({required:true});
    self.travellers_age_bands_options = ko.computed(function(){
	if (plan == 'Student-Travel' || plan == 'Student-Travel Excluding Us & Canada')
	    return AGE_BAND_STUDENT;
	else
	    return AGE_BANDS;
    });
    self.plan_type = ko.observable(DATA[self.type+'_plan_type']).extend({required:true});
    self.plan_type_options = ko.computed(function() {
    if(!plan)
        return [];
    else
        return PLANS[plan]
    });
};



var search_model = function(DATA) {
    var self = this;
    self.activity_id = window.ACTIVITY_ID;
    self.form_visible = ko.observable(!self.activity_id);
    self.cancel = function() {
        self.form_visible(false);
        window.scrollTo(0,0);
    };
    self.modify = function() {
        self.form_visible(true);
        window.scrollTo(0,0);
    };
    self.trip_type = ko.observable(DATA.trip_type || 'single');

    self.max_trip_length = ko.observable(DATA.max_trip_length).extend({
	required : true
	});
    self.max_trip_length_visible = ko.computed(function() {
        return self.trip_type() == 'multiple';
    });

    //self.traveller_type = ko.observable(DATA.traveller_type || 'Individual');

    self.plan = ko.observable(DATA.plan).extend({required:true});

    self.no_of_travellers_options = NO_OF_FAMILY_MEMBERS;

    self.no_of_travellers = ko.observable(DATA.no_of_travellers || 1).extend({
	required : true
	});

    self.member_list = ko.computed(function(){
	var member_list = []
	for (i = 1; i <= self.no_of_travellers(); i++)
	{
	    member_list.push(new member_model('member'+i, DATA, self.plan()));
	}

	return member_list;
    });

    self.plan_options = ko.computed(function() {
	if (self.trip_type() == 'multiple')
	    {
		return ['Annual Multi-Trip'];
	    }
	else
	    {
		return ['Travel-Worldwide' ,
             'Travel-Worldwide Excluding US & Canada',
             //'Student-Travel' ,
             //'Student-Travel Excluding Us & Canada' ,
             'Travel-Asia']
		}
    });


    self.from_date = ko.observable(DATA.from_date).extend({
        required: true,
        validation: {
            validator: function(val) {
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var d1 = new Date();
                var d2 = $.datepick.parseDate("dd-M-yyyy", val);
                return d2 > d1;
            },
            message:'Travel date should be in future'
            }
    });

     self.to_date_visible = ko.computed(function() {
        return self.trip_type() != 'multiple';
    });

    self.to_date = ko.observable(DATA.to_date).extend({
        required: {onlyIf: self.to_date_visible},
        validation: {
            validator: function(val) {
                if(!self.to_date_visible()) return true;
                if(!date_validation.validator(val)) {
                    this.message = date_validation.message;
                    return false;
                }
                var d1 = self.from_date();
                var d2 = val;
                if(d1) {
                    d1 = $.datepick.parseDate("dd-M-yyyy", d1);
                    d2 = $.datepick.parseDate("dd-M-yyyy", d2);
                    this.message = 'To date should be greater than from date';
                    return d2 > d1;
                }
                return true;
            }
        }
    });

    /*self.dob = ko.observable(DATA.dob).extend({
        required: true,
        validation: date_validation
    });*/



    self.to_date_visible = ko.computed(function() {
        return self.trip_type() != 'multiple';
    });

/*--  self.no_of_travellers_visible = ko.computed(function() {
	return self.traveller_type() == 'FAMILYFLOATER';
	});*/

     self.validate_form = function() {
	 var validation_group = ko.validation.group(self);
         validation_group.showAllMessages();
         var errors =  $('p.validationMessage').filter(':visible').length;
         if(errors > 0) return false;
	 else
	     {
		 if(!self.max_trip_length_visible()) self.max_trip_length('');
		 //if(!self.no_of_travellers_visible()) self.no_of_travellers(1);
		 return true;
	     }
     };

}

var DATA = DATA || {};
var FORM = new search_model(DATA);
ko.validation.configuration['insertMessages'] = false;
$(function() {
    ko.applyBindings(FORM);
    ko.validation.group(FORM).showAllMessages(false);
    window.scrollTo(0,0);
});
