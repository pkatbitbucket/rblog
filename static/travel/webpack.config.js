var path = require('path');
var ContextReplacementPlugin = require("webpack/lib/ContextReplacementPlugin");
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

module.exports = {
    context: __dirname,
    entry: {
        buy: "./buy/app/entry.js",
    },
    resolve: {
        modulesDirectories: [
            'node_modules',
        ],
        alias: {
            'form-models': __dirname + '/buy/app/lib/form-models',
            'utils': __dirname + '/buy/app/lib/utils',
            'cookie': __dirname + '/buy/app/lib/cookies',
        }
    },
    resolveLoader: { root: __dirname + "node_modules" },

    module: {
        noParse: [
            '/knockout-latest.js/',
        ],
        preLoaders: [
            /*{
                test: /\.js$/,
                exclude: /global\/lib/,
                loader: "jshint-loader",
            }*/
        ],
        loaders: [
            { test: /knockout-latest.js/, loader: "imports?require=>__webpack_require__" },
            //{ test: /knockout-3\.2\.0\.js/, loader: "imports?require=>false" },
            //{ test: /knockout-amd-desktop\.js/, loader: "imports?config=>{templates_dir: '', models_dir: ''}" },
            //{ test: /knockout-amd-buy\.js/, loader: "imports?config=>{templates_dir: '', models_dir: ''}" },
            //{ test: /knockout-amd-webpack\.js/, loader: "imports?config=>{templates_dir: '', models_dir: ''}" },
            //{ test: /require\.js$/, loader: "exports?requirejs!script" },
	        //{test: /pikaday.js/, loader: "imports?require=>false"},
        ]
    },

    plugins: [
        function() {
            this.plugin("done", function(stats) {
                require("fs").writeFileSync(
                    path.join(__dirname, "./build", "stats.json"),
                    JSON.stringify(stats.toJson())
                );
                require("fs").utimesSync(
                    path.join(__dirname, "../../base/settings.py"),
                    new Date(),
                    new Date()
                );

            });
        },
    ],

    jshint: {
    },

    output: {
        path: __dirname + "/build/",
        publicPath : '/static/travel/build/',
        pathInfo : true,
        filename: "travel.bundle.js",
    },
};
