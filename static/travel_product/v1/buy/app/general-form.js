define(['knockout','jquery'], function(ko,$) {

    var occupations_list = form_details.occupations_list;

    var salutations_list = form_details.salutations_list || [];

    var states_list = form_details.states_list;

    var cities_list = form_details.cities_list;

    var relations_list = form_details.relations_list || [];

    var nominee_relations_list = form_details.nominee_relations_list || relations_list;

    var gender_list = form_details.gender_list || [{'key':'Male','value':'Male'},{'key':'Female','value':'Female'}];

    var purpose_of_travel_list = form_details.purpose_of_travel_list || [];

    var current_city_list = ko.observableArray([]);


    function get_state(field, id, value){
        $.ajax({
            url: '/travel-insurance/get-state-city/'+value,
            method: 'GET',
            success: function(resp){
                if(!resp.error){
                    console.log("RESP: ", resp);
                    // patch for hdfc insurer
                    if (insurer_slug == "hdfc-ergo"){
                        for (state in form_details.states_list){
                            if (form_details.states_list[state].key == resp.state.toUpperCase()){
                                var state_code = form_details.states_list[state].value
                                }
                            }
                        cities = form_details.cities_list[state_code]
                        for (city in cities){
                            if (cities[city].key == resp.city.toUpperCase()){
                                var city_code = cities[city].value
                            }
                        }
                        field.Value(state_code);
                        field.parent.getFieldById('com_city').Value(city_code);
                    }
                    else{
                        field.Value(resp.state);
                        field.parent.getFieldById('com_city').Value(resp.city);
                    }

                }
                else{
                    field.Value("");
                    field.parent.getFieldById('com_city').Value("");
                }
            }
        });

    }
    function getCityList(field,id,value){
        var state;
        for(var i in states_list){
            if(states_list[i].value == value){
                state = value;
                break;
            }
        }
        if(state && cities_list[state]) field.OptionsList(cities_list[state]);
        else current_city_list([]);
    }
    proposer_template = [{
        'name': 'Proposer Information',
        'type': 'container',
        'id': 'proposer-details',
        'cssclass': 'stage-subheader',
        'children': [{
            'name': 'Name',
            'type': 'container',
            'id'  : 'name',
            'cssclass': 'stage-molecule',
            'children': [
                {
                    'id': 'salutation',
                    'name': 'salutation',
                    'type': 'select',
                    'options': salutations_list,
                    'optionsText':'key',
                    'optionsValue':'value',
                },
                {
                    'id': 'first_name',
                    'name': 'first_name',
                    'type': 'text',
                    'validators': ['NonEmpty','NameValidation'],
                    'maxlength': 50,
                    'clean': 'UpperCase',
                    'title': 'First Name',
                    'cssClass': 'uppercase'
                },
                {
                    'id': 'last_name',
                    'name': 'last_name',
                    'type': 'text',
                    'validators': ['NonEmpty','NameValidation'],
                    'maxlength': 50,
                    'clean': 'UpperCase',
                    'title': 'Last Name',
                    'cssClass': 'uppercase'
                },
            ]
        },{
            'name': 'contact details',
            'type': 'container',
            'id'  : 'contact_details_container',
            'cssclass':'stage-molecule',
            'children':[{
                'id':'mobile',
                'name':'mobile',
                'type':'text',
                'title':'mobile no.',
                'validators':['NonEmpty','PhoneValidation'],
            },{
                'id':'email',
                'name':'email',
                'type':'text',
                'title':'email id.',
                'validators':['NonEmpty','EmailValidation'],
            }],
        },
        {
            'name': 'Date Of Birth <Br/> (dd-mm-yyyy)',
            'type': 'container',
            'id': 'dob',
            'cssclass': 'stage-molecule',
            'children': [{
                'id': 'dob',
                'name': 'dob',
                'type': 'text',
                'validators': ['NonEmpty', 'DOBValidation'],
                'maxlength': 10,
                'title': 'DD-MM-YYYY',
                'clean': 'DOBClean',
            }, ],
        },
        {
            'name': 'Nominee Details',
            'type': 'container',
            'id': 'nominee_container',
            'cssclass': 'stage-molecule',
            'visible' : false,
            'children': [{
                'id': 'nominee',
                'name': 'nominee',
                'type': 'text',
                'validators': ['NonEmpty','NameValidation'],
                'maxlength': 10,
                'title': 'Full name',
            }, ],
        }
        ]
    }];


    travellor_template = [{
        'name': 'Traveller Information',
        'type': 'container',
        'id': 'traveller-details',
        'cssclass': 'stage-subheader',
        'children': [{
            'name': 'Name',
            'type': 'container',
            'id': 'name',
            'cssclass': 'stage-molecule',
            'children': [{
                'id': 'salutation',
                'name': 'salutation',
                'type': 'select',
                'options': salutations_list,
                'optionsText':'key',
                'optionsValue':'value',
                'caption':'select',
                'validators':['NonEmpty']
            }, {
                'id': 'cust_first_name',
                'name': 'cust_first_name',
                'type': 'text',
                'validators': ['NonEmpty','NameValidation'],
                'maxlength': 50,
                'clean': 'UpperCase',
                'title': 'First Name',
                'cssClass': 'uppercase',
            },
            {
                'id': 'cust_last_name',
                'name': 'cust_last_name',
                'type': 'text',
                'validators': ['NonEmpty','NameValidation'],
                'maxlength': 50,
                'clean': 'UpperCase',
                'title': 'Last Name',
                'cssClass': 'uppercase',
            }, ]
        },{
            'name': 'contact details',
            'type': 'container',
            'id'  : 'contact_details_container',
            'cssclass':'stage-molecule',
            'visible':false,
            'children':[{
                'id':'com_mobile',
                'name':'com_mobile',
                'type':'text',
                'title':'mobile no.',
                'validators':['NonEmpty','PhoneValidation'],
            },{
                'id':'com_email',
                'name':'com_email',
                'type':'text',
                'title':'email id.',
                'validators':['NonEmpty','EmailValidation'],
            }],
        },{
            'name':'Purpose of visit',
            'type': 'container',
            'id' : 'purpose_of_travel',
            'cssclass': 'stage-molecule',
            'visible':false,
            'children': [{
                'name': 'com_purpose',
                'id': 'com_purpose',
                'type': 'select',
                'validators': ['NonEmpty'],
                'options':purpose_of_travel_list,
                'optionsText':'key',
                'optionsValue':'value',
            }]
        },{
            'name': 'Marital Status',
            'type': 'container',
            'id': 'marital_status',
            'cssclass': 'stage-molecule',
            'visible': false,
            'children': [{
                'id': 'cust_marital_status',
                'name': 'cust_marital_status',
                'type': 'select',
                'options': ['Single','Married'],
            }, ],
        },{
            'name': 'Date Of Birth',
            'type': 'container',
            'id': 'dob',
            'cssclass': 'stage-molecule',
            'children': [{
                'id': 'cust_dob',
                'name': 'cust_dob',
                'type': 'date',
                'validators': ['NonEmpty','DOBValidation'],
                'title': 'DD-MM-YYYY',
                'maxAge':100,
                'minAge':0,
            }, ],
        },{
            'name':'RelationShip With Primary Member',
            'type':'container',
            'id':'relationshipwithprimary',
            'cssclass': 'stage-molecule',
            'children': [{
                'id': 'cust_relationship_with_primary',
                'name': 'cust_relationship_with_primary',
                'type': 'select',
                'options':relations_list,
                'optionsText': 'key',
                'optionsValue': 'value',
                'caption':'select relation',
                'validators':['NonEmpty']
            }, ],
        },{
            'name': 'Occupation',
            'type': 'container',
            'id': 'occupation',
            'cssclass': 'stage-molecule',
            'children': [{
                'id': 'cust_occupation',
                'name': 'cust_occupation',
                'type': 'select',
                'options': occupations_list,
                'optionsText': 'key',
                'optionsValue': 'value',
                'caption':'select occupation',
                'validators':['NonEmpty'],
            }, ],
        }, {
            'name': 'Passport Number',
            'type': 'container',
            'id': 'passport',
            'cssclass': 'stage-molecule',
            'children': [{
                'id': 'cust_passport',
                'name': 'cust_passport',
                'type': 'text',
                'validators': ['NonEmpty'],
                'title': 'e.g. H1234567',
                'clean': 'UpperCase',
                'cssClass': 'uppercase',
            },{
                'id':   'cust_passport_expiry',
                'name': 'cust_passport_expiry',
                'type': 'text',
                'title': 'expiry date:DD-MM-YYYY',
                'visible':false,
                'validators':['NonEmpty','DateValidation'],
            }],
        },{
            'name': 'Visa Type',
            'type': 'container',
            'id'  : 'visa',
            'cssclass': 'stage-molecule',
            'visible' : false,
            'children': [{
                'id':   'cust_visa_type',
                'name': 'cust_visa_type',
                'type': 'text',
            }]
        }, {
            'name': 'Nominee details',
            'type': 'container',
            'id': 'nominee',
            'cssclass': 'stage-molecule',
            'children': [{
                'id': 'nominee_name',
                'name': 'nominee_name',
                'type': 'text',
                'validators': ['NonEmpty','NameValidation'],
                'maxlength': 50,
                'title': 'Full Name',
            }, {
                'id': 'nominee_age',
                'name': 'nominee_age',
                'type': 'text',
                'validators': ['NonEmpty', 'NumberValidation', 'NomineeAgeValidation'],
                'maxlength': 2,
                'title': 'Age',
            }, {
                'id': 'nominee_relationship',
                'name': 'nominee_relationship',
                'type': 'select',
                'options': nominee_relations_list,
                'optionsText':'key',
                'optionsValue':'value',
                'caption':'select relation',
                'validators':['NonEmpty'],
            }],
        },{
            'name': 'Appointee details',
            'type': 'container',
            'id': 'appointee',
            'cssclass': 'stage-molecule',
            'visible':false,
            'children': [{
                'id': 'appointee_name',
                'name': 'appointee_name',
                'type': 'text',
                'validators': ['NonEmpty','NameValidation'],
                'maxlength': 50,
                'title': 'Full Name',
            }, {
                'id': 'appointee_relationship',
                'name': 'appointee_relationship',
                'type': 'select',
                'options': nominee_relations_list,
                'optionsText':'key',
                'optionsValue':'value',
                'caption':'select relation',
                'validators':['NonEmpty'],
            }],
        }],
    }];

    var contact_form = [{
        'name': 'Contact Details',
        'type': 'container',
        'id': 'contact-details',
        'cssclass': 'stage-header',
        'children': [{
            'name': 'Communication Address',
            'type': 'container',
            'id': 'add-details',
            'cssclass': 'stage-subheader',
            'children': [{
                'name': 'Email Id',
                'type': 'container',
                'id': 'com_email_address',
                'cssclass': 'stage-molecule',
                'visible':false,
                'children': [{
                    'name': 'com_email',
                    'id': 'com_email',
                    'type': 'text',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'Email Id'
                }]
            },{
                'name': 'Mobile',
                'type': 'container',
                'id': 'com_mobile_container',
                'cssclass': 'stage-molecule',
                'visible':false,
                'children': [{
                    'name': 'com_mobile',
                    'id': 'com_mobile',
                    'type': 'text',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'Mobile'
                }]
            },{
                'name': 'Address Line 1',
                'type': 'container',
                'id': 'com_address_1',
                'cssclass': 'stage-molecule',
                'children': [{
                    'name': 'com_address_line_1',
                    'id': 'com_address_line_1',
                    'type': 'text',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'Address Line 1'
                }]
            },
            {
                'name': 'Address Line 2',
                'type': 'container',
                'id': 'com_address_2',
                'cssclass': 'stage-molecule',
                'children': [{
                    'name': 'com_address_line_2',
                    'id': 'com_address_line_2',
                    'type': 'text',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'Address Line 2'
                }]
            },
            {
                'name': 'Address Line 3',
                'type': 'container',
                'id': 'com_address_3',
                'cssclass': 'stage-molecule',
                'children': [{
                    'name': 'com_address_line_3',
                    'id': 'com_address_line_3',
                    'type': 'text',
                    'maxlength': 30,
                    'validators': ['NonEmpty'],
                    'title': 'Address Line 3'
                }]
            }, {
                'name': 'Pincode',
                'type': 'container',
                'id': 'com_pincode_container',
                'cssclass': 'stage-molecule',
                'children': [{
                    'id': 'com_pincode',
                    'name': 'com_pincode',
                    'type': 'text',
                    'validators': ['NonEmpty', 'PincodeValidation'],
                    'maxlength': 6,
                    'dependents': ['com_state'],
                    'title': 'e.g. 400063'
                }, ]
            },{
                'name': 'State and City',
                'type': 'container',
                'id': 'com_city_state_container',
                'cssclass': 'stage-molecule',
                'visible': true,
                'children': [{
                    'id': 'com_state',
                    'name': 'com_state',
                    'type': 'select',
                    'validators': ['NonEmpty'],
                    'options': states_list,
                    'optionsText': 'key',
                    'optionsValue': 'value',
                    'dependents':['com_city'],
                    'title': 'State',
                    'otherFieldChanged':get_state,
                    'caption':'--Select State--',
                },{
                    'id': 'com_city',
                    'name': 'com_city',
                    'type': 'select',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'City',
                    'otherFieldChanged':getCityList,
                    'options':current_city_list,
                    'optionsText':'key',
                    'optionsValue':'value',
                    'caption':'--Select City--',
                }]
            }, {
                'name': 'Overseas contact',
                'type': 'container',
                'id': 'overseas_contact_container',
                'cssclass': 'stage-molecule',
                'visible': false,
                'children': [{
                    'id': 'overseas_contact_number',
                    'name': 'overseas_contact_number',
                    'type': 'text',
                    'validators': ['NonEmpty','NumberValidation'],
                    'dependents': [],
                    'title': 'contact number'
                },{
                    'id': 'overseas_contact_address',
                    'name': 'overseas_contact_address',
                    'type': 'text',
                    'validators': ['NonEmpty'],
                    'dependents': [],
                    'title': 'address'
                }]
            }]
        }, {
            'id': 'add-same',
            'name': 'add-same',
            'type': 'checkbox',
            'checked': true,
            'visible': false,
            'dependents': ['reg-add-details'],
            'clean': 'BooleanToOneZero',
            'title': 'Communication address and Home address are same'
        }, {
            'name': 'Home Address',
            'type': 'container',
            'id': 'reg-add-details',
            'cssclass': 'stage-subheader',
            'visible': false,
            'otherFieldChanged': 'toggleRegistrationAddress',
            'children':[{
                'name': 'Email Id',
                'type': 'container',
                'id': 'home_email_address',
                'cssclass': 'stage-molecule',
                'children': [{
                    'name': 'home_email',
                    'id': 'home_email',
                    'type': 'text',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'Email Id'
                }]
            },{
                'name': 'Mobile',
                'type': 'container',
                'id': 'home_mobile_container',
                'cssclass': 'stage-molecule',
                'children': [{
                    'name': 'home_mobile',
                    'id': 'home_mobile',
                    'type': 'text',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'Mobile'
                }]
            },{
                'name': 'Address Line 1',
                'type': 'container',
                'id': 'home_address_1',
                'cssclass': 'stage-molecule',
                'children': [{
                    'name': 'home_address_line_1',
                    'id': 'home_address_line_1',
                    'type': 'text',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'Address Line 1'
                }]
            },
            {
                'name': 'Address Line 2',
                'type': 'container',
                'id': 'home_address_2',
                'cssclass': 'stage-molecule',
                'children': [{
                    'name': 'home_address_line_2',
                    'id': 'home_address_line_2',
                    'type': 'text',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'Address Line 2'
                }]
            },
            {
                'name': 'Address Line 3',
                'type': 'container',
                'id': 'home_address_3',
                'cssclass': 'stage-molecule',
                'children': [{
                    'name': 'home_address_line_3',
                    'id': 'home_address_line_3',
                    'type': 'text',
                    'maxlength': 30,
                    'title': 'Address Line 3'
                }]
            }, {
                'name': 'City and State',
                'type': 'container',
                'id': 'home_city_state_container',
                'cssclass': 'stage-molecule',
                'visible': true,
                'children':[{
                    'id': 'home_state',
                    'name': 'home_state',
                    'type': 'select',
                    'validators': ['NonEmpty'],
                    'options': states_list,
                    'optionsText': 'key',
                    'optionsValue': 'value',
                    'dependents':['home_city'],
                    'title': 'State'
                },{
                    'id': 'home_city',
                    'name': 'home_city',
                    'type': 'select',
                    'validators': ['NonEmpty'],
                    'maxlength': 30,
                    'title': 'City',
                    'otherFieldChanged':getCityList,
                    'options':current_city_list,
                    'optionsText':'key',
                    'optionsValue':'value',
                }]
            }, {
                'name': 'Pincode',
                'type': 'container',
                'id': 'home_pincode_container',
                'cssclass': 'stage-molecule',
                'children': [{
                    'id': 'home_pincode',
                    'name': 'home_pincode',
                    'type': 'text',
                    'validators': ['NonEmpty', 'PincodeValidation'],
                    'maxlength': 6,
                    'dependents': [],
                    'title': 'e.g. 400063'
                }, ]
            },]
        }, ],
    }, {
        'name': 'Terms And Conditions',
        'type': 'container',
        'id': 'accept_terms_block',
        'cssclass': 'stage-header',
        'visible': false,
        'children': [{
            'type': 'container',
            'id': 'accept_terms_container',
            'validators': ['TermsAndConditionsValidation'],
            'cssclass': 'stage-subheader',
            'children': [{
                'id': 'accept_terms',
                'name': 'accept_terms',
                'type': 'checkbox',
                'checked': false,
                'title': 'I accept the terms and conditions.'
            }, ]
        }, ]
    }]

    return {
        getProposerForm         : proposer_template,
        getTravellerDetailsForm : function(insurerform){
            if(insurerform && insurerform.medical_form){
                travellor_template[0]['children'].push(insurerform.medical_form);
            }
            return travellor_template;
        },
        getContactForm          : contact_form,
    }
});
