define(['knockout', 'jquery', 'utils','./lib/cookies', 'form-models', './general-form'],
        function(ko, $, utils, cookies, formmodels, generalform){

    insurerform = require("./insurers/"+insurer_slug+"-form");


    var policy_disclaimer = "Once you make the payment, here’s the process hereon to get your chosen travel insurance policy: \n"+
                            "Based on your declaration(s) with regards to medical condition/history, "+
                            insurer_name + " Insurance Company will evaluate your case in detail, before they issue the policy.\n"+
                            "As part of the evaluation, they may call you in 6 working hours to understand your case in detail.\n"+
                            "Once the evaluation is complete," + insurer_name + " Insurance Company will take a decision and communicate accordingly.\n"+
                            "Note, the payment you are about to make is merely a collection process, and does not guarantee policy/insurance coverage";

    var showPopup = ko.observable(false);
    var disablePayment = ko.observable();
    var countOfPED = ko.observable(0);
    var errorMessage = ko.observable("");
    var all_forms = {}; //form objects

    var PopupModel = function(){
        var self = this;

        self.error_messages = ko.observableArray([errorMessage]);
        self.close = function(){
            showPopup(false);
        }
        self.back = function(){
            window.location.href = '/travel-insurance/result/'+activity_id;
        }
    }

    var popup = new PopupModel();

    if(transaction_status == 'COMPLETED'){
        disablePayment(true);
        errorMessage('Payment is already done for this transaction.');
    }

    function enablePopup(validation){
        console.log("Validation: ", validation);
        popup.error_messages(validation.error_messages);
        showPopup(true);
    }

    function invalidForm(error){
        disablePayment(true);
        errorMessage(error);
        setTimeout(function(){ disablePayment(false); },3000);
    };

    setTimeout(function(){
        ko.computed(function(){
            var all_forms_json = {}
            all_forms_json['members'] = [];
            for(var i in all_forms.member_forms){
                var formview = all_forms.member_forms[i];
                all_forms_json.members.push(formview.getValue());
            }
            var formview = all_forms.contact_form;
            all_forms_json.contact = formview.getValue();

            var formview = all_forms.proposer_form;
            if(formview) all_forms_json.proposer = formview.getValue();
            all_forms_json = extraMethods.cleanGeneralFormData(all_forms_json);
            big_json  = extraMethods.cleanInsurerFormData(all_forms_json);

            error_mobile = all_forms.member_forms[0].getFieldById('com_mobile').error();
            error_email = all_forms.member_forms[0].getFieldById('com_email').error();

            if(all_forms_json['email'] && all_forms_json['mobile'] && !error_mobile && !error_email){
                sendDataToLMS(big_json,"proposal-form");
            }
        });
    },1000);

    var sendDataToLMS = function(data,label){
        console.log("SENDING DATA TO LMS: ",data);
        data['campaign'] = 'travel';
        data['label'] = label;
        data['insurer_slug'] = insurer_slug;
        data['product_type'] = 'travel'; //talk to LMS team about this shit.
        data['transaction_id'] = transaction_id;
        data['triggered_page'] = window.location.href;

        cookies.set('email',data['email']);
        cookies.set('mobileNo',data['mobile']);
        for(var k in quote_details){ data[k] = quote_details[k]; } //quote_details coming from back end.
        $.ajax({
            url: LMS_URL,
            type: 'POST',
            data: {'data': JSON.stringify(data), 'csrfmiddlewaretoken': CSRF_TOKEN},
            success:function(resp){
            },
        });
    }

    var extraMethods = {
        ShowPopup: function(field, id, value){
            if(['no','none'].indexOf(value.toLowerCase()) < 0){
                var all_forms_json = {}
                all_forms_json['members'] = [];
                for(var i in all_forms.member_forms){
                    var formview = all_forms.member_forms[i];
                    all_forms_json.members.push(formview.getValue());
                }
                var formview = all_forms.contact_form;
                all_forms_json.contact = formview.getValue();

                var formview = all_forms.proposer_form;
                if(formview) all_forms_json.proposer = formview.getValue();
                all_forms_json = extraMethods.cleanGeneralFormData(all_forms_json);
                var big_json  = extraMethods.cleanInsurerFormData(all_forms_json);
                validation = extraMethods.validation(big_json);
                enablePopup(validation);
            }
        },
        DateValidation: function(data, error){
            var pat = "^[0-9]\+-[0-9]\+-[0-9]{4}$";
            if(data.search(pat) == 0){
                var cust_dob = utils.stringToDate(data, 'dd-mm-yyyy', '-'),
                    cust_age = utils.calculateAgeFromDate(cust_dob);
                if(cust_age > 99){
                    error("age must be below 99 years");
                    return false;
                }
                return true;
            }
            error("date should be in DD-MM-YYYY format");
            return false;
        },
        NomineeAgeValidation: function(data, error) {
            if (data < 18) {
                error('Nominee must be more than 18 years of age.');
                return false;
            }
            return true;
        },
        PassportValidation: function(data, error) {
            var matched = data.match('^[A-Z][0-9]{7}$');
            if (matched) return true;
            error('Passport number is invalid.');
            return false;
        },
        TermsAndConditionsValidation: function(data, error) {
            if (data.accept_terms) return true;
            error('Please accept the above terms and conditions to proceed.');
            return false;
        },
        DOBClean: function(field, value) {
            var dateStr = value.trim().replace(/ /g, "").replace(/\//g, "-");
            var parts = dateStr.split('-');

            if (parts.length == 3) {
                while (parts[0].length < 2) parts[0] = '0' + parts[0];
                while (parts[1].length < 2) parts[1] = '0' + parts[1];
            }

            var dateStr = parts.join("");
            var day = dateStr.match("^([3][0-1]|[1-2][0-9]|[0]{0,1}[1-9])");
            dateStr = dateStr.substr(day.index + day[0].length);
            day = day[0];

            while (day.length < 2) day = '0' + day;

            var month = dateStr.match("^([1][0-2]|[0]{0,1}[1-9])");
            dateStr = dateStr.substr(month.index + month[0].length);
            month = month[0];

            while (month.length < 2) month = '0' + month;

            var year = dateStr.substr(0, 4);
            var thisYear = parseInt((new Date()).getFullYear());
            if (year.length == 3) {
                if (parseInt(year) > (thisYear % 1000)) {
                    year = '1' + year;
                } else {
                    year = '2' + year;
                }
            } else if (year.length == 2) {
                if (parseInt(year) > (thisYear % 100)) {
                    year = '19' + year;
                } else {
                    year = '20' + year;
                }
            } else if (year.length == 1) {
                if (parseInt(year) > (thisYear % 10000)) {
                    year = '190' + year;
                } else {
                    year = '200' + year;
                }
            }

            var newvalue = day + '-' + month + '-' + year;
            field.cleanedValue(newvalue);
            return newvalue;
        },
        DOBValidation: function(data, error) {
            var x = data.split('-');
            if ( (x[0]=='' || isNaN(x[0])) || (x[1]=='' || isNaN(x[1])) || (x[2]=='' || isNaN(x[2])) ) {
                error("Invalid date");
                return false;
            }

            return true;
        },
        toggleRegistrationAddress: function(field, id, value) {
            if (id == 'add-same') {
                field.visible(!(value === 1));
            }
        },
        fetchCitiesForState: function(field, id, value) {
            if ((id == 'loc_add_state') || (id == 'loc_reg_add_state')) {
                if (data.form_details.is_cities_fetch) {
                    $.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurer_slug + '/', {
                        'state': value,
                        'csrfmiddlewaretoken': CSRF_TOKEN
                    }, function(response) {
                        response = crypt.parseResponse(response);
                        if (response.data.cities) {
                            field.OptionsList(response.data.cities);
                            field.Value(data.user_form_details[field.Id]);
                            field.visible(true);
                        }
                    });
                }
            }
        },
        fetchLocationDetails: function(field, id, value) {
            return;
            if (id == 'add_pincode') {
                $.post('/motor/' + VEHICLE_TYPE + '/api/pincode-details/' + insurer_slug + '/', {
                    'pincode': value,
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    response = crypt.parseResponse(response);
                    field.parent.getFieldById('add_state').Value(response.data.province_name);
                    field.parent.getFieldById('add_district').Value(response.data.district_name);
                    if (data.form_details.is_cities_fetch) {
                        $.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurer_slug + '/', {
                            'state': response.data.province_name,
                            'csrfmiddlewaretoken': CSRF_TOKEN
                        }, function(response) {
                            response = crypt.parseResponse(response);
                            if (response.data.cities) {
                                field.parent.getFieldById('add_city').OptionsList(response.data.cities);
                                field.parent.getFieldById('add_city').Value(data.user_form_details['add_city']);
                                field.visible(true);
                            }
                        });
                    } else {
                        field.visible(true);
                    }
                });
            }

            if (id == 'reg_add_pincode') {
                $.post('/motor/' + VEHICLE_TYPE + '/api/pincode-details/' + insurer_slug + '/', {
                    'pincode': value,
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    response = crypt.parseResponse(response);
                    field.parent.getFieldById('reg_add_state').Value(response.data.province_name);
                    field.parent.getFieldById('reg_add_district').Value(response.data.district_name);
                    if (data.form_details.is_cities_fetch) {
                        $.post('/motor/' + VEHICLE_TYPE + '/api/cities/' + insurer_slug + '/', {
                            'state': response.data.province_name,
                            'csrfmiddlewaretoken': CSRF_TOKEN
                        }, function(response) {
                            response = crypt.parseResponse(response);
                            if (response.data.cities) {
                                field.parent.getFieldById('reg_add_city').OptionsList(response.data.cities);
                                field.parent.getFieldById('reg_add_city').Value(data.user_form_details['reg_add_city']);
                                field.visible(true);
                            }
                        });
                    } else {
                        field.visible(true);
                    }
                });
            }
        },
        toggleOtherField: function(field,id,value){
            field.visible( (value=='yes') ? true:false);
        },
        disablePaymentMode: function(field,id,value){
            var error_text = "Sorry! You cannot buy this policy because you answered 'yes' for a disease question";

            var disable = (value == 'yes');
            if(disable) countOfPED(countOfPED()+1);
            else        countOfPED( (countOfPED()<=0)? 0 : countOfPED()-1 );

            errorMessage(error_text);
            disablePayment(countOfPED()>0);
            console.log("COUNT: ",countOfPED());
        },
        NoSpaceWithUpperCase: function(field, value) {
            return value.replace(/ /g,"").toUpperCase();
        },
        cleanGeneralFormData: function(all_forms_json) {
            return all_forms_json;
        },
        cleanGeneralForm: function(all_forms){
            if(form_details.num_insured > 1){
                var count = 1;
                for(var i in all_forms.member_forms){
                    var member = all_forms.member_forms[i];
                    var td = member.getFieldById('traveller-details');
                    td.Name("Traveller "+(count++)+" Information");
                }
            }
            return all_forms;
        },
        generalValidation: function(all_forms){
            proposal_ages = [];
            quote_details.ages.sort();
            for(var i in all_forms.members){
                var member = all_forms.members[i];
                var cust_dob = utils.stringToDate(member['cust_dob'], 'dd-mm-yyyy', '-'),
                cust_age = utils.calculateAgeFromDate(cust_dob);
                proposal_ages.push(cust_age);
            }
            proposal_ages.sort();
            console.log("proposal_ages: ",proposal_ages);
            for(var i in proposal_ages){
                if((proposal_ages[i] > quote_details.ages[i]+1 || proposal_ages[i] < quote_details.ages[i]-1)){
                    return {valid:false,error:"member ages entered on quote form and proposal form do not match."};
                }
            }
            return {valid:true};
        },
        prefillFormData : function(all_forms, insured_details, mapping, first_member){ //BEWARE!! Horrer ahead.
            console.log("GENERAL PREFILL CALLED");
            var current_members = {};
            var member_counter = 0;

            for(var i in insured_details){
                if(mapping.contact_mapping[i] != undefined){    //prefill contact form
                    var element = all_forms.contact_form.getFieldById(mapping.contact_mapping[i]);
                    if(element) element.Value(insured_details[i]);
                }
                else if(all_forms.proposer_form && mapping.proposer_mapping[i] != undefined){   //prefill proposer form if thr's any
                    var element = all_forms.proposer_form.getFieldById(mapping.proposer_mapping[i]);
                    var val = (i=="DOB") ? utils.reverseDOBFormat(insured_details[i]) : insured_details[i];
                    if(element) element.Value(val);
                }
                else{
                    for(var m in mapping.member_mapping){   //prefill member form
                        var index = i.search(m+"$");
                        if(index >= 0){
                            var tag = i.substr(0,index);
                            if(tag == "" || tag == 'communication_') tag = first_member;
                            if(current_members[tag] == undefined){
                                if(tag == first_member){
                                    current_members[tag] = 0;
                                }
                                else{
                                    member_counter = (member_counter == 0) ? 1 : member_counter;
                                    current_members[tag] = member_counter++;
                                }
                            }
                            var form = all_forms.member_forms[current_members[tag]];
                            if(form){
                                var element = form.getFieldById(mapping.member_mapping[m]);
                                var val = (m=="DOB") ? utils.reverseDOBFormat(insured_details[i]) : insured_details[i];
                                if(element) element.Value(val);
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    for (var k in insurerform.extraMethods) {
        extraMethods[k] = insurerform.extraMethods[k];
    }

    if((['star-health']).indexOf(insurer_slug)>=0){
        var formview = new formmodels.FormClass(extraMethods);
        formview.putFields(generalform.getProposerForm, formview.tree);
        all_forms.proposer_form = formview;
    }
    all_forms.member_forms = [];
    var travellerDetailsForm = generalform.getTravellerDetailsForm(insurerform);
    for(var i=0;i<form_details.num_insured;++i){
        var formview = new formmodels.FormClass(extraMethods);
        formview.putFields(travellerDetailsForm, formview.tree);
        all_forms.member_forms.push(formview);
    }

    var formview = new formmodels.FormClass(extraMethods);
    formview.putFields(generalform.getContactForm, formview.tree);
    all_forms.contact_form = formview;

    extraMethods.cleanGeneralForm(all_forms);
    extraMethods.cleanInsurerForm(all_forms);
    extraMethods.prefillFormData(all_forms, insured_details, insurerform.getMapping(),insurerform.getFirstMemberTag());

    var autosaving = ko.observable(false);

    var form_wait = ko.observable(false);

    return function() {
        return {
            showPopup: showPopup,
            popup : popup,
            Forms: all_forms,
            form_details: form_details,
            quote_details: quote_details,
            plan_details: plan_details,
            CSRF_TOKEN: CSRF_TOKEN,
            form_wait: form_wait,
            disablePayment:disablePayment,
            errorMessage: errorMessage,
            insurer_slug:insurer_slug,
            initialize: function() {
            },

            submitForm: function() {
                //form_wait(true);
                var big_json_data = {};
                var all_forms_json = {}
                all_forms_json['members'] = [];
                for(var i in all_forms.member_forms){
                    var formview = all_forms.member_forms[i];
                    if (formview.hasValidData()) {
                        var formjson = formview.getValue();
                        all_forms_json.members.push(formjson);
                    }
                    else{
                        invalidForm("please fix above errors before continuing");
                        return;
                    }
                }
                var formview = all_forms.contact_form;
                if (formview.hasValidData()) {
                    var formjson = formview.getValue();
                    all_forms_json.contact = formjson;
                }
                else{
                    invalidForm("please fix above errors before continuing");
                    return;
                }
                var formview = all_forms.proposer_form;
                if(formview){
                    if (formview.hasValidData()) {
                        var formjson = formview.getValue();
                        all_forms_json.proposer = formjson;
                    }
                    else{
                        invalidForm("please fix above errors before continuing");
                        return;
                    }
                }
                all_forms_json = extraMethods.cleanGeneralFormData(all_forms_json);

                general_validation = extraMethods.generalValidation(all_forms_json);

                if(!general_validation.valid){
                    invalidForm(general_validation.error);
                    return;
                }

                big_json_data  = extraMethods.cleanInsurerFormData(all_forms_json);

                validation = extraMethods.validation(all_forms_json);

                if(!validation.valid){
                    if(validation.type == 'critical') enablePopup(validation);
                    else invalidForm(validation.error);
                    return;
                }
                this.saveFormDetails(big_json_data);
                if(insurerform.hasPED()){
                    var confirmed = confirm(policy_disclaimer);
                    if(!confirmed) return;
                }

                //proposal_url & buy_url are global variables
                $.post(proposal_url,{'csrfmiddlewaretoken':CSRF_TOKEN,'data': JSON.stringify(big_json_data)}, function(response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        if (self == top) {
                            window.location.href = buy_url;
                        } else {
                            top.location.href = SITE_URL + buy_url;
                        }
                    } else {
                        alert(response.errorMessage);
                    }
                });
            },
            saveFormDetails: function(big_json_data) {
                if(big_json['mobile']!=undefined && big_json['email']!=undefined){
                    sendDataToLMS(big_json,"make-payment");
                }
                else{
                    console.log("LMS data push FAILED!!");
                }
            }
        };
    };
});
