define(['knockout', 'jquery', 'utils','./lib/cookies', 'form-models', './insurers/general-form'],
        function(ko, $, utils, cookies, formmodels, generalform){

    insurerform = require("./insurers/"+insurer_slug+"-form");

    var BackToResults = function(){
            return window.RESULT_PAGE_URL;
    }

    var MedicalDeclinedPopup = function(){
        var self = this;

        self.popupTemplate = "medicalDeclinedPopup";
        self.showPopup = ko.observable(false);
        self.error_messages = ko.observableArray([]);
        self.close = function(){
            self.showPopup(false);
        }
        self.back = BackToResults;
    }
    var popup =  new MedicalDeclinedPopup();

    //act as Global form errors.
    var formError = ko.observable("");
    formError.subscribe(function(){
        setTimeout(function(){ formError(""); },3500);
    });

    if(form_layout['non_form_errors'] && form_layout['non_form_errors'].length >0){
        formError(form_layout['non_form_errors'][0]);
    }


    var members = [];
    var membersDictionary = {};

    //first_name and last_name are observable variables. since actual fields are defined somewhere else, this memberModel is just
    //a dummy model to keep track of members.
    var MemberModel = function(form, index){
        var self = this;
        var prefix = 'form-'+index+'-';


        this.gender = form.getFieldById(prefix+'gender').Value;
        this.date_of_birth = form.getFieldById(prefix+'date_of_birth').Value;
        this.relation = form.getFieldById(prefix+'relationship').Value;
        this.first_name = form.getFieldById(prefix+'first_name').Value;
        this.last_name = form.getFieldById(prefix+'last_name').Value;
        this.passport_no = form.getFieldById(prefix+'passport_no').Value;

        this.full_name = ko.computed(function(){
            return self.first_name() + " " + self.last_name();
        });
        this.age = ko.computed(function(){
            if(self.date_of_birth()){
                return utils.calculateAgeFromDate(utils.stringToDate(self.date_of_birth(),'dd-mm-yyyy','-'));
            }
        });
        this.relation.subscribe(function(value){
            if(value && membersDictionary[value] == undefined) membersDictionary[value] = self;
        });

        this.get_nominee_member = function(){
            var relation = self.relation();
            var member = undefined;
            switch(relation){
                case 'self':
                    member = membersDictionary['spouse'];
                    break;
                case 'spouse':
                case 'son':
                case 'daughter':
                default:
                    member = membersDictionary['self'];
                    break;
            }
            return member;
        };
    }

    var createMembers = function(form){
        for(var i=0 ;i< MEMBER_LENGTH; i++){
            var mem = new MemberModel(form, i);
            members.push(mem);
            if(mem.relation()) membersDictionary[mem.relation()] = mem;
        }
    }


    var extraMethods = {
        popup: popup,
        toggleFieldVisibility: function(field, id, value){
            field.visible(value=='yes');
        },
        address_form_template: function(){
            fields = form_layout.address_form_layout;

            if(fields['non_field_errors'] && fields['non_field_errors'].length > 0){
                formError(fields['non_field_errors'][0]);
            }

            if(insurer_slug == 'reliance'){
                fields['same_address']['showsummary'] = false;
            return utils.Card([
                    utils.Container([], {'name': 'Communication Address', 'cssclass': 'container-medical', 'visible': true, 'showHeaderOnSummary': true}),
                    utils.Container(['pincode'], {'fields': fields}),
                    utils.Container(['address_line_1'], {'fields': fields}),
                    utils.Container(['address_line_2'], {'fields': fields}),
                    utils.Container(['address_line_3'], {'fields': fields}),
                    utils.Container(['state', 'city'], {'fields': fields}),
                    utils.Container(['same_address'], {'fields': fields}),
                    utils.Container([], {'name': 'Home Address', 'cssclass': 'container-medical', 'showHeaderOnSummary': true}),
                    utils.Container(['home_pincode'], {'fields': fields}),
                    utils.Container(['home_address_line_1'], {'fields': fields}),
                    utils.Container(['home_address_line_2'], {'fields': fields}),
                    utils.Container(['home_address_line_3'], {'fields': fields}),
                    utils.Container(['home_state', 'home_city'], {'fields': fields}),
                    ], {'name': 'Address'});
            }
            else{
                return utils.Card([
                        utils.Container(['pincode'], {'fields': fields}),
                        utils.Container(['address_line_1'], {'fields': fields}),
                        utils.Container(['address_line_2'], {'fields': fields}),
                        utils.Container(['address_line_3'], {'fields': fields}),
                        utils.Container(['state', 'city'], {'fields': fields}),
                        ], {'name': 'Address'});
            }
        },
        member_form_template: function(){
            member_forms = form_layout.member_form_layout;

            var quote_form_ages = quote_details.ages;
            quote_form_ages.sort(function(a,b){ return a-b; });
            for(var i in member_forms){
                var mf = member_forms[i];
                if(mf['non_field_errors'] && mf['non_field_errors'].length > 0){
                    formError(member_forms[i]['non_field_errors'][0]);
                }
                mf.date_of_birth.defaultAge = quote_form_ages[MEMBER_LENGTH-1-i];
            }
            if(member_forms.length == 1){
                return insurerform.extraMethods.get_individual_layout(member_forms);
            }
            else{
                return insurerform.extraMethods.get_family_layout(member_forms);
            }
        },
    };

    for(var i in generalform.extraMethods){
        extraMethods[i] = generalform.extraMethods[i];
    }

    for(var i in insurerform.extraMethods){
        extraMethods[i] = insurerform.extraMethods[i];
    }

    var form_template = [];
    var member_template_list = extraMethods.member_form_template();
    for(var i in member_template_list){
        form_template.push(member_template_list[i]);
    }
    form_template.push(extraMethods.address_form_template());
    form_template.push(extraMethods.tandc_template());
    var main_form = new formmodels.FormClass(extraMethods, form_template);

    createMembers(main_form);
    extraMethods.cleanGeneralForm(main_form, insurerform);
    extraMethods.cleanInsurerForm(main_form, members);

    var cards = [];

    for(var i=0;i<main_form.children.length;i++)
        if(main_form.children[i].Type=='card')
            cards.push(main_form.children[i]);

    var invalid_card_index = cards.length;

    for(var i=0; i<cards.length;i++){
        var card = cards[i];
        if(card.isValid()){
            card.done(true);
            card.collapse(true);
        }
        else{
            invalid_card_index = i;
            break;
        }
    }
    for(var i=invalid_card_index+1;i<cards.length;i++){
        var card = cards[i];
        card.collapse(true);
        card.done(false);
    }

    var sendDataToLMS = function(big_json, label){
        console.log("SENDING DATA TO LMS!!");

        var insured_details_json = {};
        insured_details_json['members'] = [];
        insured_details_json['address'] = {};

        for(var i=0;i<MEMBER_LENGTH;i++){
            insured_details_json['members'].push({});
        }
        for(var key in big_json){
            if(key.startsWith('form-')){
                var splitstr = key.split('-');
                insured_details_json['members'][Number(splitstr[1])][splitstr[2]] = big_json[key];
            }
            else{
                insured_details_json['address'][key] = big_json[key];
            }
        }
        var data = insured_details_json;

        data['email'] = insured_details_json['members'][0]['email'];
        data['mobile']= insured_details_json['members'][0]['mobile'];
        data['campaign'] = 'travel';
        data['label'] = label;
        data['insurer_slug'] = insurer_slug;
        data['product_type'] = 'travel'; //talk to LMS team about this shit.
        data['transaction_id'] = transaction_id;
        data['triggered_page'] = window.location.href;

        cookies.set('email',data['email']);
        cookies.set('mobileNo',data['mobile']);
        for(var k in quote_details){ data[k] = quote_details[k]; } //quote_details coming from back end.
        $.ajax({
            url: LMS_URL,
            type: 'POST',
            data: {'data': JSON.stringify(data), 'csrfmiddlewaretoken': CSRF_TOKEN},
            success:function(resp){
            },
        });
    }

    ko.computed(function(){
        var big_json = main_form.getValue();
        big_json = extraMethods.cleanGeneralFormData(big_json);
        //big_json = extraMethods.cleanInsurerFormData(big_json);

        error_mobile = main_form.getFieldById('form-0-mobile').error();
        error_email = main_form.getFieldById('form-0-email').error();
        if(big_json['form-0-mobile'] && big_json['form-0-email'] && !error_mobile && !error_email){
            sendDataToLMS(big_json);
        }
    });

    var showFooter = ko.computed(function(){
        var allDone = true;
        for(var i=0;i<cards.length;i++){
            if(!(cards[i].done() && cards[i].collapse())) return false;
        }
        return true;
    });
    main_form.getFieldById('accept_terms_block').visible(showFooter());
    showFooter.subscribe(function(value){
        main_form.getFieldById('accept_terms_block').visible(value);
    });

    return function(){
        return {
            form:main_form,
            management_form: management_form,
            formError: formError,
            showFooter: showFooter,
            BackToResults: BackToResults,
            submitForm: function(){
                console.log("submiting the form!!");
                var general_validation = extraMethods.generalValidations(main_form, insurerform, members);
                if(!general_validation.valid){
                    formError(general_validation.message);
                    return false;
                }
                var isValid = main_form.hasValidData();
                if(isValid){
                    insurerform.extraMethods.handleMedicalRestraints(main_form);
                    if(insurerform.medicalDeclined()) return false;
                    else return true;
                }
                else{
                    console.log("Error while submitting the form");
                    formError("Please resolve above errors");
                    return false;
                }
            }
        };
    };
});
