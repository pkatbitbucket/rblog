define(['knockout','utils'], function(ko,utils) {

    var medicalDeclined = ko.observable(false);

    var member_field_list = [
            {name: "first_name", length: 105 },
            {name: "last_name", length: 105 },
            {name: "date_of_birth", length: 105 },
            {name: "passport_no", length: 105 },
            {name: "gender", length: 105 },
            {name: "married", length: 60},
            {name: "relationship", length: 105 },
        ];

    var get_family_medical_layout = function(member_field_list){
        var ped_list = [];
        for(var i in member_field_list){
            ped_list.push([member_field_list[i]['ped']]);
        }

        return [{
            'type': 'container',
            'id': 'ped_container_question',
            'cssclass': 'stage-molecule horizontal',
            'name': 'Does any traveller suffer from any pre-existing disease?',
            'children':[{
                'type': 'member_list',
                'id': 'ped_member_list',
                'member_json': ped_list,
                'title': 'Does any traveller suffer from any pre-existing disease?',
                'cssClass': 'member-list',
            }],
        }];
    }

    var extraMethods = {
        get_individual_layout: function(member_form){
            fields = member_form[0];

            fields['gender'].type = 'radio-widget';
            fields['married'].type = 'radio-widget';

            var medical_questions = [
                utils.Question(['ped'], {'fields': fields}),
                ];


            var contact_card = utils.Card([
                    utils.Container(['first_name', 'last_name'], {'fields': fields}),
                    utils.Container(['email'], {'fields': fields}),
                    utils.Container(['mobile'], {'fields': fields}),
                    ], {'name': 'Contact Details', 'showsimplesummary': true});

            var member_card = utils.Card([
                    utils.Container(['gender'], {'fields':fields, 'name': 'Gender', 'cssclass': 'container-horizontal'}),
                    utils.Container(['married'], {'fields':fields, 'name': 'Married', 'cssclass': 'container-horizontal'}),
                    utils.Container(['date_of_birth', 'passport_no'], {'fields':fields}),
                    utils.Container(['assignee_name'], {'name': 'Assignee Details', 'fields': fields}),
                    utils.Container(['relationship'], {'fields': fields}),
                    ], {'name': 'Traveller Details', 'showsimplesummary': true});

            var medical_card = utils.Card(medical_questions, {'name': 'Medical Details'});

            return [contact_card, member_card, medical_card];
        },
        get_family_layout: function(){
            forms = form_layout.member_form_layout;

            var contact_card = utils.Card([
                    utils.Container(['first_name', 'last_name'], {'fields': forms[0]}),
                    utils.Container(['email'], {'fields': forms[0]}),
                    utils.Container(['mobile'], {'fields': forms[0]}),
                    ], {'name': 'Contact Details', 'showsimplesummary': true});

            var member_layout = [];
            var nominee_layout = [];

            //Header
            member_layout.push(utils.StaticContainer(["First Name", "Last Name", "Gender", "Date Of Birth","Married", "Passport No", "Relationship"]));

            for(var i in forms){
                var member = forms[i];
                member_layout.push( utils.Container([
                                utils.Group(['first_name', 'last_name'], {'fields': member}),
                                utils.Group(['gender', 'date_of_birth'], {'fields': member}),
                                utils.Group(['married','passport_no'], {'fields': member}),
                                utils.Group(['relationship'], {'fields': member}),
                            ]));

                nominee_layout.push( utils.Container(['assignee_name'], {'fields': member, 'showHeaderOnSummary': true}));
            };

            var member_card = utils.Card(member_layout, {'name': "Traveller Details"});
            var nominee_card = utils.Card(nominee_layout, {'name': "Assignee Details"});
            var medical_card = utils.Card(get_family_medical_layout(forms), {'name': 'Medical Details'});

            return [contact_card, member_card, nominee_card, medical_card];
        },
        validation: function(all_forms_json){
        },
        handleMedicalRestraints: function(form){
            var error_messages = [];
            for(var i=0;i<MEMBER_LENGTH;i++){
                var ques1 = form.getFieldById('form-'+i+'-ped');
                if(ques1.getValue() == 'yes'){
                    var msg = 'you suffer from a pre-existing disease';
                    if(error_messages.indexOf(msg) < 0) error_messages.push(msg);
                }
            }
            if(error_messages.length > 0){
                form.extraMethods.popup.error_messages(error_messages);
                form.extraMethods.popup.showPopup(true);
                form.setMedicalPopupModel(form.extraMethods.popup);
                medicalDeclined(true);
            }
            else{
                medicalDeclined(false);
            }
        },
        cleanInsurerForm: function (form, members){
            var continueButton = undefined;
            if(MEMBER_LENGTH > 1){
                assignee_card =  form.getFieldById("assignee_details_card");
                var ped_member_list = form.getFieldById('ped_member_list');
                for(var i=0;i<MEMBER_LENGTH;i++){
                    ped_member_list.all_member_names['form-'+i].full_name = members[i].full_name;

                    ped_member_list.set_selected_member_names();
                    assignee_card.children[i].Name = members[i].full_name;
                    assignee_card.children[i].title = members[i].full_name;
                    assignee_card.children[i].showHeaderOnSummary(true);
                }
                form.getFieldById('contact_details_card').setButtonTitle('Continue to Basic Details');
                form.getFieldById('traveller_details_card').setButtonTitle('Continue to Assignee Details');
                form.getFieldById('assignee_details_card').setButtonTitle('Continue to Medical Details');
                form.getFieldById('medical_details_card').setButtonTitle('Continue to Address');
            }
            else{
                form.getFieldById('contact_details_card').setButtonTitle('Continue to Basic Details');
                form.getFieldById('traveller_details_card').setButtonTitle('Continue to Medical Details');
                form.getFieldById('medical_details_card').setButtonTitle('Continue to Address');
            }
            var medical_card = form.getFieldById('medical_details_card');
            continueButton = medical_card.getTheDamnContinueButton();
            continueButton.clickHandlers.push("handleMedicalRestraints");
        },
        cleanInsurerFormData: function(all_forms_json) {
        },
        prefillFormData: function(all_forms,insured_details){
        },
    }

    return {
        extraMethods: extraMethods,
        member_field_list: member_field_list,
        medicalDeclined: medicalDeclined,
    }
});
