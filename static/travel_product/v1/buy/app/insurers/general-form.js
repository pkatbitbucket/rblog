define(['knockout','jquery'], function(ko,$) {

    var applyTravellerTabularCss = function(form, member_field_list){
        // header
        for(var i in member_field_list){
            field = member_field_list[i];
            form.getFieldById(field.name+"_static").cssclass("label--tabular w-"+field.length);
        }
        //member details fields
        for(var i=0; i < MEMBER_LENGTH; i++){
            for(var j in member_field_list){
                var field = member_field_list[j];
                var field_name = "form-"+i+"-"+field.name;
                var actualfield = form.getFieldById(field_name);
                if(form.getFieldById(field_name+"_copy")){
                    actualfield = form.getFieldById(field_name+"_copy");
                }
                actualfield.addCss("element--tabular w-"+field.length);
            }
        }

    };

    var addCharacterLeftCount = function(field, id, value){
            var chars_left = field.max_length() - value.length;
            field.extraInfo(chars_left + " characters left");
    }

    var current_city_list = ko.observableArray([]);
    var states_list = form_details.states_list;
    var cities_list = form_details.cities_list;

    function toggleHomeAddressVisibility(field, id, value){
        field.root.getFieldById('home_address_container').visible(value == false);
        field.root.getFieldById('home_address_line_1').parent.visible(value == false);
        field.root.getFieldById('home_address_line_2').parent.visible(value == false);
        field.root.getFieldById('home_address_line_3').parent.visible(value == false);
        field.root.getFieldById('home_state').parent.visible(value == false);
        field.root.getFieldById('home_city').parent.visible(value == false);
        field.root.getFieldById('home_pincode').parent.visible(value == false);
    }

    function get_state(field, id, value){
        var city_field = field.root.getFieldById(field.dependents[0]);

        $.ajax({
            url: '/travel-insurance/get-state-city/'+value,
            method: 'GET',
            success: function(resp){
                console.log("RESP: ", resp);
                if(!resp.error){
                    // patch for hdfc insurer
                    if (insurer_slug == "hdfc-ergo" || insurer_slug == 'universal-sompo'){
                        for (state in form_details.states_list){
                            if (form_details.states_list[state].key == resp.state.toUpperCase()){
                                var state_code = form_details.states_list[state].value
                                }
                            }
                        cities = form_details.cities_list[state_code]
                        for (city in cities){
                            if (cities[city].key == resp.city.toUpperCase()){
                                var city_code = cities[city].value
                            }
                        }
                        field.Value(state_code);
                        city_field.Value(city_code);
                    }
                    else{
                        field.Value(resp.state);
                        city_field.Value(resp.city);
                    }
                }
                else{
                    field.Value("");
                    city_field.Value("");
                }
            }
        });
    }

    function getCityList(field,id,value){
        var state;
        for(var i in states_list){
            if(states_list[i].value == value){
                state = value;
                break;
            }
        }
        if(state && cities_list[state]) field.OptionsList(cities_list[state]);
        else current_city_list([]);
    }

    var extraMethods = {
        generalValidations: function(form, insurerform, members){
            quote_details.ages.sort(function(a,b){ return a-b; });
            proposal_ages = [];
            console.log("Quote form ages: ", quote_details.ages);
            for(var i=0;i<members.length;i++) proposal_ages.push(members[i].age());
            proposal_ages.sort(function(a,b){ return a-b; });
            console.log("Proposal form ages: ", proposal_ages);

            for(var i=0;i<proposal_ages.length;i++){
                if((proposal_ages[i] > quote_details.ages[i]+1 || proposal_ages[i] < quote_details.ages[i]-1)){
                    return {valid:false,message:"member ages entered on quote form and proposal form do not match."};
                }
            }

            var passport_no = undefined;
            for(var i=0;i<members.length;i++){
                var member_passport_no = members[i].passport_no().toLowerCase();
                if(passport_no && passport_no == member_passport_no){
                    return{valid:false,message:"Passport numbers should be unique for all members."};
                }
                passport_no = member_passport_no;
            }

            return {
                valid: true,
            }
        },
        cleanGeneralForm: function(form, insurerform){
            for(var i=0;i< MEMBER_LENGTH; i++){
                form.getFieldById('form-'+i+'-first_name').clean = 'Capitalize';
                form.getFieldById('form-'+i+'-last_name').clean = 'Capitalize';
                form.getFieldById('form-'+i+'-passport_no').clean = 'Capitalize';
                form.getFieldById('form-'+i+'-first_name').addCss('uppercase');
                form.getFieldById('form-'+i+'-last_name').addCss('uppercase');
                form.getFieldById('form-'+i+'-passport_no').addCss('uppercase');
                if(i>0){
                    var relation_list = form.getFieldById('form-'+i+'-relationship').OptionsList();
                    if(relation_list[0]['value'] == 'self'){
                        relation_list.splice(0,1);
                    }
                }
            }
            if(MEMBER_LENGTH > 1){
                traveller_card =  form.getFieldById("traveller_details_card");
                traveller_card.addCss("stage-header--tabular");
                for(var i in traveller_card.children){
                    if(i > 0){
                        if(traveller_card.children[i].Type == "container"){
                            traveller_card.children[i].addCss("stage-molecule--tabular");
                            var counter = Number(i);
                            var htmlTitle = "<span class='view-mobile'>Traveller </span>"+counter; //for mobile.
                            traveller_card.children[i].Name(htmlTitle);
                            traveller_card.children[i].showHeaderOnSummary(true);
                            traveller_card.children[i].title("Traveller "+counter);
                        }
                    }
                    else{
                        traveller_card.children[i].addCss("stage-molecule--tabular clearfix hidden-xs header");
                        traveller_card.children[i].Name("");
                    }
                }
                applyTravellerTabularCss(form, insurerform.member_field_list);

                //only 1st member(proposer) should be above 18
                form.getFieldById('form-0-date_of_birth').minAge(18);
            }
            else{
                // in case of single member, Date of birth should be in sync with quote form.
                form.getFieldById('form-0-date_of_birth').minAge(quote_details.ages[0]-1);

                var gender_value = form.getFieldById('form-0-gender').Value();
                var married_value = form.getFieldById('form-0-married').Value();

                if(gender_value == undefined)   form.getFieldById('form-0-gender').Value('m');
                if(married_value == undefined)  form.getFieldById('form-0-married').Value('yes');
            }

            form.getFieldById('form-0-mobile').addCss("phone-input");
            form.getFieldById('form-0-email').addCss('large');
            //ADDRESS FORM
            form.getFieldById('address_card').setButtonTitle('Review and Make Payment');
            form.getFieldById('address_line_1').addCss("large");
            form.getFieldById('address_line_2').addCss("large");
            form.getFieldById('address_line_3').addCss("large");

            form.getFieldById('address_line_1').doInstantUpdate(true);
            form.getFieldById('address_line_1').subscribe = addCharacterLeftCount;
            form.getFieldById('address_line_2').doInstantUpdate(true);
            form.getFieldById('address_line_2').subscribe = addCharacterLeftCount;
            form.getFieldById('address_line_3').doInstantUpdate(true);
            form.getFieldById('address_line_3').subscribe = addCharacterLeftCount;

            form.getFieldById('state').caption = "State";
            form.getFieldById('city').caption = "City";
            form.getFieldById('pincode').dependents = ['state'];
            form.getFieldById('state').OptionsList(states_list);
            form.getFieldById('state').otherFieldChanged = get_state;
            form.getFieldById('state').dependents = ['city'];
            form.getFieldById('city').otherFieldChanged = getCityList;
            form.getFieldById('city').options = current_city_list;
            form.getFieldById('city').visible(true);
            getCityList(form.getFieldById('city'), '', form.getFieldById('state').getValue());

            if(insurer_slug == 'reliance'){
                form.getFieldById('same_address').subscribe = toggleHomeAddressVisibility;
                form.getFieldById('home_address_line_1').addCss("large");
                form.getFieldById('home_address_line_2').addCss("large");
                form.getFieldById('home_address_line_3').addCss("large");

                form.getFieldById('home_state').caption = "State";
                form.getFieldById('home_city').caption = "City";
                form.getFieldById('home_pincode').dependents = ['home_state'];
                form.getFieldById('home_state').OptionsList(states_list);
                form.getFieldById('home_state').otherFieldChanged = get_state;
                form.getFieldById('home_state').dependents = ['home_city'];
                form.getFieldById('home_city').otherFieldChanged = getCityList;
                form.getFieldById('home_city').options = current_city_list;
                form.getFieldById('home_city').visible(true);
                getCityList(form.getFieldById('home_city'), '', form.getFieldById('home_state').getValue());
                toggleHomeAddressVisibility(form.getFieldById('same_address'),'',form.getFieldById('same_address').getValue());
                form.getFieldById('home_address_line_1').doInstantUpdate(true);
                form.getFieldById('home_address_line_1').subscribe = addCharacterLeftCount;
                form.getFieldById('home_address_line_2').doInstantUpdate(true);
                form.getFieldById('home_address_line_2').subscribe = addCharacterLeftCount;
                form.getFieldById('home_address_line_3').doInstantUpdate(true);
                form.getFieldById('home_address_line_3').subscribe = addCharacterLeftCount;
            }
        },
        cleanGeneralFormData: function(big_json){
            return big_json;
        },
        tandc_template: function(){
            return {
                'type': 'container',
                'id': 'accept_terms_block',
                'cssclass': 'stage-header card',
                'visible': false,
                'children':[{
                    'id': 'accept_terms',
                    'name': 'accept_terms',
                    'type': 'checkbox',
                    'checked': true,
                    'validators': ['TermsAndConditionsValidator'],
                    'title': 'I declare that the information provided above is true and I authorize Coverfox Insurance Broking Pvt. Ltd. to represent me at Insurance Companies or Agencies for my Insurance needs.',
                    'value': true,
                }],
            }
        },
    }

    return {
        extraMethods: extraMethods,
    }
});
