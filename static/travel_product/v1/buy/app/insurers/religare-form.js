define(['knockout','utils'], function(ko,utils) {


    var policy_disclaimer = "Once you make the payment, here’s the process hereon to get your chosen travel insurance policy: \n"+
                            "Based on your declaration(s) with regards to medical condition/history, "+
                            insurer_name + " Insurance Company will evaluate your case in detail, before they issue the policy.\n"+
                            "As part of the evaluation, they may call you in 6 working hours to understand your case in detail.\n"+
                            "Once the evaluation is complete," + insurer_name + " Insurance Company will take a decision and communicate accordingly.\n"+
                            "Note, the payment you are about to make is merely a collection process, and does not guarantee policy/insurance coverage";

    var medicalDeclined = ko.observable(false);

    var member_field_list = [
            {name: "first_name", length: 105 },
            {name: "last_name", length: 105 },
            {name: "date_of_birth", length: 105 },
            {name: "passport_no", length: 105 },
            {name: "gender", length: 105 },
            {name: "married", length: 60 },
            {name: "relationship", length: 105 },
        ];

    var get_individual_medical_layout = function(fields){
        var children = [['liver_disease'], ['cancer_disease'], ['kidney_disease'], ['heart_disease'], ['paralysis'], ['other_disease', 'other_disease_name']];

        fields['other_disease'].dependents = ['form-0-other_disease_name'];
        fields['other_disease_name'].otherFieldChanged = function(field, id, value){ field.visible(value == 'yes'); };

        return layout = [
            utils.QuestionContainer(children, 'ped', {'fields': fields}),
            utils.Question(['claimed_policy'], {'fields': fields}),
            utils.Question(['hospitalized'], {'fields': fields}),
            ];
    }


    var extraMethods = {
        get_individual_layout: function(member_form){
            fields = member_form[0];

            fields['gender'].type = 'radio-widget';
            fields['married'].type = 'radio-widget';

            var contact_card = utils.Card([
                    utils.Container(['first_name', 'last_name'], {'fields': fields}),
                    utils.Container(['email'], {'fields': fields}),
                    utils.Container(['mobile'], {'fields': fields}),
                    ], {'name': 'Contact Details', 'showsimplesummary': true});

            var member_card = utils.Card([
                    utils.Container(['gender'], {'fields':fields, 'name': 'Gender', 'cssclass': 'container-horizontal'}),
                    utils.Container(['married'], {'fields':fields, 'name': 'Married', 'cssclass': 'container-horizontal'}),
                    utils.Container(['date_of_birth', 'passport_no'], {'fields':fields}),
                    utils.Container(['nominee_name'], {'name': 'Nominee Details', 'fields': fields}),
                    utils.Container(['relationship'], {'fields': fields}),
                    ], {'name': 'Traveller Details', 'showsimplesummary': true});

            var medical_card = utils.Card([
                    utils.Container(get_individual_medical_layout(fields), {'fields': fields}),
                    ], {'name': 'Medical Details'});

            return [contact_card, member_card, medical_card];
        },
        get_family_layout: function(member_forms){
            throw "Family Layout hasn't been done yet for Religare!!!";
        },
        validation: function(all_forms_json){
        },
        handleMedicalRestraints: function(form){
            if(MEMBER_LENGTH > 1) throw "Medical Restraints for family not implemented yet";

            var ped = form.getFieldById('form-0-ped');

            if(ped.getValue() == 'yes'){
                var confirmed = confirm(policy_disclaimer);

                medicalDeclined(!confirmed);
            }
            else{
                medicalDeclined(false);
            }
        },
        cleanInsurerForm: function (form){
            if(MEMBER_LENGTH == 1){
                form.getFieldById('contact_details_card').setButtonTitle('Continue to Basic Details');
                form.getFieldById('traveller_details_card').setButtonTitle('Continue to Medical Details');
                form.getFieldById('medical_details_card').setButtonTitle('Continue to Address');
            }
        },
        cleanInsurerFormData: function(all_forms_json) {
        },
        prefillFormData: function(all_forms,insured_details){
        },
    }

    return {
        extraMethods: extraMethods,
        member_field_list: member_field_list,
        medicalDeclined: medicalDeclined,
    }
});
