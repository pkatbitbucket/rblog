define(['knockout', 'pikaday', 'utils'], function(ko, pikaday, utils) {

    console.log("========= >> PIKADAY: ", pikaday);
    var Pikaday = pikaday;

    // Exception similar to Django's ValidationError
    var ValidationError = function(code, message){
        this.code = code;
        this.message = message;
    }

    var Clean = {
        Capitalize: function(value){
            if(value)
                return value.toUpperCase();
        }
    }

    var Validators = {
        RegexValidator: function(value, params){
            if(!value.match(params.regex)){
                throw new ValidationError(params.code, params.message);
            }
        },
        MinLengthValidator: function(value, params){
            if(value.length < params.limit_value){
                throw new ValidationError(params.code, params.message);
            }
        },
        MaxLengthValidator: function(value, params){
            if(value.length > params.limit_value){
                throw new ValidationError(params.code, params.message);
            }
        },
        MinValueValidator: function(value, params){
            if(value < params.limit_value){
                throw new ValidationError(params.code, params.message);
            }
        },
        MaxValueValidator: function(value, params){
            if(value > params.limit_value){
                throw new ValidationError(params.code, params.message);
            }
        },
        EmailValidator: function(value){
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(!re.test(value)) throw new ValidationError('invalid', 'Invalid Email Address');
        },
        TermsAndConditionsValidator: function(value){
            if(!value) throw new ValidationError('invalid', 'You must agree to the above Terms and Conditions');
        }
    }

    var CopyValueFromOriginal = function(field, id, value){
        field.Value(value);
    }


    var Field = function(params, parent, root) {
        this.parent = parent;
        this.root = root;
        this.Id = params.id;
        this.id = params.id;
        this.Name = params.name;
        this.Tag = params.tag;
        this.Type = params.type;
        this.Template = params.template;
        this.Value = ko.observable().extend({
            notify: 'always'
        });
        if(params.value != undefined) this.Value(params.value);

        this.copy_counter = 0;
        this.prefix = params.prefix;

        visibleDefault = true;
        if (typeof(params.visible) == 'boolean') {
            visibleDefault = params.visible;
        }

        this.cssclass = ko.observable(params.cssClass);

        this.addCss = function(val){ this.cssclass(this.cssclass() + " " + val); }

        this.visible = ko.observable(visibleDefault);

        this.backEndErrorMsg = ko.observable(params.backEndErrorMsg);
        this.error = ko.observable(params.errorMessage?true:false);
        this.errorMessage = ko.observable(params.errorMessage);
        this.error_messages = params.error_messages;
        this.cleanedValue = ko.observable('');
        this.title = ko.observable(params.title);
        this.regex = ko.observable(params.regex);


        this.clean = params.clean;
        this.validators = params.validators ? params.validators : [];

        this.extra_validators = params.extra_validators;
        this.dependents = params.dependents ? params.dependents : [];
        this.otherFieldChanged = params.otherFieldChanged;
        this.subscribe = params.subscribe; //function that will be called if value changes.
        this.extra = params;
        this.disabled = ko.observable(params.disabled);
        this.hidden = ko.observable( this.visible() ? (params.hidden) : false);
        this.showsummary = ko.observable(params.hasOwnProperty('showsummary')? params.showsummary : true);

        if(this.hidden()) this.Type = "hidden";

        this.getSummaryHtml = function(){
            if(!self.showsummary() || self.getValue() == undefined || self.hidden() || !self.isAvailable()) return "";
            return "<li><span class='key'>" + self.title() + " : </span><span>"+ self.getDisplayValue()+"</span></li>";
        }


        var self = this;

        this.isAvailable = function() {
            return this.visible();
        }

        this.getValue = function() {
            var fieldvalue = self.Value();
            if(self.clean){
                if(typeof(self.clean) == 'string' && Clean.hasOwnProperty(self.clean)){
                    fieldvalue = Clean[self.clean](self.Value());
                }
            }
            return fieldvalue;
        }

        this.extraInfo = ko.observable("");
        this.doInstantUpdate = ko.observable(params.doInstantUpdate);
        this.hasFocus = ko.observable(false);
        this.showExtraInfo = ko.computed(function(){
            return !self.error() && self.hasFocus();
        });

        this.getDisplayValue = function() { return self.getValue(); }

        this.validate_empty = function(){
            value = self.getValue();
            if(self.visible() && (value === "" || value===undefined || value===null))
                throw new ValidationError("required");
        }

        // Similar to Django's run_validators.
        this.run_validators = function(){
            var value = self.getValue();
            for(var i in self.validators){
                var params = self.validators[i];
                if(typeof(params) == 'object'){
                    var validator = params['validator'];
                    if(typeof(validator) == "string" && Validators.hasOwnProperty(validator)){
                        Validators[validator](value, params);
                    }
                }
                else if(typeof(params) == 'string' && Validators.hasOwnProperty(params)){
                    Validators[params](value);
                }
            }
        }

        this.validate = function(){
            try{
                self.validate_empty();
                self.run_validators();
                self.error(false);
            }
            catch(err){
                if(typeof(err) == "object"){
                    if(err.code){
                        if(self.error_messages && self.error_messages[err.code] != undefined)
                            self.errorMessage(self.error_messages[err.code]);
                        else if(err.message)
                            self.errorMessage(err.message);
                        else self.errorMessage("Invalid Value");
                    }
                }
                self.error(true);
            }
        }


        this.hasValidData = function() {
            if(self.error()) return false;

            self.validate();
            if(self.getValue() != undefined)
                return !(self.error());
            else
                return false;
        }

        this.isValid = function(){
            if(self.error()) return false;

            try{
                self.validate_empty();
                self.run_validators();
                return true;
            }
            catch(err){
                return false;
            }
        }

        this.Value.subscribe(function() {
            self.validate();

            if(self.error()) return;


            for (var index in self.dependents) {
                var dependent = self.dependents[index];
                self.root.getFieldById(dependent).notify(self.Id, self.getValue());
            }

            if(self.subscribe){
                if(typeof(self.subscribe) == 'function')    self.subscribe(self,self.Id,self.getValue());
                else if(typeof(self.subscribe) == 'string' && self.root.extraMethods.hasOwnProperty(self.subscribe)){
                    self.parent.extraMethods[self.subscribe](self,self.Id,self.getValue());
                }
            }

            //self.error(false);
        });

        this.notify = function(id, value) {
            if (typeof(self.otherFieldChanged) == 'function') {
                self.otherFieldChanged(self, id, value);
            } else if ((typeof(self.otherFieldChanged) == 'string') && self.root.extraMethods.hasOwnProperty(self.otherFieldChanged)) {
                self.root.extraMethods[self.otherFieldChanged](self, id, value);
            }
        }

    }



    var DateField = function(params, parent, root){
        params.type = "date";
        params.tag = "input";
        params.template = 'dateElement';

        var self = this;
        self.picker = undefined;
        self.date_id = 'datepicker_'+params.id+"_"+Math.floor(1000*Math.random());

        self.maxAge = ko.observable(params.maxAge ? params.maxAge : 100);
        self.minAge = ko.observable(params.minAge ? params.minAge : 0);
        self.defaultAge = ko.observable(params.defaultAge);

        var today = new Date();

        var minDate = new Date();
        minDate.setFullYear(minDate.getFullYear()-self.maxAge());

        var maxDate = new Date();
        maxDate.setFullYear(maxDate.getFullYear()-self.minAge());

        var defaultDate = new Date();

        ko.computed(function(){
            minDate.setFullYear(today.getFullYear()-self.maxAge());
            maxDate.setFullYear(today.getFullYear()-self.minAge());
            defaultDate.setFullYear(today.getFullYear() - (self.defaultAge() ? self.defaultAge() : self.minAge()));

            if(self.picker){
                self.picker.config().minDate = minDate;
                self.picker.config().maxDate = maxDate;
                self.picker.config().defaultDate = self.defaultDate;
                self.picker.config().yearRange = [minDate.getFullYear(), maxDate.getFullYear()];
            }
        });

        this.renderComplete = function(){
            defaultDate.setFullYear(today.getFullYear() - (self.defaultAge() ? self.defaultAge() : self.minAge()));
            self.picker = new Pikaday({
                field: document.getElementById(self.date_id),
                format: 'D-MM-YYYY',
                yearRange: [minDate.getFullYear(),maxDate.getFullYear()],
                defaultDate: defaultDate,
                minDate:minDate,
                maxDate:maxDate,
                onClose: function() {
                    if(this.getDate()){
                        self.Value(this.getMoment().format('D-MM-YYYY'));
                        document.getElementById(self.date_id).value = this.getMoment().format('D-MM-YYYY');
                    }
                    else{
                        self.Value('');
                        document.getElementById(self.date_id).value = "";
                    }
                }
            });
        }
        Field.call(this, params, parent, root);
    }

    var MemberListField = function(params, parent, root){
        var self = this;
        params.type = "member_list";
        params.tag = "input";
        params.template = 'memberListElement';

        this.popupTemplate = 'medicalFamilyPopup';
        this.member_fields = [];    //actual fields;
        this.all_member_names = {};

        // this.members is a 2D array. outer array is number of members
        // inner array is number of questions for each member.
        this.members_json = params.member_json;

        for(var i in this.members_json){
            var member_field_list = [];
            for(var j in this.members_json[i]){
                var field = createField(this.members_json[i][j], parent, root);
                root.fields[field.id] = field;
                member_field_list.push(field);
            }
            this.member_fields.push(member_field_list);
            this.all_member_names[this.members_json[i][0].prefix] = {};
        }

        this.selected_member_names = ko.observable();
        this.selected_member_array = ko.observableArray();

        this.set_selected_member_names = function(){
            var str = "";
            self.selected_member_array([]);

            for(var i in self.member_fields){
                var prefix = 'form-' + i;
                if(self.member_fields[i][0].getValue() == 'yes'){
                    if(self.all_member_names[prefix].full_name){
                        var val=self.all_member_names[prefix].full_name();
                        str += val + ",";
                        self.selected_member_array.push(val);
                    }
                }
            }
            self.selected_member_names(str);
        }

        ko.computed(function(){
            self.set_selected_member_names();
        });

        this.showPopup = ko.observable(false);

        var is_any_selected = false;
        for(var i=0;i< self.member_fields.length;i++){
            var val = self.member_fields[i][0].getValue();
            if(val && ['no', 'none'].indexOf(val.toLowerCase()) == -1){
                is_any_selected = true;
                break;
            }
        }
        params.value = (is_any_selected)?"yes":"no";

        this.ShowPopupOnYes = function(field, id, value){
            self.showPopup( self.Value() == 'yes');
            self.root.setMedicalPopupModel(self);
        }

        params.subscribe = this.ShowPopupOnYes;

        this.button_click = function(){
            self.showPopup(true);
            self.root.setMedicalPopupModel(self);
        }

        this.closePopup = function(){
            var is_any_selected = false;
            for(var i in self.members_json){
                is_any_selected |= (self.member_fields[i][0].getValue() == 'yes');
            }
            self.Value(is_any_selected ? 'yes':'no');
            self.showPopup(false);
        }

        Field.call(this, params, parent, root);

        this.getSummaryHtml = function(){
            if(self.getValue() == undefined || self.hidden() || !self.isAvailable()) return "";

            var value = (self.selected_member_names() != "")? self.selected_member_names() : "No"
            return "<li><span class='key'>" + self.title() + " : </span><span>"+ value +"</span></li>";
        }

        this.getValue = function(){
            var retval = {};
            for(var i=0;i<self.member_fields.length;i++){
                for(var j=0;j<self.member_fields[i].length;j++){
                    var key = self.member_fields[i][j].Id;
                    var val = self.member_fields[i][j].getValue();
                    retval[key] = val;
                }
            }
            return retval;
        }
    }

    var StaticField = function(params, parent, root){
        params.template = "staticElement";
        Field.call(this, params, parent, root);

        this.isAvailable = function(){ return false; }

        this.hasValidData = function(){ return true; }
    }

    var TextField = function(params, parent, root) {
        var self = this;
        params.tag = "input";
        params.template = 'nonCheckElement';

        this.min_length = ko.observable(params.min_length);
        this.max_length = ko.observable(params.max_length);


        this.trim = function(field, value) {
            return value.trim();
        }

        if (!params.clean) {
            params.clean = this.trim;
        }

        Field.call(this, params, parent, root);

    }

    var NumberField = function(params, parent, root){
        params.type = "number";
        params.tag = "input";
        params.template = 'numberElement';

        this.max_value = ko.observable(params.max_value);
        this.min_value = ko.observable(params.min_value);


        this.trim = function(field, value) {
            return value.trim();
        }

        if (!params.clean) {
            params.clean = this.trim;
        }

        Field.call(this, params, parent, root);

    }

    var HiddenField = function(params, parent, root) {
        params.type = "hidden";
        params.tag = "input";
        params.template = 'nonCheckElement';

        this.maxlength = ko.observable(null);

        Field.call(this, params, parent, root);
    }

    var CheckField = function(params, parent, root) {
        params.type = "checkbox";
        params.tag = "input";
        params.template = 'checkElement';

        //params.value = (params.checked === true)
        Field.call(this, params, parent, root);
    }

    var RadioWidgetField = function(params, parent, root){
        var self = this;
        params.type = "radio-widget";
        params.tag = "select";
        params.template = "radioWidgetElement";
        this.options = params.options ? params.options : [];


        Field.call(this, params, parent, root);

        this.updateValue = function(data, event){
            console.log("data: ", data);
            self.Value(data.value);
        }
        this.getDisplayValue = function(){
            for(var i=0;i<self.options.length;i++){
                if(self.getValue() == self.options[i]['value'])
                    return self.options[i]['key'];
            }
        }
    }

    var RadioField = function(params, parent, root){
        params.type = "radio";
        params.tag = "select";
        params.template = "radioElement";
        this.options = params.options ? params.options : [];
        Field.call(this, params, parent, root);
    }

    var SelectField = function(params, parent, root) {
        var self = this;
        params.type = "select";
        params.tag = "select";

        if(params.template == undefined)    params.template = 'selectElement';

        this.OptionsList = ko.observableArray([]);
        this.OptionsList(params.options);

        this.caption = params.caption;
        Field.call(this, params, parent, root);

        this.getDisplayValue = function(){
            for(var i=0;i<self.OptionsList().length;i++){
                if(self.getValue() == self.OptionsList()[i]['value'])
                    return self.OptionsList()[i]['key'];
            }
        }
    }

    var SearchSelectField = function(params, parent, root) {
        params.type = "searchselect";
        params.tag = "searchselect";
        params.template = 'searchSelectElement';

        this.OptionsList = ko.observableArray(params.options);

        this.selectedOption = ko.observable(null).extend({
            notify: 'always'
        });

        Field.call(this, params, parent, root);

        var self = this;
        this.selectedOption.subscribe(function (value) {
            if (!value) return;
            if (self.Value() != value[params.optionsValue]) {
                self.Value(value[params.optionsValue]);
            }
        });

        this.setSelectedToValue = function(value) {
            for (var index in self.OptionsList()) {
                var element = self.OptionsList()[index];
                if (value == element[self.extra.optionsValue]) {
                    self.selectedOption(element);
                    return;
                }
            }
            if (self.OptionsList().length > 0) {
                self.selectedOption(self.OptionsList()[0]);
            }
        };

        this.Value.subscribe(function (value) {
            self.setSelectedToValue(value);
        });
    }

    var ErrorField = function(params, parent, root){
        this.parent = parent;
        this.root = root;
        this.Id = params.id;
        this.Type = "error";
        this.Template = "errorElement";
        this.cssclass = params.cssclass;
        this.showHeaderOnSummary = ko.observable(false);
        this.showsummary = ko.observable(false);
        this.message = ko.observable(params.message);

        var self = this;

        visibleDefault = true;
        if (typeof(params.visible) == 'boolean') {
            visibleDefault = params.visible;
        }
        this.visible = ko.observable(visibleDefault);


        this.isAvailable = function() {
            return false;
        }

        this.addCss = function(val){
        }

        this.getSummaryHtml = function(){ return ""; }
    }

    var ButtonField = function(params, parent, root) {
        this.parent = parent;
        this.root = root;
        this.Id = params.id;
        this.Name = ko.observable(params.name);
        this.Type = "button";
        this.Template = "buttonElement";
        this.title = ko.observable(params.title);
        this.cssclass = params.cssclass;
        this.clickHandlers = params.clickHandlers;
        this.showHeaderOnSummary = ko.observable(false);
        this.showsummary = ko.observable(false);

        var self = this;

        visibleDefault = true;
        if (typeof(params.visible) == 'boolean') {
            visibleDefault = params.visible;
        }
        this.visible = ko.observable(visibleDefault);

        this.isAvailable = function() {
            return false;
        }

        this.addCss = function(val){
        }

        this.onClick = function(){
            for(var i=0;i<self.clickHandlers.length;i++){
                var clickHandler = self.clickHandlers[i];

                if(typeof(clickHandler) == 'object'){
                    var func = clickHandler['name']
                    var params = clickHandler['params']
                    if(typeof(func) == 'string' && self.root.extraMethods.hasOwnProperty(func)){
                        self.root.extraMethods[func](self.root, params);
                    }
                }
                else if(typeof(clickHandler) == 'function'){
                    clickHandler(self.root);
                }
                else if(typeof(clickHandler) == 'string' && self.root.extraMethods.hasOwnProperty(clickHandler)){
                    self.root.extraMethods[clickHandler](self.root);
                }
                else if(typeof(clickHandler) == 'string' && clickHandler=='goto_next_card'){
                    self.parent.goto_next_card();
                }
            }
        }

        this.getSummaryHtml = function(){ return ""; }
    }

    var BaseContainer = function(params, parent, root) {
        this.parent = parent;
        this.root = root;
        this.Id = params.id;
        this.id = params.id;
        this.Name = ko.observable(params.name);
        this.title = ko.observable(params.title);
        this.otherFieldChanged = params.otherFieldChanged;
        this.clean = params.clean;
        this.validators = params.validators;
        this.cssclass = params.cssclass;

        this.addCss = function(val){ this.cssclass += " " + val; }

        if(this.children == undefined) this.children = [];

        visibleDefault = true;
        if (typeof(params.visible) == 'boolean') {
            visibleDefault = params.visible;
        }
        this.visible = ko.observable(visibleDefault);

        this.error = ko.observable(false);
        this.errorMessage = ko.observable('Error in block.');

        this.showHeaderOnSummary = ko.observable(params.showHeaderOnSummary); //used in summary. :P
        this.showsimplesummary = ko.observable(params.showsimplesummary);

        var self = this;

        this.isAvailable = function() {
            return this.visible();
        }

        this.notify = function(id, value) {
            if (typeof(self.otherFieldChanged) == 'function') {
                self.otherFieldChanged(self, id, value);
            } else if ((typeof(self.otherFieldChanged) == 'string') && self.parent.extraMethods.hasOwnProperty(self.otherFieldChanged)) {
                self.parent.extraMethods[self.otherFieldChanged](self, id, value);
            }
        }

        this.hasValidData = function() {
            var isValid = true;
            for(var index in self.children){
                var child = self.children[index];
                if(!child.isAvailable()) continue;
                var isChildValid = child.hasValidData();
                if(!isChildValid) isValid = false;
            }
            return isValid;
        }

        this.isValid = function(){
            for(var i=0;i < self.children.length; i++){
                var child = self.children[i];
                if(!child.isAvailable()) continue;
                if(!child.isValid()) return false;
            }
            return true;
        }

        this.getSummaryHtml = function(){
            var summary = "";
            var childsummary = "";

            if(self.showsimplesummary()){
                summary += "<ul class='summary-items'>";
                var keyvaluepairs = self.getSimpleSummaryObject();
                for(key in keyvaluepairs) summary += "<li><span class='key'>" + key + ": </span><span>" + keyvaluepairs[key] + "</span></li>";
                summary += "</ul>";
                return summary;
            }

            if(!self.visible()) return "";

            if(self.showHeaderOnSummary()){
                summary += "<div class='key-header'>" + self.title() + "</div>";
            }
            for(var i in self.children){
                childsummary += self.children[i].getSummaryHtml();
            }
            if(self.parent.Type == "card"){
                summary += "<ul class='summary-items'>";
                summary += childsummary;
                summary += "</ul>";
            }
            else summary += childsummary;
            return summary;
        }

        this.getSimpleSummaryObject = function(){
            var summary = "";
            var childsummary = "";

            var keyvaluepairs = {};
            for(var i in self.children){
                var child = self.children[i];
                if(!child.isAvailable()) continue;
                if(child.children && child.children.length > 0){
                    var childvaluepairs = child.getSimpleSummaryObject();
                    for(key in childvaluepairs){
                        keyvaluepairs[key] = childvaluepairs[key];
                    }
                }
                else if(!child.showsummary() || child.hidden()) continue;
                else keyvaluepairs[child.title()] = child.getDisplayValue();
            }
            return keyvaluepairs;
        }

        this.getValue = function(){
            var retval = {};
            for(var i=0;i < self.children.length; i++){
                var child = self.children[i];
                if(!child.isAvailable()) continue;
                var childvalue = child.getValue();
                if(typeof(childvalue) == 'object'){
                    for(var key in childvalue){
                        retval[key] = childvalue[key];
                    }
                }
                else retval[child.Id] = childvalue;
            }
            return retval;
        }
    }


    var Card = function(params, parent, root){
        var self = this;
        this.Type = "card";
        this.Template = "cardElement";


        this.collapse = ko.observable(false);
        this.done = ko.observable(true);
        this.continueButton = undefined;
        this.errorField = undefined;

        this.getTheDamnContinueButton = function(){
            if(self.continueButton) return self.continueButton;
            else{
                for(var i=self.children.length-1;i>=0;i--){
                    if(self.children[i].Type == 'button'){
                        self.continueButton = self.children[i];
                        return self.continueButton;
                    }
                }
            }
        }

        this.setButtonTitle = function(newval){
            var button = self.getTheDamnContinueButton();
            button.title(newval);
        }

        this.getTheDamnErrorField = function(){
            if(self.errorField) return self.errorField;
            else{
                for(var i=self.children.length-1;i>=0;i--){
                    if(self.children[i].Type == 'error'){
                        self.errorField = self.children[i];
                        return self.errorField;
                    }
                }
            }
        }

        this.goto_next_card = function(){
            if(self.hasValidData()){
                self.collapse(true);
                self.done(true);
                if(self.index < Card.all_cards.length-1){
                    var nextcard = Card.all_cards[self.index+1];
                    nextcard.collapse(false);
                    utils.smoothScroll('card-'+ self.Id, 64);
                }
            }
            else{
                self.collapse(false);
                self.done(false);

                var errField = self.getTheDamnErrorField();
                errField.message("Please resolve above errors!");
                errField.visible(true);
                setTimeout(function(){ errField.visible(false); },3000);

                var errors = document.getElementsByClassName('has-error');
                if(errors.length){
                    utils.smoothScroll(errors[0], 250);
                }
            }
        }

        this.showCard = function(){
            var cards = Card.all_cards;
            for(var i=0;i<cards.length;i++){
                cards[i].collapse(true);
            }
            self.collapse(false);
        }

        BaseContainer.call(this, params, parent, root);

        self.index = Card.counter++;
        Card.all_cards.push(self);
    }
    Card.counter = 0;
    Card.all_cards = [];

    var Container = function(params, parent, root){
        var self = this;
        this.Type = "container";
        this.Template = "containerElement";
        BaseContainer.call(this, params, parent, root);
    }
    var Group = function(params, parent, root){
        var self = this;
        this.Type = "group";
        this.Template = "groupElement";

        params.showHeaderOnSummary = false;
        BaseContainer.call(this, params, parent, root);
    }


    //It's a container of questions. where 1st question will be a main question.
    //On it's 'yes' rest other questions will be visible.
    var QuestionContainer = function(params, parent, root){
        var self = this;
        this.Type = "question_container";
        this.Template = "questionContainerElement";

        this.showChildren = ko.observable(false);

        this.toggleChildrenVisibility = function(field, id, value){
            self.showChildren( value == "yes");
            for(var i=0;i<self.children.length; i++){
                self.children[i].visible(value == "yes");
                self.children[i].children[0].visible(value == "yes");
                if(value == 'no'){
                    self.children[i].children[0].Value('no');
                }
            }
            if(value == 'no'){
                self.error(false);
            }
        }


        params.main_question.children[0].subscribe = this.toggleChildrenVisibility;
        this.main_question = createContainer(params.main_question, parent, root);

        this.children = [];

        for(var i in params.children){
            var field = createContainer(params.children[i], self, root);
            this.children.push(field);
        }

        BaseContainer.call(this, params, parent, root);

        var any_one_yes = ko.computed(function(){
            var main_value = self.main_question.children[0].getValue();

            if(main_value == 'yes'){
                var is_any_selected = false;
                for(var i=0;i<self.children.length;i++){
                    var val = self.children[i].children[0].getValue();
                    is_any_selected |= (val && ['no','none'].indexOf(val.toLowerCase()) == -1);
                }
                if(!is_any_selected){
                    self.error(true);
                    self.errorMessage("Please select at least one!");
                    return false;
                }
                self.error(false);
                return true;
            }
            self.error(false);
            return false;
        });

        this.hasValidData = function() {
            var main_value = self.main_question.children[0].getValue();

            if(main_value == 'yes' && !any_one_yes()) return false;

            return true;
        }

        this.isValid = function(){
            var main_value = self.main_question.children[0].getValue();

            if(main_value == 'yes' && !any_one_yes()) return false;

            return true;
        }

        for(var i=0;i<self.children.length; i++){
            var val = self.children[i].children[0].getValue();
            if(val && ['no', 'none'].indexOf(val.toLowerCase()) == -1){
                self.main_question.children[0].Value("yes");
                break;
            }
        }

        this.getSummaryHtml = function(){
            var value = self.main_question.children[0].getValue();
            if(value && ['no','none'].indexOf(value.toLowerCase()) == -1){
                var summary = "<div>"+ self.main_question.Name()+"</div>";
                var childsummary = "";
                for(var i=0;i< self.children.length; i++){
                    childsummary += self.children[i].getSummaryHtml();
                }
                if(self.parent.Type == "card"){
                    summary += "<ul class='summary-items'>";
                    summary += childsummary;
                    summary += "</ul>"
                }
                else summary += childsummary;
            }
            else{
                summary = "<li><span class='key'>" + self.main_question.Name() + "</span> No</li>";
            }

            return summary;
        }

        var Super_getValue = self.getValue;

        self.getValue = function(){
            var val = Super_getValue();
            var main_question_value = self.main_question.getValue();
            for(var key in main_question_value) val[key] = main_question_value[key];

            return val;
        }
    }

    var createContainer = function(params, parent, root){
        var container = createField(params, parent, root);

        for(var i=0;i < params.children.length;i++){
            var field = createField(params.children[i], parent, root);
            root.fields[field.id] = field;
            container.children.push(field);
        }
        root.fields[container.id] = container;
        return container;
    }

    var createField = function(params, parent, root) {
        var field = undefined;
        switch (params.type) {
            case "question_container":
                field = new QuestionContainer(params, parent, root);
                break;
            case "card":
                field = new Card(params, parent, root);
                break;
            case "container":
                field = new Container(params, parent, root);
                break;
            case "group":
                field = new Group(params, parent, root);
                break;
            case "static":
                field = new StaticField(params, parent, root);
                break;
            case "text":
            case "email":
                field = new TextField(params, parent, root);
                break;
            case "number":
                field = new NumberField(params, parent, root);
                break;
            case "hidden":
                field = new HiddenField(params, parent, root);
                break;
            case "checkbox":
                field = new CheckField(params, parent, root);
                break;
            case "radio":
                field = new RadioField(params, parent, root);
                break;
            case "radio-widget":
                field = new RadioWidgetField(params, parent, root);
                break;
            case "select":
                field = new SelectField(params, parent, root);
                break;
            case "searchselect":
                field = new SearchSelectField(params, parent, root);
                break;
            case "button":
                field = new ButtonField(params, parent, root);
                break;
            case "error":
                field = new ErrorField(params, parent, root);
                break;
            case "date":
                field = new DateField(params, parent, root);
                break;
            case "member_list":
                field = new MemberListField(params, parent, root);
                break;
            case "question-radio":
                field = new QuestionRadioField(params, parent, root);
                break;
            case "question-choice":
                field = new QuestionChoiceField(params, parent, root);
                break;
            case "family_question":
                field = new FamilyQuestionField(params, parent, root);
                break;
            case "question_group":
                field = new QuestionFieldGroup(params, parent, root);
                break;
        }
        return field;
    }


    var FormClass = function(extraMethods, form_template) {
        var self = this;
        self.parent = this;
        self.root = this;
        self.extraMethods = {};
        self.fields = {};
        self.children = [];

        self.medicalPopupModel = ko.observable({'showPopup': false});
        self.showMPopup = ko.observable(false);

        self.setMedicalPopupModel = function(modal){
            self.medicalPopupModel(modal);
            self.showMPopup(modal.showPopup());
        }

        if(extraMethods){
            self.extraMethods = extraMethods;
        }

        this.getFieldById = function(id) {
            return self.fields[id];
        }


        this.putFields = function(json_template, parent, root){ //form_template should be an array.
            for(var i in json_template){
                var field_template = json_template[i];

                // this means other field is same. So make a copy of the original field
                // and make it dependent on the original field. :)
                if(self.fields[field_template.id]){
                    var originalfield = self.fields[field_template.id];
                    field_template.id += "_copy";
                    field_template.disabled = true;
                    if(originalfield.dependents){
                        originalfield.dependents.push(field_template.id);
                    }
                    field_template.otherFieldChanged = CopyValueFromOriginal;
                }
                var actualfield = createField(field_template, parent, root);
                if(actualfield == undefined) debugger;
                self.fields[actualfield.id] = actualfield;
                actualfield.parent.children.push(actualfield);

                if(field_template.type == 'card' || field_template.type == "container" || field_template.type == 'group'){
                    self.putFields(field_template.children, actualfield, self);
                }
            }
        }


        this.hasValidData = function() {
            for(var index in self.children){
                var child = self.children[index];
                if(!child.isAvailable()) continue;
                if(!child.hasValidData()) return false;
            }
            return true;
        }


        this.getValue = function() {
            var retval = {};
            for(var i=0;i<self.children.length;i++){
                var childval = self.children[i].getValue();
                if(typeof(childval) == 'object'){
                    for(var key in childval){
                        retval[key] = childval[key];
                    }
                }
            }
            return retval;
        }

        this.putFields(form_template, self, self);
    }

    return {
        FormClass: FormClass,
    }
});
