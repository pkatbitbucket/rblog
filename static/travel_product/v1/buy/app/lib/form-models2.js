define(['knockout', 'pikaday'], function(ko, pikaday){

    var TextField = function(template, parent, root){
        var self = this;
        this.parent = parent;
        this.root = root;
        this.id = template.id;

        this.type = 'text';
        this.template = 'TextElement';
    }

    var BaseContainer = function(template, parent, root){
        var self = this;
        this.parent = parent;
        this.root = root;
        this.id = template.id;
        this.children = [];
    }

    var Group = function(template, parent, root){
        var self = this;
        this.type = 'group';
        this.template = 'GroupElement';
        BaseContainer.call(this, template, parent, root);
    }
    var Container = function(template, parent, root){
        var self = this;
        this.type = 'card';
        this.template = 'CardElement';
        BaseContainer.call(this, template, parent, root);
    }
    var Card = function(template, parent, root){
        var self = this;
        this.type = 'card';
        this.template = 'CardElement';
        BaseContainer.call(this, template, parent, root);
    }
    var FormModel = function(extraMethods, json_template){
        var self = this;
        self.parent = this;
        self.root = this;
        self.extraMethods = {};
        self.fields = {};
        self.children = [];

        if(extraMethods){   self.extraMethods = extraMethods; }

        this.getFieldById = function(id){
            return self.fields[id];
        }

        this.createFormModel = function(json_template, parent, root){ //form_template should be an array.
            for(var i in json_template){
                var field_template = json_template[i];
                var actualfield = self.createField(field_template, parent, root);
                self.fields[actualfield.id] = actualfield;
                actualfield.parent.children.push(actualfield);

                if(field_template.type == 'card' || field_template.type == "container"){
                    self.createFormModel(field_template.children, actualfield, self);
                }
            }
        }

        this.createField = function(field_template, parent, root){
            var field = undefined;
            switch(field_template.type){
                case "card":
                    field = new Card(field_template, parent, root); //arguments: template, parent, root.
                    break;
                case "container":
                    field = new Container(field_template, parent, root);
                    break;
                case "group":
                    field = new Group(field_template, parent, root);
                    break;
                case "text":
                    field = new TextField(field_template, parent, root);
                    break;
                default:
                    throw "Any children of a form should either be a of type 'card or 'container'."
            }
            return field;
        }

        this.createFormModel(json_template, self, self);
    }


    return {
        FormModel: FormModel,
    }
});
