define(['knockout', 'jquery', 'buycookie'], function(ko, $, Cookies) {

    //var allAddons = settings.getConfiguration().addons;
    var slugify = function(value){
        return value.toLowerCase().replace(/ /g,"_");
    }

    var LayoutCounter = 0;
    var Layout = function(layout_list, params){
        if(!params) params = {};

        //Visibility of container depends on the visibility of their children
        //if all children are not visible then container should not be visible
        var all_fields_visible = false;
        var all_fields_hidden = true;
        for(var i in layout_list){
            all_fields_visible = all_fields_visible || layout_list[i].visible;
            all_fields_hidden = all_fields_hidden && layout_list[i].hidden;
        }

        visible = params.visible ? params.visible : (all_fields_visible && !all_fields_hidden);

        if(params.id == undefined){
            params.id = (params.name) ? slugify(params.name) + "_" + params.layout_type : "container_" + (LayoutCounter++);
        }
        params.name = (params.name == undefined) ? params.title : params.name;
        params.showsummary = (params.hasOwnProperty('showsummary')) ? params.showsummary : true;
        return {
            'cssclass': params.cssclass,
            'type': params.layout_type,
            'visible': visible,
            'name': params.name,
            'title': params.name,
            'id': params.id,
            'children': layout_list,
            'showHeaderOnSummary': params.showHeaderOnSummary,
            'showsimplesummary': params.showsimplesummary,
            'showsummary': params.showsummary,
            'doInstantUpdate': params.doInstantUpdate,
        }
    }


    var Static = function(name){
        css_class = "label--tabular";

        return {
            'cssClass': css_class,
            'type': 'static',
            'name': name,
            'id': name.toLowerCase().replace(/ /g,"_") + "_static",
        }
    }


    var Container = function(layout_list, params){
        if(params == undefined) params = {};
        params.layout_type = "container";

        if(params.cssclass) params.cssclass += " " + "stage-molecule";
        else    params.cssclass = "stage-molecule";

        children = [];

        if(typeof(layout_list[0]) == "string"){
            for(var i=0;i<layout_list.length;i++)
                children.push(params.fields[layout_list[i]]);
        }
        else children = layout_list;

        return Layout(children, params);
    }

    var StaticContainer = function(layout_list, params){
        if(params == undefined) params = {};
        params.layout_type = "container";
        children = [];

        for(var i=0; i < layout_list.length; i++){
            children.push(Static(layout_list[i]));
        }
        return Layout(children, params);
    }

    var Group = function(layout_list, params){
        if(params == undefined) params = {};
        params.layout_type = "group";

        children = [];
        if(typeof(layout_list[0]) == "string"){
            for(var i=0;i<layout_list.length;i++)
                children.push(params.fields[layout_list[i]]);
        }
        else children = layout_list;

        return Layout(children, params);
    }

    var Card = function(layout_list, params){
        if(params == undefined) params = {};
        params.layout_type = "card";

        if(params.cssclass) params.cssclass += " " + "stage-header card";
        else    params.cssclass = "stage-header card";

        layout_list.push(Error());
        layout_list.push(Button());
        return Layout(layout_list, params);
    }

    var Error = function(params){
        if(params == undefined) params = {};
        params.layout_type = "error";

        if(params.id == undefined) params.id = "button_" + (Error.counter++);
        if(params.id == undefined) params.message="Error in the block!";

        return {
            'type': params.layout_type,
            'cssclass': params.cssclass,
            'message': params.message,
            'id': params.id,
            'visible': false,
        }
    }
    Error.counter = 0;

    var Button = function(params){
        if(params == undefined) params = {};
        params.layout_type = "button";

        var defaultcss = "btn orange large";
        if(params.cssclass) params.cssclass += " " + defaultcss;
        else    params.cssclass = defaultcss;

        if(params.title == undefined) params.title = "Continue";
        if(params.clickHandlers == undefined) params.clickHandlers = ["goto_next_card"];

        if(params.id == undefined) params.id = "button_" + (Button.counter++);
        return {
            'type': params.layout_type,
            'cssclass': params.cssclass,
            'title': params.title,
            'clickHandlers': params.clickHandlers,
            'id': params.id,
        }
    }
    Button.counter = 0;

    var Question = function(layout_list, params){
        if(params == undefined) params = {};
        params.layout_type = "container";

        if(params.cssclass) params.cssclass += " " + "horizontal";
        else    params.cssclass = "horizontal";

        if(params.title == undefined) params.title = params.fields[layout_list[0]].title;

        return Container(layout_list, params);
    }

    var QuestionContainer = function(children_list, main_ques, params){
        if(params == undefined) params = {};
        params.layout_type = "question_container";

        var children = [];
        for(var i=0;i< children_list.length; i++){
            children.push( Question(children_list[i], {'fields': params.fields, 'cssclass': 'tiny'}));
        }

        params.fields[main_ques].visible = true;
        var layout = Layout(children, params);
        layout['main_question'] = Question([main_ques], {'fields': params.fields});
        layout.visible = true;

        return layout;
    }

    var DOBFormat = function(format){
        if(format == undefined) return format;
        var month_array = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        arr = format.split("-");
        if(!isNaN(arr[1])){
            return arr[0]+"-"+month_array[Number(arr[1])-1]+"-"+arr[2];
        }
        else{
            return format;
        }
    }
    var reverseDOBFormat = function(format){
        if(format == undefined) return format;
        var month_dict = {'jan':1,'feb':2,'mar':3,'apr':4,'may':5,'jun':6,'jul':7,'aug':8,'sep':9,'oct':10,'nov':11,'dec':12};

        var arr = format.split("-");
        if(arr[1] && month_dict[arr[1].toLowerCase()]){
            return arr[0]+'-'+month_dict[arr[1].toLowerCase()]+'-'+arr[2];
        }
        return null;
    }
    setInterval(function() {
        if (window.olark) {
            var cd = new Date().toISOString().split(".")[0];
            var d = cd.split("T")[0].split('-');
            var t = cd.split("T")[1];
            window.olark('api.chat.updateVisitorStatus', {
                snippet: 'Last seen: ' + [d[2], d[1], d[0]].join('-') + " " + t
            });
        }
    }, 60000);

    ko.bindingHandlers.inr_currency = {
        update: function(element, valueAccessor, allBindingsAccessor) {
            var valueWasNegative = false;
            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value < 0) {
                value = value * -1;
                valueWasNegative = true;
            }
            var x = value.toString();
            var res = x;
            if (x.length > 3) {
                var afterPoint = '';
                if (x.indexOf('.') > 0)
                    afterPoint = x.substring(x.indexOf('.'), x.length);
                x = Math.floor(x);
                x = x.toString();
                var lastThree = x.substring(x.length - 3);
                var otherNumbers = x.substring(0, x.length - 3);
                if (otherNumbers != '')
                    lastThree = ',' + lastThree;
                res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            }
            //var prefix = allBindingsAccessor().currencyPrefix || "Rs.";
            var prefix = "";
            ko.bindingHandlers.text.update(element, function() {
                var sign = valueWasNegative ? '-' : '';
                return prefix + " " + sign + " " + res;
            });
        },
    };



    var smoothScroll = function(target, offset) {
        var scrollContainer = target;
        do {
            scrollContainer = scrollContainer.parentNode;
            if (!scrollContainer) return;
            scrollContainer.scrollTop += 1;
        } while (scrollContainer.scrollTop == 0);

        var targetY = 0
        if(offset){
            targetY -= offset;
        }
        do {
            if (target == scrollContainer) break;
            targetY += target.offsetTop;
        } while (target = target.offsetParent);

        scroll = function(c, a, b, i) {
            i++; if (i > 30) return;
            c.scrollTop = a + (b - a) / 30 * i;
            setTimeout(function(){ scroll(c, a, b, i); }, 10);
        }
        // start scrolling
        scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
    }


    var hide_chat_box = function() {
        var otcounter = 0;
        var ot = setInterval(function() {
            console.log('TRYING CHAT BOX');
            if (window.olark) {
                console.log("CHAT BOX HIDE");
                window.olark('api.box.hide');
                clearInterval(ot);
            }
            otcounter++;
            if (otcounter > 50) {
                clearInterval(ot);
            }
        }, 3000);
    };
    var show_chat_box = function() {
        var otcounter = 0;
        var ot = setInterval(function() {
            console.log('TRYING CHAT BOX');
            if (window.olark) {
                console.log("CHAT BOX READY");
                window.olark('api.box.show');
                clearInterval(ot);
            }
            otcounter++;
            if (otcounter > 50) {
                clearInterval(ot);
            }
        }, 3000);
    };
    var notify = function(message_list) {
        console.log("NOTIFY >>", message_list);
        if (window.olark) {
            if (!(message_list instanceof Array)) {
                message_list = [message_list];
            }
            window.olark('api.chat.sendNotificationToOperator', {
                body: message_list.join('\n')
            });
        }
    };
    var event_log = function(action, label, value) {
        if (window.dataLayer) {
            window.dataLayer.push({
                'event': 'GAEvent',
                //'eventCategory': settings.getConfiguration().ga_category,
                'eventAction': action,
                'eventLabel': label,
                'eventValue': value
            });
        }
    };
    var gtm_event_log = function(event_name) {
        if (window.dataLayer) {
            window.dataLayer.push({
                'event': event_name
            });
        }
    };
    var gtm_data_log = function(data) {
        if (window.dataLayer) {
            window.dataLayer.push(data);
        }
    };
    var page_view = function(page, title) {
        if (window.dataLayer) {
            window.dataLayer.push({
                'event': 'virtual_page_view',
                'virtual_page_path': page,
                'virtual_page_title': title
            });
        }
    };
    var load_time = function(action, label, load_time, source) {
        if (window.dataLayer) {
            window.dataLayer.push({
                'event': 'ga_timer',
                'timingCategory': source,
                'timingVar': action,
                'timingLabel': label,
                'timingValue': load_time
            });
        }
    };

    var split = function(a, n) {
        var len = a.length,
            out = [],
            i = 0;
        while (i < len) {
            var size = Math.ceil((len - i) / n--);
            out.push(a.slice(i, i += size));
        }
        return out;
    };

    var insurers = {
        1: 'l-t',
        2: 'bajaj-allianz',
        3: 'bharti-axa',
        4: 'iffco-tokio',
        5: 'hdfc-ergo',
        7: 'tata-aig',
        8: 'liberty-videocon',
        9: 'universal-sompo',
        10: 'new-india',
        11: 'oriental',
    };
    var validPhone = function(data) {
        if (data.length == 10) { data = '0' + data;}
        return data.match('0[0-9]{10}');
    };
    var lmsUtils = {
        validPhone : function(data) {
            if (data == undefined || data == null) return false;
            if (data.length == 10) { data = '0' + data;}
            if (data.match('0[0-9]{10}')) return true;
        },
        sendData: function(formData, label) {
            try {
                if (typeof formData == 'string') {
                    jsonData = {};
                    $.each(formData, function() {
                        if (jsonData[this.name]) {
                            if (!jsonData[this.name].push) {
                                jsonData[this.name] = [jsonData[this.name]];
                            }
                            jsonData[this.name].push(this.value || '');
                        } else {
                            jsonData[this.name] = this.value || '';
                        }
                    });
                } else {
                    jsonData = formData;
                }

                if (!lmsUtils.validPhone(jsonData['cust_phone'])) {
                    return;
                }

                // LMS backend requires name, email and mobile as keys,
                // hence mapping from buy-form

                if (label == 'buy-form' || label == 'proposal-form') {
                    jsonData['transaction_url'] = window.location.href;
                    jsonData['mobile'] = jsonData['cust_phone'];
                    jsonData['name'] = jsonData['cust_first_name'] + ' ' + jsonData['cust_last_name'];
                    jsonData['email'] = jsonData['cust_email'];

                    delete jsonData['cust_phone'];
                    delete jsonData['cust_first_name'];
                    delete jsonData['cust_last_name'];
                    delete jsonData['cust_email'];
                }
                //////////////////////////////////////////////////////////////////
                //jsonData['campaign'] = settings.getConfiguration().lms_default_campaign;
                jsonData['label'] = label;
                jsonData['device'] = 'Desktop';
                jsonData['triggered_page'] = window.location.href;
                event_log('CallScheduled', label, 0);

                $.post('/leads/save-call-time/', {
                    'data': JSON.stringify(jsonData),
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    console.log("LMS data sent.");
                });
            } catch (e) {
                console.log("Error sending data to LMS.");
            }
        },
        sendGeneric: function(data, label, mobile, extras, callback) {
            try {
                if (!lmsUtils.validPhone(mobile)) {
                    return;
                }
                var jsonData = {};
                //jsonData['campaign'] = settings.getConfiguration().lms_default_campaign;
                jsonData['label'] = label;
                jsonData['mobile'] = mobile;
                jsonData['device'] = 'Desktop';
                jsonData['triggered_page'] = window.location.href;
                jsonData['data'] = data;

                for (var key in extras) {
                    jsonData[key] = extras[key];
                }


                console.log(jsonData);
                event_log('CallScheduled', label, 0);

                $.post('/leads/save-call-time/', {
                    'data': JSON.stringify(jsonData),
                    'csrfmiddlewaretoken': CSRF_TOKEN
                }, function(response) {
                    callback();
                    console.log("LMS data sent.");
                });
            } catch (e) {
                console.log("Error sendng data to LMS");
            }
        }
    };
    var motorOlark = {
        getVehicleAndRTO: function(quote) {
            var infoStrs = [];

            var newVehicleStr = 'Old';
            if (quote.isNewVehicle()) {
                newVehicleStr = 'New';
            }
            infoStrs.push('Vehicle Type: ' + newVehicleStr);

            var vehicleStr = '';
            if (quote.vehicle()) vehicleStr += quote.vehicle().name;
            if (quote.fuel_type()) vehicleStr += ' ' + quote.fuel_type().name;
            if (quote.variant()) vehicleStr += ' ' + quote.variant().name;
            if (vehicleStr != '') {
                infoStrs.push('Vehicle: ' + vehicleStr);
            }

            if (quote.rto_info()) {
                infoStrs.push('RTO Location: ' + quote.rto_info().name);
            }

            if (quote.isCNGFitted()) {
                infoStrs.push('CNG Kit Value: ' + quote.cngKitValue());
            }
            return infoStrs;
        },
        getIdvDetails: function(quote) {
            var infoStrs = [];

            var idvStr = ''
            if (quote.idv()) {
                idvStr = 'Idv: Lowest';
            } else {
                idvStr = 'Idv: ' + quote.idv();
            }
            infoStrs.push(idvStr);

            infoStrs.push('Registration Date: ' + quote.registrationDate());
            infoStrs.push('Manufacturing Date: ' + quote.manufacturingDate());
            infoStrs.push('Voluntary Deductible: ' + quote.voluntaryDeductible());
            infoStrs.push('Idv Electrical: ' + quote.idvElectrical() + ', Idv NonElectrical: ' + quote.idvNonElectrical());

            if (quote.isCNGFitted()) {
                infoStrs.push('CNG Kit Value: ' + quote.cngKitValue());
            }

            infoStrs.push('-------------------------------------------------------');
            infoStrs.push('Claimed Last Year: ' + quote.isClaimedLastYear());
            infoStrs.push('Previous NCB: ' + quote.previousNCB());
            infoStrs.push('-------------------------------------------------------');

            return infoStrs;
        },
        getAddonDetails: function(quote) {
            var infoStrs = [];
            for (var index in allAddons) {
                var addon = allAddons[index];
                infoStrs.push(addon.name + ' ' + quote[addon.id]());
            }
            return infoStrs;
        },
        sendStartPageNotification: function(quote) {
            try {
                var infoStrs = motorOlark.getVehicleAndRTO(quote);
                if (quote.reg_year()) {
                    infoStrs.push('Registration Year: ' + quote.reg_year());
                }
                notify(infoStrs);
            } catch (e) {
                console.log(e);
            }
        },
        sendQuoteNotification: function(quote, quoteUrl) {
            try {
                var infoStrs = motorOlark.getVehicleAndRTO(quote);
                infoStrs.push('-------------------------------------------------------');
                infoStrs = infoStrs.concat(motorOlark.getIdvDetails(quote));
                infoStrs.push('-------------------------------------------------------');
                inforStr = infoStrs.concat(quoteUrl);
                notify(infoStrs);
            } catch (e) {
                console.log(e);
            }
        },
        sendAddOnsNotification: function(quote) {
            try {
                var infoStrs = motorOlark.getAddonDetails(quote);
                notify(infoStrs);
            } catch (e) {
                console.log(e);
            }
        },
        sendPlansNotification: function(plans) {
            try {
                var planStrList = [];
                for (var index in plans) {
                    var p = plans[index];
                    var planStr = p.insurerSlug + ' | ' + p.selectedAddonsAvailable + ' | ' + p.calculatedAtIDV + ' | ' + p.calculatedPremium();
                    planStrList.push(planStr);
                }
                notify(planStrList);
            } catch (e) {
                console.log(e);
            }
        },
        sendSelectPlanNotification: function(plan) {
            try {
                var planStrList = ['Selected Plan', plan.insurerSlug + ' | ' + plan.calculatedAtIDV + ' | ' + plan.calculatedPremium()];
                notify(planStrList);
            } catch (e) {
                console.log(e);
            }
        },
        sendRefreshedPlanNotification: function(quote, plan) {
            try {
                var infoStrs = motorOlark.getVehicleAndRTO(quote);
                infoStrs.push('-------------------------------------------------------');
                infoStrs = infoStrs.concat(motorOlark.getIdvDetails(quote));
                infoStrs = infoStrs.concat(motorOlark.getAddonDetails(quote));
                infoStrs.push('-------------------------------------------------------');
                infoStrs.push(plan.insurerSlug + ' | ' + plan.calculatedAtIDV + ' | ' + plan.calculatedPremium());
                notify(infoStrs);
            } catch (e) {
                console.log(e);
            }
        },
        sendBuyFormNotification: function(formurl) {
            try {
                notify(['Buy form url: ', formurl]);
            } catch (e) {
                console.log(e);
            }
        },
    };

    var dateToString = function(date) {
        dd = date.getDate(),
        mm = date.getMonth() + 1,
        yyyy = date.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        return dd + '-' + mm + '-' + yyyy;
    };

    function stringToDate(_date, _format, _delimiter) {

        var formatLowerCase = _format.toLowerCase();
        var formatItems = formatLowerCase.split(_delimiter);
        var dateItems = _date.split(_delimiter);
        var monthIndex = formatItems.indexOf("mm");
        var dayIndex = formatItems.indexOf("dd");
        var yearIndex = formatItems.indexOf("yyyy");
        var month = parseInt(dateItems[monthIndex]);
        month -= 1;
        return new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
    }

    var validation = {
        isEmailAddress: function(str) {
            var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            return pattern.test(str);
        },
        isNotEmpty: function(str) {
            if (str == undefined) {
                return false;
            }
            var pattern = /\S+/;
            return pattern.test(str);
        },
        isNumber: function(str) {
            var pattern = /^\d+$/;
            return pattern.test(str);
        },
    };

    var getDaysBetweenDates = function(date1, date2) {
        var timeDiff = date2.getTime() - date1.getTime(),
            diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        return diffDays;
    }

    function firstDayOfPreviousMonth(yourDate) {
        return new Date(yourDate.getFullYear(), yourDate.getMonth() - 1, 1);
    }

    function calculateAgeFromDate(date) {
        var todayDate = new Date(),
            todayYear = todayDate.getFullYear(),
            todayMonth = todayDate.getMonth(),
            todayDay = todayDate.getDate(),
            startMonth = date.getMonth(),
            startDay = date.getDate(),
            startYear = date.getFullYear(),
            age = todayYear - startYear;

        if (todayMonth < startMonth - 1) {
            age--;
        }

        if (startMonth - 1 == todayMonth && todayDay < startDay) {
            age--;
        }
        return age;
    }

    function arr_diff(a1, a2) {
        var a = [],
            diff = [];
        for (var i = 0; i < a1.length; i++)
            a[a1[i]] = true;
        for (var i = 0; i < a2.length; i++)
            if (a[a2[i]]) delete a[a2[i]];
            else a[a2[i]] = true;
        for (var k in a)
            diff.push(k);
        return diff;
    }

    return {
        split: split,
        notify: notify,
        event_log: event_log,
        gtm_event_log: gtm_event_log,
        gtm_data_log: gtm_data_log,
        page_view: page_view,
        load_time: load_time,
        show_chat_box: show_chat_box,
        hide_chat_box: hide_chat_box,
        insurers: insurers,
        motorOlark: motorOlark,
        lmsUtils: lmsUtils,
        validation: validation,
        dateToString: dateToString,
        stringToDate: stringToDate,
        smoothScroll: smoothScroll,
        getDaysBetweenDates: getDaysBetweenDates,
        firstDayOfPreviousMonth: firstDayOfPreviousMonth,
        calculateAgeFromDate: calculateAgeFromDate,
        arr_diff:arr_diff,
        DOBFormat: DOBFormat,
        reverseDOBFormat:reverseDOBFormat,
        Card: Card,
        Container: Container,
        Group: Group,
        StaticContainer: StaticContainer,
        Question: Question,
        QuestionContainer: QuestionContainer,
    }
});
