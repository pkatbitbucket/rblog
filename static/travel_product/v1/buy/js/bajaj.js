var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};

var OCCUPATIONS = ['Retired', 'Executive - Senior Level', 'Executive - Middle Level', 'Executive Junior Level', 'Software Professional', 'Supervisor', 'Clerical', 'Salesperson', 'Self employed Professional', 'Shop owner', 'Student', 'Not Working', 'Housewife', 'Advocate / Lawyer', 'Chartered Accountants', 'Labourer', 'Farming', 'Service', 'Businessman/Industrialist Large Scale', 'Businessman/Industrialist Medium Scale', 'Businessman/Industrialist Small Scale', 'Reinforcing Fitter', 'Carpenter', 'Steel Fixer', 'Mason', 'Cook', 'Others'];

var STATES = ['Andaman And Nicobar Islands', 'Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chandigarh U.T.', 'Chhattisgarh', 'Dadra and Nagar Haveli', 'Daman and Diu', 'Delhi', 'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jammu and Kashmir', 'Jharkhand', 'Karnataka', 'Kerala', 'Lakshadweep U.T.', 'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Mizoram', 'Nagaland', 'Orissa', 'Pondicherry U.T.', 'Punjab', 'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Tripura', 'Uttar Pradesh', 'Uttaranchal', 'West Bengal', 'Meghalaya' ];

var COUNTRIES = ['Afganistan', 'Albania', 'Algeria', 'Andorra', 'Angola', 'Argentina', 'Armenia', 'Aruba', 'Ascension', 'Australia', 'Austria', 'Azerbaijan Republic', 'Azores', 'Bahamas', 'Bermuda', 'Bahrain', 'Bangkok', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bhutan', 'Bolivia', 'Boputatswana', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'Brunei Darussalam', 'Bulgaria', 'Burkina Fasso', 'Burundi', 'Cayman Islands', 'Cambodia', 'Cameroon', 'Canada', 'Canary Island', 'Cape Verde', 'Central African Republic', 'Chad', 'Chile', 'China', 'Ciskei', 'Cocos Keeling Island', 'Colombia', 'Comoros', 'Congo', 'Cook Island', 'Costa Rica', 'Croatia', 'Cuba', 'Cyprus', 'Czech Republic', 'Denmark', 'Diego Garcia', 'Djibouti', 'Dominican Republic', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador Rep.', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands', 'Faroe Island', 'Fiji Republic', 'Finland', 'Fr.Guinea', 'Fr.Polynesia', 'France', 'France', 'Gabonese Republic', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guinea', 'Guinea Bissau', 'Guyana', 'Haiti', 'Honduras', 'Hongkong', 'Hungary', 'Ivory Coast', 'Iceland', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kirghistan', 'Kiribati', 'Korea (North)', 'Korea (South)', 'Kuwait', 'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechstein', 'Lithuania', 'Luxembourg', 'MONTENEGRO', 'Macao', 'Macedonia', 'Madagascar', 'Madeira Island', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Mariana Island', 'Marshal Island', 'Martinque', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia', 'Moldova', 'Monaco', 'Mongolia', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'NICARAGUA', 'NORFOLK ISLAND', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'Netherlands Anthilles', 'New Caledonia', 'New Zealand', 'Niger', 'Nigeria', 'Niue Island', 'Norway', 'Oman', 'PUERTO RICO', 'Pakistan', 'Palau', 'Palestine', 'Panama', 'Papau New Guinea', 'Paraguay', 'Peru', 'Phillipines', 'Poland', 'Portugal', 'Qatar', 'Reunion', 'Rodrigues Island', 'Romania', 'Russian Federation', 'Rwandese Republic', 'Serbia', 'SlovakiA', 'St Kitts & Nevis', 'St Lucia (WI)', 'St Vincent & Grenadines (WI)', 'Samoa American', 'Samoa Western', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovak Republic', 'Slovenia', 'Soloman Island', 'Somalia Democratic Republic', 'South Africa', 'Spain', 'Srilanka', 'St.Helena', 'St.Pierre and Miquelon', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria', 'Turks & Caicos Islands', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togolese Republic', 'Tonga', 'Transkei', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Tuvalu', 'Uganda', 'USA', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'Uruguay', 'Uzbekistan', 'Virgin Islands (British)', 'Virgin Islands (US)', 'Vanuatu', 'Vatican City State', 'Venda', 'Venezuela', 'Vietnam', 'West Indies', 'Wallis and Futina Island', 'Yemen (Saana)', 'Yogoslavia', 'Zaire', 'Zambia', 'Zimbabwe'];

var RELATIONS = ['Spouse', 'Son', 'Daughter', 'Brother', 'Sister', 'Mother', 'Father', 'Others'];

var Member_model = function(type, DATA) {
    var self = this;
    self.type = type;
    self.salute = ko.observable(DATA[self.type+'Salute']).extend({required: true});
    self.martialstatus = ko.observable(DATA[self.type+'MartialStatus']).extend({required: true});
    self.fname = ko.observable(DATA[self.type+'FName']).extend({required: true});
    self.mname = ko.observable(DATA[self.type+'MName']);
    self.lname = ko.observable(DATA[self.type+'LName']);
    self.dob = ko.observable(DATA[self.type+'DOB']).extend({required: true, validation: date_validation});
    self.gender = ko.observable(DATA[self.type+'gender']).extend({required: true});

    switch(self.type) {
        case 'applicant':
            self.relationship_readonly = 'readonly';
            self.applicant_relations = ['Self'];
            break;
        case 'spouse':
            self.relationship_readonly = 'readonly';
            self.applicant_relations = ['Spouse'];
            break;
        default:
            self.relationship_readonly = '';
            self.applicant_relations = ['Dependent Son', 'Dependent Daughter'];
    }
    self.relationship = ko.observable(DATA[self.type+'Relationship']
                                        || (self.relationship_readonly ? self.applicant_relations[0] : null))
                            .extend({required: true});

    self.passport = ko.observable(DATA[self.type+'Passport']).extend({required: true});
    self.in_India = ko.observable(DATA['in_India']).extend({
        required: true,
        validation:{
            validator: function(val) {
                return val == 'Yes';
            },
            message: 'The customer currently has to be in India to buy this policy'
        }
    });
    // self.occupation = ko.observable(DATA[self.type+'Occupation']).extend({required: true});
    self.assignee_name = ko.observable(DATA[self.type+'AssigneeName']).extend({required: true});
    self.under_medication = ko.observable(DATA[self.type+'UnderMedication']).extend({required: true});
    self.illness_details = ko.observable(DATA[self.type+'IllnessDetails']).extend({required: true});
    self.suffering_since = ko.observable(DATA[self.type+'SufferingSince']).extend({required: true, validation: date_validation});
}
Member_model.prototype.submit = function() {
    var self = this;
    if(self.under_medication() == 'False') {
        self.illness_details('');
        self.suffering_since('');
    }
}

var Address_model = function(type, legend, DATA) {
    var self = this;
    self.visible = ko.observable(true);
    self.type = type;
    self.legend = legend;
    self.building = ko.observable(DATA[self.type+'_Building']).extend({required: true});
    self.addr2 = ko.observable(DATA[self.type+'_StreetName']);
    self.pincode = ko.observable(DATA[self.type+'Pincode']);

    self.country = ko.observable(DATA[self.type+'_country'] || (self.type != 'university' ? 'India' : null)).extend({required: true});
    self.state = ko.observable(DATA[self.type+'_state']).extend({required: true});
    self.city = ko.observable(DATA[self.type+'_city']).extend({required: true});
    self.email = ko.observable(DATA[self.type+'_email']).extend({email:true});
    self.mobile = ko.observable(DATA[self.type+'_mobile']).extend({
        required: true,
        //pattern: {
        //     message: 'Please enter a valid phone number.',
        //    params: /^[1,9]\d{9}$/
        //}
    });
    self.nationality = ko.observable(DATA[self.type+'_nationality'] || 'Indian').extend({required: true});
}
Address_model.prototype.copy_from = function(address) {
    var self = this;
    self.building(address.building());
    self.addr2(address.addr2());
    self.pincode(address.pincode());
    self.country(address.country());
    self.state(address.state());
    self.city(address.city());
    self.email(address.email());
    self.mobile(address.mobile());
    self.nationality(address.nationality());
}

var buy_model = function(DATA) {
    var self = this;
    self.COUNTRIES = COUNTRIES;
    self.OCCUPATIONS = OCCUPATIONS;
    self.STATES = STATES;
    self.RELATIONS = RELATIONS;
    self.SALUTATIONS = ['Mr', 'Mrs', 'Miss'];
    self.MARTIALSTATUS = ['Single', 'Married'];
    self.members_list = ko.observableArray(
        ko.utils.arrayMap(MEMBERS, function(mem) {
            return new Member_model(mem, DATA);
        })
    );

    var communication_address = new Address_model('communication', 'Communication address', DATA);
    var home_address = new Address_model('home', 'Home address (for burgalary protection)', DATA);
    self.home_required = HOME_REQUIRED;
    self.addresses = ko.observableArray([communication_address, home_address]);
    if(self.home_required) {
        self.same_home_address = ko.observable(DATA.same_home_address != undefined ? DATA.same_home_address : true);
        home_address.visible(!self.same_home_address());
        self.same_home_address.subscribe(function(val) {
            home_address.visible(!val);
        });
    }
    else
        home_address.visible(false);
    if(PRODUCT_TYPE == 'Student') {
        var university_address = new Address_model('university', 'University Information', DATA);
        self.addresses.push(university_address);
    }

    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  ($('p.validationMessage').filter(':visible').length > 0);
        if(!errors) {
            for(var i=0; i < self.members_list().length; ++i) {
                self.members_list()[i].submit();
            }
            if(self.same_home_address)
                home_address.copy_from(communication_address);
            return true;
        }
        return false;
    };
};
var DATA = DATA || {};

ko.validation.configuration['insertMessages'] = false;
ko.validation.configuration['grouping']['deep'] = true;
var FORM = new buy_model(DATA);
ko.applyBindings(FORM);
ko.validation.group(FORM).showAllMessages(false);
