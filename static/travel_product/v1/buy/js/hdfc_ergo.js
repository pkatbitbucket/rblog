var COUNTRIES = ['Afghanistan',  'Albania',  'Algeria',  'American Samoa',  'Andorra',  'Angola',  'Anguilla',  'Antarctica',  'Antigua And Barbuda',  'Argentina',  'Armenia',  'Aruba',  'Australia',  'Austria',  'Azerbaijan',  'Bahamas',  'Bahrain',  'Bangladesh',  'Barbados',  'Belarus',  'Belgium',  'Belize',  'Benin',  'Bermuda',  'Bhutan',  'Bolivia',  'Bosnia And Herzegovina',  'Botswana',  'Bouvet Island',  'Brazil',  'British Indian Ocean Territory',  'Brunei Darussalam',  'Bulgaria',  'Burkina Faso',  'Burundi',  'Cambodia',  'Cameroon',  'Canada',  'Cape Verde',  'Cayman Islands',  'Central African Republic',  'Chad',  'Chile',  'China',  'Christmas Island',  'Cocos (keeling) Islands',  'Colombia',  'Comoros',  'Congo',  'Congo, The Democratic Republic Of The',  'Cook Islands',  'Costa Rica',  "Cote D'ivoire",  'Croatia',  'Cuba',  'Cyprus',  'Czech Republic',  'Denmark',  'Djibouti',  'Dominica',  'Dominican Republic',  'East Timor',  'Ecuador',  'Egypt',  'El Salvador',  'Equatorial Guinea',  'Eritrea',  'Estonia',  'Ethiopia',  'Falkland Islands (malvinas)',  'Faroe Islands',  'Fiji',  'Finland',  'France',  'French Guiana',  'French Polynesia',  'French Southern Territories',  'Gabon',  'Gambia',  'Georgia',  'Germany',  'Ghana',  'Gibraltar',  'Greece',  'Greenland',  'Grenada',  'Guadeloupe',  'Guam',  'Guatemala',  'Guinea',  'Guinea-bissau',  'Guyana',  'Haiti',  'Heard Island And Mcdonald Islands',  'Holy See (vatican City State)',  'Honduras',  'Hong Kong',  'Hungary',  'Iceland',  'India',  'Indonesia',  'Iran, Islamic Republic Of',  'Iraq',  'Ireland',  'Israel',  'Italy',  'Jamaica',  'Japan',  'Jordan',  'Kazakstan',  'Kenya',  'Kiribati',  "Korea, Democratic People's Republic Of",  'Korea, Republic Of',  'Kosovo',  'Kuwait',  'Kyrgyzstan',  "Lao People's Democratic Republic",  'Latvia',  'Lebanon',  'Lesotho',  'Liberia',  'Libyan Arab Jamahiriya',  'Liechtenstein',  'Lithuania',  'Luxembourg',  'Macau',  'Macedonia, The Former Yugoslav Republic Of',  'Madagascar',  'Malawi',  'Malaysia',  'Maldives',  'Mali',  'Malta',  'Marshall Islands',  'Martinique',  'Mauritania',  'Mauritius',  'Mayotte',  'Mexico',  'Micronesia, Federated States Of',  'Moldova, Republic Of',  'Monaco',  'Mongolia',  'Montserrat',  'Montenegro',  'Morocco',  'Mozambique',  'Myanmar',  'Namibia',  'Nauru',  'Nepal',  'Netherlands',  'Netherlands Antilles',  'New Caledonia',  'New Zealand',  'Nicaragua',  'Niger',  'Nigeria',  'Niue',  'Norfolk Island',  'Northern Mariana Islands',  'Norway',  'Oman',  'Pakistan',  'Palau',  'Palestinian Territory, Occupied',  'Panama',  'Papua New Guinea',  'Paraguay',  'Peru',  'Philippines',  'Pitcairn',  'Poland',  'Portugal',  'Puerto Rico',  'Qatar',  'Reunion',  'Romania',  'Russian Federation',  'Rwanda',  'Saint Helena',  'Saint Kitts And Nevis',  'Saint Lucia',  'Saint Pierre And Miquelon',  'Saint Vincent And The Grenadines',  'Samoa',  'San Marino',  'Sao Tome And Principe',  'Saudi Arabia',  'Senegal',  'Serbia',  'Seychelles',  'Sierra Leone',  'Singapore',  'Slovakia',  'Slovenia',  'Solomon Islands',  'Somalia',  'South Africa',  'South Georgia And The South Sandwich Islands',  'Spain',  'Sri Lanka',  'Sudan',  'Suriname',  'Svalbard And Jan Mayen',  'Swaziland',  'Sweden',  'Switzerland',  'Syrian Arab Republic',  'Taiwan, Province Of China',  'Tajikistan',  'Tanzania, United Republic Of',  'Thailand',  'Togo',  'Tokelau',  'Tonga',  'Trinidad And Tobago',  'Tunisia',  'Turkey',  'Turkmenistan',  'Turks And Caicos Islands',  'Tuvalu',  'Uganda',  'Ukraine',  'United Arab Emirates',  'United Kingdom',  'United States',  'United States Minor Outlying Islands',  'Uruguay',  'Uzbekistan',  'Vanuatu',  'Venezuela',  'Viet Nam',  'Virgin Islands, British',  'Virgin Islands, U.s.',  'Wallis And Futuna',  'Western Sahara',  'Yemen',  'Zambia',  'Zimbabwe'];
var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};

var Member_model = function(type, DATA) {
    var self = this;
    self.type = type;
    self.salutation_visible = (DATA.policy_type=='S');
    self.salutation = ko.observable(DATA[self.type+'salutation']).extend({required: {onlyIf: function(){
        return DATA.policy_type =='S';
    }}});
    self.fname = ko.observable(DATA[self.type+'FName']).extend({required: true});
    self.mname = ko.observable(DATA[self.type+'MName']).extend({required: {onlyIf: function(){
     return DATA.policy_type =='S';
    }}});
    self.lname = ko.observable(DATA[self.type+'LName']).extend({required: true});
    self.dob = ko.observable(DATA[self.type+'DOB']).extend({required: true, validation: date_validation});
    self.gender = ko.observable(DATA[self.type+'gender']).extend({required: true});
    self.passportno = ko.observable(DATA[self.type+'Passportno']).extend({required: true});
    self.nname = ko.observable(DATA[self.type+'Nname']).extend({required: true});
    self.Nname_rel = ko.observable(DATA[self.type+'Nname_rel']).extend({required: true});
}

var buy_model = function(DATA) {
    var self = this;

    self.policy_type = ko.observable(DATA.policy_type);
    self.FName = ko.observable(DATA.FName).extend({
    required: true,
    });
    self.MName = ko.observable(DATA.MName)
    self.LName = ko.observable(DATA.LName).extend({
    required: true,
    });

    self.gender = ko.observable(DATA.gender).extend({
    required: true,
    });

    self.Passportno = ko.observable(DATA.Passportno).extend({
    required: true,
    });

    self.Nname = ko.observable(DATA.Name).extend({
    required: true,
    });
    self.Nname_rel = ko.observable(DATA.Nname_rel).extend({
    required: true,
    });
    self.nominee_relation_options = [
        {id: 'D', name: 'Daughter'},
        {id: 'F', name: 'Father'},
        {id: 'FL', name: 'Father-in-Law'},
        {id: 'GD', name: 'Grand Daughter'},
        {id: 'GF', name: 'Grand Father'},
        {id: 'GM', name: 'Grand Mother'},
        {id: 'GS', name: 'Grand Son'},
        {id: 'M', name: 'Mother'},
        {id: 'ML', name: 'Mother-in-Law'},
        {id: 'O', name: 'Other'},
        {id: 'P', name: 'Spouse'},
        {id: 'S', name: 'Son'},
        {id: 'N', name: 'Nephew'},
        {id: 'SL', name: 'Son-in-Law'},
    ];
    self.p_visit = ko.observable(DATA.p_visit).extend({
    required: true,
    });

    self.pre_existing_illness = ko.observable(DATA.pre_existing_illness).extend({
    required: true,
    });

    self.decline_insurance = ko.observable(DATA.decline_insurance).extend({
    required: true,
    });

    self.restriction_by_insurance = ko.observable(DATA.restriction_by_insurance).extend({
    required: true,
    });

    self.DOB = ko.observable(DATA.DOB).extend({
    required: true,
    validation: date_validation
    });


    self.mobile = ko.observable(DATA.mobile).extend({
    required: true,
    });

    self.email = ko.observable(DATA.email).extend({
    required: true,
    });

    self.ResAddr1 = ko.observable(DATA.ResAddr1).extend({
    required: true,
    });

    self.ResAddr2 = ko.observable(DATA.ResAddr2).extend({
    required: true,
    });

    self.ResAddr3 = ko.observable(DATA.ResAddr3).extend({
    required: true,
    });
    self.u_state = ko.observable(DATA.u_state).extend({
    required: true,
    });

    self.u_city = ko.observable(DATA.u_city).extend({
    required: true,
    });

    self.state = ko.observable(DATA.state).extend({
    required: true,
    });

    self.city = ko.observable(DATA.city).extend({
    required: true,
    });

    self.state_options = STATES;

    self.city_options = ko.computed(function(){
    return CITIES[parseInt(self.state())]
    });

    self.pincode = ko.observable(DATA.pincode).extend({
    required: true,
    });
    self.u_name= ko.observable(DATA.u_name).extend({
    required: true,
    });
    self.u_prog_name = ko.observable(DATA.u_prog_name).extend({
    required: true,
    });
    self.u_prog_duration = ko.observable(DATA.u_prog_duration).extend({
    required: true,
    });
    self.u_ResAddr1 = ko.observable(DATA.u_ResAddr1).extend({
    required: true,
    });
    self.u_ResAddr2 = ko.observable(DATA.u_ResAddr2).extend({
    required: true,
    });
    self.u_ResAddr3 = ko.observable(DATA.u_ResAddr3).extend({
    required: true,
    });
    self.sponsor_name = ko.observable(DATA.sponsor_name).extend({
    required: true,
    });
    self.s_dob = ko.observable(DATA.s_dob).extend({
    required: true,
    });
    self.medical_treatment = ko.observable(DATA.medical_treatment).extend({
    required: true,
    });
    self.physician_name = ko.observable(DATA.physician_name)
    self.physician_mob = ko.observable(DATA.physician_mob)
    self.country = ko.observable(DATA.country).extend({
    required: true,
    });
    self.countries_list = COUNTRIES;
    self.university_visible = ko.computed(function() {
        return self.policy_type() == 'S';
    });

    self.members_list = ko.observableArray(
        ko.utils.arrayMap(MEMBERS, function(mem) {
            return new Member_model(mem, DATA);
        }));

    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  $('p.validationMessage').filter(':visible').length;
        if(errors > 0) return false;
        else {
            return true;
        }
    };
};

var DATA = DATA || {};

ko.validation.configuration['insertMessages'] = false;
ko.validation.configuration['grouping']['deep'] = true;
var FORM = new buy_model(DATA);
ko.applyBindings(FORM);
ko.validation.group(FORM).showAllMessages(false);
