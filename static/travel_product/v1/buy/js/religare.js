var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};


var RELATIONS_TEXT = ["MOTHER","UNCLE","NEW BORN BABY","NEPHEW","NIECE",
		 "PATERNAL AUNT","Paternal Uncle","SELF- PRIMARY MEMBER",
		 "SISTER","SON IN LAW","SON","SPOUSE","KARTA","COPARCENER","DAUGHTER"]

function validate_disease(preferences_for_disease)
{
    
    for (index = 0; index < preferences_for_disease.length; ++index) {
	if (preferences_for_disease[index]() == 'yes')
	    return false;
    }
    return true;
}
var RELATIONS_LIST = []

var member_model = function(type, DATA){
    var self =this;
    self.type = type;
    self.Relationship = ko.observable(DATA[type+'Relationship']).extend({required:true});
    self.fname = ko.observable(DATA[self.type+'FName']).extend({required: true});
    self.lname = ko.observable(DATA[self.type+'LName']).extend({required:true});
    self.dob = ko.observable(DATA[self.type+'DOB']).extend({required: true, validation: date_validation});
    self.passport = ko.observable(DATA[self.type+'Passport']).extend({required:true});

    
    self.relationship = ko.observable(DATA[self.type+'Relationship']).extend({required: true});

    self.pre_exis_disease = ko.observable(DATA[self.type+'pedYesNoTravel']).extend({required: true});
   
    self.pre_exis_disease_details_visible = ko.computed(function(){
	return self.pre_exis_disease() == 'yes';
       });

    self.liver = ko.observable(DATA[self.type+'PEDliverDetailsTravel']).extend({required: true});
    self.cancer = ko.observable(DATA[self.type+'PEDcancerDetailsTravel']).extend({required: true});
    self.heart_disease = ko.observable(DATA[self.type+'PEDHealthDiseaseTravel']).extend({required: true});
    self.kidney_disease = ko.observable(DATA[self.type+'PEDkidneyDetailsTravel']).extend({required: true});
    self.paralysis_diseas = ko.observable(DATA[self.type+'PEDparalysisDetailsTravel']).extend({required: true});
    self.other_disease = ko.observable(DATA[self.type+'PEDotherDetailsTravel']).extend({required: true});
    self.other_disease_desc = ko.observable(DATA[self.type+'other_disease_desc']).extend({required: true});
    self.HEDTravelClaimPolicy= ko.observable(DATA[self.type+'HEDTravelClaimPolicy']).extend({required: true});
    self.HEDTravelHospitalized = ko.observable(DATA[self.type+'HEDTravelHospitalized']).extend({required: true});
    self.preferences_for_disease = [self.liver,
				    self.cancer,
				    self.heart_disease,
				    self.kidney_disease,
				    self.paralysis_diseas,
				    self.other_disease
				   ];
    
    self.validate_disease = ko.computed(function(){
	if (self.pre_exis_disease() == 'yes')
	    return validate_disease(self.preferences_for_disease)
	else
	    return false;
	});

    self.relationship_text = RELATIONS_TEXT;
    
    self.gender = ko.observable(DATA[self.type+'gender']).extend({required: true});
//    self.salute = ko.observable(DATA[self.type+'Salute']).extend({required: true});
    self.other_disease_visible = ko.computed(function(){
	return self.other_disease() == 'yes';
       });

    self.self_proposer_visible = ko.computed(function(){
	return (self.relationship() != 'SELF- PRIMARY MEMBER')
    });
    
}
	
member_model.prototype.submit = function() {
    var self = this;
    if(self.other_disease() == 'no') {
        self.other_disease_desc('');
    }
}

var buy_model = function(DATA) {
    var self = this;
    self.fname = ko.observable(DATA['FName']).extend({required: true});
    self.lname = ko.observable(DATA['LName']).extend({required:true});
    self.dob = ko.observable(DATA['DOB']).extend({required: true, validation: date_validation});
    self.nominee_name = ko.observable(DATA['NomineeName']).extend({required: true});
    self.addr1 = ko.observable(DATA['ResAddr1']).extend({required: true});
    self.addr2 = ko.observable(DATA['ResAddr2']).extend({required: true});
    self.pincode = ko.observable(DATA['pincode']).extend({required: true});
    self.city = ko.observable(DATA['city']).extend({required: true});
    self.state = ko.observable(DATA['state']).extend({required: true});
    self.email = ko.observable(DATA['email']).extend({email: true});
    self.mobile = ko.observable(DATA['mobile']).extend({required: true});
    self.gender = ko.observable(DATA['gender']).extend({required: true});
    
    self.trip_start = ko.observable(DATA.trip_start != '0');
    self.alert = ko.observable(DATA.alert != '0');
    self.tc_agree = ko.observable(DATA.tc_agree != '0');
  
    self.members_list = ko.observableArray(
        ko.utils.arrayMap(MEMBERS, function(mem) {
            return new member_model(mem, DATA);
        })
    );
    
    self.relation_error_visible = ko.computed(function(){
	var count = 0;
	for (i = 0; i<self.members_list().length; ++i)
	{
	    if (self.members_list()[i].relationship() == 'SELF- PRIMARY MEMBER')
		count++;
	}
	return count > 1;
    });

    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        var errors =  ($('p.validationMessage').filter(':visible').length > 0);
        if(!errors) {
	    for(var i=0; i < self.members_list().length; ++i) {
                self.members_list()[i].submit();
            }
            return true;
        }
	return false;
    };
    
};

ko.validation.configuration['insertMessages'] = false;
ko.validation.configuration['grouping']['deep'] = true;

var DATA = DATA || {};
var FORM = new buy_model(DATA);
ko.applyBindings(FORM);
ko.validation.group(FORM).showAllMessages(false);
