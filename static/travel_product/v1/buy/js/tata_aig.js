var date_validation = {
    validator: function(val, validate) {
        var re = /^(\d{1,2})-([a-zA-Z]{3})-(\d{4})$/;
        var match = val.match(re);
        if(!match) return false;
        var days = {'jan': 31, 'feb': 28, 'feb_leap': 29, 'mar': 31, 'apr': 30, 'may': 31, 'jun': 30, 'jul': 31, 'aug': 31, 'sep': 30, 'oct': 31, 'nov':30, 'dec': 31};
        var day = parseInt(match[1]);
        var month = match[2].toLowerCase();
        var year = parseInt(match[3]);
        if(month == 'feb' && day == 29) {
            month = ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 'feb_leap' : 'feb';
        }
        return (day > 0) && (day <= days[month]);
    },
    message: 'Please enter a valid date'
};


var Member_model = function(type, DATA) {
    var self = this;
    self.type = type;
    self.fname = ko.observable(DATA[self.type+'FName']).extend({required: true});
    self.mname = ko.observable(DATA[self.type+'MName']);
    self.lname = ko.observable(DATA[self.type+'LName']).extend({required: true});
    self.dob = ko.observable(DATA[self.type+'DOB']).extend({required: true, validation: date_validation});
    self.gender = ko.observable(DATA[self.type+'gender']).extend({required: true});

    self.qn1_chkbox = ko.observable(DATA[self.type+'qn1_chkbox'] || false);
    self.qn2_chkbox = ko.observable(DATA.qn2_chkbox || false);
    self.qn3_chkbox = ko.observable(DATA.qn3_chkbox || false);

    self.treatment = ko.observable(DATA[self.type+'treatment']).extend({required: {onlyIf : self.qn1_chkbox}});
    self.institution = ko.observable(DATA[self.type+'institution']).extend({required:{onlyIf: self.qn1_chkbox}});
    self.doc_details = ko.observable(DATA[self.type+'doc_details']).extend({required:{onlyIf: self.qn1_chkbox}});

    self.prescribed_medication = ko.observable(DATA[self.type+'prescribed_medication']).extend({required: {onlyIf : self.qn2_chkbox}});
    self.time_since = ko.observable(DATA[self.type+'time_since']).extend({required: {onlyIf : self.qn2_chkbox}});

    self.pol_no = ko.observable(DATA[self.type+'pol_no']).extend({required: {onlyIf : self.qn3_chkbox}});
    self.i_comp = ko.observable(DATA[self.type+'i_comp']).extend({required: {onlyIf : self.qn3_chkbox}});
    self.address = ko.observable(DATA[self.type+'address']).extend({required: {onlyIf : self.qn3_chkbox}});

}
var buy_model = function(DATA) {
    var self = this;
    self.FName = ko.observable(DATA.FName).extend({
    required: true,
    });

    self.MName = ko.observable(DATA.MName).extend({
    required: true,
    });
    self.LName = ko.observable(DATA.LName).extend({
    required: true,
    });

    self.gender = ko.observable(DATA.gender).extend({
    required: true,
    });

    self.DOB = ko.observable(DATA.DOB).extend({
    required: true,
    validation: date_validation
    });
    self.qn1_chkbox = ko.observable(DATA.qn1_chkbox || false);
    self.qn2_chkbox = ko.observable(DATA.qn2_chkbox || false);
    self.qn3_chkbox = ko.observable(DATA.qn3_chkbox || false);
    self.agree = ko.observable(DATA.agree).extend({
    required: true,
    });

    self.ResAddr1 = ko.observable(DATA.ResAddr1).extend({
    required: true,
    });

    self.ResAddr2 = ko.observable(DATA.ResAddr2).extend({
    required: true,
    });

    self.ResAddr3 = ko.observable(DATA.ResAddr3).extend({
    required: true,
    });

    self.district_name = ko.observable(DATA.district_name).extend({
    required: true,
    });

    self.email = ko.observable(DATA.email).extend({
        required: true,
        email: true
    });

    self.mobile = ko.observable(DATA.mobile).extend({
        required: true,
        pattern: {
            params: /^[7-9][0-9]{9}$/,
            message: "Please enter a valid phone number"
        }
    });

    self.state = ko.observable(DATA.state).extend({
    required: true,
    });

    self.city = ko.observable(DATA.city).extend({
    required: true,
    });

    self.pincode = ko.observable(DATA.pincode).extend({
    required: true,
    });

    self.qn1 = ko.observable(DATA.qn1).extend({
    required: true,
    });
    self.qn2 = ko.observable(DATA.qn2).extend({
    required: true,
    });
    self.qn3 = ko.observable(DATA.qn3).extend({
    required: true,
    });
    self.qn1visible = ko.computed(function(){
        return self.qn1() == 'Y';
    });
    self.qn2visible = ko.computed(function(){
        return self.qn2() == 'Y';
    });
    self.qn3visible = ko.computed(function(){
        return self.qn3() == 'Y';
    });
    self.members_list = ko.observableArray(
        ko.utils.arrayMap(MEMBERS, function(mem) {
            return new Member_model(mem, DATA);
        }));

    self.validate_form = function() {
        var validation_group = ko.validation.group(self);
        validation_group.showAllMessages();
        
        var errors =  $('p.validationMessage').filter(':visible').length;
        if(errors > 0) return false;
        else {
            return true;
        }
    };
};
var DATA = DATA || {};

ko.validation.configuration['insertMessages'] = false;
ko.validation.configuration['grouping']['deep'] = true;
var FORM = new buy_model(DATA);
ko.applyBindings(FORM);
ko.validation.group(FORM).showAllMessages(false);
