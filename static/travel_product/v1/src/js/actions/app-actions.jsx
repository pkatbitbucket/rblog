var AppConstants = require('../constants/app-constants.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')


var AppActions = {

	recieveCountries : function(countries){
		AppDispatcher.dispatch({
			actionType: AppConstants.RECIEVE_COUNTRIES,
			countries: countries
		})
	},

	selectCountry : function(index){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_COUNTRY,
			index: index
		})
	},

	selectRegion : function(region){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_REGION,
			region: region
		})
	},

	selectMultipleCountries : function(indexes){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_MULTIPLE_COUNTRIES,
			indexes: indexes
		})
	},

	deSelectCountry : function(index){
		AppDispatcher.dispatch({
			actionType: AppConstants.DESELECT_COUNTRY,
			index: index
		})
	},

	deselectAllCountries : function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.DESELECT_ALL_COUNTRIES,
		})
	},

	selectTripType : function(tripType){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_TRIP_TYPE,
			tripType: tripType
		})
	},

	setTravelers: function(number){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_TRAVELERS,
			number: number
		})
	},

	setResidence: function(residence){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_RESIDENCE,
			residence: residence
		})
	},

	setResidentFor: function(value){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_RESIDENT_FOR,
			value: value
		})
	},

	setOCICard: function(value){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_OCI_CARD,
			value: value
		})
	},

	setCurrentLocation: function(location){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_CURRENT_LOCATION,
			location: location
		})
	},

	selectDuration: function(duration){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_DURATION,
			duration: duration
		})
	},

	selectStartDate: function(date){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_START_DATE,
			date: date
		})
	},

	selectEndDate: function(date){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_END_DATE,
			date: date
		})
	},

	addTravelerAges: function(ages){
		AppDispatcher.dispatch({
			actionType: AppConstants.ADD_TRAVELER_AGES,
			ages: ages
		})
	},

	setMedicalCover: function(value){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_MEDICAL_COVER,
			value: value
		})
	},

	recieveQuotes: function(quotes, sum_assured, redirect_url){
		AppDispatcher.dispatch({
			actionType: AppConstants.RECIEVE_QUOTES,
			quotes: quotes,
			sum_assured: sum_assured,
			redirect_url: redirect_url
		})
	},

	fetchQuotes: function(callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.FETCH_QUOTES,
			callback:callback
		})
	},

	updateOtherPED: function(value){
		AppDispatcher.dispatch({
			actionType: AppConstants.UPDATE_OTHER_PED,
			value:value
		})
	},

	selectPED: function(index){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_PED,
			index:index
		})
	},

	deselectPED: function(index){
		AppDispatcher.dispatch({
			actionType: AppConstants.DESELECT_PED,
			index:index
		})
	},

	deselectAllPEDs: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.DESELECT_ALL_PED,
		})
	},
	selectPlan: function(index){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_PLAN,
			index:index
		})
	},

	deselectPlan: function(index){
		AppDispatcher.dispatch({
			actionType: AppConstants.DESELECT_PLAN,
			index:index
		})
	},

	validate: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.VALIDATE,
		})
	},

	setErrors: function(hasErrors){
		AppDispatcher.dispatch({
			actionType: AppConstants.VALIDATE,
			hasErrors:hasErrors
		})
	},


	// Send to LMS function, data is of the format {mobile: 920476283, email:rakesh@coverfox.com}
	sendToLMS: function(data, callback){
		AppDispatcher.dispatch({
			actionType: AppConstants.SEND_TO_LMS,
			data:data,
			callback: callback
		})
	},
	selectPlanForBuy: function(index){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_PLAN_FOR_BUY,
			index:index
		})
	},
	deselectPlanForBuy: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.DESELECT_PLAN_FOR_BUY,
		})
	},
	selectPlanForDetail: function(index){
		AppDispatcher.dispatch({
			actionType: AppConstants.SELECT_PLAN_FOR_DETAIL,
			index:index
		})
	},
	deselectPlanForDetail: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.DESELECT_PLAN_FOR_DETAIL,
		})
	},
	setUnavailableForBuying: function(index){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_UNAVAILABLE_FOR_BUYING,
			index:index
		})
	},
	resetParams: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.RESET_PARAMS,
		})
	},
	recieveRedirectUrl: function(redirect_url){
		AppDispatcher.dispatch({
			actionType: AppConstants.RECIEVE_REDIRECT_URL,
			redirect_url:redirect_url
		})
	}


}


module.exports = AppActions