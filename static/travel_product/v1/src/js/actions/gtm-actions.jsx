var AppStore = require('../stores/app-store')
var CountryStore = require('../stores/country-store')
var ResultsStore = require('../stores/results-store')
var MedicalCoverStore = require('../stores/medical-cover-store')
var GTMPush = require('../utils/GTMUtils').gtm_push


var getUserData = function(){
	return {
		isResident:AppStore.getResidentStatus().value,
		tripType:AppStore.getSelectedTripType().value,
		numberOfTravelers: AppStore.getNumberOfTravelers(),
		travelerAges: AppStore.getTravelerAges().join('||'),
		destinations: CountryStore.getSelectedCountriesSlugs().join('||'),
		daysToStartDate: AppStore.getDaysToStartDate(),
		residentFor: AppStore.getResidentFor()? AppStore.getResidentFor().value:'',
		buyablePlans: ResultsStore.getBuyablePlans().length,
		unbuyablePlans: ResultsStore.getUnbuyablePlans().length,
		selectedPlans: ResultsStore.getSelectedPlans().map(function(item){ return item.name }).join('||'),
		selectedMedicalCover: MedicalCoverStore.getSelectedMedicalCover()? MedicalCoverStore.getSelectedMedicalCover().value:null,
	}
}


var gtm_actions = {
	viewQuotes: function(){
		var userData = getUserData()
		var extra_params = {
			param1: userData.numberOfTravelers,
			param2: userData.travelerAges,
			param3: userData.destinations,
			param4: (userData.daysToStartDate > 2)? "More than 2 days":"Under 2 days",
			param6: (userData.daysToStartDate > 2)? "Show All Results":"PED Form"
		}
		if(userData.isResident){
			GTMPush('EventTravelIndianViewQuotes', 'Travel - Indian View Quotes', userData.tripType, '', extra_params)
		}
		else{
			extra_params['param5'] = userData.residentFor
			GTMPush('EventTravelNRIViewQuotes', 'Travel - NRI View Quotes', userData.tripType, '', extra_params)
		}

	},

	submitDeclinedList: function(){
		var userData = getUserData(), param6
		if(userData.unbuyablePlans == 0){
			param6 = 'Show All Results'
		}
		if(userData.buyablePlans == 0){
			param6 = 'Cannot Buy Policy'
		}

		if(userData.buyablePlans>0 && userData.unbuyablePlans>0){
			param6 = 'Show Selective Results'
		}

		var extra_params = {
			param4: (userData.daysToStartDate > 2)? "More than 2 days":"Under 2 days",
			param6: param6
		}
		if(userData.isResident){
			GTMPush('EventTravelIndianCombinedDeclined', 'Travel - Indian Combined Declined List', 'Pop UP', 'Submit PED', extra_params)
		}
		else{
			extra_params['param5'] = userData.residentFor
			GTMPush('EventTravelNRICombinedDeclined', 'Travel - NRI Combined Declined List', 'Pop UP', 'Submit PED', extra_params)
		}
	},

	comparePlans: function(){
		var userData = getUserData()
		if(userData.isResident){
			GTMPush('EventTravelIndianComparePolicy', 'Travel - Indian Compare Policy', 'Compare Policies', userData.selectedPlans, {})
		}
		else{
			extra_params['param5'] = userData.residentFor
			GTMPush('EventTravelNRIComparePolicy', 'Travel - NRI Compare Policy', 'Compare Policies', userData.selectedPlans, {})
		}
	},

	submitPhoneNumber: function(){
		var userData = getUserData()
		var extra_params = {}
		if(userData.isResident){
			GTMPush('EventTravelCall', 'Travel - Indian Call', 'Call Scheduled', '', extra_params)
		}
		else{
			GTMPush('EventTravelCall', 'Travel - NRI Call', 'Call Scheduled', '', extra_params)
		}
	},

	changeMedicalCover: function(){
		var userData = getUserData()
		var extra_params = {
			param1: userData.numberOfTravelers,
			param2: userData.travelerAges,
			param3: userData.destinations,
			param4: (userData.daysToStartDate > 2)? "More than 2 days":"Under 2 days",
		}
		if(userData.isResident){
			GTMPush('EventTravelIndianModifyQuotes', 'Travel - Indian Modify Quotes', 'Travel Results Page', userData.selectedMedicalCover, extra_params)
		}
		else{
			extra_params['param5'] = userData.residentFor
			GTMPush('EventTravelNRIModifyQuotes', 'Travel - NRI Modify Quotes', 'Travel Results Page', userData.selectedMedicalCover, extra_params)
		}

	},

	noOCIPIO: function(){
		GTMPush('EventTravelNRINoOCIPIO', 'Travel - NRI No OCIPIO', 'Policy Declined', '', {})
	},

	clickOnBuy: function(plan, nextStep){
		var userData = getUserData()
		var planName = plan.name
		var extra_params = {
			param4: (userData.daysToStartDate > 2)? "More than 2 days":"Under 2 days",
			param6: nextStep
		}
		if(userData.isResident){
			GTMPush('EventTravelIndianBuyResultsPage', 'Travel - Indian Buy Results Page', userData.tripType, planName, extra_params)
		}
		else{
			GTMPush('EventTravelNRIBuyResultsPage', 'Travel - NRI Buy Results Page', userData.tripType, planName, extra_params)
		}

	},

	submitCombinedDeclinedList: function(plan, nextStep){
		var userData = getUserData()
		var planName = plan.name
		var extra_params = {
			param4: (userData.daysToStartDate > 2)? "More than 2 days":"Under 2 days",
			param6: nextStep
		}
		if(userData.isResident){
			GTMPush('EventTravelIndianBuyPEDList', 'Travel - Indian Buy Combined Declined and PED List', userData.tripType, planName, extra_params)
		}
		else{
			GTMPush('EventTravelNRIBuyPEDList', 'Travel - NRI Buy Combined Declined and PED List', userData.tripType, planName, extra_params)
		}
	},






}

module.exports = gtm_actions