var AppConstants = require('../constants/app-constants.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')


var UIActions = {

	openModal : function(modal){
		AppDispatcher.dispatch({
			actionType: AppConstants.OPEN_MODAL,
			modal: modal
		})
	},
	
	closeModal: function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.CLOSE_MODAL,
		})
	},

	setMode : function(mode){
		AppDispatcher.dispatch({
			actionType: AppConstants.SET_MODE,
			mode: mode
		})
	},

	resetMode : function(){
		AppDispatcher.dispatch({
			actionType: AppConstants.RESET_MODE,
		})
	},

}


module.exports = UIActions