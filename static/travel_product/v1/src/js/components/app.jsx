var React = require('react')
var AppActions = require('../actions/app-actions')
var GTMActions = require('../actions/gtm-actions')
var UIActions = require('../actions/results-ui-actions')
var EventUtils = require('../utils/eventutils')

var SiteTopBar = require('./layout/sitebar')
var Router = require('react-router')

var moment = require('moment')


// App Components
var TripTypeSelector = require('./trip-type-selector')
var DestinationSelector = require('./destination-selector')
var TravelerAgeSelector = require('./traveler-age-selector')
var TravelerDatesSelector = require('./traveler-dates-selector')
var ExtraInfoSelector = require('./extra-info-selector')

var CountryStore = require('../stores/country-store')
var AppStore = require('../stores/app-store')
var ErrorStore = require('../stores/error-store')


function getStateFromStores(){
	return {
		countries: CountryStore.getCountries(),
		regions: CountryStore.getRegions(),
		selectedCountries: CountryStore.getSelectedCountries(),
		quote: AppStore.getQuoteRequest(),
		isCurrentlyInIndia: AppStore.getCurrentLocation(),
		hasErrors: ErrorStore.hasErrors(),
		hasOCICard: AppStore.hasOCICard(),
	}
}

var app = React.createClass({
	mixins : [Router.Navigation],

	getInitialState: function() {
		return getStateFromStores()
	},
	
	componentDidMount: function() {
		CountryStore.addChangeListener(this._onChange)
		AppStore.addChangeListener(this._onChange)
		ErrorStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		CountryStore.removeChangeListener(this._onChange)
		AppStore.removeChangeListener(this._onChange)
		ErrorStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	handleSubmit: function(item){
		AppActions.validate()
		var self = this
		setTimeout(function(){
			if(!self.state.hasErrors){
				AppActions.deselectAllPEDs()
				AppActions.resetParams()
				GTMActions.viewQuotes()
				self.transitionTo('results')
			}
		},300)
	},

	render: function() {
		
		var submitButtonDisabled = false

		if(this.state.isCurrentlyInIndia.value === false || (this.state.hasOCICard && this.state.hasOCICard.value === false)){
			submitButtonDisabled = true
		}

		return (
			<div>
				<SiteTopBar pagetitle={'Travel Insurance'}></SiteTopBar>

				<div className="body-bg"></div>
				<div className="body-bg-blurred hide"></div>

				<div className="app-container">
					<div className="app-quote-form">
						<h1>Buy Travel Insurance</h1>
						<TripTypeSelector />
						<DestinationSelector />
						<TravelerDatesSelector />
						<TravelerAgeSelector />
						<ExtraInfoSelector />
						<div className="action-btn">
							<button className={'submit-btn'+ (submitButtonDisabled? ' disabled':'')} onClick={this.handleSubmit}>View Quotes</button>
						</div>
						
					</div>
				</div>

				
			</div>
		)
	}

})

module.exports = app