var React = require('react');

var columnHeader = React.createClass({

	render: function() {
		return (
			<div className="column-header-wrapper">
			<div className="column-header">
				<div className="column column-1">Insurer</div>
				<div className="column column-2">Medical Cover</div>
				<div className="column column-3">Baggage Loss Cover</div>
				<div className="column column-4">Trip Cancelation Cover</div>
				<div className="column column-5">Trip Delay Cover</div>
				<div className="column column-6"></div>
			</div>
			</div>
		);
	}

});

module.exports = columnHeader;