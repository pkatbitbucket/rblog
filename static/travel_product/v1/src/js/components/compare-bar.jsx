var React = require('react')
var StickyDiv = require('react-stickydiv')
var MiniPlanCard = require('./ui/mini-plan-card.jsx')
var AppActions = require('../actions/app-actions')


var compareBar = React.createClass({

	handleRemove: function(index){
		//console.log('remove')
		AppActions.deselectPlan(index)
	},

	render: function() {	

		var plans = this.props.plans.map(function(item, i){
			return <MiniPlanCard key={i} plan={item} onRemove={this.handleRemove.bind(null, item.index)} />
			
		}, this);

		var emptyUILength = 3 - this.props.plans.length;

		var emptyUI = []

		for (var i = 0; i < emptyUILength; i++) {
			emptyUI.push(<div key={i} className="empty-compare-box">{i == 0? <span>{'Add ' + emptyUILength + ' more plan'+ (emptyUILength > 1? 's':'') +' to compare'}</span>:''}</div>)	
		}
		
		return (
			<StickyDiv zIndex={1999} offsetTop={64} >
				<div className={"compare-bar-wrapper "+ (this.props.active? 'compare-mode':'') }>
					<div className="compare-bar">
						{emptyUILength < 2?
						<div className="compare-btn-wrapper">
							<button className='submit-btn' onClick={this.props.onCompare} >{this.props.btnText}</button>
						</div>:null }
						{plans}
						{emptyUI}
						
					</div>
				</div>
			</StickyDiv>  
		);
	}

});

module.exports = compareBar;