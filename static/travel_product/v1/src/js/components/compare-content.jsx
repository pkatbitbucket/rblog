var React = require('react')
var FeaturesMap = require('../utils/feature_map')
var HoverTextMap = require('../utils/hovertextmap')
var StickyDiv = require('react-stickydiv')


var React = require('react');

var CompareRow = React.createClass({
	getInitialState: function() {
		return {
			cellHeight: null 
		};
	},

	componentDidMount: function() {
		//console.log("THE NODE",this.refs.compareRow.getDOMNode())
	},

	render: function() {

		var cells = this.props.features.map(function(item, i){
			return <div key={i} className="compare-table-cell">{item}</div>
		}, this);

		return (
			<div ref="compareRow" className={"compare-row-wrapper " + (this.props.customClass? this.props.customClass:'') }>
				<div className="compare-table-cell row-header">
					{this.props.header}

					{HoverTextMap[this.props.header]? 

					<div className="compare-tip">
						<h3>{this.props.header}</h3>
						{HoverTextMap[this.props.header]}
					</div>:null}
				</div>
				{cells}
			</div>
		);
	}

});



var CategoryRow = React.createClass({

	render: function() {
		return (
			<div className="category-row">
				{this.props.category}
			</div>
		);
	}

});



var compare = React.createClass({

	
	render: function() {
		var plans = this.props.plans
		var rows = []

		var planPremiums = plans.map(function(item){
			return <div className='compare-feature'>
							<div className="c_cover">Rs. {item.premium}</div>
						</div>
		})

		rows.push(<StickyDiv zIndex={1998} offsetTop={144} ><CompareRow customClass='premium-row' key='premium' header='Premium' features={planPremiums} /></StickyDiv>)

		for(var key in FeaturesMap){
			rows.push(<CategoryRow key={key} category={key} />)
			var catFeatures = FeaturesMap[key]
			var catFeaturesLength = catFeatures.length

			for (var i = 0; i < catFeaturesLength; i++) {
				
				var feature = catFeatures[i]

				var planFeatures = plans.map(function(plan, j){

					var planFeature = plan.features.filter(function(item){
						return item.name == feature
					})

					if(planFeature.length){
						planFeature = <div className='compare-feature'>
							{planFeature[0].cover? <div className="c_cover">{planFeature[0].cover}</div>:null}
							{(planFeature[0].deductible && planFeature[0].deductible !=0) ? <div className="c_deductible">Deductible: {planFeature[0].deductible || 'Not Covered'}</div>:null}
							{planFeature[0].remarks? <div className="c_remarks">{planFeature[0].remarks}</div>:null}
						</div>
					}	
					else{
						planFeature = 'Not Covered'
					}
					return planFeature
				})

				rows.push(<CompareRow key={feature} header={feature} features={planFeatures} />)
			};
		}

		var policyWordings = plans.map(function(plan, i){
			if(plan.plan.policy_wording.file){
				return <div className='compare-feature'><a href={MEDIA_URL + plan.plan.policy_wording.file} target="_blank">
							Policy Wording Document
						</a></div>
			}
			else{
				<div className='compare-feature'></div>
			}
		})

		if(policyWordings.length){
			rows.push(<CompareRow key={'Policy Wordings'} header={'Policy Wordings'} features={policyWordings} />)
		}

		return (
				<div className="compare-content-wrapper">
					<div className="compare-content">
						{rows}
						<div className="compare-row-wrapper compare-footer">
							The information above is a detailed summary of key features in travel insurance. To read the complete benefits, terms and conditions under the policy, you must read the policy wordings
						</div>

					</div>
				</div>
		)
	}

});

module.exports = compare;