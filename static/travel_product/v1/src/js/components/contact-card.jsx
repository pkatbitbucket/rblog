var React = require('react')
var CustomInputBox = require('./ui/custom-input-box')
var PhoneInput = require('./phone-input')
var EmailInput = require('./email-input')
var AppActions = require('../actions/app-actions.jsx')
var AppStore = require('../stores/app-store.jsx')
var GTMActions = require('../actions/gtm-actions')


var ContactCard = React.createClass({

	handleEmailSubmit: function(value, callback){
		var label = this.props.label
		AppActions.sendToLMS({email:value, label:label}, callback)
	},

	handlePhoneSubmit: function(value, callback){
		var label = this.props.label
		AppActions.sendToLMS({mobile:value, label:label}, callback)
		GTMActions.submitPhoneNumber()
	},

	render: function() {
		var contactEmail, contactPhone

		if (this.props.email) {
			contactEmail = <div className="contact-email">
						<h2>Undecided? Save these quotes for later</h2>
						<p>We will email you the quotes for future reference.</p>
						<EmailInput onSubmit={this.handleEmailSubmit} />
					</div>
		}

		if (this.props.phone) {
			contactPhone = <div className="contact-phone">
						<h2>Unable to get through?</h2>
						<p>Talk to our specialist about your travel insurance needs.</p>
						<PhoneInput onSubmit={this.handlePhoneSubmit} />
					</div>
		}


		return <div className="contact-card-wrapper">
			<div className="contact-card">
				<div className="contact-card-inner">
					{contactPhone}
					{contactEmail}
				</div>
			</div>
		</div>
	}

});

module.exports = ContactCard;



