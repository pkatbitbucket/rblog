var React = require('react');
var Calendar = require('./ui/calendar')

var dateSelector = React.createClass({

	mixins: [
	    require('react-onclickoutside')
	  ],

	getInitialState: function() {
		return {
			active: this.props.active,
			value: this.props.value
		};
	},

	handleClick: function(e){
		this.setState({
			active: true
		})

		///console.log("VALUE >>>>>",this.props.value)
	},

	handleClickOutside: function(evt) {
		if(this.state.active){
			this.setState({
				active: false
			})
		}
  	},

  	handleSelect: function(date){
  		
  		this.props.onSelect(date)
		this.setState({
	  		active:false,
	  		value:date
	  	})
	  	

  	},

	render: function() {
		var value = this.state.value
		var active = this.state.active
		var minDate = this.props.minDate


		return (
			<div className="date-input-wrapper" >
				<div className="cal-icon"></div>
				<div className={"date-input" + (value? ' has-value':'') + (active? ' active':'')} onClick={this.handleClick}>
					{value? <span className="date-input-label">{this.props.label}</span>: null}
					{value? <span className="date-input-value">{this.state.value.format('MMM Do YYYY')}</span>: null}
					{value? null:<span className="date-input-placeholder">{this.props.placeholder}</span>}
				</div>
				<div className="calendar-input">
					{this.state.active? <Calendar onSelect={this.handleSelect} title={this.props.placeholder} minDate={minDate} startDate={this.state.value} />:null }
				</div>
			</div>
		);
	}

});

module.exports = dateSelector;