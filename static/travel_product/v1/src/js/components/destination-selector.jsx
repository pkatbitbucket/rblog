var React = require('react')
var Select = require('react-select')
var AppActions = require('../actions/app-actions')
var CountryStore = require('../stores/country-store')
var AppStore = require('../stores/app-store')
var ErrorStore = require('../stores/error-store')
var EventUtils = require('../utils/eventutils')


function getStateFromStores(){
	return {
		countries: CountryStore.getCountries(),
		regions: CountryStore.getRegions(),
		selectedRegion: CountryStore.getSelectedRegion(),
		selectedCountries: CountryStore.getSelectedCountries(),
		tripType: AppStore.getSelectedTripType(),
		error: ErrorStore.getDestinationError()
	}
}

var destinationSelector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		CountryStore.addChangeListener(this._onChange)
		AppStore.addChangeListener(this._onChange)
		ErrorStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		CountryStore.removeChangeListener(this._onChange)
		AppStore.removeChangeListener(this._onChange)
		ErrorStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	handleCountriesChange: function(value, values) {
		AppActions.deselectAllCountries()
		var indexes = values.map(function(item, i){
			return item.index
		});
		AppActions.selectMultipleCountries(indexes)
		EventUtils.fire_cust_ev('quote_changed', {})
	},

	updateRegion: function(value, values){
		AppActions.selectRegion(values[0])
		EventUtils.fire_cust_ev('quote_changed', {})
	},

	render: function() {
		var countries = this.state.countries
		var regions = this.state.regions
		var selectedCountries = this.state.selectedCountries
		/*var selectedCountriesSlugs = selectedCountries.map(function(item, i){
			return item.value
		})*/
		var selectedCountriesSlugs = selectedCountries

		var countryOptions = countries.map(function(item, i){
			item.index = i
			return item
		})

		var regionOptions = regions.map(function(item, i){
			item.index = i
			return item
		})

		var selector, customClass, multi, placeholder, value, options, onchange, searchable
		
		if(this.state.tripType.value == 'SINGLE'){
			customClass = 'single-trip-destinations'
			multi = true
			placeholder = "Select your destination"
			value = selectedCountriesSlugs
			options = countryOptions
			onchange = this.handleCountriesChange
			searchable = true
			//selector = <Select multi={true} placeholder="Which countries are you travelling to?" value={selectedCountriesSlugs} options={countryOptions} onChange={this.handleCountriesChange} clearable={false} />
		}
		else{
			customClass = 'multi-trip-destinations'
			multi = false
			placeholder = "Which countries are you travelling to?"
			value = this.state.selectedRegion
			options = regionOptions
			onchange = this.updateRegion
			searchable = false
			selector = <Select placeholder="Select your destination." value={this.state.selectedRegion? this.state.selectedRegion:null} options={regionOptions} onChange={this.updateRegion} searchable={false} clearable={false} />
		}

		singletripselector = <Select multi={true} placeholder="Which countries are you travelling to?" value={selectedCountriesSlugs} options={countryOptions} onChange={this.handleCountriesChange} clearable={false} />
		multitripselector = <Select placeholder="Select your destination." value={this.state.selectedRegion? this.state.selectedRegion:null} options={regionOptions} onChange={this.updateRegion} searchable={false} clearable={false} />


		var errorelement

		if(this.state.error){
			errorelement = <div className="error-elem">{this.state.error}</div>
		}

		return (
			<div className={" "+customClass}>
				{(this.state.tripType.value == 'SINGLE')? singletripselector:null}
				{(this.state.tripType.value == 'MULTI')? multitripselector:null}
				{errorelement}
			</div>

		);
	}

});

module.exports = destinationSelector;


