var React = require('react');

var disclaimerBlock = React.createClass({

	render: function() {
		return (
			<div className='disclaimer-block-wrapper'>
				<div className="disclaimer-block">
					{this.props.children}
				</div>
			</div>
		);
	}

});

module.exports = disclaimerBlock;