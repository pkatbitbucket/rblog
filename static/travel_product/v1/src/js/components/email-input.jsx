var React = require('react')
var CustomInputBox = require('./ui/custom-input-box')

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

var emailInput = React.createClass({
	getInitialState: function() {
		return {
			email: null,
			showError: false,
			isDone: false
		};
	},

	handleChange: function(value){
		this.setState({
			email: value
		}, function(){
			var matched = this.state.email && validateEmail(this.state.email)
			if(matched || this.state.email == ''){
				this.setState({
					showError:false
				});
			}
		})
	},

	handleClick: function(){
		var matched = this.state.email && validateEmail(this.state.email)
		if(matched){
			console.log("SUBMITTED first")
			var self = this
			this.props.onSubmit(this.state.email, function(){
				self.setState({
					isDone: true 
				});
			})
		}else{
			this.setState({
				showError:true
			});
		}
		
	},

	render: function() {
		var element

		if(!this.state.isDone){
			element = <div className="input-wrapper">
				<CustomInputBox customClass='has-right-button' hasError={this.state.showError} errorText='Please enter a valid email id' placeholder="Enter your email id" onChange={this.handleChange} />
				<button className='submit-btn' onClick={this.handleClick}>Submit</button>
			</div>
		}
		else{
			element = <div className="success-message">Thanks! We will get back to you soon.</div>
		}



		return <div>{element}</div>
	}

})

module.exports = emailInput