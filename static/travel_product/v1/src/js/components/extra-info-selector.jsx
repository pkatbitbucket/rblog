var React = require('react')
var AppActions = require('../actions/app-actions')
var GTMActions = require('../actions/gtm-actions')
var AppStore = require('../stores/app-store')
var Select = require('react-select')
var ErrorStore = require('../stores/error-store')

// UI Helpers
var ControlGroup = require('./ui/controlgroup')
var Constants = require('../constants/app-settings.jsx')



function getStateFromStores(){
	return {
		isResident: AppStore.getResidentStatus(),
		isCurrentlyInIndia: AppStore.getCurrentLocation(),
		booleanOptions: AppStore.getBooleanOptions(), // move this to a better place
		tripType: AppStore.getSelectedTripType(),
		ntravelers: AppStore.getNumberOfTravelers(),
		hasOCICard: AppStore.hasOCICard(),
		residentFor: AppStore.getResidentFor(),
		error: ErrorStore.getExtraInfoError()
	}
}

var extraInfoSelector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
		ErrorStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
		ErrorStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	handleResidenceUpdate: function(item){
		AppActions.setResidence(item)
	},

	handleLocationUpdate: function(item){
		AppActions.setCurrentLocation(item)
	},

	handleOCIUpdate: function(data){
		AppActions.setOCICard(data)
		if(data.value == false){
			GTMActions.noOCIPIO()	
		}
		
	},

	handleResidenceForUpdate: function(value, values){
		AppActions.setResidentFor(values[0])
	},

	render: function() {
		var errorelement

		if(this.state.error){
			errorelement = <div className="error-elem">{this.state.error}</div>
		}

		var nonresident

		var travelerTxt, ociQuestion, residenceDurationQuestion

		if(this.state.tripType.value == 'SINGLE' && this.state.ntravelers > 1){
			travelerTxt = 'Are all travelers'
			ociQuestion = 'Do all travelers have OCI/PIO card?'
			residenceDurationQuestion = 'How long have travelers been in India?'
		}
		else{
			
			travelerTxt = 'Are you a'
			ociQuestion = 'Do you have an OCI/PIO card?'
			residenceDurationQuestion = 'How long have you been in India?'
		}

		var residenceDuration

		if(this.state.hasOCICard && this.state.hasOCICard.value === true){

			residenceDuration = <div>
					<p>{residenceDurationQuestion}</p>
					<Select options={Constants.travelerStayDurationOptions} value={this.state.residentFor? this.state.residentFor.value:null} onChange={this.handleResidenceForUpdate} clearable={false} searchable={false} />
				</div>

		}

		if(this.state.hasOCICard && this.state.hasOCICard.value === false){
			residenceDuration = <div className='error-elem-bg'>
					<p>None of the insurance companies covers travelers who are not residents of India and do not have an OCI/PIO card</p>
				</div>
		}

		if(this.state.isResident){

			if(this.state.isResident.value === false){
				nonresident = <div className='non-resident-block'>
				<p>{ociQuestion}</p>
				<ControlGroup customClass="small" layout="horizontal" selectedValue={this.state.hasOCICard} items={this.state.booleanOptions} onUpdate={this.handleOCIUpdate}></ControlGroup>
				{residenceDuration}
				{errorelement}
			</div>
			}

		}

		

		var notInIndia


		if(this.state.isCurrentlyInIndia.value === false){
			notInIndia = <div className='error-elem-bg'>
			    <p>None of the insurance companies covers travelers who are not in India at the start of the trip</p>
			</div>
		}


		return (
			<div className="extra-info-selector">
				<div>
					<p>{travelerTxt} resident of India?</p>
					<ControlGroup customClass="small" layout="horizontal" items={this.state.booleanOptions} selectedValue={this.state.isResident} onUpdate={this.handleResidenceUpdate} ></ControlGroup>					
				</div>

				<div>
					{nonresident}
				</div>

				<div>
					<p>Is this trip starting from India?</p>
					<ControlGroup customClass="small" layout="horizontal" items={this.state.booleanOptions} selectedValue={this.state.isCurrentlyInIndia} onUpdate={this.handleLocationUpdate} ></ControlGroup>
					{notInIndia}
				</div>

				

				
			</div>
		);
	}

});

module.exports = extraInfoSelector
