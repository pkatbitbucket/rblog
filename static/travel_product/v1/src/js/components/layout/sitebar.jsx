var React = require('react');

var bar = React.createClass({

	render: function() {
		return (
			<header>
				<div className="header-wrapper">
					{/*<div className="hamburger-menu">

					</div>*/}
					<div className="logo">
						<a href='/'><img src={"/static/global/img/logos/cf-white-orange-h71.png"} alt="Logo" /></a> 
					</div>
					<div className="page-title">
						{this.props.pagetitle}
					</div>
					<div className="bar-text pull-right">
						Call us (Toll Free) for assistance: <strong>{TRAVEL_TOLL_FREE}</strong>
					</div>
				</div>						
			</header>	
		);
	}
});

module.exports = bar;