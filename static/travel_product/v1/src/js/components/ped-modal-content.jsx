var React = require('react')
var PEDStore = require('../stores/ped-store')
var AppStore = require('../stores/app-store')
var AppActions = require('../actions/app-actions') 
var EventUtils = require('../utils/eventutils')



function getStateFromStores(){
	return {
		allPEDs: PEDStore.getAllPEDs(),
		selectedPEDs: PEDStore.getSelectedPEDs(),
		getInsurersFromSelectedPEDs: PEDStore.getInsurersFromSelectedPEDs(),
		otherPED: PEDStore.getOtherPED(),
		ntravelers: AppStore.getNumberOfTravelers()
	}
}


var ped = React.createClass({
	getInitialState: function() {
			return getStateFromStores()
		},

	componentDidMount: function() {
		PEDStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		PEDStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	handleClick: function(item){
		if(item.is_selected){
			AppActions.deselectPED(item.index)
			EventUtils.fire_cust_ev('quote_changed', {})
		}
		else{
			AppActions.selectPED(item.index)
			EventUtils.fire_cust_ev('quote_changed', {})
		}
	},

	updateOtherPED: function(event){
		//replace evrything other than this with characters with blank
		event.target.value = event.target.value.replace(/[^a-zA-Z.,'\/]/g,'') 
		var value = event.target.value
		AppActions.updateOtherPED(value)
	},

	render: function() {

		var showOthers = this.props.showOthers

		var peds = this.state.allPEDs.map(function(item, i){
			item.index = i
			return item
		});

		var PEDElements = peds.map(function(item, i){
			return <div key={i} className={'ped-item checkbox-item ' + (item.is_selected? 'selected':'')} onClick={this.handleClick.bind(null, item)}><span className='label'>{item.verbose}</span></div>
		}, this);

		var otherElement

		if(showOthers){
			otherElement = <div className='ped-item other-ped'>Others, please specify <input className='cf-input input-others' placeholder={"Comma separated if multiple"} onChange={this.updateOtherPED} defaultValue={this.state.otherPED} /> <p>Please specify any other diseases any traveller is suffering from.</p></div>
		}

		var userHasPEDs = false

		if(this.state.selectedPEDs.length > 0 || (this.state.otherPED && this.state.otherPED != '')){
			userHasPEDs = true
		}

		
		return (
			<div className='ped-popup-container'>
				<div className="modal-header">
					<h2>Please declare any pre-existing diseases</h2>
					<p className="sub">An insurer may not provide insurance if {(this.state.ntravelers == 1)? 'you are':'a traveler is'} suffering from a certain disease. <br />Please provide accurate information below so that we can ensure you buy the right policy.</p>
				</div>
				<div className="modal-inner-content">
					{PEDElements}
					{otherElement}
				</div>
				<div className="modal-action">
					<button className='submit-btn' onClick={this.props.onClose} >{userHasPEDs? 'Continue':'No diseases to declare.'}</button>		
				</div>
			</div>
		);
	}

});

module.exports = ped;