var React = require('react')
var CustomInputBox = require('./ui/custom-input-box')

var phoneInput = React.createClass({
	getInitialState: function() {
		return {
			phone: null,
			showError: false,
			isDone: false
		};
	},

	handleChange: function(value){
		this.setState({
			phone: value
		}, function(){
			var matched = this.state.phone && this.state.phone.match('[0-9]{10}')
			if(matched || this.state.phone == ''){
				this.setState({
					showError:false
				});
			}
		})
	},

	handleClick: function(){
		var matched = this.state.phone && this.state.phone.match('[0-9]{10}')
		if(matched){
			var self = this
			this.props.onSubmit(this.state.phone, function(){
				self.setState({
					isDone: true 
				});
			})
		}else{
			this.setState({
				showError:true
			});
		}
	},

	render: function() {
		var element

		if(!this.state.isDone){
			element = <div className="input-wrapper">
				<CustomInputBox maxLength={10} customClass='has-right-button' hasError={this.state.showError} errorText="Please enter a valid phone number" placeholder="Enter 10 digit phone number" onChange={this.handleChange} />
				<button className='submit-btn' onClick={this.handleClick}>Call Me</button>
			</div>
		}
		else{
			element = <div className="success-message">Thanks! We will get back to you soon.</div>
		}

		return <div>{element}</div>
		
	}

})

module.exports = phoneInput