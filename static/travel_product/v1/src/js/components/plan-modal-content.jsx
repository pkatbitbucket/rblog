var React = require('react')
var ResultsStore = require('../stores/results-store')
var AppActions = require('../actions/app-actions')
var PlanCard = require('./ui/plan-card')
var FeaturesMap = require('../utils/feature_map')
var ContactCard = require('./contact-card')


function getStateFromStores(){
	return {		
		selectedPlanForDetail: ResultsStore.getSelectedPlanForDetail(),
	}
}


var planModal = React.createClass({
	getInitialState: function() {
			return getStateFromStores()
		},

	componentDidMount: function() {
		ResultsStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		ResultsStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	render: function() {
		var plan = this.state.selectedPlanForDetail
		var plancard
		var plandetails = []

		if(plan){
			plancard = <PlanCard hideActions={true} plan={plan} onBuy={this.props.onBuy.bind(null,plan, plan.index)} />		
		
			for(var key in FeaturesMap){
				plandetails.push(<div key={key} className='detail-section-header'>{key}</div>)
				var catFeatures = FeaturesMap[key]
				var catFeaturesLength = catFeatures.length

				for (var i = 0; i < catFeaturesLength; i++) {
					
					var feature = catFeatures[i]

						var planFeature = plan.features.filter(function(item){
							return item.name == feature
						})

						if(planFeature.length){
							planFeature = <div className='compare-feature'>
								{planFeature[0].cover? <div className="c_cover">{planFeature[0].cover}</div>:null}
								{(planFeature[0].deductible && planFeature[0].deductible !=0)? <div className="c_deductible">Deductible: {planFeature[0].deductible || 'N/A'}</div>:null}
								{planFeature[0].remarks? <div className="c_remarks">{planFeature[0].remarks}</div>:null}
							</div>

							plandetails.push(<div className='detail-section-feature' key={feature}><div className='feature-header'>{feature}</div>{planFeature}</div>)
						}
					
				};
			}

		}

		
		return (

			<div className='plan-popup-container'>
				<div className="modal-header">
					<h2>Plan Details</h2>
					<div className="close-btn" onClick={this.props.onClose}>X</div>
				</div>
				<div className="modal-inner-content">

					{plancard}

					<div className="detail-section">
						{plandetails}
						{plan && plan.sub_limit? <span>
							{plan.sub_limit}
						</span> :null}
						{plan && plan.plan.policy_wording.file? <div className="detail-footer">
							The information above is a detailed summary of key features in travel insurance. To read the complete benefits, terms and conditions under the policy, you must read the <a href={MEDIA_URL + plan.plan.policy_wording.file} target="_blank">policy wordings</a>.
						</div>:null}
					</div>
				</div>
				<div className="modal-action">
					<ContactCard phone={true} label='detail-page' />
					
				</div>
			</div>
		)
	}

});

module.exports = planModal