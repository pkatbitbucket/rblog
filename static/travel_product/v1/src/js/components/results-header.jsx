var React = require('react')
var KVitem = require('./ui/kv-item')

var AppStore = require('../stores/app-store')
var CountryStore = require('../stores/country-store')
var ResultsStore = require('../stores/results-store')
var MedicalCoverStore = require('../stores/medical-cover-store')
var AppActions = require('../actions/app-actions')
var GTMActions = require('../actions/gtm-actions')
var Select = require('react-select')
var PEDStore = require('../stores/ped-store')
var TitledDropdown = require('./ui/titled-dropdown')
var Router = require('react-router')
var EventUtils = require('../utils/eventutils')

var UIActions = require('../actions/results-ui-actions')

var Constants = require('../constants/app-settings')
var CountriesList = require('../utils/individual_travel_places')
var RegionsList = Constants.regions

function getStateFromStores(){
	return {
		selectedTripType: AppStore.getSelectedTripType(),
		selectedDuration: AppStore.getSelectedDuration(),
		startDate: AppStore.getStartDate(),
		endDate: AppStore.getEndDate(),
		travelerAges: AppStore.getTravelerAges(),
		medicalCoverOptions: MedicalCoverStore.getMedicalCoverOptions(),
		selectedMedicalCover: MedicalCoverStore.getSelectedMedicalCover(),
		selectedRegion: CountryStore.getSelectedRegion(),
		selectedPEDs: PEDStore.getSelectedPEDs(),
		otherPED: PEDStore.getOtherPED(),
	}
}

var resultsHeader = React.createClass({
	mixins : [Router.Navigation],

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		CountryStore.addChangeListener(this._onChange)
		AppStore.addChangeListener(this._onChange)
		ResultsStore.addChangeListener(this._onChange)
		PEDStore.addChangeListener(this._onChange)
		MedicalCoverStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		CountryStore.removeChangeListener(this._onChange)
		AppStore.removeChangeListener(this._onChange)
		ResultsStore.removeChangeListener(this._onChange)
		PEDStore.removeChangeListener(this._onChange)
		MedicalCoverStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	handleMedicalCoverChange: function(value, values){
		AppActions.setMedicalCover(values[0])
		AppActions.fetchQuotes()
		GTMActions.changeMedicalCover()
		EventUtils.fire_cust_ev('quote_changed', {})

	},

	handleUpdate: function(){
		AppActions.fetchQuotes()
	},

	handlePEDChange: function(){
		UIActions.openModal('ped-with-others')
	},

	handleGoBack: function(){
		var redirectUrl = (this.state.selectedTripType.value == 'SINGLE')?'/travel-insurance/':'/travel-insurance/multi-trip/';
		
		window.location=redirectUrl
		// this.transitionTo('/')
	},

	render: function() {
		/*var selectedCountries = CountryStore.getSelectedCountries().map(function(item){
 				return item.substring(0,1).toUpperCase() + item.substring(1, item.length) //making first letter capital
 			});

		selectedCountries = selectedCountries.join(', ')
		*/	
		var selectedCountriesSlug = CountryStore.getSelectedCountries()
		var selectedCountriesLabels = []
		if(selectedCountriesSlug && selectedCountriesSlug!=null && selectedCountriesSlug.length > 0){
			for(var i = 0; i < selectedCountriesSlug.length; i++){
				for (var j = 0; j < CountriesList.length; j++){
					if(selectedCountriesSlug[i] == CountriesList[j][0]){
						selectedCountriesLabels.push(CountriesList[j][1])
					}
				}
			}
		}
		selectedCountriesLabels = selectedCountriesLabels.join(', ')
				
		// gettign selected region name
		var selectedRegion = ""
		for(var i = 0; i < RegionsList.length; i++){
			if(RegionsList[i].value == this.state.selectedRegion){
				selectedRegion = RegionsList[i].label
				break
			}
		}

		var PEDs = this.state.selectedPEDs.map(function(item, i){
			return <li key={i}>{item.verbose}</li>
		})

		var otherPED = this.state.otherPED
		
		if(otherPED && (otherPED != '')){
			PEDs.push(<li key={otherPED}>{this.state.otherPED}</li>)	
		}

		
		var PEDElement = (PEDs.length > 0)? <ul className='no-bullet-list light'>{PEDs}</ul>:<p className="sub">No pre existing disease declared</p>


		return (
			<div className= {"results-header-wrapper " + (this.props.customClass? this.props.customClass:'') }>
				
				<div className="results-header-block">
					<h3 className="block-title">Showing Results For <button onClick={this.handleGoBack} className="empty-btn light">Edit</button></h3>
					<h3>{(this.state.selectedTripType.value == 'SINGLE')? 'Single Trip Travel Insurance':'Multi Trip Travel Insurance'}</h3>

					{(this.state.selectedTripType.value == 'SINGLE')? <p className="sub">Traveling to: {selectedCountriesLabels}</p>:null}

					{(this.state.selectedTripType.value == 'MULTI')? <p className="sub">Traveling {selectedRegion}</p>:null}

					<KVitem title='Start Date' customClass='dark' value={this.state.startDate.format('MMM Do YYYY')} />
					
					{(this.state.selectedTripType.value == 'SINGLE')? <KVitem title='End Date' customClass='dark' value={this.state.endDate.format('MMM Do YYYY')} />:null}
					{(this.state.selectedTripType.value == 'MULTI')? <KVitem title='Duration of longest trip' customClass='dark' value={this.state.selectedDuration.label} />:null}
					<KVitem title='Traveler Ages' customClass='dark' value={this.state.travelerAges.join(', ')} />
				</div>

				<div className="results-header-block">
					<h3 className="block-title">Medical Cover</h3>
					<p className="sub">We have selected optimal Medical Cover amount automatically. You can modify this amount if required.</p>
					<TitledDropdown customClass='dd-md-cover' options={this.state.medicalCoverOptions} title="Medical Cover" value={this.state.selectedMedicalCover} onChange={this.handleMedicalCoverChange} searchable={false} />
				</div>
				<div className="results-header-block">
					<h3 className="block-title">Pre Existing Diseases <button onClick={this.handlePEDChange} className="empty-btn light">Edit</button></h3>
					{PEDElement}
				</div>
			</div>
		);
	}

});

module.exports = resultsHeader;