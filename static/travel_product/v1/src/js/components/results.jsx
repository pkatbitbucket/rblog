var React = require('react')
var AppConstants = require('../constants/app-constants.jsx')
var AppSettings = require('../constants/app-settings.jsx')
var AppActions = require('../actions/app-actions')
var GTMActions = require('../actions/gtm-actions')
var UIActions = require('../actions/results-ui-actions')
var Router = require('react-router')
var moment = require('moment')


var AppStore = require('../stores/app-store')
var ResultsStore = require('../stores/results-store')
var PEDStore = require('../stores/ped-store')
var UIStore = require ('../stores/results-ui-store')
var CountryStore = require('../stores/country-store')


var SiteTopBar = require('./layout/sitebar')
var PlanCard = require('./ui/plan-card')
var LoadingBox = require('./ui/loading-box')
var Modal = require('./ui/modal')
var PEDModalContent = require('./ped-modal-content')
var PlanModalContent = require('./plan-modal-content')
var CompareContent = require('./compare-content')

var ResultsHeader = require('./results-header')
var CompareBar = require('./compare-bar')
var DisclaimerBlock = require('./disclaimer-block')
var EventUtils = require('../utils/eventutils')



var webAPIutils = require('../utils/webAPIutils')
var Transition = React.addons.CSSTransitionGroup

var ContactCard = require('./contact-card')

var insurersWithDeclinedRisks = AppSettings.INSURERS_WITH_DECLINED_RISKS



function getStateFromStores(){
	
	return {
		buyablePlans: ResultsStore.getBuyablePlans(),
		unbuyablePlans: ResultsStore.getUnbuyablePlans(),
		isLoading: ResultsStore.getLoadingStatus(),
		selectedPlans: ResultsStore.getSelectedPlans(),
		activeModal: UIStore.getActiveModal(),
		activeMode: UIStore.getActiveMode(),
		selectedPlanForBuy: ResultsStore.getSelectedPlanForBuy(),
		selectedPlanForDetail: ResultsStore.getSelectedPlanForDetail(),
		selectedPEDs: PEDStore.getSelectedPEDs(),
		startDate: AppStore.getStartDate(),
		redirectUrl: ResultsStore.getRedirectUrl(),
		travelerAges: AppStore.getTravelerAges(),
		selectedCountriesSlugs: CountryStore.getSelectedCountriesSlugs(),
		selectedSchengenCountries: CountryStore.getSelectedSchengenCountries()
	}
}

var app = React.createClass({

	mixins : [Router.Navigation],

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		ResultsStore.addChangeListener(this._onChange)
		AppStore.addChangeListener(this._onChange)
		UIStore.addChangeListener(this._onChange)
		AppActions.fetchQuotes(function(){
			EventUtils.fire_cust_ev('quote_changed', {})
		})

		// This is temp 

		 dataLayer.push({
		'travelvirtualpage': '/vp/travel/initiate-results',
		'event': 'EventTravelResults'
		});

		var startDate = AppStore.getStartDate()
 		startDate = startDate.startOf('day')
 		var today = moment().startOf('day')
 		var diff = startDate.diff(today, 'days') 

 		if(diff>2){
 			UIActions.closeModal()
 		}else{
 			UIActions.openModal('ped')
 		}
 		
	},

	componentWillUnmount: function() {
		ResultsStore.removeChangeListener(this._onChange)
		AppStore.removeChangeListener(this._onChange)
		UIStore.removeChangeListener(this._onChange)
		UIStore.resetUI()
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	addToCompare: function(index, is_selected){
		//console.log('compare')
		//this.transitionTo('compare')

		if(is_selected){
			AppActions.deselectPlan(index)
		}
		else{
			var selectedPlans = this.state.selectedPlans
			if(selectedPlans.length < 3){
				AppActions.selectPlan(index)
			}
		}
	},

	handleCompare: function(){
		//UIActions.openModal('COMPARE')

		if(this.state.activeMode == 'COMPARE'){
			UIActions.resetMode()
			window.scrollTo(0, 170);
		}
		else{
			UIActions.setMode('COMPARE')	
			GTMActions.comparePlans()
		}
	},


	handleDetail: function(plan, index){
		AppActions.selectPlanForDetail(index)
		UIActions.openModal('detail')
		EventUtils.fire_cust_ev('quote_changed', {})
	},

	gotoQuoteForm: function(){
		this.transitionTo('/')
	},

	handleBuy: function(plan, index){
		AppActions.selectPlanForBuy(index)

		var startDate = AppStore.getStartDate()
 		startDate = startDate.startOf('day')
 		var today = moment().startOf('day')
 		var diff = startDate.diff(today, 'days') 

 		if(diff>2){
 			if(insurersWithDeclinedRisks.indexOf(plan.insurerSlug) != -1){
	 			UIActions.openModal('ped')
	 			GTMActions.clickOnBuy(plan, 'Combined Declined List')
	 		}
	 		else{
	 			GTMActions.clickOnBuy(plan, 'Go to proposal form')	
	 			webAPIutils.goToTransaction(plan.pk)
	 		}
 		}
 		else{
 			GTMActions.clickOnBuy(plan, 'Go to proposal form')	
 			webAPIutils.goToTransaction(plan.pk)
 		}

	},

	handlePEDClose: function(){
		
		UIActions.closeModal()
		webAPIutils.getQuotesBlank(AppStore.getQuoteRequest())
		var selectedPlanForBuy = this.state.selectedPlanForBuy

		if(selectedPlanForBuy){
			if(selectedPlanForBuy.can_buy){
				GTMActions.submitCombinedDeclinedList(selectedPlanForBuy, 'Go to proposal form')
				webAPIutils.goToTransaction(this.state.selectedPlanForBuy.pk)
			}
			else{
				GTMActions.submitCombinedDeclinedList(selectedPlanForBuy, 'Back to results')
			}
		}
		else{
			GTMActions.submitDeclinedList()
		}


		AppActions.deselectPlanForBuy()
	},

	handleDetailClose: function(){
		UIActions.closeModal()

		setTimeout(function(){
			AppActions.deselectPlanForDetail()
		}, 300)
		
	},
	

	render: function() {


		var startDate = this.state.startDate
		startDate = startDate.startOf('day')
		var today = moment().startOf('day')
		var diff = startDate.diff(today, 'days')

		var isLoading = this.state.isLoading
		var activeModal = this.state.activeModal

		var buyablePlans = this.state.buyablePlans
		var unbuyablePlans = this.state.unbuyablePlans

		var buyablePlanCards = buyablePlans.map(function(item, i){
			return <PlanCard key={i} plan={item} onAddToCompare={this.addToCompare.bind(null, item.index, item.is_selected)} onDetail={this.handleDetail.bind(null, item, item.index)} onBuy={this.handleBuy.bind(null, item, item.index)} />
		}, this)


		var unbuyablePlanCards = unbuyablePlans.map(function(item, i){
			var customClass = ' canttouchthis'
			return <PlanCard customClass={customClass} key={i} plan={item} onAddToCompare={this.addToCompare.bind(null, item.index, item.is_selected)} onDetail={this.handleDetail.bind(null, item, item.index)} onBuy={this.handleBuy.bind(null, item, item.index)} />
		}, this)

		var unbuyablePlanCardsHeader


		if(unbuyablePlans.length > 0){
		unbuyablePlanCardsHeader = <div className='unbuyable-container'>
		<div className='unbuyable-plans-message grid-row'>
		<p>You are not eligible to buy the following plans due to one of the reasons listed below</p>
		<ul>
			<li>Individuals with pre existing disease declared earlier may not be covered by the insurer.</li>
			<li>Age of traveler may be more than the maximum age limit specified by the insurer.</li>
			{(diff < 2 || diff == 2 )? <li>Some insurers may not be able to issue the policy on time if your travel start date is within 2 days.</li>:null}
		</ul>
		</div>
		</div> }
 

		if((unbuyablePlans.length + buyablePlans.length) == 0){
			unbuyablePlanCardsHeader = <div className='unbuyable-container'>
			<div className='unbuyable-plans-message grid-row'>
			<p className="text-center">Darn! It looks like we could not find any plans specific to your requirements. But we are sure our specialists can work something out for you. Please share your mobile number below for a call back or call us on our toll free number <strong>{TRAVEL_TOLL_FREE}</strong>.</p>
			{(this.state.travelerAges.length>1)? <p className="text-center">You could also try buying individually for everyone. <button className="empty" onClick={this.gotoQuoteForm}>Start here</button></p>:null}
			</div>
			</div>
		}
		

		var planCardWrapper =  <div className="plans-wrapper">
				{(buyablePlans.length > 0)? <div className='plan-card-header-wrapper'>
					<div className="plan-card-header">
						<p>Showing {buyablePlans.length} plans. <span className="sub pull-right">Prices inclusive of taxes</span></p>
						{(this.state.selectedCountriesSlugs && this.state.selectedCountriesSlugs.indexOf('schengen') > -1 || this.state.selectedSchengenCountries.length > 0)? <p>All plans displayed are approved by Schengen visa authority</p>:null}
					</div>
				</div>:null}
					{buyablePlanCards}
					{unbuyablePlanCardsHeader}
					{unbuyablePlanCards}
					<ContactCard email={false} phone={true} label='results-page' />
					<p className='perm-link'>{SITE_URL+this.state.redirectUrl}</p>

					{(buyablePlans.length + unbuyablePlans.length > 0 )? <DisclaimerBlock>The comparison table displayed above is a summary of top 4 key features in travel insurance. You can compare further by clicking on '+ Add to compare' on your shortlisted insurers and then hitting 'Compare'.</DisclaimerBlock>:null}
				</div> 


		var compareBar

		if(this.state.selectedPlans.length > 0){
			compareBar = <CompareBar active={this.state.activeMode == 'COMPARE'} plans={this.state.selectedPlans} onCompare={this.handleCompare} btnText={(this.state.activeMode == 'COMPARE')? 'Back To Results':'Compare'} />
		}


		var resultsHeader

		if(!isLoading){
			resultsHeader = <ResultsHeader customClass={(this.state.activeMode == 'COMPARE')? 'recede':''} />
		}

		return (
			<div>

				<Modal size="medium" isOpen={ activeModal == 'ped' || activeModal == 'ped-with-others' }>
					{/*<PEDModalContent onClose={this.handlePEDClose} showOthers={ diff < 2 || diff == 2 || activeModal == 'ped-with-others' } />*/}
					<PEDModalContent onClose={this.handlePEDClose} showOthers={ true } />
				</Modal>

				<Modal size="medium" isOpen={ activeModal == 'detail' } onClose={this.handleDetailClose} >
					<PlanModalContent onClose={this.handleDetailClose} onBuy={this.handleBuy} />
				</Modal>

				
				<SiteTopBar pagetitle={'Travel Insurance'} />

				<div className="body-bg-blurred " />
				<Transition transitionName="loading-box">
					{isLoading? <LoadingBox title="We are fetching the best quotes from all top travel insurers" subtitle="This might take a few seconds" />:null}
				</Transition>

				<Transition transitionName="results-header">
					{resultsHeader}
				</Transition>

				
				{compareBar}

			
				<Transition transitionName="compare-content">
					{(this.state.activeMode == 'COMPARE')? <CompareContent plans={this.state.selectedPlans} />:'' }
				</Transition>

				<Transition transitionName="plans">
					{(isLoading || this.state.activeMode == 'COMPARE')? null: <div>{planCardWrapper}</div> }
				</Transition>

				

			</div>




		)
	}

})

module.exports = app