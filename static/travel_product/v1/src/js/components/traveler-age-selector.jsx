var React = require('react')
var AppActions = require('../actions/app-actions')
var AppStore = require('../stores/app-store')
var OtherInfo = require('./traveler-info-selector')
var CustomInputBox = require('./ui/custom-input-box')
var ErrorStore = require('../stores/error-store')
var EventUtils = require('../utils/eventutils')




function getStateFromStores(){
	return {
		tripType: AppStore.getSelectedTripType(),
		ntravelers: AppStore.getNumberOfTravelers(),
		error: ErrorStore.getTravelerAgesError(),
		travelerAges: AppStore.getTravelerAges()
	}
}

var travelerAgeSelector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
		ErrorStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
		ErrorStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	enterAge: function(i){
		var currentInputBox = React.findDOMNode(this.refs['age-input-'+i])
		var nextInputBox = React.findDOMNode(this.refs['age-input-'+(i+1)])
		if(nextInputBox && currentInputBox.value.length == 2 )
		{
			nextInputBox.select()
			nextInputBox.focus()
		}

		var ntravelers = this.state.ntravelers
		var ages = []

		for (var i = 0; i < ntravelers; i++) {
			var value = React.findDOMNode(this.refs['age-input-'+i]).value
			if(!isNaN(value) && value !== ''){
				ages.push(value)
			}
			//ages.push(React.findDOMNode(this.refs['age-input-'+i]).value)
		}

		AppActions.addTravelerAges(ages)
		EventUtils.fire_cust_ev('quote_changed', {})
	},

	handleSingleAge: function(value){
		if(!isNaN(value)){
			AppActions.addTravelerAges([value])	
		}

		if(value == ''){
			AppActions.addTravelerAges([])		
		}
		EventUtils.fire_cust_ev('quote_changed', {})
	},

	render: function() {

		var travelerAges

		var travelerInputBoxes = []

		for (var i = 0; i < this.state.ntravelers; i++) {
			travelerInputBoxes.push(<input defaultValue={(this.state.travelerAges)[i]} ref={'age-input-'+i} key={i} className="traveler-age-small" maxLength="2" onChange={this.enterAge.bind(null, i)} />)
		};

		
		if(this.state.ntravelers == 1){
			travelerAges = (
					<div className="traveler-age-input">
						<CustomInputBox placeholder="Your Age" onChange={this.handleSingleAge} maxLength="2" defaultValue={(this.state.travelerAges)[0]} />
					</div>
				)
		}
		else{
			travelerAges = (
					<div className="traveler-age-input family-ages">
						<h3 className="control-title">Traveler Ages (You, Spouse, Kids only)</h3>
						{travelerInputBoxes}
					</div>
				)
		}

		var errorelement

		if(this.state.error){
			errorelement = <div className="error-elem">{this.state.error}</div>
		}

		return (
			<div className="traveler-age-selector">
				<OtherInfo></OtherInfo>
				{travelerAges}
				{errorelement}
			</div>
		);
	}

});

module.exports = travelerAgeSelector;