var React = require('react')
var AppActions = require('../actions/app-actions')
var AppStore = require('../stores/app-store')
var DateSelector = require('./date-selector')
var moment = require('moment')
var ErrorStore = require('../stores/error-store')
var EventUtils = require('../utils/eventutils')



function getStateFromStores(){
	return {
		tripType: AppStore.getSelectedTripType(),
		durationOptions: AppStore.getDurationOptions(),
		selectedDuration: AppStore.getSelectedDuration(),
		startDate: AppStore.getStartDate(),
		endDate: AppStore.getEndDate(),
		error: ErrorStore.getDatesError(),
		singleTripDuration: AppStore.getSingleTripDuration(),
	}
}

var travelerDatesSelector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
		ErrorStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
		ErrorStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},
	
	handleStartDate: function(date){
	 	AppActions.selectStartDate(date)
	 	EventUtils.fire_cust_ev('quote_changed', {})
	},

	handleEndDate: function(date){
	 	AppActions.selectEndDate(date)
	 	EventUtils.fire_cust_ev('quote_changed', {})
	},
	
	render: function() {

		var customClass, placeholder
		var tripType = this.state.tripType.value

		if(tripType == 'SINGLE'){
			customClass = 'single-trip-dates'
			placeholder = 'Trip Start Date'
		}
		else{
			customClass = 'multi-trip-dates'
			placeholder = 'Next Trip Start Date'
		}	

		var errorelement

		if(this.state.error){
			errorelement = <div className="error-elem">{this.state.error}</div>
		}

		var showTripDuration = false

		if(this.state.startDate && this.state.endDate && tripType == 'SINGLE' && this.state.endDate >= this.state.startDate){
			showTripDuration = true
		}

		var tripDurationElement

		if(this.state.singleTripDuration > 0){
			tripDurationElement = <div className='single-trip-duration'>
				<span className="days">{this.state.singleTripDuration}</span><span> day{this.state.singleTripDuration == 1? '': 's'} trip</span>
			</div>
		}



		return (
			<div className={"date-selector-row "+customClass}>
				<DateSelector placeholder={placeholder} onSelect={this.handleStartDate} value={this.state.startDate} label={'Start Date'} minDate={moment()}> </DateSelector>
				{tripType == 'SINGLE'? <DateSelector placeholder={'Trip End Date'} onSelect={this.handleEndDate} value={this.state.endDate} label={'End Date'} minDate={this.state.startDate} ></DateSelector>:null }
				{showTripDuration? tripDurationElement:null }
				{errorelement}
			</div>
		);
	}

});

module.exports = travelerDatesSelector;