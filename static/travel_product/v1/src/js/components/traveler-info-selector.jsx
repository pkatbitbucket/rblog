var React = require('react')
var AppActions = require('../actions/app-actions')
var AppStore = require('../stores/app-store')
var settings = require('../settings')
var TitledDropdown = require('./ui/titled-dropdown')
var EventUtils = require('../utils/eventutils')



function getStateFromStores(){
	return {
		tripType: AppStore.getSelectedTripType(),
		ntravelers: AppStore.getNumberOfTravelers(),
		durationOptions: AppStore.getDurationOptions(),
		selectedDuration: AppStore.getSelectedDuration(),
	}
}

var travelerInfoSelector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	updateDuration: function(value, values) {
	    AppActions.selectDuration(values[0])
	    EventUtils.fire_cust_ev('quote_changed', {})
	 },

	updateTravelers: function(value, values){
		AppActions.setTravelers(value)
		EventUtils.fire_cust_ev('quote_changed', {})
	},

	render: function() {

		var travelerInfo, customClass

		var ntravlerOptions = []


		for (var i = 1; i <= settings.MAX_TRAVELERS; i++) {
			ntravlerOptions.push({
				label: i,
				value: i
			})
		};

		var selectedNumberOfTravelers = ntravlerOptions.filter(function(item){
			return item.value == this.state.ntravelers
		}, this)[0]

	

		if(this.state.tripType.value == 'SINGLE'){

			customClass="ntravelers-selector"
			travelerInfo = (
						<TitledDropdown options={ntravlerOptions} title="Number of Travelers" value={selectedNumberOfTravelers} onChange={this.updateTravelers} searchable={false} />					
				)
		}
		else{
			customClass="duration-selector"
			travelerInfo = (
						<TitledDropdown  options={this.state.durationOptions} title="Longest trip will be" value={this.state.selectedDuration} onChange={this.updateDuration} searchable={false} />
				)
		}

		return (
			<div className={"traveler-info-input " + customClass}>
				{travelerInfo}
			</div>
		);
	}

});

module.exports = travelerInfoSelector;