var React = require('react')
//var AppActions = require('../actions/app-actions')
var AppActions = require('../actions/app-actions')
var AppStore = require('../stores/app-store')
var EventUtils = require('../utils/eventutils')


// UI Helpers
var ControlGroup = require('./ui/controlgroup')


function getStateFromStores(){
	return {
		tripTypeOptions: AppStore.getTripTypeOptions(),
		selectedTripType: AppStore.getSelectedTripType()
	}
}

var tripTypeSelector = React.createClass({

	getInitialState: function() {
		return getStateFromStores()
	},

	componentDidMount: function() {
		AppStore.addChangeListener(this._onChange)
	},

	componentWillUnmount: function() {
		AppStore.removeChangeListener(this._onChange)
	},

	_onChange: function() {
		this.setState(getStateFromStores())
	},

	handleUpdate: function(item){
		AppActions.selectTripType(item)
		EventUtils.fire_cust_ev('quote_changed', {})

	},

	render: function() {
		return (
			<ControlGroup layout="horizontal" customClass="trip-type-selector boxed" items={this.state.tripTypeOptions} selectedValue={this.state.selectedTripType} onUpdate={this.handleUpdate} ></ControlGroup>
		);
	}

});

module.exports = tripTypeSelector;