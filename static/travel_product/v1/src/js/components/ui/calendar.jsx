var React = require('react')
var moment = require('moment')

var DayNames = React.createClass({
	render: function() {
		return <div className="week names">
			<span className="day">Su</span>
			<span className="day">Mo</span>
			<span className="day">Tu</span>
			<span className="day">We</span>
			<span className="day">Th</span>
		   	<span className="day">Fr</span>
		   	<span className="day">Sa</span>
		</div>;
	}
});

var Week = React.createClass({

	getInitialState: function() {
		return {
			ishighlighted: false 
		};
	},

	

	render: function() {
		var days = [],
			date = this.props.date,
			month = this.props.month;

		for (var i = 0; i < 7; i++) {
			var day = {
				name: date.format("dd").substring(0, 1),
				number: date.date(),
				isCurrentMonth: date.month() === month.month(),
				isToday: date.isSame(new Date(), "day"),
				date: date
			};
			days.push(<span key={day.date.toString()} className={"day" + (day.isToday ? " today" : "") + ((day.date.startOf('day') < this.props.minDate.startOf('day'))? " unaccessible":"") +(day.isCurrentMonth ? "" : " different-month") + ((day.date.isSame(this.props.startDate) || day.date.isSame(this.props.endDate)) ? " selected" : "") + (((this.props.startDate < day.date) &&  (day.date < this.props.endDate)) ? " intermediate": "")} onClick={this.props.select.bind(null, day)}>{day.number}</span>);
			date = date.clone();
			date.add(1, "d");

		}

		return <div className="week" key={days[0].toString()}>
			{days}
		</div>
	}
});

var calendar = React.createClass({

	getInitialState: function() {
		var calendarBeginDate = this.props.minDate || moment()
		if(this.props.startDate){
			calendarBeginDate = this.props.startDate
		}
		return {
			month: calendarBeginDate.startOf("day").clone(),
			startDate: this.props.startDate,
			endDate: this.props.EndDate
		};
	},

	previous: function() {
		var month = this.state.month;
		month.add(-1, "M");
		this.setState({ month: month });
	},

	next: function() {
		var month = this.state.month;
		month.add(1, "M");
		this.setState({ month: month });
	},

	select: function(day) {

		var minDate = this.props.minDate || moment()

		if(day.date.startOf('day') >= minDate.startOf('day')){
			this.state.startDate = day.date
			if(this.props.onSelect){
				this.props.onSelect(day.date)	
			}
		}


		// if(moment() < day.date){
		// 	if(!this.state.startDate){
		// 		this.state.startDate = day.date	
		// 	}
		// 	else{
		// 		if(this.props.multi){
		// 			if(day.date < this.state.startDate){
		// 				this.state.endDate = this.state.startDate
		// 				this.state.startDate = day.date
		// 			}
		// 			else{
		// 				this.state.endDate = day.date
		// 			}
		// 		}
		// 		else{
		// 			this.state.startDate = day.date	
		// 		}
		// 	}
		// }
		
		
		
	},

	render: function() {

		var currentMonth = this.state.month
		var nextMonth = currentMonth.clone().add(1, "M")

		return <div className='calendar-container'>
		<div className="calendar-title">{this.props.title}</div>
		<div className='calendar-month'>
			<div className="header">
				<i onClick={this.previous}>Prev</i>
				{this.renderMonthLabel(currentMonth)}
			</div>
			<DayNames />
			{this.renderWeeks(currentMonth)}
		</div>

		<div className='calendar-month'>
			<div className="header">
				{this.renderMonthLabel(nextMonth)}
				<i className='next' onClick={this.next}>Next</i>
			</div>
			<DayNames />
			{this.renderWeeks(nextMonth)}
		</div>

		</div>;
	},

	renderWeeks: function(month){
		var weeks = [],
			done = false,
			date = month.clone().startOf("month").add("w" -1).day("Sunday"),
			monthIndex = date.month(),
			count = 0;

		while (!done) {
			weeks.push(<Week key={date.toString()} date={date.clone()} month={month} select={this.select} startDate={this.state.startDate} endDate={this.state.endDate} minDate={this.props.minDate || moment()} />);
			date.add(1, "w");
			done = count++ > 2 && monthIndex !== date.month();
			monthIndex = date.month();
		}
		return weeks;
	},

	renderMonthLabel: function(month) {
		return <span>{month.format("MMMM, YYYY")}</span>;
	}

});

module.exports = calendar;



