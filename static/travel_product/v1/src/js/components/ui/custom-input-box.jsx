var React = require('react');

var customInputBox = React.createClass({

	getInitialState: function() {
		return {
			value: this.props.defaultValue 
		};
	},

	handleChange: function(event){
		this.setState({
			value: event.target.value 
		});

		this.props.onChange(event.target.value)
	},

	render: function() {
		var hasError = this.props.hasError
		var customClass = this.props.customClass

		var inputClass = "custom-input-box " + (customClass? customClass:'') + (hasError? ' has-error':'')
		var placeholder = hasError? this.props.errorText:this.props.placeholder

		var rand = Math.floor((Math.random() * 100) + 1)


		return (
			<div className={inputClass}>
				<input defaultValue={this.props.defaultValue} maxLength={this.props.maxLength} id={"input-"+rand} className={this.state.value? 'has-value':''} onChange={this.handleChange} />
				<label htmlFor={"input-"+rand} >{placeholder}</label>
			</div>
		);
	}

});

module.exports = customInputBox;