var React = require('react');

var KVitem = React.createClass({
	getInitialState: function() {
		return {
			connectorLeft: 0,
			connectorRight: 0 
		};
	},
	componentDidMount: function() {

		var newConnectorLeft = this.refs.connectorTitle.getDOMNode().offsetWidth
		var newConnectorRight = this.refs.connectorValue.getDOMNode().offsetWidth

		

		this.setState({
			connectorLeft: newConnectorLeft + 'px',
			connectorRight: newConnectorRight + 'px'
		});
	},

	render: function() {

		var connectorStyle = {
		  left: this.state.connectorLeft,
		  right: this.state.connectorRight,
		};

		return (
			<div className="kv-item-wrapper">
				<div className={"kv-item small " + this.props.customClass}>
					<div className="connector" style={connectorStyle}></div>
					<div className="key">
						<span ref="connectorTitle">{this.props.title}</span>
					</div>
					<div className="value">
						<span ref="connectorValue">{this.props.value}</span>
					</div>
				</div>
			</div>
		);
	}

});

module.exports = KVitem;