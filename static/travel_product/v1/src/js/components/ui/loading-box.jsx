var React = require('react');

var LoadingBox = React.createClass({

	render: function() {
		return (
			<div className="loading-box-wrapper ">
				<img src={'/static/travel_product/v1/src/img/loading.png'} />
				<div className="loading-box-title">{this.props.title}</div>
				<div className="loading-box-subtitle">{this.props.subtitle}</div>
			</div>
		);
	}

});

module.exports = LoadingBox;