var React = require('react')


var miniPlanCard = React.createClass({

	render: function() {
		var plan = this.props.plan

		return (
			
			<div className="plan-card-wrapper">
				<div className="plan-card">
				<div className="plan-column column-1">
					<div className="plan-remove" onClick={this.props.onRemove} >x</div>
					<div className="plan-name-mini">
						{plan.name}
					</div>
					<div className="plan-insurer-name">
						{plan.insurer}
					</div>

				</div>
				</div>
			</div>
		);
	}

});

module.exports = miniPlanCard;