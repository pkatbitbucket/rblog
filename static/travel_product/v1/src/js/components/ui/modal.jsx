var React = require('react');

var modal = React.createClass({

	render: function() {
		var element = document.body
		
		if(this.props.isOpen){
			element.classList.add("modal-open")
		}
		else{
			element.classList.remove("modal-open")
		}

		return (
			<div className={"modal-wrapper "+ (this.props.isOpen? 'reveal':'')}>
				{/*<div className="modal-bg-overlay"></div>*/}
				<div className={"modal-content " + (this.props.customClass? this.props.customClass:'') + ' ' +(this.props.size? this.props.size:'small')}>
					{this.props.children}
				</div>
			</div>
		);
	}

});

module.exports = modal;