var React = require('react');

var planCard = React.createClass({


	render: function() {
		var plan = this.props.plan
		return (
			<div className={"plan-card-wrapper " + (this.props.customClass? this.props.customClass:'')}>
				<div className="plan-card">
				
				<div className="plan-column column-1" onClick={this.props.onDetail}>
					<div className="insurer">
						<img src={'/static/travel_product/v1/src/img/insurers/' + plan.insurerSlug + '.png'} className='img-responsive' /> 
					</div>
				</div>
				<div className="plan-column features-column column-2">
					<span className="plan-header">Medical Cover</span>
					{plan.medical_cover || 'Not Covered'}
					<div className="feature-sub">Deductible: {plan.medical_cover_deductible || 'N/A'}</div>
					<div className="feature-tip">
						<h3>Medical Cover</h3>
						Expenses for hospitalisation of the insured for emergency medical treatment due to illness or accident.
					</div>
				</div>
				<div className="plan-column features-column column-3">
					<span className="plan-header">Existing Diseases Cover</span>
					<span className="plan_ped">{plan.ped || 'Not Covered'}</span>
					<div className="feature-tip">
						<h3>Existing Diseases Cover</h3>
						Expenses for existing diseases in life threatening scenarios.
					</div>
				</div>
				<div className="plan-column features-column column-4">
				<span className="plan-header">Baggage Loss Cover</span>
					{plan.baggage_loss || 'Not Covered'}
					<div className="feature-tip">
						<h3>Baggage Loss Cover</h3>
						Expenses for complete & permanent loss of insured traveller’s checked baggage.
					</div>
				</div>
				
				<div className="plan-column features-column column-5">
					<span className="plan-header">Trip Delay Cover</span>
					{plan.trip_delay || 'Not Covered'}
					<div className="feature-tip">
						<h3>Trip Delay Cover</h3>
						Expenses due to trip delay beyond specified number of hours
					</div>
				</div>
				<div className="plan-column column-7">

					<div className="btn orange-flat btn-inline-label" onClick={this.props.onBuy}>
                        <span className="btn-label">Buy For</span>
                        <span>Rs. {plan.premium}</span>
                    </div>


					{/*(plan.can_buy == true && !this.props.hideActions)? <div className="see-details" onClick={this.props.onDetail}>
						Plan Details
					</div> :null */}
					
				</div>
				<div className="plan-name">
						{plan.name} 
						{(plan.can_buy == true && !this.props.hideActions)? <span className="see-details pull-right" onClick={this.props.onDetail}>
						See Details
					</span> :null }

					{(!this.props.hideActions)? <div className={"compare-btn pull-right " + (plan.is_selected? 'selected':'')} onClick={this.props.onAddToCompare}>
					{plan.is_selected? [<span>&#x2713;</span> ,'Added to compare']:'+ Add to compare'}</div>:null}

					</div>
				</div>
			</div>
		);
	}

});

module.exports = planCard;