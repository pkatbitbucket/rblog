var React = require('react');

var radiocontrol = React.createClass({

	render: function() {
		var change = this.props.change
		var selected = this.props.selected
		var item = this.props.item
		var desc
		var classname = 'radio-control ' + (selected ? 'selected':'')

		if(item.desc){
			desc = <div className="desc">{item.desc}</div>
		}
		return (
			<div onClick={change.bind(null, item)} className={classname}>
				<div className="label">{item.label}</div>
				{desc}
			</div>
		);
	}

});

module.exports = radiocontrol;