var React = require('react')
var Select = require('react-select')

var titledDropdown = React.createClass({

	render: function() {
		return (
			<div className={"titled-dropdown " + (this.props.customClass? this.props.customClass : '')}>
				<div className="title-wrapper">
					{this.props.title}
				</div>
				<div className="dd-wrapper">
					<Select options={this.props.options} value={this.props.value} onChange={this.props.onChange} searchable={this.props.searchable} clearable={false} />
				</div>
			</div>
		);
	}

});

module.exports = titledDropdown;