module.exports = {

	// COUNTRY Actions
	SELECT_COUNTRY: 'SELECT_COUNTRY',
	SELECT_REGION: 'SELECT_REGION',
	RECIEVE_COUNTRIES: 'RECIEVE_COUNTRIES',
	DESELECT_COUNTRY: 'SELECT_COUNTRY',
	DESELECT_ALL_COUNTRIES: 'DESELECT_ALL_COUNTRIES',
	SELECT_MULTIPLE_COUNTRIES: 'SELECT_MULTIPLE_COUNTRIES',


	// Trip Type
	SELECT_TRIP_TYPE: 'SELECT_TRIP_TYPE',

	SET_TRAVELERS: 'SET_TRAVELERS',
	SET_RESIDENCE: 'SET_RESIDENCE',
	SET_RESIDENT_FOR: 'SET_RESIDENT_FOR',
	SET_OCI_CARD: 'SET_OCI_CARD',
	SET_CURRENT_LOCATION: 'SET_CURRENT_LOCATION',
	SELECT_DURATION: 'SELECT_DURATION_OF_LONGEST_TRIP',

	SELECT_START_DATE: 'SELECT_START_DATE',
	SELECT_END_DATE: 'SELECT_END_DATE',

	ADD_TRAVELER_AGES: 'ADD_TRAVELER_AGES',
	RECIEVE_QUOTES: 'RECIEVE_QUOTES',
	FETCH_QUOTES: 'FETCH_QUOTES',

	UPDATE_OTHER_PED: 'UPDATE_OTHER_PED',
	SELECT_PED: 'SELECT_PED',
	DESELECT_PED: 'DESELECT_PED',
	DESELECT_ALL_PED: 'DESELECT_ALL_PED',
	SET_MEDICAL_COVER: 'SET_MEDICAL_COVER',
	SET_MEDICAL_COVER_FROM_VALUE: 'SET_MEDICAL_COVER_FROM_VALUE',

	SELECT_PLAN: 'SELECT_PLAN',
	DESELECT_PLAN: 'DESELECT_PLAN',
	SET_UNAVAILABLE_FOR_BUYING:'SET_UNAVAILABLE_FOR_BUYING',

	// UI 
	OPEN_MODAL:'OPEN_MODAL',
	CLOSE_MODAL:'CLOSE_MODAL',
	OPEN_PED_MODAL:'OPEN_PED_MODAL',
	CLOSE_PED_MODAL:'CLOSE_PED_MODAL',
	SET_MODE: 'SET_MODE',
	VALIDATE: 'VALIDATE',
	SET_ERRORS: 'SET_ERRORS',
	RESET_PARAMS: 'RESET_PARAMS',
	RECIEVE_REDIRECT_URL:'RECIEVE_REDIRECT_URL',


	SEND_TO_LMS:'SEND_TO_LMS',
	SELECT_PLAN_FOR_BUY:'SELECT_PLAN_FOR_BUY',
	DESELECT_PLAN_FOR_BUY:'DESELECT_PLAN_FOR_BUY',
	SELECT_PLAN_FOR_DETAIL:'SELECT_PLAN_FOR_DETAIL',
	DESELECT_PLAN_FOR_DETAIL:'DESELECT_PLAN_FOR_DETAIL',
	INSURERS_WITH_DECLINED_RISKS: ['reliance', 'star-health'],
	INSURERS_WITH_NO_PEDS: ['bajaj-allianz', 'bharti-axa', 'hdfc-ergo'],

}