module.exports = {
	// PEDs Initial Data
	allPEDs: CONTEXT_DATA.form.fields.declined_risks,

	// Medical Cover Options
	medicalCoverOptions: CONTEXT_DATA.form.fields.sum_assured.map(function(sum){
		return {'label': '$'+sum[1], 'value': sum[0]}
	}),

	// Regions
	regions: [{
		label: "Worldwide",
		value: "W"
	},
	{
		label: "Worldwide excluding US/Canada",
		value: "W_NO_US_CANADA"
	}],

	// Generic Boolean options array
	booleanOptions: [{
		label: 'Yes',
		value: true
	},
	{
		label:'No',
		value:false
	}],


	// All the durations for multi trip policy
	durationOptions: [{
		label: "30 days",
		value: 30
	},
	{
		label: "45 days",
		value: 45
	},
	{
		label: "60 days",
		value: 60
	}],

	travelerStayDurationOptions : [{
	label: 'Less than 6 months',
	value: 'LESS_THAN_6'
	},

	{
		label: 'More than 6 months',
		value: 'MORE_THAN_6'
	},
	],

	// Trip Type Options
	tripTypeOptions: [{
		label: "Single Trip Policy",
		desc: "Individual or family doing one trip of up to 180 days starting from India",
		value: "SINGLE"
	},
	{
		label: "Annual Multi Trip Policy",
		desc: "Individuals doing multiple trips to different destinations within a year",
		value: "MULTI"
	}],	

	INSURERS_WITH_DECLINED_RISKS: ['reliance', 'star-health'],
	INSURERS_WITH_NO_PEDS: ['bajaj-allianz', 'bharti-axa', 'hdfc-ergo'],



}