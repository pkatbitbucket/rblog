'use strict'

require("../css/style.less")
var React = require('react/addons')
var App = require('./components/app')
var Results = require('./components/results')
var olark = require('./utils/olarkutils')

var Transition = React.addons.CSSTransitionGroup

// Router
var Router = require('react-router')
var DefaultRoute = Router.DefaultRoute
var Route = Router.Route
var RouteHandler = Router.RouteHandler


var webAPIutils = require('./utils/webAPIutils')
webAPIutils.getInitialData()



var AppWrapper = React.createClass({
	render: function(){
    var key = Math.floor((Math.random() * 100) + 1)
		return (
      <Transition transitionName='example'>
			 <RouteHandler key={key} />
      </Transition>
		)
	}
})


var routes = (
  <Route name="app" path="/" handler={AppWrapper}>    
  	<Route name="results" path="results" handler={Results} />
    <DefaultRoute handler={App} />
  </Route>
)

Router.run(routes, function (Handler) {
  React.render(<Handler/>, document.getElementById('content'));
})



