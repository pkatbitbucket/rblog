var AppConstants = require('../constants/app-constants.jsx')
var AppSettings = require('../constants/app-settings.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')
var LMSutils = require('../utils/LMSutils')
var moment = require('moment')
var TravelStorageUtils = require('../utils/TravelStorageUtils')

var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"


var CountryStore = require('../stores/country-store')
var MedicalCoverStore = require('../stores/medical-cover-store')
var PEDStore = require('../stores/ped-store')


var tripTypeOptions = AppSettings.tripTypeOptions
var durationOptions = AppSettings.durationOptions
var booleanOptions = AppSettings.booleanOptions
var residenceOptions = AppSettings.travelerStayDurationOptions


var _tripType = tripTypeOptions[0]
var _ntravelers = 1
var _travelerAges = []
var _duration = durationOptions[0]
var _isResident = booleanOptions[0]
var _isCurrentlyInIndia = booleanOptions[0]
var _startDate, _endDate
var _hasOCICard = booleanOptions[0]
var _residentFor
var _durationValue



// save to local storage

function saveToLocalStorage(){
	if(typeof(Storage) !== "undefined") {
		var _localStorage = {}
		if(_startDate){
			_localStorage._startDate = _startDate.format('YYYY-MM-DD')
		}
    	if(_endDate){
    		_localStorage._endDate =  _endDate.format('YYYY-MM-DD')	
    	}
    	_localStorage._ntravelers =  _ntravelers
    	_localStorage._tripTypeValue =  _tripType.value
    	_localStorage._travelerAges =  _travelerAges
    	_localStorage._isResident = _isResident.value
    	_localStorage._durationValue = _durationValue	
    	_localStorage._tripStartIndia = _isCurrentlyInIndia.value
    	if(_hasOCICard!=undefined && _hasOCICard.value!=undefined){
    		_localStorage._ocipioCard = _hasOCICard.value
    	}
    	
    	if(_residentFor!=undefined){
    		_localStorage._residentFor = _residentFor.value	
    	}
    	TravelStorageUtils.setValuesInLocalStorage("travel", _localStorage)
	}
}


// populate from local storage

function populateFromLocalStorage(){
	
	if(typeof(Storage) !== "undefined") {

		if(JSON.parse(localStorage.travel)._tripTypeValue != undefined){

			var ttv = (JSON.parse(localStorage.travel)._tripTypeValue)

			_tripType = tripTypeOptions.filter(function(item){
				return item.value == ttv
			})[0]

		}

		if(JSON.parse(localStorage.travel)._durationValue != undefined){
			var ddv = (JSON.parse(localStorage.travel)._durationValue)

			_duration = durationOptions.filter(function(item){
				return item.value == ddv
			})[0]
		}

		if(JSON.parse(localStorage.travel)._endDate != undefined){
			_endDate = moment((JSON.parse(localStorage.travel)._endDate), 'YYYY-MM-DD')
		}

		if(JSON.parse(localStorage.travel)._startDate != undefined){
			_startDate = moment((JSON.parse(localStorage.travel)._startDate), 'YYYY-MM-DD')
		}

		if(JSON.parse(localStorage.travel)._ntravelers!= undefined){
			_ntravelers = (JSON.parse(localStorage.travel)._ntravelers)
		}

		if(JSON.parse(localStorage.travel)._travelerAges!= undefined){
			_travelerAges = (JSON.parse(localStorage.travel)._travelerAges)
		}

		if(JSON.parse(localStorage.travel)._isResident!= undefined){
			_isResident = booleanOptions.filter(function(item){
				return item.value == JSON.parse(localStorage.travel)._isResident
			})[0]
		}

		if(JSON.parse(localStorage.travel)._residentFor != undefined){
			_residentFor = residenceOptions.filter(function(item){
				return item.value == (JSON.parse(localStorage.travel)._residentFor)
			})[0]
		}
		if(JSON.parse(localStorage.travel)._ocipioCard!= undefined){
			_hasOCICard = booleanOptions.filter(function(item){
				return item.value == (JSON.parse(localStorage.travel)._ocipioCard)
			})[0]
		}
		if(JSON.parse(localStorage.travel)._tripStartIndia!= undefined){
			_isCurrentlyInIndia = booleanOptions.filter(function(item){
				return item.value == (JSON.parse(localStorage.travel)._tripStartIndia)
			})[0]
		}
	}
}


populateFromLocalStorage()

var appStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback){
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	getTripTypeOptions: function(){
 		return tripTypeOptions
 	},

 	getSelectedTripType: function(){
 		return _tripType
 	},

 	getNumberOfTravelers: function(){
 		return _ntravelers
 	},

 	getResidentStatus: function(){
 		return _isResident
 	},

 	getCurrentLocation: function(){
 		return _isCurrentlyInIndia
 	},

 	getBooleanOptions: function(){
 		return booleanOptions
 	},

 	getSelectedDuration: function(){
 		return _duration
 	},

 	getDurationOptions: function(){
 		return durationOptions
 	},

 	getStartDate: function(){
 		return _startDate
 	},

 	getEndDate: function(){
 		return _endDate
 	},

 	getTravelerAges: function(){
 		return _travelerAges
 	},

 	hasOCICard: function(){
 		return _hasOCICard
 	},

 	getResidentFor: function(){
 		return _residentFor
 	},

 	getDaysToStartDate: function(){
 		var startDate = _startDate
 		startDate = startDate.startOf('day')
 		var today = moment().startOf('day')
 		var diff = startDate.diff(today, 'days') 

 		return diff
 	},

 	getSingleTripDuration: function(){
 		var diff = 0
 		if(_startDate && _endDate){
	 		var startDate = _startDate
	 		startDate = startDate.startOf('day')
	 		var endDate = _endDate
	 		endDate = endDate.startOf('day')
	 		diff = endDate.diff(startDate, 'days')
 		}
 		return diff + 1
 	},
 	
 	getQuoteRequest: function(){

 		var quote = {
 			type : (_tripType.value == 'SINGLE')? 'single':'multi',
 			isResident: _isResident? 1:0 ,
 			adults: _travelerAges.join(',')
 		}

 		quote.declined_risks = PEDStore.getSelectedPEDslugs()

 		var selectedMedicalCover = MedicalCoverStore.getSelectedMedicalCover()

 		if(selectedMedicalCover){
 			quote.sum_assured = selectedMedicalCover.value
 		}

 		if(_isResident == false && _residentFor){
			quote.resident_for = ((_residentFor.value == 'LESS_THAN_6')? 0:0.5)
 		}

 		if(_tripType.value == 'SINGLE'){
 			quote.places = CountryStore.getSelectedCountries()
 			/*quote.places = CountryStore.getSelectedCountries().map(function(item){
 				return item.value
 			});*/
 		}
 		else{
 			quote.duration = _duration.value

 			var region = CountryStore.getSelectedRegion()

 			if(region){

 				if(region != 'W'){
	 				quote.places = ['worldwide']
	 				quote.places_excluded = ["canada", "usa-united-states-of-america"]
	 			}else{
	 				quote.places = ['worldwide', "canada", "usa-united-states-of-america"]
	 				quote.places_excluded = []
	 			}

 			}

 			
 		}

 		if(_startDate){
 			quote.from_date = _startDate.format('YYYY-MM-DD');
 		}

 		if(_endDate){
 			quote.to_date = _endDate.format('YYYY-MM-DD');
 		}

 		return quote
 	},

 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.SELECT_TRIP_TYPE:
 		 		_tripType = action.tripType
 		 		if(action.tripType.value == 'MULTI'){
 		 			_ntravelers = 1
 		 			if(_travelerAges.length > _ntravelers){
	 		 			_travelerAges.splice(_ntravelers, (_travelerAges.length-_ntravelers))
	 		 		}
 		 		}
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SET_TRAVELERS:
 		 		_ntravelers = parseInt(action.number)
 		 		if(_travelerAges.length > _ntravelers){
 		 			_travelerAges.splice(_ntravelers, (_travelerAges.length-_ntravelers))
 		 		}
 		 		
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SET_RESIDENCE:
 		 	
 		 		_isResident = action.residence
 		 		_hasOCICard = undefined
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SET_RESIDENT_FOR:
 		 		_residentFor = action.value
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SET_OCI_CARD:
 		 		_hasOCICard = action.value
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SET_CURRENT_LOCATION:
 		 		_isCurrentlyInIndia = action.location
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_DURATION:
 		 	
 		 		_duration = action.duration
 		 		_durationValue = parseInt(action.duration.value)
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_START_DATE:
 		 		_startDate = action.date
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_END_DATE:
 		 		_endDate = action.date
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.ADD_TRAVELER_AGES:
 		 		_travelerAges = action.ages
 		 		var _temp = []
 		 		for(var i=0; i < action.ages.length; i++){
		 			if(action.ages[i]!="" && !isNaN(action.ages[i]) && !isNaN(parseInt(action.ages[i])) && parseInt(action.ages[i]) > 0) {
						_temp.push(parseInt(action.ages[i]))
		 			}
 		 		}
 		 		_travelerAges = _temp
 		 		saveToLocalStorage()
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.VALIDATE:
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SET_ERRORS:
 		 		_hasErrors = action.hasErrors
 		 		appStore.emitChange()
 		 		break

 		 	case AppConstants.SEND_TO_LMS:
 		 		var quoteRequest = appStore.getQuoteRequest()
 		 		var extras = action.data
 		 		var callback = action.callback
 		 		extras['travel_date'] = appStore.getStartDate().format('DD/MM/YYYY')
 		 		
 		 		for(var key in quoteRequest){
 		 			extras[key] = quoteRequest[key]
 		 		}

 		 		LMSutils.sendToLMS({}, 'results-page', extras, callback)
 		 		break
 		 }
 	})
})


module.exports = appStore