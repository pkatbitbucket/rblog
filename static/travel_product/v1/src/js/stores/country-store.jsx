var AppConstants = require('../constants/app-constants.jsx')
var AppSettings = require('../constants/app-settings.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')
var schengenCountries = require('../utils/schengen_places.js')
var TravelStorageUtils = require('../utils/TravelStorageUtils')

schengenCountries = schengenCountries.map(function(item){
	return item[0]
})

// move this somewhere else
var popularCountries = ["Argentina","Australia","Austria",
"Bahamas","Belgium",
"Brazil","Canada",
"China","Colombia",
"Cuba","Denmark",
"Egypt","Europe",
"France","Germany",
"Greece","Indonesia",
"Ireland","Italy",
"Japan","Malaysia",
"Maldives","Mauritius",
"Mexico","Nepal",
"Netherlands","New Zealand",
"Nigeria","Norway",
"Philippines","Poland",
"Portugal","Qatar",
"Russia","Saudi Arabia",
"Schengen Countries","Singapore",
"South Africa","Spain",
"Sri Lanka","Sweden",
"Switzerland","Tanzania",
"Thailand","Turkey",
"Uganda","Ukraine",
"United Arab Emirates (UAE)","United Kingdom (UK)",
"United States of America (USA)","Vatican City (Holy See)",
"Zimbabwe"]

var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var _countries = []
var _selectedRegion = null
var _selectedCountries = null
var _regions = AppSettings.regions

function setupCountries(){
	var temp1 = [], temp2 = []
	var nCountries = _countries.length
	for (var i = 0; i < nCountries; i++) {
		if(popularCountries.indexOf(_countries[i].label)>-1){
			temp1.push(_countries[i])
		}
		else{
			temp2.push(_countries[i])
		}
	};

	_countries = temp1.concat(temp2)
}


function selectCountry(index){
	_countries[index].is_selected = true
	_selectedCountries.push(_countries[index].value)
}

function deSelectCountry(index){
	_countries[index].is_selected = false
	var foundIndex = _selectedCountries.indexOf(_countries[index].value)
	if(foundIndex > -1){
		_selectedCountries.splice(foundIndex,1)
	}
}

function deselectAllCountries(){
	_countries = _countries.map(function(item, i){
		item.is_selected = false
		return item
	})
	_selectedCountries =[]
}

function selectMultipleCountries(indexes){
	var length = indexes.length
	for (var i = 0; i < length; i++) {
		selectCountry(indexes[i])
	}
}


// save to local storage

function saveToLocalStorage(){
	if(typeof(Storage) !== "undefined") {
		var _localStorage = {}
    	_localStorage._selectedCountriesSlugs = countryStore.getSelectedCountriesSlugs()
    	_localStorage._selectedRegion =  _selectedRegion
    	TravelStorageUtils.setValuesInLocalStorage("travel", _localStorage)
	}
}


// populate from local storage

function populateFromLocalStorage(){
	if(typeof(Storage) !== "undefined") {
		if(JSON.parse(localStorage.travel)._selectedCountriesSlugs!= undefined){
			 _selectedCountries = JSON.parse(localStorage.travel)._selectedCountriesSlugs
			
		/*	for (var j = _countries.length - 1; j >= 0; j--) {
				for (var i = 0; i < sc.length; i++) {
					if(_countries[j].value == sc[i]){
						_countries[j].is_selected = true
					}
				}
			}*/
		}

		if(JSON.parse(localStorage.travel)._selectedRegion!= undefined){
			_selectedRegion = JSON.parse(localStorage.travel)._selectedRegion
		}
	}
}


var countryStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	getCountries: function(){
 		return _countries
 	},

 	getRegions: function(){
 		return _regions
 	},

 	getSelectedRegion: function(){
 		if(_selectedRegion!=undefined && _selectedRegion!=null && _selectedRegion.length > 0){
			return _selectedRegion
		}
		else {
			return []
		}
 	},

 	getSelectedCountries: function(){
		if(_selectedCountries!=undefined && _selectedCountries!=null && _selectedCountries.length > 0){
			return _selectedCountries
		}
		else {
			return []
		}
 	},

 	getSelectedCountriesSlugs: function(){
		if(_selectedCountries!=undefined && _selectedCountries!=null && _selectedCountries.length > 0){
			return _selectedCountries
		}
		else {
			return []
		}
		
 	},

 	getSelectedSchengenCountries: function(){
 		var allSelectedCountriesSlugs = countryStore.getSelectedCountriesSlugs()
 		if(allSelectedCountriesSlugs!=undefined && allSelectedCountriesSlugs!=null && allSelectedCountriesSlugs.length > 0){
 			return allSelectedCountriesSlugs.filter(function(item){
 				return schengenCountries.indexOf(item) > -1
 			});
 		}
 		else {
 			return []
 		}
 	},


 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.SELECT_COUNTRY:
 		 	
 		 		selectCountry(action.index)
 		 		saveToLocalStorage()
 		 		countryStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_REGION:
 		 	
 		 		_selectedRegion = action.region.value
 		 		saveToLocalStorage()
 		 		countryStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_MULTIPLE_COUNTRIES:
 		 	
 		 		selectMultipleCountries(action.indexes)
 		 		saveToLocalStorage()
 		 		countryStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_COUNTRY:
 		 		deselectCountry(action.index)
 		 		saveToLocalStorage()
 		 		countryStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_ALL_COUNTRIES:
 		 		deselectAllCountries()
 		 		saveToLocalStorage()
 		 		countryStore.emitChange()
 		 		break

 		 	case AppConstants.RECIEVE_COUNTRIES:
 		 		_countries = action.countries
 		 		setupCountries()
 		 		populateFromLocalStorage()
 		 		countryStore.emitChange()
 		 		break
 		 }
 	})
})


module.exports = countryStore