var AppConstants = require('../constants/app-constants.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')
var moment = require('moment')

var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var CountryStore = require('../stores/country-store')
var AppStore = require('../stores/app-store')

var _destinationError, _datesError, _travelerAgesError, _extraInfoError
var _validate = false

var errorStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	hasErrors: function(){
 		if(_destinationError || _datesError || _travelerAgesError || _extraInfoError){
 			return true
 		}
 		else{
 			return false
 		}
 	},

 	getDestinationError: function(){
 		if(_validate){
 			var tripType = AppStore.getSelectedTripType()
 			var countries = CountryStore.getSelectedCountries()
 			var selectedRegion = CountryStore.getSelectedRegion()

 			if(tripType.value == 'SINGLE' && countries.length == 0){
 				_destinationError = 'Please select the countries you are visiting'
 				return _destinationError
 			}

 			if(tripType.value == 'MULTI' && !selectedRegion){
 				_destinationError = 'Please select your destination'
 				return _destinationError
 			}
 		}
 		_destinationError = undefined
 		return _destinationError
 	},

 	getDatesError: function(){
 		if(_validate){
 			var tripType = AppStore.getSelectedTripType().value
 			var startDate = AppStore.getStartDate()
 			var endDate = AppStore.getEndDate()

 			if(!startDate){
 				_datesError = "Please select trip start date"
 				return _datesError	
 			}

 			if(startDate && (startDate.startOf('day') < moment().startOf('day'))){
 				_datesError = "Trip start date cannot be in the past"
 				return _datesError	
 			}

 			if(endDate && (endDate.startOf('day') < moment().startOf('day'))){
 				_datesError = "Trip end date cannot be in the past"
 				return _datesError	
 			}

 			if(tripType == 'SINGLE'){

 				if(!endDate && tripType == 'SINGLE'){
	 				_datesError = "Please select trip end date"
	 				return _datesError	
	 			}

	 			if(startDate.startOf('day') > endDate.startOf('day')){
	 				_datesError = "Hello time traveller! End date cannot come before start date."
	 				return _datesError
	 			}

	 			if(endDate.startOf('day').diff(startDate.startOf('day'), 'days') > 180 ){
	 				_datesError = "Your trip duration cannot exceed 180 days."
	 				return _datesError
	 			}

 			}

 		}

 		_datesError = undefined
 		return _datesError
 		
 		
 	},

 	getTravelerAgesError: function(){
 		if(_validate){
 			var ntravelers = AppStore.getNumberOfTravelers()
 			var travelerAges = AppStore.getTravelerAges()
 			var tripType = AppStore.getSelectedTripType().value


 			if(travelerAges.length != ntravelers){
 				if(ntravelers == 1){
 					_travelerAgesError =  "Please enter traveler’s age"
 				}
 				else{
 					_travelerAgesError =  "Please enter age of "+ ((travelerAges.length == 0)? 'the ':'') +(ntravelers - travelerAges.length)+ ((travelerAges.length > 0)? ' more':'') +" travelers"	
 				}

 				return _travelerAgesError
 			}
 			else{

 				if(ntravelers == 1){
 					if(travelerAges.length == 1 && parseInt(parseFloat(travelerAges[0])) < 1){
 						_travelerAgesError =  "Traveler must be atleast 1 year old."
	 					return _travelerAgesError	
 					}

 					if(tripType == 'MULTI' && parseInt(travelerAges[0]) < 18 ){
						_travelerAgesError =  "Traveller must be atleast 18 years old to buy an annual multitrip travel policy."
	 					return _travelerAgesError
 					}
	 				// if(travelerAges.length == 1 && parseInt(travelerAges[0]) < 18 ){
	 				// 	_travelerAgesError =  "Traveller must be atleast 18 years old to buy travel insurance."
	 				// 	return _travelerAgesError
	 				// }
	 			}

	 			else{
	 				var negativeAges = travelerAges.filter(function(item){
	 					return parseInt(item) <= 0
	 				})

					if(negativeAges.length > 0){
						_travelerAgesError =  "Invalid age.Traveler age(s) must be 1 year or above."
	 					return _travelerAgesError
					}

	 				var adultAges = travelerAges.filter(function(item){
	 					return parseInt(item) > 17
	 				})

	 				if(adultAges.length == 0){
	 					_travelerAgesError =  "Don't send your kids alone in the world. At least have one adult above the age of 17 accompany them."
	 					return _travelerAgesError
	 				}

	 				
	 			}

 			}

 			
 		}

 		_travelerAgesError = undefined
 		return _travelerAgesError
 	},

 	getExtraInfoError: function(){
 		if(_validate){
 			var isResident = AppStore.getResidentStatus()
 			if(isResident.value === false){
 				var hasOCICard = AppStore.hasOCICard()
 				if(!hasOCICard){
 					_extraInfoError = "Please enter this value"
 					return _extraInfoError
 				}
 				if(hasOCICard && hasOCICard.value === true){
 					var residentFor = AppStore.getResidentFor()
 					if(!residentFor){
 						_extraInfoError = "Please select one option"
 						return _extraInfoError
 					}
 				}
 			}
 		}
 		_extraInfoError = undefined
 		return _extraInfoError
 	},

 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.VALIDATE:
 		 		_validate = true
 		 		errorStore.emitChange()
 		 		break
 		 }
 	})
})


module.exports = errorStore