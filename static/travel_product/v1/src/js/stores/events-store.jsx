var AppConstants = require('../constants/app-constants.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')


var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var CountryStore = require('../stores/country-store')
var AppStore = require('../stores/app-store')



var eventsStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},
 	
 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.VALIDATE:
 		 		eventsStore.emitChange()
 		 		break
 		 }
 	})
})

module.exports = eventsStore