var AppConstants = require('../constants/app-constants.jsx')
var AppSettings = require('../constants/app-settings.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')


var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var AppStore = require('../stores/app-store')

var moment = require('moment')

var _medicalCoverOptions = AppSettings.medicalCoverOptions

var _selectedMedicalCover


// save to local storage

function saveToLocalStorage(){
	if(typeof(Storage) !== "undefined") {
    	if(_selectedMedicalCover){
    		localStorage._selectedMedicalCoverValue = JSON.stringify(_selectedMedicalCover.value)
    	}
	}
}


// populate from local storage

function populateFromLocalStorage(){
	if(typeof(Storage) !== "undefined") {
		if(localStorage._selectedMedicalCoverValue){
			var mcv = JSON.parse(localStorage._selectedMedicalCoverValue)
			_selectedMedicalCover = _medicalCoverOptions.filter(function(item){
				return item.value == mcv
			})[0]
		}
	}
}

function setMedicalCoverFromValue(value){
	_selectedMedicalCover = _medicalCoverOptions.filter(function(item){
		return item.value == value
	})[0]
}

function resetParams(){
	_selectedMedicalCover = undefined
	if(localStorage._selectedMedicalCover){
		localStorage.removeItem(_selectedMedicalCover);
	}
}


populateFromLocalStorage()


var medicalCoverStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	getMedicalCoverOptions: function(){
 		return _medicalCoverOptions
 	},

 	getSelectedMedicalCover: function(){
 		return _selectedMedicalCover
 	},

 	
 	
 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	 case AppConstants.SET_MEDICAL_COVER:
 		 		_selectedMedicalCover = action.value
 		 		saveToLocalStorage()
 		 		medicalCoverStore.emitChange()
 		 		break

 		 	case AppConstants.RECIEVE_QUOTES:
 		 		if(!_selectedMedicalCover){
 		 			setMedicalCoverFromValue(action.sum_assured)	
 		 			saveToLocalStorage()
 		 		}
 		 		medicalCoverStore.emitChange()
 		 		break

 		 	case AppConstants.RESET_PARAMS:
 		 		resetParams()
 		 		medicalCoverStore.emitChange()
 		 		break


 		 }
 	})
})


module.exports = medicalCoverStore