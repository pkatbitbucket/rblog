var AppConstants = require('../constants/app-constants.jsx')
var AppSettings = require('../constants/app-settings.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')


var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"


var _allPEDs = []
var _otherPED

var PEDdata = AppSettings.allPEDs

var c = 0
for (var key in PEDdata){

	_allPEDs.push({
		index: c,
		slug: key,
		verbose: PEDdata[key].malady,
		insurers: PEDdata[key].insurers,
	})

	c++
}




function selectPED(index){
	_allPEDs[index].is_selected = true
}

function deselectPED(index){
	_allPEDs[index].is_selected = false
}



// save to local storage

function saveToLocalStorage(){
	if(typeof(Storage) !== "undefined") {
    	localStorage._selectedPEDslugs = JSON.stringify(pedStore.getSelectedPEDslugs())
    	if(_otherPED || _otherPED == ''){
    		localStorage._otherPED = JSON.stringify(pedStore.getOtherPED())
    	}
	}
}


// populate from local storage

function populateFromLocalStorage(){
	if(typeof(Storage) !== "undefined") {
		if(localStorage._selectedPEDslugs){

			var sp = JSON.parse(localStorage._selectedPEDslugs)

			for (var j = _allPEDs.length - 1; j >= 0; j--) {
				for (var i = 0; i < sp.length; i++) {
					if(_allPEDs[j].slug == sp[i]){
						_allPEDs[j].is_selected = true
					}
				}
			}
		}
		if(localStorage._otherPED){
    		_otherPED = JSON.parse(localStorage._otherPED)
    	}
	}
}

function deselectAllPEDs(){
	var sp = pedStore.getSelectedPEDs()
	for (var i = 0; i < sp.length; i++) {
		deselectPED(sp[i].index)
	};
	_otherPED = ''
}

populateFromLocalStorage()


var pedStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	getAllPEDs: function(){
 		return _allPEDs
 	},

 	getSelectedPEDs: function(){
 		return _allPEDs.filter(function(item, i){
 			return item.is_selected === true
 		});
 	},

 	getOtherPED: function() {
 		return _otherPED	
 	},



 	getSelectedPEDindexes: function(){
 		return _allPEDs.filter(function(item, i){
 			return item.is_selected === true
 		});
 	},

 	getSelectedPEDslugs: function(){
 		var sp = pedStore.getSelectedPEDs()
 		return sp.map(function(item, i){
 			return item.slug
 		});
 	},

 	getInsurersFromSelectedPEDs: function(){
 		var allInsurers = []
 		var s = pedStore.getSelectedPEDs()
 		
 		for (var i = 0; i < s.length; i++){
 			var PEDInsurers = s[i].insurers
 			for (var j = 0; j < PEDInsurers.length; j++){
 				if(allInsurers.indexOf(PEDInsurers[j]) == -1){
 					allInsurers.push(PEDInsurers[j])
 				}
 			};
 		};
 		return allInsurers
 	},

 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.UPDATE_OTHER_PED:
 		 		_otherPED = action.value
 		 		saveToLocalStorage()
 		 		pedStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_PED:
 		 		selectPED(action.index)
 		 		saveToLocalStorage()
 		 		pedStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_PED:
 		 		deselectPED(action.index)
 		 		saveToLocalStorage()
 		 		pedStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_ALL_PED:
 		 		deselectAllPEDs()
 		 		saveToLocalStorage()
 		 		pedStore.emitChange()
 		 		break
 		 }
 	})
})


module.exports = pedStore