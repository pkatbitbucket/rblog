var AppConstants = require('../constants/app-constants.jsx')
var AppSettings = require('../constants/app-settings.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')
var webAPIutils = require('../utils/webAPIutils')
var AppActions = require('../actions/app-actions')

var AppStore = require('../stores/app-store')
var PEDStore = require('../stores/ped-store')

var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var moment = require('moment')


var _isLoading
var _fetchedPlans = []
var _redirectUrl

var insurersWithNoPEDs = AppSettings.INSURERS_WITH_NO_PEDS




var formatFetchedPlans = function(){

	if(_fetchedPlans.length){

	var temp = _fetchedPlans.map(function(item, index){
		item.insurer = item.plan.insurer.name
		item.insurerSlug = item.plan.insurer.slug
		item.name = item.plan.name
		item.can_buy = true
		item.index = index

		var medical_cover = item.features.filter(function(i){
			return i.name == 'Medical Cover'
		})[0];

		var baggage_loss = item.features.filter(function(i){
			return i.name == "Baggage Loss Cover"
		})[0];

		var ped = item.features.filter(function(i){
			return i.name == "Existing Diseases Cover"
		})[0];

		var trip_delay = item.features.filter(function(i){
			return i.name == "Trip Delay"
		})[0];

		if(medical_cover){
			item.medical_cover = medical_cover.cover
			item.medical_cover_deductible = medical_cover.deductible
		}

		if(baggage_loss){
			item.baggage_loss = baggage_loss.cover
		}

		if(trip_delay){
			item.trip_delay = trip_delay.cover
		}

		if(ped){
			item.ped = ped.cover
		}


		return item
	})

	_fetchedPlans = temp

	}



}

var makeAllPlansBuyable = function(){
	for (var i = _fetchedPlans.length - 1; i >= 0; i--) {
		_fetchedPlans[i].can_buy = true
	};
}

var makeAllPlansUnBuyable = function(){
	for (var i = _fetchedPlans.length - 1; i >= 0; i--) {
		_fetchedPlans[i].can_buy = false
	};
}


var setPlansAvailability = function(){
	

	var insurers = PEDStore.getInsurersFromSelectedPEDs()
	var otherPED = PEDStore.getOtherPED()

	var hasPED = false

	if(otherPED && otherPED != ''){
		hasPED = true
	}

	if(insurers.length>0){
		hasPED = true	
	}

	makeAllPlansBuyable()
	for (var i = insurers.length - 1; i >= 0; i--) {
		var plans = resultsStore.getPlansByInsurerSlug(insurers[i])

		for (var j = plans.length - 1; j >= 0; j--) {
			plans[j].can_buy = false
		};
	};

	if(hasPED){

		for (var i = insurersWithNoPEDs.length - 1; i >= 0; i--) {
			var plans = resultsStore.getPlansByInsurerSlug(insurersWithNoPEDs[i])

			for (var j = plans.length - 1; j >= 0; j--) {
				plans[j].can_buy = false
			};

		};

	}

	

	var startDate = AppStore.getStartDate()
	startDate = startDate.startOf('day')
	var today = moment().startOf('day')
	var diff = startDate.diff(today, 'days')

	// if trip start date is within 2 days then all plans are unbuyable
	// except if the user has not selected a reliance PED

	if((diff < 2 || diff == 2) && hasPED){
		makeAllPlansUnBuyable()
		if(insurers.indexOf('reliance') == -1){
			var plans = resultsStore.getPlansByInsurerSlug('reliance')
			for (var j = plans.length - 1; j >= 0; j--) {
				plans[j].can_buy = true
			};
		}
	}


}



var resultsStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	getFetchedPlans: function(){
 		return _fetchedPlans
 	},

 	getRedirectUrl: function(){
 		return _redirectUrl
 	},

 	getBuyablePlans: function(){
 		return _fetchedPlans.filter(function(item, i){
 			return item.can_buy === true
 		})
 	},

 	getUnbuyablePlans: function(){
 		return _fetchedPlans.filter(function(item, i){
 			return item.can_buy === false
 		})
 	},

 	getLoadingStatus: function(){
 		return _isLoading
 	},

 	getPlansByInsurerSlug: function(insurerSlug){
 		return _fetchedPlans.filter(function(item){
 			return item.insurerSlug == insurerSlug
 		})
 	},

 	getSelectedPlanForBuy: function(){
 		var selectedPlanForBuy

 		if(_fetchedPlans){
	 		return _fetchedPlans.filter(function(item, i){
	 			return item.is_selected_for_buy
	 		})[0]
 		}

 		return selectedPlanForBuy
 	},

 	getSelectedPlanForDetail: function(){
 		var selectedPlanForDetail

 		if(_fetchedPlans){
	 		return _fetchedPlans.filter(function(item, i){
	 			return item.is_selected_for_detail
	 		})[0]
 		}

 		return selectedPlanForDetail
 	},

 	getSelectedPlans: function(){
 		if(_fetchedPlans){
	 		return _fetchedPlans.filter(function(item, i){
	 			item.index = i
	 			return item.is_selected
	 		})
 		}
 		else{
 			return []
 		}	
 	},

 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.RECIEVE_REDIRECT_URL:
 		 		_redirectUrl = action.redirect_url
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.RECIEVE_QUOTES:
 		 	
 		 		_isLoading = false
 		 		_fetchedPlans = action.quotes
 		 		_redirectUrl = action.redirect_url
 		 		formatFetchedPlans()
 		 		setPlansAvailability()
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.FETCH_QUOTES:
 		 	
 		 		_isLoading = true
 		 		_fetchedPlans = []
 		 		webAPIutils.getQuotes(AppStore.getQuoteRequest(), action.callback)
 		 		resultsStore.emitChange()
 		 		break
 		 		
 		 	case AppConstants.SELECT_PLAN:
 		 		_fetchedPlans[action.index].is_selected = true
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_PLAN:
 		 		_fetchedPlans[action.index].is_selected = false
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_PLAN_FOR_BUY:
 		 		if(resultsStore.getSelectedPlanForBuy()){
 		 			resultsStore.getSelectedPlanForBuy().is_selected_for_buy = false	
 		 		}
 		 		_fetchedPlans[action.index].is_selected_for_buy = true
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_PLAN_FOR_BUY:
 		 		if(resultsStore.getSelectedPlanForBuy()){
 		 			resultsStore.getSelectedPlanForBuy().is_selected_for_buy = false	
 		 		}
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_PLAN_FOR_DETAIL:
 		 		if(resultsStore.getSelectedPlanForDetail()){
 		 			resultsStore.getSelectedPlanForDetail().is_selected_for_detail = false	
 		 		}
 		 		_fetchedPlans[action.index].is_selected_for_detail = true
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_PLAN_FOR_DETAIL:
 		 		if(resultsStore.getSelectedPlanForDetail()){
 		 			resultsStore.getSelectedPlanForDetail().is_selected_for_detail = false	
 		 		}
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.SET_UNAVAILABLE_FOR_BUYING:
 		 		_fetchedPlans[action.index].can_buy = false
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.UPDATE_OTHER_PED:
 		 		AppDispatcher.waitFor([PEDStore.dispatcherIndex]);
 		 		setPlansAvailability()
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.SELECT_PED:
 		 		AppDispatcher.waitFor([PEDStore.dispatcherIndex]);
 		 		setPlansAvailability()
 		 		resultsStore.emitChange()
 		 		break

 		 	case AppConstants.DESELECT_PED:
 		 		AppDispatcher.waitFor([PEDStore.dispatcherIndex]);
 		 		setPlansAvailability()
 		 		resultsStore.emitChange()
 		 		break
 		 }
 	})
})


module.exports = resultsStore