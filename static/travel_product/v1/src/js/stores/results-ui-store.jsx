var AppConstants = require('../constants/app-constants.jsx')
var AppDispatcher = require('../dispatchers/app-dispatcher.jsx')


var EventEmitter = require('events').EventEmitter
var assign = require('object-assign')

var CHANGE_EVENT = "change"

var AppStore = require('../stores/app-store')
var ResultsStore = require('../stores/results-store')

var moment = require('moment')

var activeModal = null
var activeMode = null

var _showPEDModal = true




// save to local storage

function saveToLocalStorage(){
	if(typeof(Storage) !== "undefined") {
    	localStorage._showPEDModal = JSON.stringify(_showPEDModal? 1:0)
	}
}


// populate from local storage

function populateFromLocalStorage(){
	if(typeof(Storage) !== "undefined") {
		if(localStorage._showPEDModal){
			_showPEDModal = (JSON.parse(localStorage._showPEDModal) == 1)? true:false
		}
	}
}


populateFromLocalStorage()


var uiStore = assign({}, EventEmitter.prototype, {
	emitChange: function(){
		this.emit(CHANGE_EVENT)
	},

	addChangeListener: function(callback){
		this.on(CHANGE_EVENT, callback)
	},

	removeChangeListener: function(callback) {
    	this.removeListener(CHANGE_EVENT, callback)
 	},

 	getActiveModal: function(){
 		return activeModal
 	},

 	getActiveMode: function(){
 		return activeMode
 	},

 	resetUI: function() {
 		activeModal = null
 		activeMode = null
 	},
 	
 	dispatcherIndex: AppDispatcher.register(function(action) {
 		 switch(action.actionType) {
 		 	case AppConstants.OPEN_MODAL:
 		 		activeModal = action.modal
 		 		uiStore.emitChange()
 		 		break

 		 	case AppConstants.CLOSE_MODAL:
 		 		activeModal = null
 		 		uiStore.emitChange()
 		 		break

 		 	case AppConstants.SET_MODE:
 		 		activeMode = action.mode
 		 		uiStore.emitChange()
 		 		break

 		 	case AppConstants.RESET_MODE:
 		 		activeMode = null
 		 		uiStore.emitChange()
 		 		break

 		 }
 	})
})


module.exports = uiStore