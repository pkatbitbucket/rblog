var gtm_push = function(gtm_event, gtm_category, gtm_action, gtm_label, gtm_extra_params){
        var data = {
            "event":gtm_event,
            "category":gtm_category,
            "action":gtm_action,
            "label":gtm_label,
        }
        for(param in gtm_extra_params){
            data[param] = gtm_extra_params[param]
        }

        dataLayer.push(data)
    }


module.exports = {
    gtm_push: gtm_push
}