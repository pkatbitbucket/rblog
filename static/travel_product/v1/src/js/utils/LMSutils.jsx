var reqwest = require('reqwest')
var Cookie = require('react-cookie')

var lmsUtils = {
        sendToLMS: function(data, label, extras, callback){
             var jsonData = {};
                jsonData['campaign'] = 'TRAVEL';
                jsonData['label'] = label;
                jsonData['device'] = 'Desktop';
                jsonData['triggered_page'] = window.location.href;
                jsonData['data'] = data;

                for (var key in extras){
                    jsonData[key] = extras[key];
                }

                if(jsonData['mobile'] && jsonData['mobile'] != ''){
                    Cookie.save('mobileNo', jsonData['mobile'], {path: '/'})
                }
                if(jsonData['email'] && jsonData['email'] != ''){
                    Cookie.save('email', jsonData['email'], {path: '/', encode: function(item){return item} })
                }

                reqwest({
                    url: '/leads/save-call-time/',
                    method: 'post',
                    data: {
                        'data': JSON.stringify(jsonData),
                        'csrfmiddlewaretoken': CSRF_TOKEN
                    },
                    success: function(response){
                        callback();
                    }
                })
        }


    };


module.exports = lmsUtils