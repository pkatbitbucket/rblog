var assign = require('object-assign');

var TravelStorageUtils = {
	setValuesInLocalStorage: function(form, obj){
		if(!!Storage){
			var data = localStorage.getItem(form)
			data = !data ? {} : JSON.parse(data)
	        assign(data, obj)
		    localStorage.setItem(form, JSON.stringify(data))
		}
	}
}

module.exports = TravelStorageUtils;