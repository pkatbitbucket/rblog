var fire_cust_ev = function(ev, data) {
		console.log(ev);
        var evt = new CustomEvent(ev, {
            detail: data
        });
        window.dispatchEvent(evt);
    };

var add_listener = function(evname, func) {
    window.addEventListener(evname, function(e) {
        console.log(e.type, e.detail);
        if (func) {
            func(e);
        }
    });
};

module.exports = {
	fire_cust_ev: fire_cust_ev,
	add_listener:add_listener
}

