

module.exports = {

"Medical Benefits": ["Medical Cover", "Out-Patient Care", "Existing Diseases Cover", "Personal Accident", "Accidental Death & Disability (Public Transport)", "Medical Evacuation", "Medical Repatriation", "Dental Cover"],
"Travel benefits": ["Baggage Loss Cover","Delay Of Baggage", "Trip Delay", "Trip Cancellation", "Missed Connection", ],
"Additional Benefits": ["Personal Liability", "Emergency Cash Advance", "Loss Of Passport"]

}
