module.exports = {
	"Medical Cover":"Expenses for hospitalisation of the insured for emergency medical treatment due to illness or accident",
	"Medical Evacuation":"Expenses for transportation of the insured from foreign country to India/transportation to the nearest hospital (as per chosen plan)",
	"Medical Repatriation":"Expenses for transportation of insured person’s body to India in the event of death",
	"Dental Cover":"Expenses for emergency dental care or dental treatment arsing due to an injury (as per chosen plan)",
	"Out-Patient Care":"Expenses for medically necessary out-patient treatment due to illness/injury",
	"Personal Accident":"Lump sum amount in the event of Death/Permanent Total or Partial disability (as per chosen plan & depending on the severity of partial disability)",
	"Accidental Death & Disability (Public Transport)":"Lump sum amount in the event of Death/Permanent Disability (as per chosen plan) while travelling in a public transport, subject to conditions",
	"Existing Diseases Cover":"Expenses for existing diseases in life threatening scenarios",
	"Baggage Loss Cover":"Expenses for complete & permanent loss of insured traveller’s checked baggage.",
	"Delay Of Baggage":"Expenses for emergency purchase of toiletries, medication & clothing due to delay in the arrival of checked-in baggage",
	"Trip Delay":"Expenses due to trip delay beyond specified number of hours",
	"Trip Cancellation":"Expenses against travel charges/personal accommodation paid, in the event of trip cancellation",
	"Personal Liability":"Expenses against legal liability for bodily injury or property damage to third party arising out of an accident",
	"Loss Of Passport":"Expenses for obtaining a new/duplicate passport if the insured loses his/her original passport",
	"Missed Connection": "Expenses incurred on missing the connecting flight due to late arrival of the incoming flight.",
	"Emergency Cash Advance": "Financial assistance service providing emergency cash following incidents like theft/burglary of luggage or money."
}