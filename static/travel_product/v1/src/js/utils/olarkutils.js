var EventUtils = require('./eventutils')
var AppStore = require('../stores/app-store')
var PedStore = require('../stores/ped-store')
var ResultStore = require('../stores/results-store')

var hide_chat_box = function() {
    var otcounter = 0;
    var ot = setInterval(function() {
        console.log('TRYING CHAT BOX');
        if (window.olark) {
            console.log("CHAT BOX HIDE");
            window.olark('api.box.hide');
            clearInterval(ot);
        }
        otcounter++;
        if (otcounter > 50) {
            clearInterval(ot);
        }
    }, 3000);
};

var show_chat_box = function() {
    var otcounter = 0;
    var ot = setInterval(function() {
        console.log('TRYING CHAT BOX');
        if (window.olark) {
            console.log("CHAT BOX READY");
            window.olark('api.box.show');
            clearInterval(ot);
        }
        otcounter++;
        if (otcounter > 50) {
            clearInterval(ot);
        }
    }, 3000);
};

var notify = function(message_list) {
    console.log("NOTIFY >>", message_list);
    if (window.olark) {
        if (!(message_list instanceof Array)) {
            message_list = [message_list];
        }
        window.olark('api.chat.sendNotificationToOperator', {
            body: message_list.join('\n')
        });
    }
};

EventUtils.add_listener('quote_changed', function(e){
	var latest_quote = AppStore.getQuoteRequest();
    var selected_peds = PedStore.getSelectedPEDs();
    var result_url = ResultStore.getRedirectUrl()
    var plan_detail = ResultStore.getSelectedPlanForDetail()
	var message_list = ["Quote Changed"];
	var yes_no = {1: 'Yes', 0: 'No'};
    var resident_for = {0: 'Less than 6 months', 0.5:'More than 6 months'}
    for (x in latest_quote){
		if (x == 'adults')
		{
			msg = 'Traveler Ages: ' + latest_quote[x];
			no_of_traveller = latest_quote[x].split(',').length;
			message_list.push("No Of Traveler: " + no_of_traveller);
		}
        else if (x == 'type')
        {
            if (latest_quote[x] == 'multi')
                delete latest_quote['to_date']
            msg = 'Trip Type: ' + latest_quote[x];
        }
        else if (x == 'isResident')
            msg = 'Are you a resident of India?: ' + yes_no[latest_quote[x]];
        else if (x == 'resident_for')
            msg = "How long have you been in India?: " + resident_for[latest_quote[x]]
		else
			msg = x + ": " + latest_quote[x];
		message_list.push(msg);
	}
    if (selected_peds.length > 0)
    {
        disease_list = []
        for (disease in selected_peds)
        {
            disease_list.push(selected_peds[disease].verbose)
        }
        message_list.push("Pre-Existing Diseases: " + disease_list.join())
    }
    if (result_url)
    {
        message_list.push("Result Page URL: " + SITE_URL + result_url)
    }
    if (plan_detail)
    {
        msg = "Plan Details: Insurer: " + plan_detail.insurer + ", " + "Premium: " + plan_detail.premium
        message_list.push(msg)
    }
    /*if (e.detail.buy_url)
        message_list.push("Buy Page URL: " + SITE_URL + e.detail.buy_url)*/
	notify(message_list);
})

