

var reqwest = require('reqwest')
var AppActions = require('../actions/app-actions')
var places = require('./individual_travel_places')
var EventUtils = require('../utils/eventutils')

var rawCountries, rawQuotes, rawSumAssured, redirectUrl


function getCountries(callback){
	rawCountries = places.map(function(item){return {label: item[1], value: item[0]} })
	if(callback){
		callback();
	} 
}


function getQuotes(quoteRequest, callback){
	quoteRequest.csrfmiddlewaretoken = CSRF_TOKEN
	
	reqwest({
		url: '',
		method: 'post',
		data: reqwest.toQueryString(quoteRequest, true),
		success: function (response){
			rawQuotes = response.data.slab_list_view.data.slab_list
			rawSumAssured = response.data.slab_list_view.data.form.initial.sum_assured
			redirectUrl = response.redirect_url
			if(callback){
				setTimeout(function(){
					callback();
					EventUtils.fire_cust_ev('quote_changed', {})
				}, 600);
			}
		}
	})
}



function goToTransaction(pk, callback){

	reqwest({
		url: '/travel-insurance/buy/add/',
		method: 'post',
		data: {slab: pk, csrfmiddlewaretoken: CSRF_TOKEN},
		success: function (response){
			//EventUtils.fire_cust_ev('quote_changed', {'buy_url':response.redirect_url})
			window.location.href = response.redirect_url
			if(callback){
				callback();
			}
		}
	})

}

module.exports = {
	getInitialData: function(){
		getCountries(function(){
			AppActions.recieveCountries(rawCountries)
		})
	},
	
	getQuotes: function(quoteRequest, callback){
		getQuotes(quoteRequest, function(){
			AppActions.recieveQuotes(rawQuotes, rawSumAssured, redirectUrl)

			if(callback){
				callback()
			}
		})
	},

	getQuotesBlank: function(quoteRequest){
		getQuotes(quoteRequest, function(){
			AppActions.recieveRedirectUrl(redirectUrl)

		})
	},

	goToTransaction: function(pk){
		goToTransaction(pk, function(){})
	}

}