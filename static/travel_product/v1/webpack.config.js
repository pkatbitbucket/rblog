var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpack = require('webpack')
var path = require('path');

module.exports = {
    entry: {
        buy: "./buy/app/entry.js",
        steps: "./src/js/index.jsx",
    },
    output: {
        path: './build',
        filename: '[name].[hash].bundle.js',
    },
    module: {

        noParse: [
            '/knockout-latest.js/',
        ],
        loaders: [{
            test: /\.less$/,
            loader: ExtractTextPlugin.extract("css-loader!autoprefixer-loader!less-loader")
        }, {
            test: /\.jsx$/,
            loader: 'jsx-loader?insertPragma=React.DOM&harmony'
        },
        { test: /\.png$/, loader: "file-loader" },
        { test: /\.jpg$/, loader: "file-loader" },
        { test: /knockout-latest.js/, loader: "imports?require=>__webpack_require__" },
        { test: /react-onclickoutside\/index.js/, loader: "imports?React=react" },

        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx'],
        modulesDirectories: [
            'node_modules',
        ],
        alias: {
            'form-models': __dirname + '/buy/app/lib/form-models',
            'utils': __dirname + '/buy/app/lib/utils',
            'buycookie': __dirname + '/buy/app/lib/cookies',
            'pikaday': __dirname + '/buy/app/lib/pikaday',
        }
    },
    plugins: [
        new ExtractTextPlugin("style.css"),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        function() {
            this.plugin("done", function(stats) {
                require("fs").writeFileSync(
                    path.join(__dirname, "./build", "stats.json"),
                    JSON.stringify(stats.toJson())
                );
                require("fs").utimesSync(
                    path.join(__dirname, "../../../base/settings.py"),
                    new Date(),
                    new Date()
                );

            });
        },
    ]
};
