Practices
=========
* Follow test driven development.
* writing tests for existing code or when you find a bug
    * first, write a test for the correct behaviour.
    * change your code until it passes.
    * repeat.
* writing tests for new code
    * write a test for the correct behaviour.
    * change your code until it passes.
    * repeat.

What tools we will use?
-----------------------
* pytest (basic for writing all kinds of tests)
* pytest-django (used for writing both integration and unit tests)
* pytest-selenium (used for writing for functional tests)

why we are using py.test?
-------------------------
* No boilerplate, no required api
    * There is no need to import unittest.
    * There is no need to derive from TestCase.
    * There is no need to for special self.assertEqual(), since we can use Python's built in assert statement.
* Powerful pytest fixtures
* The resource is set up even when we don't need it.
* Works with python’s unittest library as well


What to test?
-------------
* Wherever you implement the business logic.
    * If your model has custom methods, you should test that with unit tests.
    * Same goes for custom views, forms, template tags, context processors, middleware, management commands, etc.

What not to test?
-----------------
* Built-in Python function/library
* Built into Django
    * examples like the fields on a Model or
    * testing how the built-in template.Node renders included tags.

What Reviewers and QA team will look in Pull Requests?
------------------------------------------------------
* Will check for tests for all the use cases if it is a new feature given by The Manager.
* Will check for tests for every bug raised in the existing system
* Will approve only if the code have the required test cases for various use cases.

Where to write tests?
---------------------

Test Files Structure

```shell
rblog
|-- base
    |-- tests
        |-- __init__.py
        |-- <app-name-1>
            |-- __init__.py
            |-- <category-name-1>
                |-- __init__.py
                |-- test_<file-name-1>.py
                |-- test_<file-name-2>.py
            |-- <category-name-2>
                |-- __init__.py
                |-- test_<file-name-1>.py
            ....
        |-- <app-name-2>
            |-- __init__.py
            |-- <category-name-1>
                |-- __init__.py
                |-- test_<file-name-1>.py
                |-- test_<file-name-1>.py
            ....

```

Example

```shell
|--base
    |-- tests    .
        |-- __init__.py
        |-- leaflet_campaign
        |   |-- conftest.py
        |   |-- __init__.py
        |   |-- test_models.py
        |   |-- test_urls.py
        |   `-- test_views.py
        |-- motor
        |   |-- __init__.py
        |   |-- insurers
        |   |   |-- __init__.py
        |   |   `-- proposal_forms
        |   |       `-- twowheeler
        |   |           `-- test_landt.py
        |   |-- models
        |   |   |-- __init__.py
        |   |   |-- test_quote.py
        |   |   |-- test_rtos.py
        |   |   `-- test_transaction.py
        |   |-- utils
        |   |   |-- __init__.py
        |   |   |-- insurers
        |   |   |   |-- __init__.py
        |   |   |   `-- test_landt.py
        |   |   |-- proposal
        |   |   |   |-- __init__.py
        |   |   |   |-- test_buy_form.py
        |   |   |   `-- test_generate_form.py
        |   |   |-- test_cache.py
        |   |   |-- test_common.py
        |   |   |-- test_location.py
        |   |   |-- test_payment.py
        |   |   |-- test_policy.py
        |   |   |-- test_premium.py
        |   |   |-- test_quote.py
        |   |   |-- test_transaction.py
        |   |   `-- test_vehicle.py
        |   `-- views
        |       `-- __init__.py
        `-- travel
            `-- utils
```

What are the things to keep in mind while running tests?
--------------------------------------------------------
* Install the requirements if you're running tests for the first time.
* Before running tests, consider creating and reusing the clone of live db (`coverfox`) as test database (`test_coverfox`).
* use make commands to
    * drop existing test database
    * create test database using live database
    * running tests


What are the steps to run tests?
================================

* Before running tests make sure you have

    * the following 'LOGGING' value in the `base/local_settings.py`.

    ```python
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            },
            'require_production_false': {
                '()': 'utils.RequireProductionFalse'
            },
            'require_production_true': {
                '()': 'utils.RequireProductionTrue'
            },
        },
        'formatters': {
            'verbose': {
                'format': (
                    '%(levelname)s %(asctime)s %(module)s %(process)d '
                    '%(thread)d %(message)s'
                )
            },
            'syslog': {
                'format': (
                    '%(name)s [%(levelname)s]-[%(funcName)s]: %(message)s'
                )
            },
        },
        'handlers': {
            'null': {
                'level': 'DEBUG',
                'class': 'logging.NullHandler',
            },
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'verbose'
            },
        },
        'loggers': {
            'celery': {
                'handlers': ['console'],
                'level': 'ERROR',
                'propogate': False,
            },
            'django.security': {
                'handlers': ['console'],
                'level': 'WARNING',
                'propagate': False,
            },
            'django': {
                'handlers': ['console'],
                'level': 'ERROR',
                'propagate': False,
            },
            '': {
                'handlers': ['console'],
                'level': 'ERROR',
                'propagate': True,
            },
        }
    }
    ```

    * If there is a test database (`test_coverfox`) in the database, delete it using
    ```shell
    make drop-live-test-db
    ```
    * Create test database using
    ```shell
        make create-live-test-db
    ```

* Now, run your tests using the following command.

```shell
make test-<app-name>
```

* If a test command is using django's test management command and not using `py.test` to run tests, feel free to change it and raise a PR.

Example:
```shell
test-<app-name>:
    @cd base && py.test tests/<app-name>
```
