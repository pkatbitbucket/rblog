var path = require('path');
var webpack = require('webpack');
//var ContextReplacementPlugin = require("webpack/lib/ContextReplacementPlugin");
//var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");

module.exports = {
    entry: {
        motordesktop: "./static/motor_product/entry/desktop.js",
        motormobile: "./static/js/motor/mobile.js",
        // This will be removed in the new bike build
        motormobileold: "./static/motor_product/entry/mobile.js",
        motorbuy: "./static/motor_product/entry/buy.js",
        travelbuy: "./static/travel_product/v1/buy/app/entry.js",
        traveldesktop: "./static/travel_product/v1/src/js/index.jsx",
    },
    resolve: {
        extensions: ['', '.js', '.jsx'],
        modulesDirectories: [
            'node_modules',
        ],
        alias: {
                   'async': __dirname +'/static/motor_product/global/lib/requirejs-plugins/async',
                   'text': __dirname +'/static/motor_product/global/lib/require/text',
                   'requirejs$': __dirname +'/static/motor_product/global/lib/require/require',
                   'knockout': __dirname +'/static/motor_product/global/lib/knockout/knockout-3.2.0',
                   'knockout-amd-desktop': __dirname +'/static/motor_product/amd-js/knockout-amd-desktop',
                   'knockout-amd-buy': __dirname +'/static/motor_product/amd-js/knockout-amd-buy',
                   'knockout-amd-mobile': __dirname +'/static/motor_product/amd-js/knockout-amd-mobile',
                   'knockout-history': __dirname +'/static/motor_product/global/lib/knockout/knockout-history',
                   'knockout-router': __dirname +'/static/motor_product/global/lib/knockout/knockout-router',
                   'models': __dirname +'/static/motor_product/global/models',
                   'viewmodels': __dirname +'/static/motor_product/global/viewmodels',
                   'configuration': __dirname +'/static/motor_product/global/configuration',
                   'utils': __dirname +'/static/motor_product/global/utils',
                   'motor-form-models': __dirname +'/static/motor_product/global/form-models',
                   'kocookie' : __dirname +'/static/motor_product/global/lib/cookies/cookies',
                   'bstore' : __dirname +'/static/motor_product/global/bstore',
                   'main-desktop' : __dirname +'/static/motor_product/desktopMVP/app/assets/js/main',
                   'main-buy' : __dirname +'/static/motor_product/desktopMVPBuy/app/assets/js/main',
                   'main-mobile' : __dirname +'/static/motor_product/mobile/assets/js/main',
                   'components' : __dirname +'/static/motor_product/global/components.js',
                   'motorevents' : __dirname +'/static/motor_product/global/events.js',
                   'constants' : __dirname +'/static/motor_product/global/app-constants.js',
                   'actions' : __dirname +'/static/motor_product/global/app-actions.js',
                   'crypt': __dirname +'/static/motor_product/global/lib/encryption/response.js',
                   'rtoStateMapping': __dirname +'/static/motor_product/global/rto-state-mapping.js',
                   'fastlane': __dirname +'/static/motor_product/global/fastlane.js',
                   'liveOffline': __dirname + '/global/liveOffline.js',
                   // travel
                   'form-models': __dirname + '/static/travel_product/v1/buy/app/lib/form-models',
                   'travelutils': __dirname + '/static/travel_product/v1/buy/app/lib/utils',
                   'buycookie': __dirname + '/static/travel_product/v1/buy/app/lib/cookies',
                   'pikaday': __dirname + '/static/travel_product/v1/buy/app/lib/pikaday',
                   //'login': __dirname + '/global/login.js',

        }
    },
    resolveLoader: { root: __dirname + "node_modules" },

    module: {
        noParse: [
            '/knockout-3\.2\.0\.js/',
            '/knockout-latest.js/',
        ],
        loaders: [
            //{ test: /js\/motor\/*\.js$/, loader: 'babel-loader' },
            {
              include: [
                path.resolve(__dirname, "static/js/motor"),
              ],
              loader: 'babel-loader'
            },
            { test: /knockout-3\.2\.0\.js/, loader: "imports?require=>__webpack_require__" },
            { test: /pikaday.js/, loader: "imports?require=>false"},
            { test: /\.jsx$/, loader: 'jsx-loader?insertPragma=React.DOM&harmony'},
            { test: /\.png$/, loader: "file-loader" },
            { test: /\.jpg$/, loader: "file-loader" },
            { test: /knockout-latest.js/, loader: "imports?require=>__webpack_require__" },
            { test: /react-onclickoutside\/index.js/, loader: "imports?React=react" },
        ]
    },

    plugins: [
        //new CommonsChunkPlugin("common", "common.[hash].js"), //["desktop", "buy"], 2),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        function() {
            this.plugin("done", function(stats) {
                require("fs").writeFileSync(
                    path.join(__dirname, "./static/build", "stats.json"),
                    JSON.stringify(stats.toJson())
                );
                require("fs").utimesSync(
                    path.join(__dirname, "./base/settings.py"),
                    new Date(),
                    new Date()
                );

            });
        },
    ],

    output: {
        path: __dirname + "/static/build/",
        publicPath : '/static/build/',
        pathInfo : true,
        filename: "[name].[hash].bundle.js",
    },
};
